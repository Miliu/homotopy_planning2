// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CLowerBound.cpp
 *
 *  Created on: 19/01/2011
 *      Author: emili
 */

#include "CLowerBound.h"

using namespace std;

CLowerBound::CLowerBound(	const vector<CTPath>& hc,
							const TSegmentMap& rf,
							const vector<CPoint2D<float> > centroids,
							const CPoint2Di& start,
							const CPoint2Di& goal,
							const CPoint2Di& c,
							cv::Mat rfPlot,
							const bool& plot)
:
							hc_(hc),
							rf_(rf),
							centroids_(centroids),
							start_(start),
							goal_(goal),
							c_(c.x(), c.y()),
							rfPlot_(rfPlot),
							plot_(plot)
{
	if(plot_)
	{
		Point2i size(rfPlot.size());
		cv::namedWindow("lowerBound", CV_WINDOW_AUTOSIZE);
		cvMoveWindow("lowerBound", 3 * size.x + 100, 1 * size.y + 100);
		imshow("lowerBound", rfPlot_);
	}
}

void
CLowerBound::compute()
{
	computeLB();

#ifdef _NDEBUG
	cout << "Post-processed LB paths -------------" << endl;
	vector<CRFPoint> tmp;
	for(unsigned int i = 0; i < path_.size(); ++i)
	{
		tmp = addParallelEdges(channel_.at(i), path_.at(i));
		if(tmp.size() != path_.at(i).size())
		{
			cout << "path " << i << "- (Original -> New)" << endl;
			debug::printVector("", "", path_.at(i), false);
			cout << " -> ";
			debug::printVector("", "", tmp, false);
			cout << endl;
		}

		path_.at(i) = tmp;
	}
#else
	for(unsigned int i = 0; i < path_.size(); ++i)
		path_.at(i) = addParallelEdges(channel_.at(i), path_.at(i));
#endif
}

void
CLowerBound::computeLB()
{
	float dist;
	vector<CRFPoint> path;

#ifdef _LBDEBUG
	if(plot_)
	{
		rfPlot_.copyTo(lbPlot_);
	}
#endif

	for(vector<CTPath>::const_iterator i = hc_.begin(); i < hc_.end(); ++i)
		channel_.push_back(channel(*i));

	for(vector<vector<CRFSegment> >::const_iterator i = channel_.begin(); i < channel_.end(); ++i)
	{
		funnel(*i, path, dist);
		path_.push_back(path);
		index_.push_back((CIdxValue<unsigned int, float>(i - channel_.begin(), dist)));

#ifdef _NDEBUG
		cout << i - channel_.begin() << "- " << hc_.at(i - channel_.begin()) << "-----------------" << endl;
		debug::printVector("chan: ", "", *i, false); cout << endl;
		debug::printVector("path: ", "", path, false); cout << endl;
		cout << "dist: " << dist << endl;

		if(plot_)
		{
			rfPlot_.copyTo(lbPlot_);
			for(vector<CRFSegment>::const_iterator j = i->begin(); j < i->end() - 1; ++j)
				cv::line(lbPlot_, Point2i(j->getP1().x(), j->getP1().y()), Point2i((j+1)->getP1().x(), (j+1)->getP1().y()), cv::Scalar(211, 0, 255), 1);
			for(vector<CRFSegment>::const_iterator j = i->begin(); j < i->end() - 1; ++j)
				cv::line(lbPlot_, Point2i(j->getP2().x(), j->getP2().y()), Point2i((j+1)->getP2().x(), (j+1)->getP2().y()), cv::Scalar(20, 105, 134), 1);
			for(vector<CRFPoint>::iterator j = path.begin(); j < path.end() - 1; ++j)
				cv::line(lbPlot_, Point2i(j->x(), j->y()), Point2i((j+1)->x(), (j+1)->y()), cv::Scalar(0, 127, 255), 1); //taronja
			cv::imshow("lowerBound", lbPlot_);
			//cv::waitKey();
		}
#endif
	}
//#ifdef _NDEBUG
	debug::printVector("Index -----------", "", index_);
//#endif
//	sort(index_.begin(), index_.end());
}

//// segment: p1 -> Left; p2 -> Right
//vector<CRFSegment>
//CLowerBound::channel(const CTPath& hc)
//{
////	cout << "CHANNEL ----------------------" << endl;
//	vector<CRFSegment> ch;
//	CPoint2Di p(start_);
//	CSegment2D<int> s;
//
//	vector<int> vertex = hc.getVertexSequence();
//	CTString edge = hc.getEdgeSequence();
//
//	ch.push_back(CRFSegment(start_, start_, edge::e));
//	for(unsigned int i = 0; i < edge.size(); ++i)
//	{
//		s = rf_[edge.at(i)];
////		cout << "EDGE: " << edge.at(i) << " " << s << ":";
//
//		CPoint2D<float> c(centroids_.at(vertex.at(i)));
//		CPoint2D<float> v1f(s.getP1().x(), s.getP1().y());
//		CPoint2D<float> v2f(s.getP2().x(), s.getP2().y());
//
////#ifdef _LBDEBUG
////		CPoint2D<int> v1(s.getP1());
////		CPoint2D<int> v2(s.getP2());
////
////		// start
////		cv::line(lbPlot_,Point2i(v1.x()-5, v1.y()),Point2i(v1.x()+5, v1.y()),cv::Scalar(255,0,0));
////		cv::line(lbPlot_,Point2i(v1.x(), v1.y()-5),Point2i(v1.x(), v1.y()+5),cv::Scalar(255,0,0));
////		cv::line(lbPlot_,Point2i(v2.x()-5, v2.y()),Point2i(v2.x()+5, v2.y()),cv::Scalar(255,0,0));
////		cv::line(lbPlot_,Point2i(v2.x(), v2.y()-5),Point2i(v2.x(), v2.y()+5),cv::Scalar(255,0,0));
////
////		cv::line(lbPlot_,Point2i(c.x()-5, c.y()),Point2i(c.x()+5, c.y()),cv::Scalar(0,0,255));
////		cv::line(lbPlot_,Point2i(c.x(), c.y()-5),Point2i(c.x(), c.y()+5),cv::Scalar(0,0,255));
////		cv::imshow("lowerBound", lbPlot_);
////		cv::waitKey();
////#endif
//
////		cout << "V: " << v1f << ", " << v2f << endl;
//		v1f -= c;
//		v2f -= c;
////		cout << "V2: " << v1f << ", " << v2f << endl;
//		float value = util::perpDotProduct<float>(v1f, v2f);
////		cout << "value: " << value << endl;
//		if(value > -0.00001 && value < 0.00001) // parallel
//		{
////			cout << "parallel!!!" << endl;
//			runtime_error("LowerBound::Channel::parallel");
//		}
//		else if(value < 0)
//		{
////			cout << "CCW " << endl;
//			ch.push_back(CRFSegment(s, edge.at(i))); // CCW
//		}
//		else if(value > 0)
//		{
////			cout << "CW " << endl;
//			ch.push_back(CRFSegment(s.getP2(), s.getP1(), edge.at(i))); // CW
//		}
////		cout << "ADDED: " << ch.back() << endl;
//	}
//	ch.push_back(CRFSegment(goal_, goal_, edge::e));
//
////	cout << "CHANNEL -------------------FI-" << endl;
//	return ch;
//}


// segment: p1 -> Left; p2 -> Right
vector<CRFSegment>
CLowerBound::channel(const CTPath& hc)
{
//	cout << "CHANNEL ----------------------" << endl;
	vector<CRFSegment> ch;
	CPoint2Di p(start_);
	CSegment2D<int> s;

	vector<int> vertex = hc.getVertexSequence();
	CTString edge = hc.getEdgeSequence();

	unsigned int k0_counter = 0;
	unsigned int k0_begin = 0;

	ch.push_back(CRFSegment(start_, start_, edge::e));
	for(unsigned int i = 0; i < edge.size(); ++i)
	{
		s = rf_[edge.at(i)];
//		cout << "EDGE: " << edge.at(i) << " " << s << ":";

		CPoint2D<float> c(centroids_.at(vertex.at(i)));
		CPoint2D<float> v1f(s.getP1().x(), s.getP1().y());
		CPoint2D<float> v2f(s.getP2().x(), s.getP2().y());

// 	plot centroid & segment of each edge
//#ifdef _LBDEBUG
//		CPoint2D<int> v1(s.getP1());
//		CPoint2D<int> v2(s.getP2());
//
//		// start
//		cv::line(lbPlot_,Point2i(v1.x()-5, v1.y()),Point2i(v1.x()+5, v1.y()),cv::Scalar(255,0,0));
//		cv::line(lbPlot_,Point2i(v1.x(), v1.y()-5),Point2i(v1.x(), v1.y()+5),cv::Scalar(255,0,0));
//		cv::line(lbPlot_,Point2i(v2.x()-5, v2.y()),Point2i(v2.x()+5, v2.y()),cv::Scalar(255,0,0));
//		cv::line(lbPlot_,Point2i(v2.x(), v2.y()-5),Point2i(v2.x(), v2.y()+5),cv::Scalar(255,0,0));
//
//		cv::line(lbPlot_,Point2i(c.x()-5, c.y()),Point2i(c.x()+5, c.y()),cv::Scalar(0,0,255));
//		cv::line(lbPlot_,Point2i(c.x(), c.y()-5),Point2i(c.x(), c.y()+5),cv::Scalar(0,0,255));
//		cv::imshow("lowerBound", lbPlot_);
//		cv::waitKey();
//#endif

//		cout << "V: " << v1f << ", " << v2f << endl;
		v1f -= c;
		v2f -= c;
//		cout << "V2: " << v1f << ", " << v2f << endl;
		float value = util::perpDotProduct<float>(v1f, v2f);
//		cout << "value: " << value << endl;
		if(value > -0.00001 && value < 0.00001) // parallel
		{
			runtime_error("LowerBound::Channel::parallel");
		}
		else if(value < 0)
		{
//			cout << "CCW " << endl;
			ch.push_back(CRFSegment(s, edge.at(i))); // CCW
		}
		else if(value > 0)
		{
//			cout << "CW " << endl;
			ch.push_back(CRFSegment(s.getP2(), s.getP1(), edge.at(i))); // CW
		}
//		cout << "ADDED: " << ch.back() << endl;


//		cout << "edge: " << edge.at(i) << endl;
		if(edge.at(i).getSubsegment() == 0) //start alpha_k_0 substring
		{
			if(k0_counter == 0)
			{
//				cout << "Start" << endl;
				k0_begin = i;
			}
			k0_counter++;
		}
		else if(edge.at(i).getSubsegment() != 0 && k0_counter == 1) //one alpha_k_0 (not a substring)
		{
//			cout << "Discard" << endl;
			k0_begin = -1;
			k0_counter = 0;
		}

		if((edge.at(i).getSubsegment() != 0 || i == edge.size()-1) && k0_counter > 1) //alpha_k_0 substring
		{
			// detectar sentit i girar el que toqui
			// detectar sentit -> 2 perpdot prod amb el punt c,el centroid de la pos k0_begin i 2 punts left seguits
			CPoint2D<float> v1f(c.x(), c.y());
			CPoint2D<float> v2f(ch.at(k0_begin).getP1().x(), ch.at(k0_begin).getP1().y()); // left

			v1f -= c_;
			v2f -= c_;

			int dir1 = 0;
			int dir2 = 0;
			if(util::perpDotProduct<float>(v1f, v2f) < 0)
				dir1 = -1;
			else
				dir1 = 1;

			v1f.setVector(ch.at(k0_begin+1).getP1().x(), ch.at(k0_begin+1).getP1().y());
			v1f -= c_;
			if(util::perpDotProduct<float>(v2f, v1f) < 0)
				dir2 = -1;
			else
				dir2 = 1;


			if(dir1 == dir2)
			{	// canviar els right
//				cout << "canviar LEFT" << endl;
				// indexes: the s point is already on the channel
				CPoint2Di tmp;
				unsigned int k0_end = k0_begin + k0_counter;
				for(unsigned int i = 0; i < k0_counter / 2; ++i)
				{
					tmp = ch.at(k0_begin + 1 + i).getP1();
					ch.at(k0_begin + 1 + i).setP1(ch.at(k0_end - i).getP1());
					ch.at(k0_end - i).setP1(tmp);
				}
			}
			else
			{	// girar els left
//				cout << "canviar RIGHT" << endl;
				// indexes: the s point is already on the channel
				CPoint2Di tmp;
				unsigned int k0_end = k0_begin + k0_counter;
				for(unsigned int i = 0; i < k0_counter / 2; ++i)
				{
					tmp = ch.at(k0_begin + 1 + i).getP2();
					ch.at(k0_begin + 1 + i).setP2(ch.at(k0_end - i).getP2());
					ch.at(k0_end - i).setP2(tmp);
				}
			}



			// girar -> de k0_begin a k0_begin+k0_counter

//			cout << "ALPHA_K_0 : " << k0_begin << " - " << k0_counter << endl;
			k0_begin = -1;
			k0_counter = 0;
		}
	}
	ch.push_back(CRFSegment(goal_, goal_, edge::e));

//	cout << "CHANNEL -------------------FI-" << endl;
	return ch;
}


// Modified funnel algorithm tested with success in:
// with no full restriction (better to test wether it fails)
// 0 - "map/basic1.tiff";
// 5 - "map/test_iros10_2.tiff";
// 6 - "map/test_iros10_2_big.tiff";
// 10 - "map/stress_big_15_icaps.tiff";
// 11 - "map/test_iros10_2_big2.tiff"
void
CLowerBound::funnel(const vector<CRFSegment>& channel,
					vector<CRFPoint>& path,
					float& dist)
{
//	cout << "FUNNEL ---------------------------------------------" << endl;
	path.clear();
	CPoint2Di apex, vr, vl, vr_p, vl_p;
	int idxApex = 0, idxR = 0, idxL = 0; // index
	bool updateLeft = false, updateRight = false;

	// Init scan state
	apex = channel.at(0).getP1();
	vl = channel.at(0).getP1();
	vr = channel.at(0).getP2();

	// Add start point.
	dist = 0;
	path.push_back(CRFPoint(apex));

	for(unsigned int i = 1; i < channel.size(); ++i)
	{
//		cout << "i: " << i << " -> edge: " << channel.at(i).getEdge() << endl;
		vl_p = channel.at(i).getP1();
		vr_p = channel.at(i).getP2();
//		cout << "v_ps: " << vl_p << " " << vr_p << endl;

//		cout << "APEX, EDGE: " << channel.at(idxApex).getEdge() << ", " << channel.at(i).getEdge() << endl;

		// Update right vertex.
//		cout << "triarea right: " << triarea2(apex, vr, vr_p) << "<=0" << endl;
		if(triarea2(apex, vr, vr_p) <= 0)
		{

//			cout << "update right (" << triarea2(apex, vl, vr_p) << "> 0): ";
			if((apex == vr || triarea2(apex, vl, vr_p) > 0) ||
				(path.back().getEdge() != channel.at(i).getEdge() && // mod 27-01-2011
				path.back().getEdge().getSubindex() == channel.at(i).getEdge().getSubindex()))
			{
//				cout << "tighten" << endl;
				// Tighten the funnel.
				vr = vr_p;
				idxR = i;
			}
			else if(apex == vr_p){} //nothing ->  mod emili
			else {updateRight = true;}
		}

		// Update left vertex.
//		cout << "triarea left: " << triarea2(apex, vl, vl_p) << ">=0" << endl;
		if(triarea2(apex, vl, vl_p) >= 0)
		{
//			cout << "update left (" << triarea2(apex, vr, vl_p) << "< 0): ";
			if((apex == vl || triarea2(apex, vr, vl_p) < 0) ||
				(path.back().getEdge() != channel.at(i).getEdge() && // mod 27-01-2011
				path.back().getEdge().getSubindex() == channel.at(i).getEdge().getSubindex()))
			{
//				cout << "tighten" << endl;
				// Tighten the funnel.
				vl = vl_p;
				idxL = i;
			}
			else if(apex == vl_p){} //nothing ->  mod emili
			else {updateLeft = true;}
		}


		// when l_funnel over right & r_funnel over left
		// situation not taken into account in the std funnel alg.
		if(updateLeft && updateRight)
		{
//			cout << "UPDATE L/R" << endl;
//			cout << "V: " << vl << ", " << vr << endl;
//			cout << "Vp: " << vl_p << ", " << vr_p << endl;

			int a1 = triarea2(vl, vl_p, vr_p); // compute area
			int a2 = triarea2(vr, vl_p, vr_p);

			a1 < 0 ? a1 = -a1: a1 = a1; // absolute value
			a2 < 0 ? a2 = -a2: a2 = a2;

			// take vl/vr as apex according to the minimum area with next segment
			if(a1 < a2)
			{
				updateRight = true;
				updateLeft = false;
			}
			else
			{
				updateRight = false;
				updateLeft = true;
			}
		}

		if(updateLeft)
		{
//			cout << "UPDATE L" << endl;
			if(vr != path.back()) // mod emili
			{
				dist += vr.distance(path.back());
				CEdge e = channel.at(idxR).getEdge();
				path.push_back(CRFPoint(vr, e, e == edge::e? -1: idxR - 1));
//				cout << "ADDED: " << path.back() << endl;
			}

			// Make current right the new apex.
			apex = vr;
			idxApex = idxR;

			// Reset portal
			vl = apex;
			vr = apex;
			idxL = idxR = idxApex;

			// Restart scan
			i = idxApex;
//			cout << "APEX: " << channel.at(i).getEdge() << endl;
			updateLeft = false;
		}
		else if(updateRight)
		{
//			cout << "UPDATE R" << endl;
			if(vl != path.back()) // mod emili
			{
				dist += vl.distance(path.back());
				CEdge e = channel.at(idxL).getEdge();
				path.push_back(CRFPoint(vl, e, e == edge::e? -1: idxL - 1));
//				cout << "ADDED: " << path.back() << endl;
			}

			// Make current left the new apex.
			apex = vl;
			idxApex = idxL;

			// Reset portal
			vl = apex;
			vr = apex;
			idxL = idxR = idxApex;

			// Restart scan
			i = idxApex;
//			cout << "APEX: " << channel.at(i).getEdge() << endl;
			updateRight = false;
		}
	}

	if(path.back() != channel.back().getP1())
	{
		dist += channel.back().getP1().distance(path.back());
		path.push_back(channel.back().getP1());
	}
//	cout << "FUNNEL -----------------------------------------FI--" << endl;
}

vector<CRFPoint>
CLowerBound::addParallelEdges(	const vector<CRFSegment>& ch,
								const vector<CRFPoint>& p)
{
	vector<CRFPoint> o;
	for(vector<CRFPoint>::const_iterator i = p.begin(); i < p.end()-1; ++i)
	{
		o.push_back(*i);
		if(		i->getEdge().getSubindex() == (i+1)->getEdge().getSubindex() &&
				i->getEdge() != edge::e &&
				((i+1)->getIndex() - i->getIndex() > 1))
		{
//			cout << "Busco parallels entre " << *i << " - " << *(i+1) << endl;
			unsigned int subindex = i->getEdge().getSubindex();
			for(int j = i->getIndex() + 1; j < (i+1)->getIndex(); ++j)
			{
				if(ch.at(j + 1).getEdge().getSubindex() == subindex)
				{
					o.push_back(CRFPoint(	ch.at(j + 1).getP1(),
											ch.at(j + 1).getEdge(),
											j));
				}
			}
		}
	}
	o.push_back(p.back());
	return o;
}
