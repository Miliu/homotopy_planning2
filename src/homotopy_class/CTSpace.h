// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CTSpace.h
 *
 *  Created on: 06/11/2009
 *  Modified on: 29/01/2010
 *  Modified on: 06/04/2010
 *  	- Old CTopologicalSpace
 *  	- Lower bound computation removed
 *  	- Added save function that generates a file with the reference frame info
 *  Modified on: 27/04/2010
 *  	- Used class CTString instead TString
 *  Modified on: 17/06/2010
 *  	- Adapt the class for the dynamic_homotopy_planning project
 *  Modified on: 16/11/2010
 *  	- Added a method to compute hc lower bound
 *  Modified on: 20/01/2011
 *  	- Lower bound part moved to CLowerBound
 *      Author: Emili
 */

#ifndef CTSPACE_H_
#define CTSPACE_H_

#include <cstdlib>
#include <ctime>
#include <vector>
#include <list>
#include <map>
#include <string>
#include <iostream>
#include <algorithm>  // for std::for_each

#include <boost/foreach.hpp>
#include <boost/config.hpp>
#include <boost/utility.hpp>                // for boost::tie
//#include <boost/graph/graph_utility.hpp>
//#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/pending/queue.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

//#include <boost/shared_ptr.hpp>

#include "util.h"
#include "CPoint2D.h"
#include "CIdxValue.h"
#include "CMap.h"
#include "CEdge.h"
#include "CVertex.h"
#include "CRFSegment.h"
#include "CTPath.h"

#include "debug.h"
typedef boost::adjacency_list<	boost::vecS,
								boost::vecS,
								boost::undirectedS,
								CVertex,
								CEdge > TGraph;

typedef boost::graph_traits<TGraph>::vertex_descriptor TVertex;
typedef boost::queue<TVertex> TQueue;

// for lower bound
typedef std::map<CEdge, CSegment2D<int> > TSegmentMap;

class CTSpace
{
public:
	CTSpace(CMapPtr map,
			const CPoint2Di& mStart,
			const CPoint2Di& mTarget,
			const bool& plot = false);
	virtual ~CTSpace();

	void build();
	void build(const CPoint2Di& c);

	// for path planning algorithms
	int point2Vertex(const CPoint2Di& p);
	std::vector<CRFSegment> getRFSegments() const {return rfSegments_;}
	std::vector<CRFSegment> getRFSubsegments() const {return rfSubsegments_;}

	void computeHomotopyClasses(const unsigned int& maxLength);
	std::vector<CTString> getHomotopyClasses() const {return homotopyClasses_;}
	std::vector<CTPath> getFullHomotopyClasses() const {return fullHC_;}

	// for path planning plot
	cv::Mat getRFPlot() const {return rfPlot_;}

	// mod 30-12-2010
	TSegmentMap getBoundarySegmentLUT() const {return boundarySegmentLUT_;}
	std::vector<CPoint2D<float> > getCentroids() const {return centroids_;}

private:
	void constructor();
	CPoint2Di computeCPoint();
	CPoint2Di computeRandomCPoint();
	bool validCPoint(const CPoint2Di& c);

	int anglei(const CPoint2Di& p1, const CPoint2Di& p2)
	{
		CPoint2Di tmp(p2);
		tmp -= p1;
		return round(atan2(tmp.y(), tmp.x()) * 1000);
	}

	void computeRFSegments(std::vector<CBPoint>& obstacles, CPoint2Di& c);
	std::vector<CRFSegment> computeL_kSegments(const CBPoint& obstacle, const CPoint2Di& c);
	void computeRFSubsegments(const std::vector<CRFSegment>& s);
	std::vector<CRFSegment> computeL_kSubsegments(const unsigned int& index, const CRFSegment& s);
	void computeRFBoundarySubsegments(const std::vector<CRFSegment>& s);

	void initWedge(	std::vector<TGraph::vertex_descriptor>& wedge,
					unsigned int idxWedge,
					unsigned int& idxSubwedge);
	void updateWedge(	std::vector<TGraph::vertex_descriptor>& wedge,
						unsigned int idxWedge,
						unsigned int& idxSubwedge);
	void buildGraph1Obstacle();
	void buildGraph();
	void printGraph(TGraph graph);
	void printWedge(const std::vector<TVertex>& v);



	void save(const std::string& filename);


	std::vector<CTString> BFSJenkins(	TGraph& graph,
										const TVertex& start,
										const TVertex& goal,
										const unsigned int& length);
	bool duplicated(	const CTString& pSorted,
						const std::vector<CTString>& hcSorted);

	void computeWedgeCentroids(TGraph graph);
	CPoint2D<float> computeCentroid(	const CEdge& e1min,
										const CEdge& e1max,
										const CEdge& e2min,
										const CEdge& e2max,
										const unsigned int& idx);

	CMapPtr map_;
	std::vector<CBPoint> bPoints_;
	CPoint2Di mStart_, mTarget_;

	CPoint2Di c_;
	std::vector<CRFSegmentIndex> rfSegmentsIndex_;
	std::vector<CRFSegment> rfSegments_;
	std::vector<CRFSegment> rfSubsegments_;
	std::vector<CRFSegment> rfBoundarySubsegments_;

	std::vector<std::vector<CRFSegment> > rawRFSubsegments_;
	std::vector<std::vector<TVertex> > rawWedges_;
	std::vector<CPoint2D<float> > centroids_;

	TGraph g2_;
	TVertex gStart_, gTarget_;
	std::vector<CTString> homotopyClasses_;
	std::vector<CTPath> fullHC_;

	// for plotting
	bool plot_;
	std::vector<cv::Mat> mapPlanes_;
	cv::Mat rfPlot_;
	cv::Mat graphPlot_;
	std::vector<cv::Scalar> colors_;

	// lower bound
	TSegmentMap 								segmentLUT_;
	TSegmentMap 								boundarySegmentLUT_;
};

#endif /* CTSPACE_H_ */

