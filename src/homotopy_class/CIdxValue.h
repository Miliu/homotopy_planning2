// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CIdxValue.h
 *
 *  Created on: 13/12/2010
 *      Author: emili
 */

#ifndef CIDXVALUE_H_

#include <iostream>

template <typename T, typename U>
class CIdxValue
{
public:
	CIdxValue(	const T& index,
				const U& value)
	:
		index_(index),
		value_(value)
	{}
	T getIndex() const {return index_;}
	U getValue() const {return value_;}
	bool operator<(const CIdxValue& s) const {return (value_ < s.value_);}

	friend std::ostream& operator<<(std::ostream& cout, const CIdxValue& s)
	{
		cout << s.getIndex() << " " << s.getValue();
		return cout;
	}

private:
	T index_;
	U value_;
};

#define CIDXVALUE_H_


#endif /* CIDXVALUE_H_ */
