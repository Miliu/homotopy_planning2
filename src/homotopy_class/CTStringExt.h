// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CTStringExt.h
 *
 *	Extends functionality of CTString.
 *	Takes into account when a topological path with alphak_0 substring is
 *	reversible without changing its topology.
 *  Created on: 10/05/2010
 *      Author: Emili
 */


#ifndef CTSTRINGEXT_H_
#define CTSTRINGEXT_H_

#include "CTString.h"
#include <vector>
#include <iostream>

// Extended CTString
class CTStringExt
:
	public CTString
{
public:
	CTStringExt()
	:
		CTString(),
		reversed_(false)
	{}

	void operator=(const CTStringExt& s)
	{
		str_ = s.str_;
		reversed_ = s.reversed_;
	}
	bool isReversed() const {return reversed_;}
	void setReversed(const bool& reversed){reversed_ = reversed;}
private:
	bool reversed_;
};


// CTString Reversible
class CTStringR
:
	public CTString
{
public:
	CTStringR()
	:
		reversible_(false),
		begin_(tstring::npos),
		end_(0)
	{}

	CTStringR(	const CTString& s,
				const unsigned int& nObstacles)
	:
		CTString(s)
	{
		std::vector<CEdge>::iterator fst = str_.begin();
		std::vector<CEdge>::iterator end = str_.end() - nObstacles + 1;;
		std::vector<CEdge>::iterator i, j;
		unsigned int count;
		bool stop = false;

		while(!stop)
		{
			i = std::find_if(fst, str_.end(), CEdgeEqualTypeAndSubsegment(CEdge('A', 0, 0)));
			if(i != str_.end())
			{
				j = i+1;
				count = 1;
				while(j != str_.end() && j->getType() == 'A' && j->getSubsegment() == 0 && count < nObstacles)
				{
					j++;
					count++;
				}
				if(count  == nObstacles)
				{
					reversible_ = true;
					begin_ = i - str_.begin();
					end_ = j - str_.begin();
					stop = true;
				}
				else
					fst = j+1;
			}
			else
			{
				reversible_ = false;
				begin_ = tstring::npos;
				end_ = 0;
				stop = true;
			}
		}
//		std::cout << "reversible, begin, end: " << reversible_ << " " << begin_ << " " <<  end_ << std::endl;
	}

	void operator=(const CTStringR& s)
	{
		str_ = s.str_;
		reversible_ = s.reversible_;
		begin_ = s.begin_;
		end_ = s.end_;
	}

	bool isReversible() const {return reversible_;}
	CEdge atR(const unsigned int& i) const
	{
		if(reversible_)
			if(inReversibleSubstring(i))
			{
				return CTString::str_.at(end_ - 1 - (i - begin_));
				//return CTString::string_.at(size() - 1 - i); // its wrong
			}
			else
			{
				return str_.at(i);
				std::cout << "Warning. atReverse. out of reverse boundary" << std::endl;
			}
		else
		{
			std::cout << "Warning. atReverse: path not reversible" << std::endl;
			return CEdge();
		}
	}


	bool inReversibleSubstring(const unsigned int& i) const {return (i >= begin_ && i < end_);}

	friend std::ostream& operator<<(std::ostream& cout, const CTStringR& s)
	{
		for(std::vector<CEdge>::const_iterator i = s.str_.begin(); i < s.str_.end(); i++)
			cout << *i;
		cout << " Reversible: " << s.reversible_ << "(" << s.begin_ << "," << s.end_ << ")";
		return cout;
	}

	friend std::ostream& toLatex(std::ostream& cout, const CTStringR& s)
	{
		for(std::vector<CEdge>::const_iterator i = s.str_.begin(); i < s.str_.end(); i++)
			toLatex(cout, *i);

		cout << "TOLATEX no acabat!";
		return cout;
	}
private:
	bool reversible_;
	unsigned int begin_, end_;
};

#endif /* CTSTRINGEXT_H_ */

