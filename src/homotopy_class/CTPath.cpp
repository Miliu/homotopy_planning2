// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CTPath.cpp
 *
 *  Created on: 27/01/2010
 *      Author: Emili
 */

#include "CTPath.h"

using namespace std;

CTPath::CTPath()
:
		vertexCicle_(false)
{}

CTPath::CTPath(const int& vertex)
:
		vertexCicle_(false)
{
	vertexIdx_[vertex].push_back(vertexSeq_.size());
	vertexSeq_.push_back(vertex);
}

CTPath::CTPath(const CTPath& path)
:
		edgeSeq_(path.edgeSeq_),
		edgeIdx_(path.edgeIdx_),
		vertexSeq_(path.vertexSeq_),
		vertexIdx_(path.vertexIdx_),
		vertexCicle_(path.vertexCicle_)
{}

CTPath::~CTPath()
{
	//~edgeSeq_;
	//falta edgeIdx, vertexIdx
}

void CTPath::operator=(const CTPath& path)
{
	edgeSeq_ = path.edgeSeq_;
	edgeIdx_ = path.edgeIdx_;
	vertexSeq_ = path.vertexSeq_;
	vertexIdx_ = path.vertexIdx_;
	vertexCicle_ = path.vertexCicle_;
}

void CTPath::add(const CEdge& edge, const int& vertex)
{
	if(edge.getType() == 'B' || edge.getSubsegment() <= 0)
		edgeIdx_[edge.plain()].push_back(edgeSeq_.size());
	if(edge.getType() == 'A' && edge.getSubsegment() >= 0) // alpha with subsegment >= 0
		edgeIdx_[edge.changeType().plain()].push_back(edgeSeq_.size());
	edgeSeq_.add(edge);

	vertexIdx_[vertex].push_back(vertexSeq_.size());
	vertexSeq_.push_back(vertex);
	if(vertexIdx_[vertex].size() >= 2)
		vertexCicle_ = true;
}

ostream& operator<<(ostream& cout, const CTPath& path)
{
	// Edges
	cout << path.edgeSeq_;
	cout << "\t\t";
	std::vector<unsigned int>::const_iterator j;
	TEdgeMap::const_iterator k = path.edgeIdx_.begin();
	while(k != path.edgeIdx_.end())
	{
		cout << k->first << "( ";
		for(j = k->second.begin(); j != k->second.end(); ++j)
			cout << *j << " ";
		cout << ")" ;
		++k;
	}

	// Vertexs
	cout << "\t|";
	for(vector<int>::const_iterator i = path.vertexSeq_.begin(); i < path.vertexSeq_.end(); ++i)
		cout << *i << " ";
	cout << "\t\t";
	std::vector<int>::const_iterator l;
	TVertexMap::const_iterator m = path.vertexIdx_.begin();
	while(m != path.vertexIdx_.end())
	{
		cout << m->first << "( ";
		for(l = m->second.begin(); l != m->second.end(); ++l)
			cout << *l << " ";
		cout << ")" ;
		++m;
	}
	cout << " Cicle: " << path.vertexCicle_;
	return cout;
}

bool CTPath::checkCriteria()
{
	if(edgeSeq_.size() >= 3)
	{
		#ifdef _NDEBUG
		cout << "WrapJenkins ---------------";
		#endif
		if(wrapJenkins())
			return true;
#ifdef _NDEBUG
		else
			cout << "NO" << endl;
#endif

		#ifdef _NDEBUG
		cout << "WrapHdz -------------------";
		#endif
		if(wrapHdz())
			return true;
#ifdef _NDEBUG
		else
			cout << "NO" << endl;
#endif

		if(edgeSeq_.size() >= 4)
		{
			#ifdef _NDEBUG
			cout << "SelfcrossingJenkins -------";
			#endif
			if(selfCorssingJenkins())
			{
#ifdef _NDEBUG
				cout << endl;
#endif
				return true;
			}
#ifdef _NDEBUG
			else
				cout << "NO" << endl;
#endif

		}
		#ifdef _NDEBUG
		cout << "DuplicatedInAlphaSubstr----";
		#endif
		if(duplicatedInAlphasSubstring())
			return true;
#ifdef _NDEBUG
		else
			cout << "NO" << endl;
#endif

	}
	return false;
}

// The same as the checkCriteria + vertexCicle criterion
bool CTPath::checkCriteriaFull()
{
	if(vertexCicle_)
	{
#ifdef _NDEBUG
		cout << "VertexCicle ---------------YES" << endl;
#endif
		return true;
	}
#ifdef _NDEBUG
	else
		cout << "VertexCicle ---------------NO" << endl;
#endif

	if(edgeSeq_.size() >= 3)
	{
		#ifdef _NDEBUG
		cout << "WrapJenkins ---------------";
		#endif
		if(wrapJenkins())
			return true;
#ifdef _NDEBUG
		else
			cout << "NO" << endl;
#endif

		#ifdef _NDEBUG
		cout << "WrapHdz -------------------";
		#endif
		if(wrapHdz())
			return true;
#ifdef _NDEBUG
		else
			cout << "NO" << endl;
#endif

		if(edgeSeq_.size() >= 4)
		{
			#ifdef _NDEBUG
			cout << "SelfcrossingJenkins -------";
			#endif
			if(selfCorssingJenkins())
			{
#ifdef _NDEBUG
				cout << endl;
#endif
				return true;
			}
#ifdef _NDEBUG
			else
				cout << "NO" << endl;
#endif

		}
		#ifdef _NDEBUG
		cout << "DuplicatedInAlphaSubstr----";
		#endif
		if(duplicatedInAlphasSubstring())
			return true;
#ifdef _NDEBUG
		else
			cout << "NO" << endl;
#endif

	}
	return false;
}

//a_k_s1...X_k_s2...a_k_s3 or b_k_s1...X_k_s2...b_k_s3
// and s1 == s3
bool CTPath::wrapJenkins()
{
	CEdge e = edgeSeq_.back();
	vector<unsigned int>* v;
	if(e.getType() == 'B' || e.getSubsegment() <= 0 ) // beta || alpha_k_s1 with s1 <= 0
		v = &edgeIdx_[e.plain()];
	else
		v = &edgeIdx_[e.changeType().plain()];

	vector<unsigned int>::const_iterator it = v->begin();
	while(it < v->end()-1)
	{
		if(	(edgeSeq_.at(*it) == e) &&
			(it != v->end() - 1) && (v->back() - *it) > 1)
		{
//#ifdef _NDEBUG
//			cout << "s1 == s3: " << *it << "," << edgeSeq_.size() - 1 << endl;
//#endif
			for(unsigned int i = *it + 1; i < v->back(); ++i)
			{
				if(	edgeSeq_.at(i).getSubindex() == e.getSubindex() &&
					edgeSeq_.at(i).getSubsegment() != e.getSubsegment())
				{
#ifdef _NDEBUG
					cout << "s1 == s3: " << *it << "," << edgeSeq_.size() - 1 << "; " << "X_k_s2: " << i << endl;
#endif
					return true;
				}
			}
			return false;
		}
		++it;
	}
	return false;
}

// X_k_s1...X_k_s2...X_k_s3
bool CTPath::wrapHdz()
{
	CEdge e = edgeSeq_.back();
	if(e.getType() == 'B') // beta
		return wrapHdz_pos(e, edgeIdx_[e.plain()]);
	else if(e.getSubsegment() > 0) // alpha > 0
		return wrapHdz_pos(e, edgeIdx_[e.changeType().plain()]);
	else if(e.getSubsegment() < 0) // alpha < 0
		return wrapHdz_neg(e, edgeIdx_[e.plain()]);
	else // type == 'A' && subsegment == 0
		return (wrapHdz_pos(e, edgeIdx_[e.changeType().plain()]) || wrapHdz_neg(e, edgeIdx_[e.plain()]));
}


bool
CTPath::wrapHdz_pos(const CEdge& e, const vector<unsigned int>& v)
{
	vector<unsigned int>::const_reverse_iterator it_s2 = v.rbegin() + 1;
	while(it_s2 < v.rend()-1)
	{
		if(edgeSeq_.at(*it_s2).getSubsegment() < e.getSubsegment()) //s2 < s3?
		{
			vector<unsigned int>::const_reverse_iterator it_s1 = it_s2;
			++it_s1;
			while(it_s1 < v.rend())
			{
				if(edgeSeq_.at(*it_s1).getSubsegment() > edgeSeq_.at(*it_s2).getSubsegment()) //s1 > s2?
				{
#ifdef _NDEBUG
					cout << "s1,s2,s3>=0 && s1>s2<s3: " << *it_s1 << "," << *it_s2 << "," << edgeSeq_.size() - 1 << endl;
#endif
					return true;
				}
				++it_s1;
			}
		}
		++it_s2;
	}
	return false;
}

bool
CTPath::wrapHdz_neg(const CEdge& e, const vector<unsigned int>& v)
{
	vector<unsigned int>::const_reverse_iterator it_s2 = v.rbegin() + 1;
	while(it_s2 < v.rend() - 1)
	{
		if(edgeSeq_.at(*it_s2).getSubsegment() > e.getSubsegment()) //s2 > s3?
		{
			vector<unsigned int>::const_reverse_iterator it_s1 = it_s2;
			++it_s1;
			while(it_s1 < v.rend())
			{
				if(edgeSeq_.at(*it_s1).getSubsegment() < edgeSeq_.at(*it_s2).getSubsegment()) //s1 < s2?
				{
#ifdef _NDEBUG
					cout << "s1,s2,s3<=0 && s1<s2>s3: " << *it_s1 << "," << *it_s2 << "," << edgeSeq_.size() - 1 << endl;
#endif
					return true;
				}
				++it_s1;
			}
		}
		++it_s2;
	}
	return false;
}

//X_k_s1...a_k_s2...b_k_s3...X_k_s4
//X_k_s1...b_k_s2...a_k_s3...X_k_s4
bool CTPath::selfCorssingJenkins()
{
	CEdge e = edgeSeq_.back();
	if(e.getType() == 'B')
		return selfCorssingJenkins_pos(e, edgeIdx_[e.plain()]);
	else if(e.getSubsegment() > 0)
		return selfCorssingJenkins_pos(e, edgeIdx_[e.changeType().plain()]);
	else if(e.getSubsegment() < 0)
		return selfCorssingJenkins_neg(e, edgeIdx_[e.plain()]);
	else // type == 'A' && subsegment == 0
		return (selfCorssingJenkins_pos(e, edgeIdx_[e.changeType().plain()]) || selfCorssingJenkins_neg(e, edgeIdx_[e.plain()]));
	return false;
}

bool
CTPath::selfCorssingJenkins_pos(const CEdge& e,
								const vector<unsigned int>& v)
{
	if(v.size() < 2) return false;

	// compare with the fst X_k
	vector<unsigned int>::const_iterator it_X_k = v.begin();
	CEdge e_s1 = edgeSeq_.at(*it_X_k);
	if(e_s1.getSubsegment() > e.getSubsegment()) // s1>s4
	{
		#ifdef _NDEBUG
		cout << "s1>s4: "  << *it_X_k << "," << edgeSeq_.size() - 1 << "; ";
		#endif
		if(find_a_mb_m(*it_X_k + 1, edgeSeq_.size() - 1, e_s1)) //find a_m_s2, b_m_s3
			return true;

		//seems that wrapHdz covers it
		while(++it_X_k != v.end() - 1) // look for s1<s4
		{
			e_s1 = edgeSeq_.at(*it_X_k);
			if(e_s1.getSubsegment() < e.getSubsegment())
			{
				#ifdef _NDEBUG
				cout << "s1<s4: "  << *it_X_k << "," << edgeSeq_.size()-1 << "; ";
				#endif
				if(find_b_ma_m(*it_X_k + 1, edgeSeq_.size() - 1, e_s1))  // find b_m_s2, a_m_s3
					return true;
			}
		}
	}
	else if(e_s1.getSubsegment() < e.getSubsegment()) //s1<s4
	{
		#ifdef _NDEBUG
		cout << "s1<s4: " << *it_X_k << "," << edgeSeq_.size() - 1 << "; ";
		#endif
		if(find_b_ma_m(*it_X_k + 1, edgeSeq_.size() - 1, e_s1)) // find b_m_s2, a_m_s3
			return true;

		//seems that wrapHdz covers it
		while(++it_X_k != v.end() - 1) // look for s1>s4
		{
			e_s1 = edgeSeq_.at(*it_X_k);
			if(e_s1.getSubsegment() > e.getSubsegment())
			{
				#ifdef _NDEBUG
				cout << "s1>s4: "  << *it_X_k << "," << edgeSeq_.size() - 1 << "; ";
				#endif
				if(find_a_mb_m(*it_X_k + 1, edgeSeq_.size() - 1, e_s1)) //find a_m_s2, b_m_s3
					return true;
			}
		}
	}
	return false;
}

bool
CTPath::selfCorssingJenkins_neg(const CEdge& e,
								const vector<unsigned int>& v)
{
	if(v.size() < 2) return false;

	// compare with the fst X_k
	vector<unsigned int>::const_iterator it_X_k = v.begin();
	CEdge e_s1 = edgeSeq_.at(*it_X_k);
	if(e_s1.getSubsegment() < e.getSubsegment()) // s1<s4
	{
		#ifdef _NDEBUG
		cout << "s1<s4: "  << *it_X_k << "," << edgeSeq_.size()-1 << "; ";
		#endif
		if(find_a_mb_m(*it_X_k + 1, edgeSeq_.size() - 1, e_s1)) //find a_m_s2, b_m_s3
			return true;

		//seems that wrapHdz covers it
		while(++it_X_k != v.end() - 1) // look for s1>s4
		{
			e_s1 = edgeSeq_.at(*it_X_k);
			if(e_s1.getSubsegment() > e.getSubsegment())
			{
				#ifdef _NDEBUG
				cout << "s1>s4: "  << *it_X_k << "," << edgeSeq_.size()-1 << "; ";
				#endif
				if(find_b_ma_m(*it_X_k + 1, edgeSeq_.size() - 1, e_s1))  // find b_m_s2, a_m_s3
					return true;
			}
		}
	}
	else if(e_s1.getSubsegment() > e.getSubsegment()) //s1>s4
	{
		#ifdef _NDEBUG
		cout << "s1>s4: " << *it_X_k << "," << edgeSeq_.size()-1 << "; ";
		#endif
		if(find_b_ma_m(*it_X_k + 1, edgeSeq_.size() - 1, e_s1)) // find b_m_s2, a_m_s3
			return true;

		//seems that wrapHdz covers it
		while(++it_X_k != v.end() - 1) // look for s1<s4
		{
			e_s1 = edgeSeq_.at(*it_X_k);
			if(e_s1.getSubsegment() < e.getSubsegment())
			{
				#ifdef _NDEBUG
				cout << "s1<s4: "  << *it_X_k << "," << edgeSeq_.size()-1 << "; ";
				#endif
				if(find_a_mb_m(*it_X_k + 1, edgeSeq_.size() - 1, e_s1)) //find a_m_s2, b_m_s3
					return true;
			}
		}
	}
	return false;
}

bool
CTPath::find_a_mb_m(	const unsigned int& ibegin,
						const unsigned int& iend,
						const CEdge& e)
{
//	#ifdef _NDEBUG
//	cout << "find_a_mb_m; range: [" << ibegin << "," << iend << "); " << "e: " << e << endl;
//	#endif
	unsigned int i, j;
	for(i = ibegin; i < iend - 1; ++i)
	{
		if(	edgeSeq_.at(i).getType() == 'A' &&
			edgeSeq_.at(i).getSubindex() != e.getSubindex())
		{
			for(j = i + 1; j < iend; ++j)
			{
				if(	edgeSeq_.at(j).getType() == 'B' &&
					edgeSeq_.at(j).getSubindex() == edgeSeq_.at(i).getSubindex())
				{
					#ifdef _NDEBUG
					cout << "find_a_mb_m; range: [" << ibegin << "," << iend << "); " << "e: " << e << "; ";
					cout <<	"a_m_s2 < b_m_s3: " << i << "," << j;
					#endif
					return true;
				}

			}
		}
	}
	return false;
}

bool
CTPath::find_b_ma_m(	const unsigned int& ibegin,
						const unsigned int& iend,
						const CEdge& e)
{
//	#ifdef _NDEBUG
//	cout << "find_b_ma_m; range: [" << ibegin << "," << iend << "); " << "e: " << e << endl;
//	#endif
	unsigned int i, j;
	for(i = ibegin; i < iend - 1; ++i)
	{
		if(	edgeSeq_.at(i).getType() == 'B' &&
			edgeSeq_.at(i).getSubindex() != e.getSubindex()	)
		{
			for(j = i + 1; j <  iend; ++j)
			{
				if(	edgeSeq_.at(j).getType() == 'A' &&
					edgeSeq_.at(j).getSubindex() == edgeSeq_.at(i).getSubindex())
				{
					#ifdef _NDEBUG
					cout << "find_b_ma_m; range: [" << ibegin << "," << iend << "); " << "e: " << e << "; ";
					cout << "b_m_s2 < a_m_s3: " << i << "," << j;
					#endif
					return true;
				}
			}
		}
	}
	return false;
}

bool
CTPath::duplicatedInAlphasSubstring()
{
	CEdge e = edgeSeq_.back();
	if(e.getType() == 'A')
	{
		int i = edgeSeq_.size() - 2;
		while(i >= 0)
		{
			if(edgeSeq_.at(i).getType() == 'B')
				return false;
			else if(edgeSeq_.at(i) == e)
			{
#ifdef _NDEBUG
				cout << "a_k_s1==a_k_s2: " << edgeSeq_.size() - 1 << "," << i << endl;
#endif
				return true;
			}
			--i;
		}
	}
	return false;
}
