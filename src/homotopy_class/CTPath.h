// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CTPath.h
 *
 *  Created on: 03/11/2010
 *    - Modification of version used in ICRA11 & IWC11
 *    - Canonical sequence removed
 *    - EdgeMap:
 *    	- Element for each obstacle.
 *    	- Example: obstacle k -> elements k- & k-.
 *    				k- contains a vector with the positions of edges a_k_s where s <= 0.
 *    				k+ contains a vector with the positions of edges X_k_s where s >= 0.
 *    				Notice that a_k_0 are duplicated.
 *    - Criteria:
 *    	- Wrap Jenkins: a_k_s1...X_k_s2...a_k_s3 or b_k_s1...X_k_s2...b_k_s3
 *    		- Discard if s1 == s3
 *    	- Wrap Emili: X_k_s1...X_k_s2...X_k_s3
 *    		- Discard if s1,s2,s3>=0 && s1>s2 && s2<s3
 *    		- Discard if s1,s2,s3<=0 && s1<s2 && s2>s3
 *    	- Wrap Jenkis:
 *    		X_k_s1...a_k_s2...b_k_s3...X_k_s4 => s2<s3
 *    		- Discard if s1,s4>=0 && s1>s4
 *    		- Discard if s1,s4<=0 && s1<s4
 *			X_k_s1...b_k_s2...a_k_s3...X_k_s4 => s2>s3
 *    		- Discard if s1,s4>=0 && s1<s4
 *    		- Discard if s1,s4<=0 && s1>s4
 *      Author: Emili
 */

#ifndef CTPATH_H_
#define CTPATH_H_

#include "debug.h"
#include "CTString.h"
#include "SOfPtr.h"
#include <map>
#include <vector>
#include <iostream>
#include <stdexcept>
#include <boost/shared_ptr.hpp>

typedef std::map<CEdge, std::vector<unsigned int> > TEdgeMap;
typedef std::map<int, std::vector<int> > TVertexMap;

class CTPath
{
public:
	CTPath();
	CTPath(const int& vertex); // to set the initial vertex of the path
	CTPath(const CTPath& path);
	virtual ~CTPath();

	CTString getEdgeSequence() const {return edgeSeq_;}
	std::vector<int> getVertexSequence() const {return vertexSeq_;}
	bool hasVertexCicle() const
	{

		return vertexCicle_;
	}

	void add(const CEdge& edge, const int& vertex);

	bool checkCriteria();
	bool checkCriteriaFull();

	void operator=(const CTPath& path);
	bool operator==(const CTPath& path) const;
	bool operator!=(const CTPath& path) const;

	friend std::ostream& operator<<(std::ostream& cout, const CTPath& path);

private:
	bool wrapJenkins();

	bool wrapHdz();
	bool wrapHdz_pos(const CEdge& e, const std::vector<unsigned int>& v);
	bool wrapHdz_neg(const CEdge& e, const std::vector<unsigned int>& v);

	bool selfCorssingJenkins();
	bool selfCorssingJenkins_pos(const CEdge& e, const std::vector<unsigned int>& v);
	bool selfCorssingJenkins_neg(const CEdge& e, const std::vector<unsigned int>& v);

	bool duplicatedInAlphasSubstring();

	bool find_a_mb_m(	const unsigned int& ibegin,
						const unsigned int& iend,
						const CEdge& e);
	bool find_b_ma_m(	const unsigned int& ibegin,
						const unsigned int& iend,
						const CEdge& e);

//	bool criterion1();
//	bool criterion2();
//	bool csCriterion2(); //idem but applied to the canonical sequence only
//	bool criterion3();

	CTString			edgeSeq_;
	TEdgeMap	 		edgeIdx_;
	std::vector<int> 	vertexSeq_;
	TVertexMap			vertexIdx_;
	bool 				vertexCicle_;
};


/*
typedef std::vector<unsigned int> TUintVector;
typedef std::map<CEdge, TUintVector> TStrMap;
typedef boost::shared_ptr<TStrMap> TStrMapPtr;

// For canonical sequence
typedef std::vector<int> TIntVector;
typedef std::map<CEdge, TIntVector> TCSMap;
typedef boost::shared_ptr<TCSMap> TCSMapPtr;

typedef std::map<int, TIntVector> TVertexMap;
typedef boost::shared_ptr<TVertexMap> TVertexMapPtr;

class CTPath
{
public:
	CTPath(	const CTString& string,
			const TStrMapPtr& index,
			const CTString& csString,
			const TCSMapPtr& csIndex);
	CTPath(const CTPath& path);
	//virtual ~CTPath();

	CTString getString() const {return string_;}
	bool hasIndex();

	void add(const CEdge& edge, const int& vertex);
	//void add(CEdge* edge, const int& vertex);

	bool checkCriteria();

	void operator=(const CTPath& path);
	bool operator==(const CTPath& path) const;
	bool operator!=(const CTPath& path) const;

	friend std::ostream& operator<<(std::ostream& cout, const CTPath& path);
	friend std::ostream& toLatex(std::ostream& cout, const CTPath& path);

private:
	bool criterion1();
	bool criterion2();
	bool csCriterion2(); //idem but applied to the canonical sequence only
	bool criterion3();

	CTString	string_;
	TStrMapPtr 	index_;

	// Canonical sequence
	void updateCanonicalSequence(CTPath& path, const CEdge& edge);

	CTString	csString_;
	TCSMapPtr 	csIndex_;
	int			csPosBeta_; // last beta position

	TVertexMapPtr vIndex_; // vertices index
	bool		vCicle_; //vetex cicle (not with the start)
};
*/
#endif /* CTPATH_H_ */
