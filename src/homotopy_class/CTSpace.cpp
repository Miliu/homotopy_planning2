// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CTSpace.cpp
 *
 *  Created on: 06/11/2009
 *      Author: Emili
 */

#include "CTSpace.h"

using namespace boost::posix_time;

struct my_node_writer
{
        // my_node_writer() {}
        my_node_writer(TGraph& g_)
        :
        	g (g_)
        {}

        template <class Vertex>
        void operator()(std::ostream& out, Vertex v)
        {
             out << " [label=\"" << g[v] << "\"]" << std::endl;
        }
        // bleah.  I can't get references right...
        // according to http://www.knowledgesearch.org/doc/examples.html
        // it should be a reference here, not the type itself.
        // but g++ either barfs, or the program segfaults.
        TGraph g;
};

struct my_edge_writer
{
        my_edge_writer(TGraph& g_)
        :
        	g (g_)
        {}

        template <class Edge>
        void operator()(std::ostream& out, Edge e)
        {
                // just an example, showing that local options override global
                out << " [color=purple]" << std::endl;
                out << " [label=\"" << g[e]  << "\"]" << std::endl;
        }
        TGraph g;
};

struct my_graph_writer
{
        void operator()(std::ostream& out) const
        {
                out << "graph [bgcolor=lightgrey]" << std::endl;
                out << "node [shape=circle color=blue]" << std::endl;
                // just an example, showing that local options override global
                out << "edge [color=red]" << std::endl;
        }
} myGraphWrite;


using namespace std;
using namespace boost;

CTSpace::CTSpace(	CMapPtr map,
					const CPoint2Di& mStart,
					const CPoint2Di& mTarget,
					const bool& plot)
:
			map_(map),
			mStart_(mStart),
			mTarget_(mTarget),
			plot_(plot)
{
	cout << "start: " << mStart << endl;
	cout << "goal: " << mTarget << endl;

	if(plot_)
	{
		CPoint2Di size = map_->getVSizeCell();
		rfPlot_.create(size.y(), size.x(), CV_8UC3);
		rfPlot_ = cv::Scalar(255, 255, 255);

		cv::namedWindow("referenceFrame", CV_WINDOW_AUTOSIZE);
		cvMoveWindow("referenceFrame", 3 * size.x() + 100, 100);
		imshow("referenceFrame", rfPlot_);

		cv::namedWindow("graph", CV_WINDOW_AUTOSIZE);
		cvMoveWindow("graph", 4 * size.x() + 100, 100);

		colors_.push_back(cv::Scalar(255, 0, 0)); // BGR color format
		colors_.push_back(cv::Scalar(0, 255, 0));
		colors_.push_back(cv::Scalar(0, 0, 255));
		colors_.push_back(cv::Scalar(0, 255, 255));
		colors_.push_back(cv::Scalar(0, 0, 0));

		//mapPlot_ = map->getCObstacle();
		cv::split(rfPlot_, mapPlanes_);
		mapPlanes_[0] = map->getCObstacle();
		mapPlanes_[1] = map->getCObstacle();
		mapPlanes_[2] = map->getCObstacle();
		cv::merge(mapPlanes_, rfPlot_);
	}
}

CTSpace::~CTSpace()
{}

void
CTSpace::build()
{
	do
	{
		c_ = computeCPoint();
		constructor();
		if(gStart_ == gTarget_)
			cout << "Start & Target at the same vertex. Rebuilding graph..." << endl;
	}
	while(gStart_ == gTarget_);
}

void
CTSpace::build(const CPoint2Di& c)
{
	c_ = c;
	constructor();
	if(gStart_ == gTarget_)
		throw runtime_error("Start & target at the same vertex.");
	if(!validCPoint(c_))
		throw runtime_error("Invalid C point.");
}

void
CTSpace::constructor()
{
	cout << "constructor ------------------" << endl;

	cout << "C point: " << c_ << endl;
	bPoints_ = map_->getBPoints();

	if(plot_) debug::printVector("B points -------------", "", bPoints_);

	computeRFSegments(bPoints_, c_);
	if(plot_) debug::printVector("Segments -------------", "S", rfSegments_);

	computeRFSubsegments(rfSegments_);
	if(plot_)
	{
		debug::printVectorOfVector("Raw subsegments -------------", "S", rawRFSubsegments_);
		debug::printVector("Post-processed subsegments -------------", "", rfSubsegments_);
	}
	computeRFBoundarySubsegments(rfSubsegments_);
	if(plot_)
	{
		debug::printVector("Post-processed boundary subsegments -------------", "", rfBoundarySubsegments_);
	}
	debug::printVector("Segments index -------------", "", rfSegmentsIndex_);

	if(plot_)
	{
		cv::merge(mapPlanes_, rfPlot_);
		for(vector<vector<CRFSegment> >::iterator i = rawRFSubsegments_.begin(); i < rawRFSubsegments_.end(); ++i)
			for(vector<CRFSegment> ::iterator j = i->begin(); j < i->end(); ++j)
			{
				cv::line(rfPlot_, Point2i(j->getP1().x(), j->getP1().y()), Point2i(j->getP2().x(), j->getP2().y()), colors_.at((j-i->begin())% colors_.size()), 1);
			}

		for(vector<CBPoint>::iterator i = bPoints_.begin(); i < bPoints_.end(); ++i)
			cv::circle(rfPlot_, Point2i(i->getPoint().x(), i->getPoint().y()), 2, cv::Scalar(0, 0, 255),2);
		cv::circle(rfPlot_, Point2i(c_.x(), c_.y()), 2, cv::Scalar(255, 0, 0),2);
		cv::rectangle(rfPlot_, Point2i(mStart_.x()-3, mStart_.y()-3), Point2i(mStart_.x()+3, mStart_.y()+3),cv::Scalar(0, 0, 255),-1);
		cv::circle(rfPlot_, Point2i(mTarget_.x(), mTarget_.y()), 2, cv::Scalar(0, 255, 0),2);

//		// centroids
//		for(vector<CPoint2D<float> >::iterator i = centroids_.begin(); i < centroids_.end(); ++i)
//			cv::circle(rfPlot_, Point2i(i->x(), i->y()), 2, cv::Scalar(255, 0, 0),2);

		cv::imshow("referenceFrame", rfPlot_);
	}

	cout << "Graph --------------" << endl;
	if(bPoints_.size() == 1)
		buildGraph1Obstacle();
	else //if(bPoints_.size() > 1)
		buildGraph();

	if(plot_)
	{
		printGraph(g2_);

		ofstream f;
		f.open("test/graph.txt",ios::out);
		write_graphviz(f,g2_,my_node_writer(g2_),my_edge_writer(g2_),myGraphWrite);
		if(system("circo test/graph.txt -T png -o test/graph.png") == 0)
		{
			graphPlot_ = cv::imread("test/graph.png");
			imshow("graph", graphPlot_);
		}
		else
			cout << "CTSpace. Error. Cannot generate graph image" << endl;

		cout << "Vertex per wedge-------" << endl;
		int idx = -1;
		for(vector<vector<TGraph::vertex_descriptor> >::iterator i = rawWedges_.begin(); i < rawWedges_.end(); i++)
		{
			cout << "W" << ++idx << ": ";
			printWedge(*i);
			cout << endl;
		}
	}

	computeWedgeCentroids(g2_);
	if(plot_)
	{
		for(vector<CPoint2D<float> >::iterator i = centroids_.begin(); i < centroids_.end(); ++i)
			cv::circle(rfPlot_, Point2i(i->x(), i->y()), 1, cv::Scalar(255, 0, 0), 1);

		cv::imshow("referenceFrame", rfPlot_);
	}


	if(!bPoints_.empty())
	{
		cout << "Start/Goal nodes----------" << endl;
		gStart_ = vertex(point2Vertex(mStart_), g2_);
		gTarget_ = vertex(point2Vertex(mTarget_), g2_);
		cout << "start: " << mStart_ << "\t->\t" << g2_[gStart_] << endl;
		cout << "target: " << mTarget_ << "\t->\t" << g2_[gTarget_] << endl;

		cout << "Saving reference frame----------" << endl;
		save("test/reference_frame.txt");
	}


	cout << "Done" << endl;
}


CPoint2Di
CTSpace::computeCPoint()
{
	CPoint2Di c;
	do
		c = computeRandomCPoint();
	while(!validCPoint(c));
	return c;
}

CPoint2Di
CTSpace::computeRandomCPoint()
{
	int x,y;
	CPoint2Di size = map_->getVSizeCell();
	const int sx = size.x();
	const int sy = size.y();
	srand(time(0));

	do
	{
		x = static_cast<int>(sx * (rand() / (RAND_MAX + 1.0f)));
		y = static_cast<int>(sy * (rand() / (RAND_MAX + 1.0f)));
	}
	while(map_->isOccupied(x, y));
	return CPoint2Di(x,y);
}

bool
CTSpace::validCPoint(const CPoint2Di& c)
{
	if(bPoints_.size() >= 2)
	{
		CPoint2Di m;
		vector<CBPoint>::iterator i, j, iEnd = --(bPoints_.end());
		for(i = bPoints_.begin(); i < iEnd; ++i)
		{
			for(j = i + 1; j < bPoints_.end(); ++j)
			{
				m = j->getPoint() - i->getPoint();
				if((c.y() * m.x()) == (c.x() * m.y()))
					return false;
			}
		}
	}
	return true;
}

void
CTSpace::computeRFSegments(vector<CBPoint>& obstacles, CPoint2Di& c)
{
	rfSegmentsIndex_.clear();
	rfSegments_.clear();

	vector<CRFSegment> l_k;
	for(vector<CBPoint>::const_iterator i = obstacles.begin(); i < obstacles.end(); ++i)
	{
		 l_k = computeL_kSegments(*i, c);
//		 cout << "SEGMENTS: " << endl;
//		 	for(vector<CRFSegment>::const_iterator j = l_k.begin(); j < l_k.end(); ++j)
//		 		cout << *j << " || ";
//		 	cout << endl;
		 rfSegments_.insert(rfSegments_.end(), l_k.begin(), l_k.end());
	}


	int idx = -1;
	for(vector<CRFSegment>::const_iterator i = rfSegments_.begin(); i < rfSegments_.end(); ++i)
		 rfSegmentsIndex_.push_back(CRFSegmentIndex(++idx, anglei(i->getP1(), i->getP2())));
	sort(rfSegmentsIndex_.begin(), rfSegmentsIndex_.end());
}

vector<CRFSegment>
CTSpace::computeL_kSegments(const CBPoint& obstacle, const CPoint2Di& c)
{
	vector<CRFSegment> segments;
	segments.reserve(2);
	double m;
	CPoint2Di p1, p2;

	// Map limits that ensure to cover all the cells of the map
	// Warning: if yMin = 0, the cell (0,0) is considered to start in the middle, not in the corner
	const CPoint2Di size = map_->getVSizeCell();
	const int yMin = -1;
	const int xMax = size.x();
	const int yMax = size.y();
	int y;

	// line equation: (y-c.y)=m(x-c.x)
	CPoint2Di p = obstacle.getPoint();
	CPoint2Di delta(c);
	delta -= p;
	if(delta.x() != 0)
	{
		m = static_cast<double>(delta.y()) / delta.x();
		p1.setVector(0, static_cast<int>(round(-m * c.x() + c.y())));
		if(p1.y() < yMin)
		{
			y = 0;
			p1.setVector(static_cast<int>(round(((y-c.y()) / m) + c.x())), y);
		}
		else if(p1.y() > yMax)
		{
			y = yMax-1;
			p1.setVector(static_cast<int>(round(((y-c.y()) / m) + c.x())), y);
		}

		p2.setVector(xMax - 1, static_cast<int>(round(m *(xMax - c.x()) + c.y())));
		if(p2.y() < yMin)
		{
			y = 0;
			p2.setVector(static_cast<int>(round(((y-c.y()) / m) + c.x())), y);
		}
		else if(p2.y() > yMax)
		{
			y = yMax;
			p2.setVector(static_cast<int>(round(((y-c.y()) / m) + c.x())), y);
		}

		if((p.y() < c.y() && c.y() < p1.y()) || (p.y() > c.y() && c.y() > p1.y()))
		{
			segments.push_back(CRFSegment(c, p1, CEdge('A', obstacle.getId())));
			segments.push_back(CRFSegment(c, p2, CEdge('B', obstacle.getId())));
		}
		else
		{
			segments.push_back(CRFSegment(c, p1, CEdge('B', obstacle.getId())));
			segments.push_back(CRFSegment(c, p2, CEdge('A', obstacle.getId())));
		}
	}
	else // vertical line
	{
		p1.setVector(c.x(), yMin);
		p2.setVector(c.x(), yMax);

		if((p.x() < c.x() && c.x() < p1.x()) || (p1.x() > c.x() && c.x() > p.x()))
		{
			segments.push_back(CRFSegment(c, p1, CEdge('A', obstacle.getId())));
			segments.push_back(CRFSegment(c, p2, CEdge('B', obstacle.getId())));
		}
		else
		{
			segments.push_back(CRFSegment(c, p1, CEdge('B', obstacle.getId())));
			segments.push_back(CRFSegment(c, p2, CEdge('A', obstacle.getId())));
		}
	}
	return segments;
}

void
CTSpace::computeRFSubsegments(const vector<CRFSegment>& s)
{
	// needed to compute the graph
	rawRFSubsegments_.clear();
	for(vector<CRFSegment>::const_iterator i = s.begin(); i < s.end(); ++i)
		rawRFSubsegments_.push_back(computeL_kSubsegments(i - s.begin(), *i));

	// merge subsegments (to check intersections)
	unsigned int k;
	vector<vector<CRFSegment> >::iterator i, j;
	for(i = rawRFSubsegments_.begin(); i < rawRFSubsegments_.end(); i+=2)
	{
		k = rfSubsegments_.size();
		rfSubsegments_.insert(rfSubsegments_.end(), i->begin(), i->end());
		j = i+1;
		rfSubsegments_.at(k).setP1(j->begin()->getP2());
		rfSubsegments_.insert(rfSubsegments_.end(), j->begin()+1, j->end());
	}

	// to speedup lower bound calculation
	for(vector<CRFSegment>::iterator i = rfSubsegments_.begin(); i < rfSubsegments_.end(); ++i)
		segmentLUT_.insert(pair<CEdge, CSegment2D<int> >(i->getEdge(),CRFSegment(i->getP1(), i->getP2())));
}

vector<CRFSegment>
CTSpace::computeL_kSubsegments(const unsigned int& index, const CRFSegment& s)
{
	cout << "Segment: " << s << endl; // DEIXA_HO!!!!!!!! sino no funciona (EN RELEASE): GRAN INCOGNITA!!!!
	vector<CRFSegment> subsegments = map_->computeSubsegments(s.getP1(), s.getP2());
//	debug::printVector("SUBSEG:", "", subsegments);
//	cout << endl;

	if(!subsegments.empty())
	{
		CEdge edge = s.getEdge();
		if(edge.getType() == 'A')
		{
			int idx = 1;
			for(vector<CRFSegment>::iterator i = subsegments.begin(); i < subsegments.end(); ++i)
				i->setEdge(CEdge(edge.getType(), edge.getSubindex(), --idx));
		}
		else if(edge.getType() == 'B')
		{
			// Assumed that bPoints order is not changed!!!
			int label = bPoints_.at(index / 2).getLabel();
			//cout << "IDX, LABEL: " << index << ", " << label << endl;
			char type = 'A';
			bool changed = false;
			int idx = -1;

			for(vector<CRFSegment>::iterator i = subsegments.begin(); i < subsegments.end(); ++i)
			{
				i->setEdge(CEdge(type, edge.getSubindex(), ++idx));
				if(!changed && label == i->getObstacle())// comprovar si funciona!!
				{
					type = 'B';
					changed = true;
				}
			}
		}
	}
	return subsegments;
}

void
CTSpace::computeRFBoundarySubsegments(const vector<CRFSegment>& s)
{
	rfBoundarySubsegments_.clear();

	for(vector<CRFSegment>::const_iterator i = s.begin(); i < s.end(); ++i)
	{
		CRFSegment stmp = map_->computeBoundarySubsegment(*i);
		cout << "S: " << *i << " -> SB: " << stmp << endl;
		rfBoundarySubsegments_.push_back(stmp);
	}

	// to speedup lower bound calculation
	for(vector<CRFSegment>::iterator i = rfBoundarySubsegments_.begin(); i < rfBoundarySubsegments_.end(); ++i)
		boundarySegmentLUT_.insert(pair<CEdge, CSegment2D<int> >(i->getEdge(),CRFSegment(i->getP1(), i->getP2())));
}

void CTSpace::initWedge(vector<TGraph::vertex_descriptor>& wedge, unsigned int idxWedge, unsigned int& idxSubwedge)
{
	idxSubwedge = 0;

	TGraph::vertex_descriptor vd = add_vertex(g2_);
	g2_[vd] = CVertex(idxWedge, idxSubwedge);
	wedge.push_back(vd);
}

void CTSpace::updateWedge(vector<TGraph::vertex_descriptor>& wedge, unsigned int idxWedge, unsigned int& idxSubwedge)
{
	idxSubwedge++;
	if(idxSubwedge >= wedge.size())
	{
		TGraph::vertex_descriptor vd = add_vertex(g2_);
		g2_[vd] = CVertex(idxWedge, idxSubwedge);
		wedge.push_back(vd);
	}
}

void CTSpace::buildGraph1Obstacle()
{
	g2_.clear();
	rawWedges_.clear();

	// Segments -------------------------------------
	vector<CRFSegment> currSegment, nextSegment;
	// Wedges ---------------------------------------
	unsigned int idxCurrWedge = 0, idxNextWedge = 1;
	unsigned int idx;

	// rawWedges initialization
	rawWedges_.reserve(rawRFSubsegments_.size());
	rawWedges_.resize(rawRFSubsegments_.size());

	unsigned int idxSegment = 1;

	currSegment = rawRFSubsegments_.at(rfSegmentsIndex_.at(idxSegment).getIndex() % rfSegmentsIndex_.size());
	nextSegment = rawRFSubsegments_.at((rfSegmentsIndex_.at(idxSegment).getIndex() + 1) % rfSegmentsIndex_.size());

	//cout << "SIZE: curr & next "  << currSegment.size() << " " << nextSegment.size() << endl;
	unsigned int subidxCurrWedge = 0, subidxNextWedge = 0;

	initWedge(rawWedges_.at(idxCurrWedge), idxCurrWedge, subidxCurrWedge);
	initWedge(rawWedges_.at(idxNextWedge), idxNextWedge, subidxNextWedge);

	idx = 0;
	if(currSegment.size() > 1)
		idx = 1;
	add_edge(rawWedges_.at(idxCurrWedge).at(subidxCurrWedge),
			rawWedges_.at(idxNextWedge).at(subidxNextWedge),
			currSegment.at(idx).getEdge(), g2_);
	//(*index_)[currSegment.at(idx).getEdge()]; // Add edge to index

	idx = 0;
	if(nextSegment.size() > 1)
		idx = 1;
	add_edge(rawWedges_.at(idxCurrWedge).at(subidxCurrWedge),
			rawWedges_.at(idxNextWedge).at(subidxNextWedge),
			nextSegment.at(idx).getEdge(), g2_);
	//(*index_)[nextSegment.at(idx).getEdge()]; // Add edge to index
}

void CTSpace::buildGraph()
{
	g2_.clear();
//	//for(vector<vector<int> >::iterator i= rawWedges_.begin(); i < rawWedges_.end(); ++i)
//	for(vector<vector<TGraph::vertex_descriptor> >::iterator i= rawWedges_.begin(); i < rawWedges_.end(); ++i)
//		i->clear();
	rawWedges_.clear();
//	vertices_.clear();


//	index_.reset(new TStrMap); // initilialize index

	// Segments -------------------------------------
	vector<CRFSegment> prevSegment, currSegment, nextSegment;
	vector<CRFSegment>::iterator itPrevSegment, itCurrSegment, itNextSegment;
	// Wedges ---------------------------------------
	unsigned int idxCurrWedge = 0, idxNextWedge = 1;
	unsigned int subidxCurrWedge, subidxNextWedge;
	//int vertexId = 0;

	// rawWedges initialization
	rawWedges_.reserve(rawRFSubsegments_.size());
	rawWedges_.resize(rawRFSubsegments_.size());

	unsigned int idxSegment = 1;
	unsigned int numRawSubsegmentsProcessed = 0;
	while(numRawSubsegmentsProcessed < rawRFSubsegments_.size())
	{
		prevSegment = rawRFSubsegments_.at(rfSegmentsIndex_.at((idxSegment - 1) % rfSegmentsIndex_.size()).getIndex());
		currSegment = rawRFSubsegments_.at(rfSegmentsIndex_.at(idxSegment % rfSegmentsIndex_.size()).getIndex());
		nextSegment = rawRFSubsegments_.at(rfSegmentsIndex_.at((idxSegment + 1) % rfSegmentsIndex_.size()).getIndex());

		subidxCurrWedge = 0; subidxNextWedge = 0;
		for(itCurrSegment = currSegment.begin(); itCurrSegment < currSegment.end(); itCurrSegment++)
		{
			if(rawWedges_.at(idxCurrWedge).empty())
				initWedge(rawWedges_.at(idxCurrWedge), idxCurrWedge, subidxCurrWedge);

			if(rawWedges_.at(idxNextWedge).empty())
				initWedge(rawWedges_.at(idxNextWedge), idxNextWedge, subidxNextWedge);

			add_edge(rawWedges_.at(idxCurrWedge).at(subidxCurrWedge),
					rawWedges_.at(idxNextWedge).at(subidxNextWedge),
					itCurrSegment->getEdge(), g2_);

			//(*index_)[itCurrSegment->getEdge()]; // Add edge to index

			if(itCurrSegment->getObstacle() != rfsegment::bound)
			{
				// Check with the previous segment
				itPrevSegment = find_if(prevSegment.begin(), prevSegment.end(), CRFSegmentEqualObstacle(*itCurrSegment));
				if(itPrevSegment != prevSegment.end())
					updateWedge(rawWedges_.at(idxCurrWedge), idxCurrWedge, subidxCurrWedge);

				// Check with the next segment
				itNextSegment = find_if(nextSegment.begin(), nextSegment.end(), CRFSegmentEqualObstacle(*itCurrSegment));
				if(itNextSegment != nextSegment.end())
					updateWedge(rawWedges_.at(idxNextWedge), idxNextWedge, subidxNextWedge);
			}
		}
		idxCurrWedge++;
		idxNextWedge = (idxNextWedge + 1) % rawWedges_.size();
		idxSegment++;
		numRawSubsegmentsProcessed++;
	}
}

void
CTSpace::printGraph(TGraph graph)
{
	boost::graph_traits<TGraph>::vertex_iterator vi, vi_end, next;
	boost::graph_traits<TGraph>::out_edge_iterator ei, ei_end;

	tie(vi, vi_end) = boost::vertices(graph);
	for (next = vi; vi != vi_end; vi = next)
	{
		std::cout  << graph[*vi] << " -> ";
		for (tie(ei, ei_end) = out_edges(*vi, graph); ei != ei_end; ++ei)
		{
			std::cout << graph[target(*ei, graph)] << "[" << graph[*ei] << "] ";// next vertex with edge connection
		}
		std::cout << std::endl;
		++next;
	}
}

void
CTSpace::printWedge(const vector<TVertex>& v)
{
	vector<TVertex>::const_iterator i;
	for(i = v.begin(); i < v.end(); i++)
		cout << g2_[*i] << " | ";
}


// NO ESTA ACABADA!!!!!!!!!!!!!
int
CTSpace::point2Vertex(const CPoint2Di& point)
{
//	cout << "Point2Vertex ----------------------" << endl;
	CPoint2Di p(point);
	int vertex;

	// Wedge selection ------------------------------
	int angle = anglei(c_, p);
	unsigned int i = 0;
	while(i < rfSegmentsIndex_.size() &&
		!(angle >= rfSegmentsIndex_.at(i).getValue() &&  angle <= rfSegmentsIndex_.at((i+1)%rfSegmentsIndex_.size()).getValue()))
		i++;

	if(i == rfSegmentsIndex_.size())	i = rfSegmentsIndex_.size() - 1;

//	cout << "Wedge: " << i << endl;

	// Subwedge selection ------------------------------
	if(rawWedges_.at(i).size() == 1)
		vertex = rawWedges_.at(i).at(0);
	else
	{
		p = point;
		unsigned long distPoint = p.distance2(c_); // distance2 from c to point
		unsigned int subwedge = 0;
		unsigned int idx = rfSegmentsIndex_.at(i).getIndex();
		CPoint2Di p2 = rawRFSubsegments_.at(idx).at(subwedge).getP2();
		unsigned long distObstacle = p2.distance2(c_);
		while(distPoint > distObstacle)
		{
			subwedge++;
			p2 = rawRFSubsegments_.at(idx).at(subwedge).getP2();
			distObstacle = p2.distance2(c_);
		}
		if(subwedge == rawWedges_.at(i).size()) // out of bounds
			subwedge--;	// last subwedge
//		cout << "subwedge idx: " << subwedge << endl;
//		cout << "subwedge size: " << rawWedges_.at(i).size() << endl;
		vertex = rawWedges_.at(i).at(subwedge);
	}

//	cout << "Point2Vertex -----------------FI---" << endl;
	return vertex;

}

// save reference frame info into a file to be used with MATLAB
void CTSpace::save(const std::string& filename)
{
	ofstream f;
	f.open(filename.c_str(), ios::out);
	f << "start: " << mStart_ << endl;
	f << "goal: " << mTarget_ << endl;
	f << "size: " << map_->getVSizeCell() << endl;
	f << "c: " << c_ << endl;
	f << "num_b_points: " << bPoints_.size() << endl;
	for(unsigned int i = 0; i < bPoints_.size(); i++)
		f << "b_{" << i+1 << "} " << bPoints_.at(i) << endl;
	f << "num_subsegments: " << rfSubsegments_.size() << endl;
	for(vector<CRFSegment>::iterator i = rfSubsegments_.begin(); i < rfSubsegments_.end(); i++)
	{
		toLatex(f, *i);
		f<< endl;
	}
	f.close();
}

void CTSpace::computeHomotopyClasses(const unsigned int& maxLength)
{
	cout << "Compute Homotopy Classes" << endl;
	if(!bPoints_.empty())
	{
		homotopyClasses_ = BFSJenkins(g2_, gStart_, gTarget_, maxLength);
		cout << "-----------------------------------------------" << endl;

		cout << "Homotopy Classes found (" << homotopyClasses_.size() << ")" << endl;
		if(plot_)
		{
			debug::printVector("In internal format....", "", homotopyClasses_);

			cout << "In Latex format...." << endl;
			unsigned int counter = 0;
			for(vector<CTString>::iterator i = homotopyClasses_.begin(); i < homotopyClasses_.end(); i++)
			{
				cout << ++counter << " & ";
				toLatex(cout,*i);
				cout << " \\\\" << endl;
			}
		}
		cout << "-----------------------------------------------" << endl;
	}
	else
		cout << "Skipping" << endl;
	cout << "Done" << endl;
}

vector<CTString>
CTSpace::BFSJenkins(	TGraph& graph,
						const TVertex& start,
						const TVertex& goal,
						const unsigned int& length)
{
	fullHC_.clear();

	TQueue Q;
	graph_traits<TGraph>::out_edge_iterator ei, ei_end;

	vector<CTPath> candidate;
	vector<CTString> stringHC, stringHCCandidate, stringHCCandidateAlphaSorted;
	CTString newStringAlphaSorted; //, newStringReversed;

	CTPath prevPath(start), currPath;
	TVertex u, v;

	// First step ----------------------------------------------
	Q.push(start);
	u = Q.top(); Q.pop();
#ifdef _NDEBUG
	cout << "Vertex: " << u << endl;
#endif
	for (tie(ei, ei_end) = out_edges(u, graph); ei != ei_end; ++ei)
	{
		v = target(*ei, graph);
		currPath = prevPath;
		currPath.add(graph[*ei], v);
		candidate.push_back(currPath);
		Q.push(v);

		if(v == goal) // string terminate in the target vertex
			stringHC.push_back(currPath.getEdgeSequence());
#ifdef _NDEBUG
		cout << candidate.back() << endl;
		//cout << toLatex(cout,candidate.back().getEdgeSequence()) << endl;
#endif
	}

	unsigned int k = 0;
	while(candidate.size() > k && candidate.at(k).getEdgeSequence().size() < length)
	{
		// Iterative steps ----------------------------------------------
		u = Q.top(); Q.pop();
		prevPath = candidate.at(k);
#ifdef _NDEBUG
		cout << "Vertex: " << u << endl;
#endif
		for (tie(ei, ei_end) = out_edges(u, graph); ei != ei_end; ++ei)
		{
			v = target(*ei, graph); // next vertex
			if(graph[*ei] != prevPath.getEdgeSequence().back())
			{
				currPath = prevPath;
				currPath.add(graph[*ei], v);
#ifdef _NDEBUG
				cout << currPath << endl;
				//cout << toLatex(cout,currPath.getEdgeSequence()) << endl;
#endif

				//if(!currPath.checkCriteria())
				if(!currPath.checkCriteriaFull())
				{
//					newStringReversed = currPath.getEdgeSequence();
//					newStringReversed.reverse();

					newStringAlphaSorted = currPath.getEdgeSequence();
					newStringAlphaSorted.sortAlpha();

//					if(!duplicated(newStringReversed, stringHCCandidate,
//							newStringAlphaSorted, stringHCCandidateAlphaSorted))
					if(!duplicated(newStringAlphaSorted, stringHCCandidateAlphaSorted))
					{
						candidate.push_back(currPath);

						stringHCCandidate.push_back(currPath.getEdgeSequence());
						stringHCCandidateAlphaSorted.push_back(newStringAlphaSorted);
						Q.push(v);

						if(v == goal) // string terminate in the target vertex
						{
							stringHC.push_back(currPath.getEdgeSequence());
							fullHC_.push_back(currPath);
						}
					}
				}
			}
		}
		k++;
#ifdef _NDEBUG
		cout << endl;
		cout << "Candidate(" << candidate.size() << "):  " << k << endl;
#endif
	}
	return stringHC;
}

bool
CTSpace::duplicated(	const CTString& pSorted,
						const vector<CTString>& hcSorted)
{
	// aquest segur que fa falta
	if(find(hcSorted.begin(), hcSorted.end(), pSorted) != hcSorted.end())
	{
#ifdef _NDEBUG
		cout << "BFS -- duplicated 2 " << endl;
#endif
		return true;
	}
	return false;
}

void
CTSpace::computeWedgeCentroids(TGraph graph)
{
	cout << "Compute Wedge Centroids ------------------------" << endl;

//	boost::graph_traits<TGraph>::vertex_iterator vi, vi_end, next;
//	boost::graph_traits<TGraph>::out_edge_iterator ei, ei_end;
//
//	tie(vi, vi_end) = boost::vertices(graph);
//	for (next = vi; vi != vi_end; vi = next)
//	{
//		std::cout  << graph[*vi] << " -> ";
//		for (tie(ei, ei_end) = out_edges(*vi, graph); ei != ei_end; ++ei)
//		{
//			std::cout << graph[target(*ei, graph)] << "[" << graph[*ei] << "] ";// next vertex with edge connection
//		}
//		std::cout << std::endl;
//		++next;
//	}

	std::vector<std::vector<CPoint2D<float> > > centroid_;


	//std::vector<std::vector<TVertex> > rawWedges_

	centroids_.resize(num_vertices(g2_));
	for(vector<vector<TVertex> >::iterator it = rawWedges_.begin(); it < rawWedges_.end(); ++it)
	{
		for(vector<TVertex>::iterator i = it->begin(); i < it->end(); ++i)
		{
			unsigned int e1id = 0, e2id = 0;
			bool e1set = false, e2set = false;
			CEdge e1min, e1max, e2min, e2max;

			boost::graph_traits<TGraph>::out_edge_iterator ei, ei_end;
			std::cout  << *i << " " << graph[*i] << " -> ";
			for (tie(ei, ei_end) = out_edges(*i, graph); ei != ei_end; ++ei)
			{
				std::cout << graph[target(*ei, graph)] << "[" << graph[*ei] << "] ";// next vertex with edge connection

				if(!e1set)
				{
//					cout << "e1set(" << graph[*ei].getSubindex() << ") ";
					e1id = graph[*ei].getSubindex();
					e1min = graph[*ei];
					e1max = graph[*ei];
					e1set = true;
				}
				else if(e1set && !e2set && e1id != graph[*ei].getSubindex())
				{
//					cout << "s2set(" << graph[*ei].getSubindex() << ") ";
					e2id = graph[*ei].getSubindex();
					e2min = graph[*ei];
					e2max = graph[*ei];
					e2set = true;
				}


				if(graph[*ei].getSubindex() == e1id)
				{
					if(abs(graph[*ei].getSubsegment()) < abs(e1min.getSubsegment())) e1min = graph[*ei];
					if(abs(graph[*ei].getSubsegment()) > abs(e1min.getSubsegment())) e1max = graph[*ei];
				}
				if(graph[*ei].getSubindex() == e2id)
				{
					if(abs(graph[*ei].getSubsegment()) < abs(e2min.getSubsegment())) e2min = graph[*ei];
					if(abs(graph[*ei].getSubsegment()) > abs(e2min.getSubsegment())) e2max = graph[*ei];
				}
				// look for graph[*ei]


			}
			//std::cout << std::endl;
			//cout << "ID (1,2): " << s1id << ", " << s2id << endl;
			//cout << "E1(" << e1min << " | " << e1max << ") " << "E2 (" << e2min << " | " << e2max << ")" << endl;
//			cout << endl << "EDGES1: " << e1min << ", " << e1max << ", " << e2min << ", " << e2max << endl;
			CPoint2D<float> p = computeCentroid(e1min, e1max, e2min, e2max, it - rawWedges_.begin());
			centroids_.at(*i) = p;
//			cout << " --------------------------- " << endl;
//			cout << "CENTROID (" <<  *i << "): " << p << endl;

		}
		cout << endl;
	}

//

	cout << "Compute Wedge Centroids --------------------FI--" << endl;

}

CPoint2D<float>
CTSpace::computeCentroid(	const CEdge& e1min,
							const CEdge& e1max,
							const CEdge& e2min,
							const CEdge& e2max,
							const unsigned int& idx)
{
//	cout << endl << "EDGES2: " << e1min << ", " << e1max << ", " << e2min << ", " << e2max << endl;
	vector<CRFSegment>::iterator i_e1min, i_e1max, i_e2min, i_e2max;
	const unsigned int idx1 = rfSegmentsIndex_.at(idx).getIndex();
	const unsigned int idx2 = rfSegmentsIndex_.at((idx + 1) % rfSegmentsIndex_.size()).getIndex();
	cout << "IDX(" << idx << "): " << idx1 << ", " << idx2 << endl;
	if(rawRFSubsegments_.at(idx1).front().getEdge().getSubindex() == e1min.getSubindex())
	{
//		debug::printVector("1- subseg E1: ", "", rawRFSubsegments_.at(idx1), false); cout << endl;
//		debug::printVector("1- subseg E2: ", "", rawRFSubsegments_.at(idx2), false); cout << endl;
		i_e1min = find_if(rawRFSubsegments_.at(idx1).begin(), rawRFSubsegments_.at(idx1).end(), CRFSegmentEqualEdge(e1min));
		i_e1max = find_if(rawRFSubsegments_.at(idx1).begin(), rawRFSubsegments_.at(idx1).end(), CRFSegmentEqualEdge(e1max));
		i_e2min = find_if(rawRFSubsegments_.at(idx2).begin(), rawRFSubsegments_.at(idx2).end(), CRFSegmentEqualEdge(e2min));
		i_e2max = find_if(rawRFSubsegments_.at(idx2).begin(), rawRFSubsegments_.at(idx2).end(), CRFSegmentEqualEdge(e2max));

//#ifdef _NDEBUG
//		if(	i_e1min == rawRFSubsegments_.at(idx1).end() ) runtime_error("TSpace::FindWedgeSegments::i_e1min not found");
//		if(	i_e1max == rawRFSubsegments_.at(idx1).end() ) runtime_error("TSpace::FindWedgeSegments::i_e1max not found");
//		if(	i_e2min == rawRFSubsegments_.at(idx2).end() ) runtime_error("TSpace::FindWedgeSegments::i_e2min not found");
//		if(	i_e2min == rawRFSubsegments_.at(idx2).end() ) runtime_error("TSpace::FindWedgeSegments::i_e2min not found");
//#endif
	}
	else
	{
//		debug::printVector("2- subseg E1: ", "", rawRFSubsegments_.at(idx1), false); cout << endl;
//		debug::printVector("2- subseg E2: ", "", rawRFSubsegments_.at(idx2), false); cout << endl;
		i_e1min = find_if(rawRFSubsegments_.at(idx2).begin(), rawRFSubsegments_.at(idx2).end(), CRFSegmentEqualEdge(e1min));
		i_e1max = find_if(rawRFSubsegments_.at(idx2).begin(), rawRFSubsegments_.at(idx2).end(), CRFSegmentEqualEdge(e1max));
		i_e2min = find_if(rawRFSubsegments_.at(idx1).begin(), rawRFSubsegments_.at(idx1).end(), CRFSegmentEqualEdge(e2min));
		i_e2max = find_if(rawRFSubsegments_.at(idx1).begin(), rawRFSubsegments_.at(idx1).end(), CRFSegmentEqualEdge(e2max));
//#ifdef _NDEBUG
//		if(	i_e1min == rawRFSubsegments_.at(idx2).end() ) runtime_error("TSpace::FindWedgeSegments::i_e1min not found");
//		if(	i_e1max == rawRFSubsegments_.at(idx2).end() ) runtime_error("TSpace::FindWedgeSegments::i_e1max not found");
//		if(	i_e2min == rawRFSubsegments_.at(idx1).end() ) runtime_error("TSpace::FindWedgeSegments::i_e2min not found");
//		if(	i_e2min == rawRFSubsegments_.at(idx1).end() ) runtime_error("TSpace::FindWedgeSegments::i_e2min not found");
//#endif
	}

//	cout << "rfseg : " << *i_e1min << ", " << *i_e1max << ", " << *i_e2min << ", " << *i_e2max << endl;

	CPoint2Di p = i_e1min->getP1();
	p += i_e1max->getP2();
	p += i_e2min->getP1();
	p += i_e2max->getP2();

	return CPoint2D<float>(static_cast<float>(p.x()) / 4,  static_cast<float>(p.y()) / 4);
}
