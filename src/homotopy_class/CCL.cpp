/*******************************************************************************
 *  CCL.cpp
 *
 *					 Alberto Ortiz - UIB (2008)
 *
 *******************************************************************************/

#include "CCL.h"

using namespace std;

int CCL::searchDirection_[8][2] = {{0,1},{1,1},{1,0},{1,-1},{0,-1},{-1,-1},{-1,0},{-1,1}};

CCL::CCL(	const cv::Mat& map,
			const CPoint2D<int>& size,
			const unsigned int& minArea,
			const int& firstObstacleId,
			const bool& plot)
:
	vsize_(size),
	size_(vsize_.x() * vsize_.y()),
	map_(map),
	minArea_(minArea),
	plot_(plot),
	firstObstacleId_(firstObstacleId)
{
	// get boundary rows/cols of the map
	r1_ = map_.row(0);
	c1_ = map_.col(0);
	rn_ = map_.row(map_.rows - 1);
	cn_ = map_.col(map_.cols - 1);

	currLabel_.create(vsize_.y(), vsize_.x(), CV_32S);
	currLabel_ = cv::Scalar(0);

//	prevLabel_.create(vsize_.y(), vsize_.x(), CV_32S);
//	prevLabel_ = cv::Scalar(0);

	connectedComponentsCount_ =	labelIndex_ = 0;

	if(plot_)
	{
		labelPlot_.create(vsize_.y(), vsize_.x(), CV_8UC3);
		colors_.push_back(cv::Vec3b(255, 0, 0)); // BGR color format
		colors_.push_back(cv::Vec3b(0, 255, 0));
		//colors_.push_back(cv::Vec3b(0, 0, 255));
		colors_.push_back(cv::Vec3b(0, 255, 255));
		colors_.push_back(cv::Vec3b(255, 255, 255));
		colors_.push_back(cv::Vec3b(0, 0, 0));
	}
	purple_ = cv::Vec3b(255,0,255);

	idCounter_ = firstObstacleId_;

//	obstacleCoords_.push_back(ccl::SObstacleCoord());
}

void CCL::update(const bool& deleteCoordinates)
{
	obstacleCoords_.push_back(ccl::SObstacleCoord());
	currLabel_ =  cv::Scalar(0);
	currObstacles_.clear();
	validIdx_.clear();
	bPoints_.clear();

	labelIndex_ = 0;
	connectedComponentsCount_ =	labelIndex_ = 0;

	// set boundary to free
	r1_ = cv::Scalar(255);
	c1_ = cv::Scalar(255);
	rn_ = cv::Scalar(255);
	cn_ = cv::Scalar(255);

	numObstacles_ = CHANlabel(false);
	//cout << "Num obstacles: " << numObstacles_ << endl;
	postProcessing();

	if(plot_)
	{
		int r, c;
		for(r = 0; r < labelPlot_.rows; r++)
		{
			for(c = 0; c < labelPlot_.cols; c++)
			{
				if(currLabel_.at<int>(r, c) < 0) // contour
					labelPlot_.at<cv::Vec3b>(r, c) = colors_.back();
				else if(currLabel_.at<int>(r, c) == 0) // free zone
					labelPlot_.at<cv::Vec3b>(r, c) = colors_.at(colors_.size() - 2);
				else // obstacles
				{
					labelPlot_.at<cv::Vec3b>(r, c) = cv::Vec3b(237, 149, 100);
//					labelPlot_.at<cv::Vec3b>(r, c) = colors_.at(currLabel_.at<int>(r, c) % (colors_.size() - 2));
				}
			}
		}

		for(vector<ccl::SObstacleInfo>::iterator i = currObstacles_.begin() + firstObstacleId_; i < currObstacles_.end(); ++i)
		{
			ostringstream stream;
			cv::circle(labelPlot_, i->massCentre_, 5, cv::Scalar(0,0,255));
			stream << i->id_;
			cv::putText(labelPlot_, stream.str(), i->massCentre_, cv::FONT_HERSHEY_SCRIPT_SIMPLEX, 1.0, cv::Scalar(0,0,0));
		}

//		cout << "CCL: bPoints: " << endl;
//		for(vector<CBPoint>::iterator i = bPoints_.begin(); i < bPoints_.end(); ++i)
//		{
//			cout << *i << endl;
//		}

	//	getchar();

//		vector<ccl::SObstacleCoord>::iterator i;
//		for(i = obstacleCoords_.begin() + 1; i < obstacleCoords_.end(); ++i)
//		{
//			if(i->obstacle_.size() < minArea_)
//			{
////				deleteObstacle(i);
////				cout << "Obstacle " << i - obstacleCoords_.begin() << " deleted" << endl;
//			}
//			else
//			{
//				cv::circle(labelPlot_, i->massCentre_, 5, cv::Scalar(0,0,255));
//			}
//		}


//		unsigned int j;
//		for(vector<ccl::SObstacleCoord>::iterator i = obstacleCoords_.begin(); i < obstacleCoords_.end(); i++)
//		{
//			for(j = 0; j < i->obstacle_.size(); j++)
//			{
//				labelPlot_.at<cv::Vec3b>(i->obstacle_.at(j)) = purple_;
//			}
//
////			for(j = 0; j < i->externalContour_.size(); j++)
////			{
////				labelPlot_.at<cv::Vec3b>(i->externalContour_.at(j)) = purple_;
////			}
//		}
	}

	unsigned int count = 0;
	for(vector<ccl::SObstacleCoord>::iterator i = obstacleCoords_.begin(); i < obstacleCoords_.end(); i++)
	{
//		cout 	<< "Label " << count << " Num pixels (obstacle, countour): "
//				<< i->obstacle_.size() << ", " << i->internalContour_.size() << ", " << i->externalContour_.size()
//				<< " Total: " <<  i->obstacle_.size() + i->internalContour_.size() + i->externalContour_.size() << endl;


//		cout << "Obstacle: ";
//		for(unsigned int j = 0; j < i->obstacle_.size(); j++)
//		{
//			cout << "[" << i->obstacle_.at(j).x << "," << i->obstacle_.at(j).y << "]";
//		}
//		cout << endl;
//
//		cout << "InternalContour: ";
//		for(unsigned int j = 0; j < i->internalContour_.size(); j++)
//		{
//			cout << "[" << i->internalContour_.at(j).x << "," << i->internalContour_.at(j).y << "]";
//		}
//		cout << endl;
//
//		cout << "ExternalContour: ";
//		for(unsigned int j = 0; j < i->externalContour_.size(); j++)
//		{
//			cout << "[" << i->externalContour_.at(j).x << "," << i->externalContour_.at(j).y << "]";
//		}
//		cout << endl;

		if(deleteCoordinates)
		{
			i->obstacle_.clear();
			i->internalContour_.clear();
			i->externalContour_.clear();
		}
		count++;
	}
	if(deleteCoordinates)
		obstacleCoords_.clear();



	//currLabel_.copyTo(prevLabel_);
	prevObstacles_ = currObstacles_;
}

int CCL::CHANlabel(bool contour)
{
	int sizeX = vsize_.x();
	int sizeY = vsize_.y();

	int height_1 = sizeY - 1;
	int width_1 = sizeX - 1;

	int tracingDirection;

	ccl::SObstacleCoord dummy;
	dummy.sumX_ = 0;
	dummy.sumY_ = 0;


//	int connectedComponentsCount = 0;
//	int labelIndex = 0;

//	obstacleCoords_.push_back(std::vector<>vecPoint2iPtr());
//	obstacleCoords_.push_back(vector<Point2i>());


	for(int cy = 1; cy < height_1; cy++)
	{
		labelIndex_ = 0;
		for(int cx = 1; cx < width_1; cx++)
		{
			if(cellOccupied(cy, cx))
			{
				if (labelIndex_ != 0)
				{ // use pre-pixel label
					if(currLabel_.at<int>(cy, cx) != labelIndex_) // emili
					{
						currLabel_.at<int>(cy, cx) = labelIndex_;

						obstacleCoords_.at(labelIndex_).obstacle_.push_back(Point2i(cx, cy));
						obstacleCoords_.at(labelIndex_).sumX_ += cx;
						obstacleCoords_.at(labelIndex_).sumY_ += cy;
					}

				}
				else
				{
					labelIndex_ = currLabel_.at<int>(cy, cx);
					if (labelIndex_ == 0) // New obstacle found?
					{
						//bPoints_.push_back(CVector2(cx, cy));

//						if(prevLabel_.at<int>(cy, cx) > 0)
//							labelIndex_ = prevLabel_.at<int>(cy, cx);
//						else
//						{
							labelIndex_ = ++connectedComponentsCount_;
//							cout << "OBSTACLE found ID " << labelIndex_ << endl;
							//cout << "afageixo dummy:   <<"
							obstacleCoords_.push_back(dummy);
//						}

						tracingDirection = 0;
						CHANcontourTracing(cy, cx, labelIndex_, tracingDirection, contour, false); // external contour
						//currLabel_.at<int>(cy, cx) = labelIndex_;

						//cout << "EOOO " << endl;
						//obstacleCoords_.at(labelIndex_).obstacle_.push_back(Point2i(cx, cy));
						//cout << labelIndex_;
					}
				}
			}
			else if (labelIndex_ != 0)
			{ // white (background) pixel & pre-pixel has been labeled
				if(currLabel_.at<int>(cy, cx) == 0)
				{
					tracingDirection = 1;
					CHANcontourTracing(cy, cx - 1, labelIndex_, tracingDirection, contour, true); // internal contour
				}
				labelIndex_ = 0;
			}
		}
	}

	return connectedComponentsCount_;
}

void CCL::CHANcontourTracing(int cy, int cx, int labelIndex, int tracingDirection, bool contour, const bool& internalContour)
{
	char tracingStopFlag = 0, searchAgain = 1;
	int fx, fy, sx = cx, sy = cy;
	//cout << "ContourTracing-----------" << endl;

	CHANtracer(cy, cx, tracingDirection, internalContour);
	//if (contour) m_Contourmap->setValue(cx,cy,labelIndex);

	if (cx != sx || cy != sy)
	{
		fx = cx;
		fy = cy;

		//cout << "contour: ";
		while (searchAgain)
		{
			tracingDirection = (tracingDirection + 6) % 8;
			if(currLabel_.at<int>(cy, cx) != labelIndex) // emili
			{
				currLabel_.at<int>(cy, cx) = labelIndex;
				//obstacleCoords_.at(labelIndex_).push_back(Point2i(cx, cy));
				obstacleCoords_.at(labelIndex_).obstacle_.push_back(Point2i(cx, cy));
				obstacleCoords_.at(labelIndex_).sumX_ += cx;
				obstacleCoords_.at(labelIndex_).sumY_ += cy;
				//cout << labelIndex;
			}
			//CHANtracer(&cy, &cx, &tracingDirection);
			CHANtracer(cy, cx, tracingDirection, internalContour);

			//if (contour) m_Contourmap->setValue(cx,cy,labelIndex);

			if (cx == sx && cy == sy)
			{
				tracingStopFlag = 1;
			}
			else if (tracingStopFlag)
			{
				if(cx == fx && cy == fy)
				{
					searchAgain = 0;
				}
				else
				{
					tracingStopFlag = 0;
				}
			}
			//cout << "EOOOO" << endl;
			//cout << ".";

		}
//		cout << endl;
	}
	//cout << "ContourTracing------FI-----" << endl;
}

void CCL::CHANtracer(int& cy, int& cx, int& tracingDirection, const bool& internalContour)
{
	//cout << "CHANTracer ------------" << endl;
	int y, x;
	for (int i = 0; i < 7; i++)
	{
		y = cy + searchDirection_[tracingDirection][0];
		x = cx + searchDirection_[tracingDirection][1];

		if(!cellOccupied(y, x))
		{

			if(currLabel_.at<int>(y, x) != -1) // emili
			{
				currLabel_.at<int>(y, x) = -1;
				if(internalContour)
					obstacleCoords_.at(labelIndex_).internalContour_.push_back(Point2i(x, y));
				else
					obstacleCoords_.at(labelIndex_).externalContour_.push_back(Point2i(x, y));
				//cout << "-1";
			}

			tracingDirection = (tracingDirection + 1) % 8;
		}
		else
		{
			cy = y;
			cx = x;
			break;
		}
	}
	//cout << "CHANTracer ----------FI--" << endl;
}

void CCL::save(const string filename)
{
	ofstream f;
	f.open(filename.c_str(), ios::out);

	if(!f.is_open())
	{
		cout << "CCL.Error. " << filename.c_str() << " cannot be created." << endl;
		return;
	}

	int r, c;
	for(r = 0; r < currLabel_.rows; r++)
	{
		for(c = 0; c < currLabel_.cols; c++)
		{
			f << setw(4) << currLabel_.at<int>(r, c);
		}
		f << endl;
	}
	f.close();
}

void CCL::postProcessing()
{
	// DEBUG
	for(vector<ccl::SObstacleCoord>::iterator i = obstacleCoords_.begin() + 1; i < obstacleCoords_.end(); ++i)
	{
		cout << "obstacle: " << i - obstacleCoords_.begin() << endl;
		cout << "\tsumX: " << i->sumX_ << endl;
		cout << "\tsumY: " << i->sumY_ << endl;
		cout << "\tobstacle points: " << i->obstacle_.size() << endl;
		cout << "\tinternal contour: " << i->internalContour_.size() << endl;
		cout << "\texternal contour: " << i->externalContour_.size() << endl;
	}


	for(vector<ccl::SObstacleCoord>::iterator i = obstacleCoords_.begin() + 1; i < obstacleCoords_.end(); ++i)
	{
		if(i->obstacle_.size() < minArea_)
		{
			deleteObstacle(i);
			//cout << "Obstacle " << i - obstacleCoords_.begin() << " deleted" << endl;
		}
		else
		{
			// TODO: fix it in the future for 1-pixel osbtacles
			// Quan hi ha osbstacle d'un pixel, conte només el valor del dummy on sumX = sumY = 0 & obstacle size = 0
			// En aquest cas el contorn té 7 píxels per comptes de 8
			cout << i->sumX_ << " " << i->obstacle_.size() << " " << i->sumY_ << " " << i->obstacle_.size() << endl;
			i->massCentre_ = Point2i(i->sumX_ / i->obstacle_.size(), i->sumY_ / i->obstacle_.size());
			validIdx_.push_back(i - obstacleCoords_.begin());
		}
	}

	// compute label correspondence throuhg iterations
	Point2i p;
	vector<unsigned int> candidate;
	for(vector<unsigned int>::iterator i = validIdx_.begin(); i < validIdx_.end(); ++i)
	{
		candidate.clear();
		for(vector<ccl::SObstacleInfo>::iterator j = prevObstacles_.begin(); j < prevObstacles_.end(); ++j)
		{
			p = obstacleCoords_.at(*i).massCentre_ - j->massCentre_;
			if(static_cast<unsigned int>(p.x * p.x + p.y * p.y) < ccl::distThreshold2)
				candidate.push_back(j - prevObstacles_.begin());
		}

		ccl::SObstacleInfo o;
		if(candidate.size() == 0) // new obstacle;
		{
			cout << "NEW OBSTACLE" << endl;
			idCounter_++;
			//cout << "---------------------- New obstacle " << idCounter_ << endl;
			o.id_ = idCounter_;
			o.label_ = currLabel_.at<int>(obstacleCoords_.at(*i).obstacle_.at(0));
			o.massCentre_ = obstacleCoords_.at(*i).massCentre_;
			o.area_ = obstacleCoords_.at(*i).obstacle_.size();
			o.alreadyAssigned_ = false;
			currObstacles_.push_back(o);
		}
		else if(candidate.size() == 1) // update obstacle info
		{
			cout << "UPDATE OBSTACLE" << endl;
			// added in 25-09-2010
			if(!prevObstacles_.at(candidate.at(0)).alreadyAssigned_)
			{
				o.id_ = prevObstacles_.at(candidate.at(0)).id_;
				prevObstacles_.at(candidate.at(0)).alreadyAssigned_ = true;
			}
			else
			{
				idCounter_++;
				o.id_ = idCounter_;
			}

			o.label_ = currLabel_.at<int>(obstacleCoords_.at(*i).obstacle_.at(0));
			o.massCentre_ = obstacleCoords_.at(*i).massCentre_;
			o.area_ = obstacleCoords_.at(*i).obstacle_.size();
			o.alreadyAssigned_ = false;
			currObstacles_.push_back(o);
			//cout << "---------------------- Update obstacle " << o.id_ << endl;
		}
		else // choose obstacle using area
		{
			cout << "AREA CRITERION HAS TO BE IMPLEMENTED" << endl;
			cout << "N candidates: " << candidate.size() << endl;
			// si la area es similar es fa l'assignació sino es considera que l'obstacle s'ha eliminat
			bool assigned = false;
//			vector<unsigned int>::iterator k = candidate.begin();
//			do
//			{
//
//			}
//			while(k < candidate.end() || assigned);

			if(!assigned)
			{
				cout << "OBSTACLE DELETED!!!!!!!" << endl;
			}
			getchar();
		}

	}

	// compute B points
	for(vector<ccl::SObstacleInfo>::iterator i = currObstacles_.begin(); i < currObstacles_.end(); ++i)
	{
		//if(i->label_ != 1) // discards border
		if(i->label_ > firstObstacleId_)
		{
			bPoints_.push_back(CBPoint(i->id_, i->label_,
										CPoint2D<int>(i->massCentre_.x, i->massCentre_.y),
										currLabel_.at<int>(i->massCentre_) > 0 ? true:false));
		}
	}
}

void CCL::deleteObstacle(const vector<ccl::SObstacleCoord>::iterator& obstacle)
{
	vector<Point2i>::iterator i;
	for(i = obstacle->obstacle_.begin(); i < obstacle->obstacle_.end(); ++i)
		currLabel_.at<int>(*i) = 0;
	for(i = obstacle->internalContour_.begin(); i < obstacle->internalContour_.end(); ++i)
		currLabel_.at<int>(*i) = 0;
	for(i = obstacle->externalContour_.begin(); i < obstacle->externalContour_.end(); ++i)
		currLabel_.at<int>(*i) = 0;

	obstacle->obstacle_.clear();
	obstacle->internalContour_.clear();
	obstacle->externalContour_.clear();
}


#undef CCL

