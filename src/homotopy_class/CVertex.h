// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CVertex.h
 *
 *  Created on: 17/02/2010
 *      Author: Emili
 */

#ifndef CVERTEX_H_
#define CVERTEX_H_

class CVertex
{
public:
	CVertex()
	:
		wedge_(0),
		region_(0)
	{}
	CVertex(const CVertex& v)
	:
		wedge_(v.wedge_),
		region_(v.region_)
	{}
	CVertex(	const unsigned int& wedge,
				const unsigned int& region)
	:
		wedge_(wedge),
		region_(region)
	{}

	unsigned int getWedge() const {return wedge_;}
	unsigned int getRegion() const {return region_;}

	void operator=(const CVertex& v)
	{
		wedge_ = v.wedge_;
		region_ = v.region_;
	}

	bool operator==(const CVertex& v) const
	{
		return (wedge_ == v.wedge_ && region_ == v.region_);
	}

	bool operator!=(const CVertex& v) const
	{
		return (wedge_ != v.wedge_ || region_ != v.region_);
	}

	friend std::ostream& operator<<(std::ostream& cout, const CVertex& v)
	{
		cout << v.wedge_ << "." << v.region_;
		return cout;
	}

private:
	unsigned int wedge_;
	unsigned int region_;
};

#endif /* CVERTEX_H_ */
