// Copyright (c) 2009-2018 Emili Hernandez

#ifndef _CEDGE_H_
#define _CEDGE_H_

#include <string>
#include <iostream>
#include <map>

namespace edge
{
	const std::string e_type ="ABCe";
	//const CEdge empty('e', 0, 0);
};

class CEdge
{

public:
	CEdge()
	:
		type_('e'),
		subindex_(0),
		subsegment_(0)
	{}
	CEdge(const CEdge& e)
	:
		type_(e.type_),
		subindex_(e.subindex_),
		subsegment_(e.subsegment_)
	{}
	CEdge(	const char& type,
			const unsigned int& subindex,
			const int& subsegment = 0)
	:
		type_(type),
		subindex_(subindex),
		subsegment_(subsegment)
	{
		if(edge::e_type.find(type) == std::string::npos)
			std::cout << "Warning. CEdge. Invalid, undefined behavior." << std::endl;
	}

	char getType() const {return type_;}
	unsigned int getSubindex() const {return subindex_;}
	int getSubsegment() const {return subsegment_;}
	void setSubsegment(const int& subsegment){subsegment_ = subsegment;}
	CEdge changeType() const
	{
		if(type_ == 'A')
			return CEdge('B', subindex_, subsegment_);
		else
			return CEdge('A', subindex_, subsegment_);
	}
	CEdge plain() const {return CEdge(type_, subindex_, 0);}

	bool equalType(const CEdge& e) const {return (type_ == e.type_);}

	void operator=(const CEdge& e)
	{
		type_ = e.type_;
		subindex_ = e.subindex_;
		subsegment_ = e.subsegment_;
	}
	bool operator==(const CEdge& e) const
	{
		return (type_ == e.type_ && subindex_ == e.subindex_ && subsegment_ == e.subsegment_);
	}
	bool operator!=(const CEdge& e) const
	{
		return (type_ != e.type_ || subindex_ != e.subindex_ || subsegment_ != e.subsegment_);
	}
	bool operator<(const CEdge& e) const
	{
		if(type_ < e.type_)
			return true;
		else if(type_ > e.type_)
			return false;
		else //type == e.type
		{
			if(subindex_ != e.subindex_)
				return (subindex_ < e.subindex_);
			else // subindex == e.subindex
				return (subsegment_ < e.subsegment_);
		}
	}
	friend std::ostream& operator<<(std::ostream& cout, const CEdge& e)
	{
		cout << e.type_ << e.subindex_ << "," << e.subsegment_;
		return cout;
	}
	friend std::ostream& toLatex(std::ostream& cout, const CEdge& e)
	{
		if(e.type_ == 'A')
			cout << "\\alpha_{"<< e.subindex_ << "_{" << e.subsegment_ << "}}";
		else if(e.type_ == 'B')
			cout << "\\beta_{"<< e.subindex_ << "_{" << e.subsegment_<< "}}";
		return cout;
	}
	friend std::ostream& toHTML(std::ostream& cout, const CEdge& e)
	{
		if(e.type_ == 'A')
			cout << "&alpha;<sub>" << e.subindex_ << "<sub>" << e.subsegment_ << "</sub></sub>";
		else if(e.type_ == 'B')
			cout << "&beta;<sub>" << e.subindex_ << "<sub>" << e.subsegment_ << "</sub></sub>";
		return cout;
	}

private:
	char type_;
	unsigned int subindex_;
	int subsegment_;
};

namespace edge
{
	const CEdge e;
};

#include <boost/shared_ptr.hpp>
typedef boost::shared_ptr<CEdge> CEdgePtr;

class CEdgeEqualType
{
public:
	CEdgeEqualType(const CEdge& e)
	:
		edge_(e)
	{}
	bool operator() (const CEdge& e)
	{
		return (e.getType() == edge_.getType());
	}

private:
	CEdge edge_;
};

class CEdgeEqualTypeAndSubsegment
{
public:
	CEdgeEqualTypeAndSubsegment(const CEdge& e)
	:
		edge_(e)
	{}
	bool operator() (const CEdge& e)
	{
		return ((e.getType() == edge_.getType()) && (e.getSubsegment() == edge_.getSubsegment()));
	}

private:
	CEdge edge_;
};

//class CEdgeNot
//{
//public:
//	CEdgeNot(const CEdge& e)
//	:
//		e_(e)
//	{}
//	bool operator() (const CEdge& e){return (e != e_);}
//
//private:
//	CEdge e_;
//};

#endif // _CEDGE_H_
