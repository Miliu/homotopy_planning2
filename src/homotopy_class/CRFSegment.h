// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CRFSegment.h
 *
 *  Created on: 21/09/2010
 *      Author: emili
 */

#ifndef CRFSEGMENT_H_
#define CRFSEGMENT_H_

#include "CSegment2D.h"
#include "CEdge.h"
#include <iostream>

namespace rfsegment
{
	const int bound = -1; // no obstacle/bound
};

class CRFSegment: public CSegment2D<int>
{
public:
	CRFSegment()
	:
		CSegment2D<int>(),
		edge_(),
		obstacle_(-1)
		{}
	CRFSegment(	const CPoint2D<int>& p1,
				const CPoint2D<int>& p2,
				const CEdge& edge = CEdge(),
				const int& obstacle = rfsegment::bound)
	:
		CSegment2D<int>(p1, p2),
		edge_(edge),
		obstacle_(obstacle)
		{}
	CRFSegment(	const CSegment2D<int>& s,
				const CEdge& edge = CEdge(),
				const int& obstacle = rfsegment::bound)
	:
		CSegment2D<int>(s.getP1(), s.getP2()),
		edge_(edge),
		obstacle_(obstacle)
		{}
	CRFSegment(	const CRFSegment& s)
	:
		CSegment2D<int>(s.p1_, s.p2_),
		edge_(s.edge_),
		obstacle_(s.obstacle_)
	{}

	CEdge getEdge() const {return edge_;}
	int getObstacle() const {return obstacle_;}

	void setEdge(const CEdge& edge) {edge_ = edge;}

	friend std::ostream& operator<<(std::ostream& cout, const CRFSegment& s)
	{
		cout << "[" << s.p1_ << ";" << s.p2_ << ";" << s.edge_ << ";" <<  s.obstacle_ << "]";
		return cout;
	}

	friend std::ostream& toLatex(std::ostream& cout, const CRFSegment& s)
	{
		toLatex(cout, s.edge_);
		cout << " " << s.p1_ << " " << s.p2_ << " " <<  s.obstacle_;
		return cout;
	}

private:
	CEdge	edge_;
	int		obstacle_; // obstacle id found
};


class CRFSegmentIndex
{
public:
	CRFSegmentIndex(const unsigned int& index,
					const int& value)
	:
		index_(index),
		value_(value)
	{}
	int getIndex() const {return index_;}
	int getValue() const {return value_;}
	bool operator<(const CRFSegmentIndex& s) const {return (value_ < s.value_);}

	friend std::ostream& operator<<(std::ostream& cout, const CRFSegmentIndex& s)
	{
		cout << "[" << s.getIndex() << ", " << s.getValue() << "]";
		return cout;
	}

private:
	unsigned int index_;
	int value_;
};


class CRFSegmentEqualObstacle
{
public:
	CRFSegmentEqualObstacle(const CRFSegment& s)
	:
		s_(s)
	{}
	bool operator() (const CRFSegment& s){return (s.getObstacle() == s_.getObstacle());}

private:
	CRFSegment s_;
};

class CRFSegmentEqualEdge
{
public:
	CRFSegmentEqualEdge(const CEdge& e)
	:
		e_(e)
	{}
	bool operator() (const CRFSegment& s){return (s.getEdge() == e_);}

private:
	CEdge e_;
};
#endif /* CRFSEGMENT_H_ */
