/*
 * CCL.h
 *  Original class developer: Alberto Ortiz - UIB. 2008
 *  Created on: 02/11/2009
 *  Modified on : 04/06/2010
 *  Modified on : 09/06/2010
 *  	* Do not take into account obstacles with less than minArea pixels
 *
 * This version does not have any memory thought sucessive iterations
 *
 *      Author: Emili Hernandez
 */

#ifndef CCL_H
#define CCL_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv/cv.hpp>
#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <boost/shared_ptr.hpp>

#include "CPoint2D.h"
#include "CBPoint.h"

/**
 * @class CCL
 *
 * @author Alberto Ortiz
 *
 * @brief Class for connected component labelling.
 *
 * This class is an adaptation of:

		A Linear-Time Component-Labeling Algorithm Using Contour Tracing Technique
		Fu Chang, Chun-Jen Chen, and Chi-Jen Lu 
		Institute of Information Science, Academia Sinica (Taiwan)
		source code: http://ocrlnx03.iis.sinica.edu.tw/~dar/Download%20area/ccl.php3
		PDF file: http://www.iis.sinica.edu.tw/~fchang/paper/component_labeling_cviu.pdf

		@ARTICLE{Chang04alinear-time,
				author = {Fu Chang and Chun-jen Chen and Chi-Jen Lu},
				title = {A linear-time component-labeling algorithm using contour tracing technique},
				journal = {Comput. Vis. Image Underst},
				year = {2004},
				volume = {93},
				pages = {206--220}
		}
*/


typedef cv::Point_<int> Point2i;

namespace ccl
{
	const uchar occThreshold = 128; // pixel occThreshold ? occupied:free
	const unsigned int distThreshold = 5;//15; // max pixel distance between massCentre through iterations
	const unsigned int distThreshold2 = distThreshold*distThreshold; // squared distance

	struct SObstacleCoord
	{
		Point2i massCentre_;
		unsigned long int sumX_;
		unsigned long int sumY_;
		std::vector<Point2i> obstacle_;
		std::vector<Point2i> internalContour_;
		std::vector<Point2i> externalContour_;
	};

	struct SObstacleInfo
	{
		unsigned int id_;
		int label_;
		Point2i massCentre_;
		unsigned long int area_; // in pixels
		bool alreadyAssigned_; //mod 25-09-2010. Flag to allow knowing if the obstacle id has been alredy assigned
	};
}

// ERROR WITH 1-PIXEL OBSTACLES AND MIN_AREA


class CCL
{
  public:
	CCL(	const cv::Mat& map,
			const CPoint2D<int>& size,
			const unsigned int& minArea,
			const int& firstObstacleId,
			const bool& plot = false);
	void update(const bool& deleteCoordinates = true); // mod 22-12-2010 HBug

	cv::Mat getMap() const {return currLabel_;}
	cv::Mat getColorMap() const
	{
		if(plot_)
			return labelPlot_;
		else
		{
			std::cout << "CCL. Warning. Plot disabled." << std::endl;
			return currLabel_;
		}
	}
	int getFirstObstacleId() const {return firstObstacleId_;}

	std::vector<CBPoint> getBPoints() const {return bPoints_;}
	void setBPoints(std::vector<CBPoint> bPoints) {bPoints_ = bPoints;} // xapussa temporal!! Emili 08-10-2010
	void save(const std::string filename);

	// TODO return a reference & turn SObstacleCoord into a class??
	std::vector<ccl::SObstacleCoord> getObstacleCoordinates() const {return obstacleCoords_;}
private:

	/**
	 * Functions and data from:
	 * "A Linear-Time Component-Labeling Algorithm Using Contour Tracing Technique"
	 */
	static int searchDirection_[8][2];
	int CHANlabel(bool contour);
	void CHANcontourTracing(int cy, int cx, int labelindex, int tracingdirection, bool contour, const bool& internalContour);
	void CHANtracer(int& cy, int& cx, int& tracingDirection, const bool& internalContour);

	bool cellOccupied(const int& r, const int& c) const {return (map_.at<uchar>(r, c) < ccl::occThreshold);}

	void postProcessing();
	void deleteObstacle(const std::vector<ccl::SObstacleCoord>::iterator& obstacle);

	/**
	 * Attributes
	 */
	int numObstacles_;
	std::vector<CBPoint> bPoints_; // obstacles representative point

	CPoint2D<int> vsize_;
	unsigned long int size_;
	CPoint2D<int> offset_;

	cv::Mat map_;
	cv::Mat currLabel_;
	cv::Mat prevLabel_;

	cv::Mat r1_, rn_, c1_, cn_;
	int connectedComponentsCount_;
	int labelIndex_;
	unsigned int minArea_;

	// for plotting
	bool plot_;
	cv::Mat labelPlot_;
	std::vector<cv::Vec3b> colors_;

	std::vector<ccl::SObstacleCoord> obstacleCoords_;

	std::vector<unsigned int> validIdx_;
	std::vector<ccl::SObstacleInfo> prevObstacles_;
	std::vector<ccl::SObstacleInfo> currObstacles_;
	unsigned int idCounter_;

	int firstObstacleId_; // 0 for static maps | 1 for dynamic-unknown maps

	cv::Vec3b purple_; // for tests
};

#endif

