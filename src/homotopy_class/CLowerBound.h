// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CLowerBound.h
 *
 *  Created on: 19/01/2011
 *  Modified on: 25/01/2010
 *  	- funnel algorithm modified to allow "l/r overlapping at the same time"
 *  	- channel now computed using the centroids of the wedges of the graph
 *      Author: emili
 */

#ifndef CLOWERBOUND_H_
#define CLOWERBOUND_H_

#include "debug.h"
#include "CTPath.h"
#include "CRFPoint.h"
#include "CRFSegment.h"
#include "CTSpace.h" //only for structs -> it should be removed

#include <vector>

typedef CPoint2D<int> CPoint2Di;

class CLowerBound
{
public:
	CLowerBound(const std::vector<CTPath>& hc,
				const TSegmentMap& rf,
				const std::vector<CPoint2D<float> > centroids,
				const CPoint2Di& start,
				const CPoint2Di& goal,
				const CPoint2Di& c,
				cv::Mat rfPlot,
				const bool& plot);
	void compute();

	std::vector<std::vector<CRFPoint> > getPath() const {return	path_;}
	std::vector<std::vector<CRFSegment> > getChannel() const {return channel_;}
	std::vector<CIdxValue<unsigned int, float> > getIndex() const {return index_;}

private:
	int	triarea2(	const CPoint2Di& a,
					const CPoint2Di& b,
					const CPoint2Di& c)
	{
		CPoint2Di v1(b); v1 -= a;
		CPoint2Di v2(c); v2 -= a;
		return v2.x() * v1.y() - v1.x() * v2.y();
	}
	std::vector<CRFSegment> channel(const CTPath& hc);
	void funnel(const std::vector<CRFSegment>& channel,
					std::vector<CRFPoint>& path,
					float& dist);
	void computeLB();

	// Post processing required due to rounding numerical errors in the RF
	std::vector<CRFPoint> addParallelEdges(	const std::vector<CRFSegment>& ch,
											const std::vector<CRFPoint>& p);

	std::vector<CTPath> 			hc_; // homotopy classes
	TSegmentMap 					rf_; // reference frame
	std::vector<CPoint2D<float> > 	centroids_;
	CPoint2Di						start_;
	CPoint2Di						goal_;
	CPoint2D<float>					c_;

	std::vector<std::vector<CRFSegment> > 			channel_;
	std::vector<std::vector<CRFPoint> > 			path_;
	std::vector<CIdxValue<unsigned int, float> >	index_;

	// plot
	cv::Mat rfPlot_;
	cv::Mat lbPlot_;
	bool plot_;
};

#endif /* CLOWERBOUND_H_ */


//	std::vector<CRFSegment> channel(const CTString& str);
//	CSegment2D<int> nextChannelSegment(	const std::vector<CRFSegment>& ch,
//										const CSegment2D<int>& s,
//										const CPoint2Di& p,
//										const bool& parallel);
//	bool parallelEdges(	const CTString& str,
//							const unsigned int& prev,
//							const unsigned int& curr);
