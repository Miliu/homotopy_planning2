// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CTString.h
 *
 *  Created on: 13/04/2010
 *  	Topological string: vector of CEdgePtr
 *  Modified on: __/10/2010
 *  	String made of CEdge instead of CEdgePtr
 *      Author: Emili
 */

#ifndef CTSTRING_H_
#define CTSTRING_H_

#include "CEdge.h"
#include "SOfPtr.h"
#include <vector>
#include <iostream>
#include <limits>

namespace tstring
{
	const unsigned int npos = std::numeric_limits<unsigned int>::max();
}

class CTString
{
public:
	CTString(){}

	CTString(const CTString& s)
	:
		str_(s.str_)
	{}
	virtual ~CTString(){str_.clear();}

	void operator=(const CTString& s) {str_ = s.str_;}
	bool operator==(const CTString& s) const { return (str_ == s.str_);}
	bool operator!=(const CTString& s) const { return (str_ != s.str_);}

	void add(const CEdge& edge){str_.push_back(edge);}
	unsigned int size() const {return str_.size();}
	bool empty() const {return str_.empty();}
	CEdge at(const unsigned int& i) const {return str_.at(i);}
	CEdge back() const {return str_.back();}
	void reverse(){	std::reverse(str_.begin(), str_.end());}

//	// find & sort algorithms
//	unsigned int findEqualTypeElementReverse(const CEdge& edge)
//	{
//		std::vector<CEdge>::reverse_iterator i = find_if(	str_.rbegin(),
//															str_.rend(),
//															CEdgeEqualType(CEdge('B', 0)));
//		if(i != str_.rend())
//			return (i - str_.rbegin());
//		return tstring::npos;
//	}

	void sortAlpha()
	{
		std::vector<CEdge>::iterator iBeta, i = str_.begin();
		CEdge e('B','0');
		while(i < str_.end())
		{
			if(i->getType() == 'A')
			{
				iBeta = std::find_if(i, str_.end(), CEdgeEqualType(e));
				std::sort(i, iBeta);
				i = iBeta;
			}
			i++;
		}
	}

	friend std::ostream& operator<<(std::ostream& cout, const CTString& s)
	{
		for(std::vector<CEdge>::const_iterator i = s.str_.begin(); i < s.str_.end(); ++i)
			cout << *i;
		return cout;
	}

	friend std::ostream& toLatex(std::ostream& cout, const CTString& s)
	{
		for(std::vector<CEdge>::const_iterator i = s.str_.begin(); i < s.str_.end(); ++i)
			toLatex(cout, *i);
		return cout;
	}

	friend std::ostream& toHTML(std::ostream& cout, const CTString& s)
	{
		for(std::vector<CEdge>::const_iterator i = s.str_.begin(); i < s.str_.end(); ++i)
			toHTML(cout, *i);
		return cout;
	}

//	//ICAPS
//	bool findAk0substring(unsigned int& i, int& begin, int& end) const
//	{
//		bool started = false;
//		while(i < str_.size())
//		{
//			if(!started && str_.at(i).getSubsegment() == 0)
//			{
//				begin = i;
//				started = true;
//			}
//			else if(started && str_.at(i).getSubsegment() != 0)
//			{
//				end = i;
//				return (end - begin) > 1;
//			}
//			i++;
//		}
//		end = i;
//		return (end - begin) > 1;
//	}

	int alpha0Substring(const unsigned int& i) const
	{
		if(str_.at(i).getSubsegment() != 0) return i;

		unsigned int j = i;
		while(j < str_.size() && str_.at(j).getSubsegment() == 0)
			++j;
		return j;
	}


protected:
	std::vector<CEdge> str_;
};

#endif /* CTSTRING_H_ */
