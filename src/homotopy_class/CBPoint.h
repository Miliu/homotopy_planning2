// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CBPoint.h
 *
 *  Created on: 17/06/2010
 *  Modified on: 18/09/2010
 *      Author: emili
 */

#ifndef CBPOINT_H_
#define CBPOINT_H_

#include "CPoint2D.h"

class CBPoint
{
public:
	CBPoint() // treure en el futur. Emili 08-10-2010
	:
		id_(0),
		label_(-1),
		point_(CPoint2D<int>()),
		insideObstacle_(false)
	{}
	CBPoint(const unsigned int& id, const int& label, const CPoint2D<int>& point, const bool& insideObstacle)
	:
		id_(id),
		label_(label),
		point_(point),
		insideObstacle_(insideObstacle)
	{}
	CBPoint(const CBPoint& p)
	:
		id_(p.id_),
		label_(p.label_),
		point_(p.point_),
		insideObstacle_(p.insideObstacle_)
	{}

	void operator=(const CBPoint& p)
	{
		id_ = p.id_;
		label_ = p.label_;
		point_ = p.point_;
		insideObstacle_ = p.insideObstacle_;
	}

	bool operator<(const CBPoint& p) const {return (id_ < p.id_);}
	unsigned int getId() const {return id_;}
	int getLabel() const {return label_;}
	CPoint2D<int> getPoint() const {return point_;}
	bool isInsideObstacel() const {return insideObstacle_;}

	friend std::ostream& operator<<(std::ostream& cout, const CBPoint& p)
	{
		cout << p.getId() << "," << p.getLabel() << "," <<  p.getPoint() << "," << p.isInsideObstacel();
		return cout;
	}

private:
	unsigned int 	id_;
	int 			label_;
	CPoint2D<int> 	point_;
	bool 			insideObstacle_;
};



#include <boost/shared_ptr.hpp>
typedef boost::shared_ptr<CBPoint> CBPointPtr;

enum EBPointChangeType {b_added = 0, b_removed, b_updated};
class CBPointChange
{
public:
	CBPointChange(CBPointPtr ptr, EBPointChangeType type)
	:ptr_(ptr),type_(type){}
	CBPointPtr getPtr() const {return ptr_;}
	EBPointChangeType getType() const {return type_;}

	friend std::ostream& operator<<(std::ostream& cout, const CBPointChange& c)
	{
		cout << *(c.getPtr()) << " -> ";
		switch(c.getType())
		{
		case b_added: cout << "added";
			break;
		case b_removed: cout << "removed";
			break;
		case b_updated: cout << "updated";
			break;
		default:
			break;
		}
		return cout;
	}

private:
	CBPointPtr ptr_;
	EBPointChangeType type_;

};

// class to find a b_point by its id
class CBPointId
{
public:
	CBPointId(const unsigned int& id):id_(id){};
	bool operator() (const CBPoint& p){return (p.getId() == id_);}

private:
	unsigned int id_;
};

#endif /* CBPOINT_H_ */
