// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CRFPoint.h
 *
 *  Created on: 19/01/2011
 *      Author: emili
 */

#ifndef CRFPOINT_H_
#define CRFPOINT_H_

#include "CPoint2D.h"
#include "CEdge.h"
#include <iostream>

class CRFPoint: public CPoint2D<int>
{
public:
	CRFPoint(	const CPoint2D<int>& p,
				const CEdge& edge = CEdge(),
				const int& index = -1)
	:
		CPoint2D<int>(p),
		edge_(edge),
		index_(index)
	{}
	CRFPoint(	const CRFPoint& p)
	:
		CPoint2D<int>(p.x(), p.y()),
		edge_(p.edge_),
		index_(p.index_)
	{}

	CEdge getEdge() const {return edge_;}
	void setEdge(const CEdge& edge) {edge_ = edge;}

	int getIndex() const {return index_;}

	friend std::ostream& operator<<(std::ostream& cout, const CRFPoint& p)
	{
		cout << "[[" << p.x() << ";" << p.y() << "];" << p.getEdge() << ";" << p.getIndex() << "]";
		return cout;
	}

private:
	CEdge	edge_;
	int		index_;
};

#endif /* CRFPOINT_H_ */
