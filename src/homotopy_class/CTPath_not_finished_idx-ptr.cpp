// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CTPath.cpp
 *
 *  Created on: 27/01/2010
 *      Author: Emili
 */

#include "CTPath.h"

using namespace std;

namespace tpath
{
	const CEdge emptyEdge_ = CEdge('e', 0, 0);
	CEdgePtr emptyEdgePtr_(new CEdge(emptyEdge_));
}

CTPath::CTPath(	const CTString& edgeSeq,
				const TEdgeMapPtr& edgeIdx)
:
				edgeSeq_(edgeSeq),
				edgeIdx_(TEdgeMapPtr(new TEdgeMap(*edgeIdx)))
{
	if(edgeSeq_.empty())
	{
		vertexIdx_.reset(new TVertexMap);
		vertexCicle_ = false;
	}
	else
		throw runtime_error("CTPath constructor cannot set vertexIdx");
}

CTPath::CTPath(const CTPath& path)
:
		edgeSeq_(path.edgeSeq_),
		vertexCicle_(path.vertexCicle_)
{
	if(path.edgeIdx_) edgeIdx_.reset(new TEdgeMap(*(path.edgeIdx_)));
	if(path.vertexIdx_) vertexIdx_.reset(new TVertexMap(*(path.vertexIdx_)));
}

CTPath::~CTPath()
{

}

void CTPath::operator=(const CTPath& path)
{
	edgeSeq_ = path.edgeSeq_;
	if(path.edgeIdx_) edgeIdx_.reset(new TEdgeMap(*path.edgeIdx_));
	if(path.vertexIdx_) vertexIdx_.reset(new TVertexMap(*(path.vertexIdx_)));
	vertexCicle_ = path.vertexCicle_;
}

void CTPath::add(const CEdge& edge, const int& vertex)
{
	//if(edgeIdx_) (*(edgeIdx_))[edge].push_back(edgeSeq_.size());
	if(edgeIdx_) (*(edgeIdx_))[edge.plain()].push_back(edgeSeq_.size());
	edgeSeq_.add(edge);

	if(vertexIdx_)
	{
		(*(vertexIdx_))[vertex].push_back(edgeSeq_.size());
		if((*(vertexIdx_))[vertex].size() >= 2)
			vertexCicle_ = true;
	}
}


ostream& operator<<(ostream& cout, const CTPath& path)
{
	// Edges
	cout << path.edgeSeq_;
	if(path.edgeIdx_)
	{
		cout << "\t\t";
		std::vector<unsigned int>::const_iterator j;
		TEdgeMap::const_iterator k = (path.edgeIdx_)->begin();
		while(k != (path.edgeIdx_)->end())
		{
			cout << k->first << "( ";
			for(j = k->second.begin(); j != k->second.end(); j++)
				cout << *j << " ";
			cout << ")" ;
			++k;
		}
	}

	// Vertexs
	cout << path.vertexIdx_;
	if(path.vertexIdx_)
	{
		cout << "\t\t";
		std::vector<int>::const_iterator j;
		TVertexMap::const_iterator k = (path.vertexIdx_)->begin();
		while(k != (path.vertexIdx_)->end())
		{
			cout << k->first << "( ";
			for(j = k->second.begin(); j != k->second.end(); j++)
				cout << *j << " ";
			cout << ")" ;
			++k;
		}
	}
	cout << " CICLE: " << path.vertexCicle_;
	return cout;
}

bool CTPath::checkCriteria()
{
	/*
	if(string_.size() >= 3)
	{
		if(criterion1())
		{
//			cout << " CRITERION 1 ";
			return true;
		}
		if(string_.size() >= 4)
		{
			//if(criterion2()) {cout << " CRITERION 2 "; return true;}
			if(csCriterion2() && vCicle_)
			{
//				cout << " CRITERION 2 ";
				return true;
			}
		}
		if(criterion3())
		{
//			cout << " CRITERION 3 ";
			return true;
		}
	}
	*/
	return false;
}

bool CTPath::wrapJenkins()
{
	CEdge e = edgeSeq_.back();
	if((*edgeIdx_)[e].size() > 1)
	{
		unsigned int lIdx = (*edgeIdx_)[e].at(0); // first index
		unsigned int hIdx = (*edgeIdx_)[e].back(); // last index
		if((hIdx - lIdx) > 1)
		{
			//Check subsegments
			for(unsigned int i = lIdx; i <= hIdx; ++i)
			{
				if(edgeSeq_.at(i).getSubindex() == e.getSubindex())
					return true;
			}
		}
	}
	return false;
}

/*
bool CTPath::criterion1()
{
	CEdge e = string_.back();
	if((*index_)[e].size() > 1)
	{
		unsigned int low_i = (*index_)[e].at(0); // first index
		unsigned int high_i = (*index_)[e].back(); // last index
		if((high_i - low_i) > 1)
		{
			//Jenkins version
//			CEdge op_e = e.changeType();
//			if((*index_)[op_e].size() > 0)
//			{
//				unsigned int op_i = (*index_)[op_e].back();
//				if((op_i > low_i) && (op_i < high_i))
//					return true;
//			}

			//With subsegments
			for(unsigned int i = low_i; i <= high_i; i++)
			{
				if(string_.at(i).getSubindex() == e.getSubindex())
					return true;
			}
		}
	}
	return false;
}
*/
/*
bool CTPath::criterion2()
{
	CEdge e_k = string_.back();
	CEdge op_e_k = e_k.changeType();

	if((*index_)[op_e_k].size() > 0)
	{
		if(((*index_)[e_k].back() - (*index_)[op_e_k].at(0)) > 2)
		{
			CEdge e_m, op_e_m;
			TStrMap::iterator index_it = (*index_).begin();
			int i = 0, end = (*index_).size() / 2;
			while(i < end)
			{
				e_m = index_it->first;
				if(	(e_m.getSubindex() != e_k.getSubindex()) &&
					((*index_)[e_m].size() > 0)	)
				{
					op_e_m = e_m.changeType();
					if((*index_)[op_e_m].size() > 0)
					{
						if(e_k.getType() == 'A')
						{
							TUintVector::iterator vec_it = (*index_)[e_m].begin();
							while(vec_it != (*index_)[e_m].end())
							{
								if( *vec_it < (*index_)[op_e_m].back() &&
									*vec_it	> (*index_)[op_e_k].at(0))
									return true;
								vec_it++;
							}
						}
						else // e_k.type == 'B'
						{
							TUintVector::iterator vec_it = (*index_)[op_e_m].begin();
							while(vec_it != (*index_)[op_e_m].end())
							{
								if( *vec_it < (*index_)[e_m].back() &&
									*vec_it	> (*index_)[op_e_k].at(0))
									return true;
								vec_it++;
							}

						}
					}

				}
				index_it++; i++;
			}
		}
	}
	return false;
}
*/

//bool CTPath::criterion3()
//{
//	TString::reverse_iterator i = find_if(string_.rbegin(), string_.rend(),
//			CEdgeEqualType(CEdgePtr(new CEdge('B', 0))));
//	CEdge e = *(string_.back());
//
//	size_t size_e = (*index_)[e].size();
//	if(size_e > 1) // only strings with repeated edges
//	{
//		if(i != string_.rend())
//		{
//			// second to last index > last beta index ?
//			if((*index_)[e].at(size_e - 2) > (*index_)[**i].back())
//				return true;
//		}
//		else // no break: alpha string with duplicated alpha
//		{
//			return true;
//		}
//	}
//	return false;
//}

/*
bool CTPath::criterion3()
{
	unsigned int i = string_.findEqualTypeElementReverse(CEdge('B', 0));
	CEdge e = string_.back();
	size_t size_e = (*index_)[e].size();
	if(size_e > 1) // only strings with repeated edges
	{
		if(i != tstring::npos)
		{
			// second to last index > last beta index ?
			if((*index_)[e].at(size_e - 2) > (*index_)[string_.at(i)].back())
			{
				return true;
			}
		}
		else // no break: alpha string with duplicated alpha
		{
			return true;
		}
	}
	return false;
}
*/
