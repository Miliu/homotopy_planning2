// Copyright (c) 2018 Emili Hernandez
/*
 * main.cpp
 *
 *  Created on: 17/09/2010
 *      Author: emili
 *
 *  HBug does not work with real datasets -> to generate results create map and remove everything that's not an obstacle
 */

#include "CMap.h"
#include "CEnvironment.h"
#include "CTSpace.h"
#include "CLowerBound.h"
#include "CHTML.h"

#include "CAStar.h"
#include "CHAStar.h"
#include "CRRT.h"
#include "CHRRT.h"
//#include "CBug2.h"
#include "CHBug.h"

#include <iostream>

#include <boost/date_time/posix_time/posix_time.hpp>
using namespace boost::posix_time;

using namespace std;
int main()
{
	srand(0);// time( NULL ) );

	bool plot = true;
	unsigned int numIterations = 1;
	ptime time1, time2;
	time_duration diff;

	// 0 - "map/basic1.tiff";
	// 1 - "map/O2CA2Session_2010-09-03_16-28-05/"; // traj. quadrada i passa pel mig dels dos obstacles, is: nbins = 200 range = 10m
	// 2 - "map/stress_big_13.tiff";
	// 3 - "map/mini.tiff";
	// 4 - "map/mini2.tiff";
	// 5 - "map/test_iros10_2.tiff";
	// 6 - "map/test_iros10_2_big.tiff";
	// 7 - "map/empty.tiff";
	// 8 - "map/h.tiff";
	// 9 - "map/concave.tiff";
	// 10 - "map/stress_big_15_icaps.tiff";
	// 11 - "map/test_iros10_2_big2.tiff";
	// 12 - "map/cluttered.tiff";
	// 13 - "map/cluttered_convex.tiff";
	// 14 - "map/cirs_2obstacles_without_contour.tiff";
	// 15 - "map/basic1_rotated.tiff";
	// 16 - "map/formigues-scale5.tiff";
	// 17 - "map/formigues-scale5_2.tiff";
	// 18 - "map/example.tiff";
	// 19 - "map/mini3.tiff";
	// 22 - "map/cluttered_mod.tiff";

	unsigned int idx = 16;//23; //22;//20; //19;
	CEnvironment env(idx, plot);
	env.build();

	CMapPtr map = env.getMap();
	CPoint2D<double> start = env.getStart();
	CPoint2D<double> goal = env.getGoal();
	CPoint2D<double> c = env.getC();
	CPoint2D<int> cstart = map->metric2Cell(start);
	CPoint2D<int> cgoal = map->metric2Cell(goal);
	CPoint2D<int> cc = map->metric2Cell(c);

	// xapussilla
	vector<CBPoint> obstacles = map->getBPoints();
	if(idx == 2)
	{
		CBPoint p;
		p = obstacles.at(5);
		obstacles.at(5) = CBPoint(p.getId(), p.getLabel(), CPoint2D<int>(514, 766), p.isInsideObstacel());
		p = obstacles.at(7);
		obstacles.at(7) = CBPoint(p.getId(), p.getLabel(), CPoint2D<int>(883, 547), p.isInsideObstacel());
	}
	if(idx == 16 || idx == 17)
	{
		CBPoint p;
		p = obstacles.at(5);
		obstacles.at(5) = CBPoint(p.getId(), p.getLabel(), CPoint2D<int>(383,81), p.isInsideObstacel());

		p = obstacles.at(7);
		obstacles.at(7) = CBPoint(p.getId(), p.getLabel(), CPoint2D<int>(346,85), p.isInsideObstacel());

		p = obstacles.at(8);
		obstacles.at(8) = CBPoint(p.getId(), p.getLabel(), CPoint2D<int>(224,97), p.isInsideObstacel());

//		CBPoint p;
//		p = obstacles.at(6);
//		obstacles.at(6) = CBPoint(p.getId(), p.getLabel(), CPoint2D<int>(125, 275), p.isInsideObstacel());
//		p = obstacles.at(9);
//		obstacles.at(9) = CBPoint(p.getId(), p.getLabel(), CPoint2D<int>(824, 521), p.isInsideObstacel());
//		p = obstacles.at(11);
//		obstacles.at(11) = CBPoint(p.getId(), p.getLabel(), CPoint2D<int>(629, 526), p.isInsideObstacel());
	}
	if(idx == 18)
	{
//		CBPoint p;
//		p = obstacles.at(0);
//		obstacles.at(0) = CBPoint(1, p.getLabel(), CPoint2D<int>(60/2, 244/2), p.isInsideObstacel());
//		p = obstacles.at(1);
//		obstacles.at(1) = CBPoint(2, p.getLabel(), CPoint2D<int>(58/2, 80/2), p.isInsideObstacel());

		CBPoint p;
		p = obstacles.at(0);
		obstacles.at(0) = CBPoint(1, p.getLabel(), CPoint2D<int>(60, 244), p.isInsideObstacel());
		p = obstacles.at(1);
		obstacles.at(1) = CBPoint(2, p.getLabel(), CPoint2D<int>(58, 80), p.isInsideObstacel());
	}

	if(idx == 23 || idx == 24)
	{
		// per exemple d'exeucio per la tesis
		CBPoint p;
		p = obstacles.at(0);
		obstacles.at(0) = CBPoint(1, p.getLabel(), CPoint2D<int>(130,36), p.isInsideObstacel());

		p = obstacles.at(1);
		obstacles.at(1) = CBPoint(2, p.getLabel(), CPoint2D<int>(130,145), p.isInsideObstacel());
	}


	map->setBPoints(obstacles);

	time1 = ptime(microsec_clock::universal_time());
	CTSpace tspace(map, cstart, cgoal, true);
	try
	{
		tspace.build(cc);
	}
	catch ( std::exception& e )
	{
		cout << e.what() << endl;
	}

//	cv::waitKey();

	vector<CTString> homotopyClasses;
	vector<CRFSegment> subsegments;
	tspace.computeHomotopyClasses(20);
	time2 = ptime(microsec_clock::universal_time());
	diff = time2 - time1;
	cout << "tspace & homotopy classes: " << setprecision(6) << diff.total_microseconds() / 1000000.0f << " s" << endl;
	//getchar();

	homotopyClasses = tspace.getHomotopyClasses();
	subsegments = tspace.getRFSubsegments();

	// Lower bound ----------------------------------------
	time1 = ptime(microsec_clock::universal_time());
	CLowerBound lb(	tspace.getFullHomotopyClasses(),
					tspace.getBoundarySegmentLUT(),
					tspace.getCentroids(),
					cstart,
					cgoal,
					cc,
					tspace.getRFPlot(),
					plot);
	lb.compute();
	time2 = ptime(microsec_clock::universal_time());
	diff = time2 - time1;
	cout << "Lower bound: " << setprecision(6) << diff.total_microseconds() / 1000000.0f << " s" << endl;

	vector<CIdxValue<unsigned int, float> > lbIndex = lb.getIndex();
	vector<vector<CRFPoint> > 	lbPath = lb.getPath();
	vector<vector<CRFSegment> > 	lbChannel = lb.getChannel();

//
//	CHTML f1("Homotopy Classes", env.getRoot());
//	f1.addHomotopyClasses(lbIndex, homotopyClasses);
//	f1.save("test/web/hc.html");
//

//	CHTML f2("Sorted Homotopy Classes", env.getRoot());
//	vector<CIdxValue<unsigned int, float> > lbIndex2 = lbIndex;
//	sort(lbIndex2.begin(), lbIndex2.end());
//	f2.addHomotopyClasses(lbIndex2, homotopyClasses);
//	f2.save("test/web/hc_sorted.html");

/*
	if(homotopyClasses.size() > 0)
	{

////		cout << "HA*-Path " << "-------------" << endl;
		unsigned int idx = 0;
		for(vector<CTString>::iterator i = homotopyClasses.begin()+idx; i < homotopyClasses.end(); i++)
		//vector<CTString>::iterator i = homotopyClasses.begin()+idx;
		{


			CHAStar hastar(map, subsegments, tspace.getRFPlot(), plot);
			hastar.computePath(map->metric2Cell(start), map->metric2Cell(goal), CTStringR(*i, map->getBPoints().size()));
			//hastar.computePath(lbChannel.at(i - homotopyClasses.begin()), CTStringR(*i, map->getBPoints().size()));
			if(plot)
			{
				cout << "Path: " << *i << endl;
				cout << "Nº of cells processed: " << hastar.getNCellsProcessed() << "/" << map->getSizeCell();
				cout << " - " <<  fixed << setprecision(2) << static_cast<float>(hastar.getNCellsProcessed())/map->getSizeCell() * 100 << "%" << endl;
				cout << "Elapsed time: " << setprecision(6) << hastar.getComputationTime() << " s." << endl;
				cout << "Length: " << hastar.getLength() << endl;
				cout << endl;
				cv::waitKey();
			}
			else
			{
				cout << idx << " " << setprecision(6) << hastar.getComputationTime()  << " " << fixed << hastar.getLength() << " %" << *i << endl;
			}


			float accCells = 0.0f;
			float accLength = 0.0f;
			float accTime = 0.0f;
			if(!plot) cout << "%";
			for(unsigned int it = 0; it < numIterations; ++it)
			{

				CHRRT hrrt(map, subsegments, tspace.getRFPlot(), plot);
//				hrrt.computePath(	map->metric2Cell(start),
//									map->metric2Cell(goal),
//									round(8 / map->getResolution()),
//									//0.01,
//									0.01,
//									8 / map->getResolution(),
//									CTStringR(*i, map->getBPoints().size()));
				hrrt.computePath(	map->metric2Cell(start),
									map->metric2Cell(goal),
									round(8 / map->getResolution()),
									//0.01,
									0.01,
									8 / map->getResolution(),
									CTStringR(*i, map->getBPoints().size()));
				accTime += hrrt.getComputationTime();
				accLength += hrrt.getLength();
				accCells += static_cast<float>(hrrt.getNCellsProcessed())/map->getSizeCell() * 100;
				if(!plot) {cout << ".";flush(cout);}
			}
			if(plot)
			{
				cout << "Path: " << *i << endl;
//				cout << "Nº of cells processed: " << hrrt.getNCellsProcessed() << "/" << map->getSizeCell();
//				cout << " - " <<  fixed << setprecision(2) << static_cast<float>(hrrt.getNCellsProcessed())/map->getSizeCell() * 100 << "%" << endl;
//				cout << "Elapsed time: " << setprecision(6) << hrrt.getComputationTime() << " s." << endl;
//				cout << "Length: " << hrrt.getLength() << endl;
//				cout << endl;
				cv::waitKey();
			}
			else
			{
				//cout << idx << " " << setprecision(3) << hrrt.getComputationTime()  << " " << fixed << hrrt.getLength() << '%' << *i << endl;
				cout 	<<endl;
				cout 	<< idx << " " << setprecision(6) << accTime / numIterations  << " "
						<< fixed << accLength / numIterations << " "
						<< accCells / numIterations << " %" << *i << endl;
			}

			idx++;
		}

	}
	else
		cout << " No homotopic classes." << endl;

*/

//  HBug -----------------------------------------------------------------------
	if(homotopyClasses.size() > 0)
	{
		cout << "HBug-Path " << "-------------" << endl;
		unsigned int idx = 0;
		for(vector<CTString>::iterator i = homotopyClasses.begin()+idx; i < homotopyClasses.end(); i++)
		{
			//CHBug hbug(map, subsegments, tspace.getRFPlot(), plot);
//			cout << "HBUG-Path " << "----- " << *i << endl;

			float accCells = 0.0f;
			float accLength = 0.0f;
			float accTime = 0.0f;
			if(!plot) cout << "%";
			for(unsigned int it = 0; it < numIterations; ++it)
			{
				CHBug hbug(map, tspace.getBoundarySegmentLUT(), tspace.getRFPlot(), plot);
				hbug.computePath(	lbPath.at(i - homotopyClasses.begin()),
									CTStringR(*i, map->getBPoints().size()));
				accTime += hbug.getComputationTime();
				accLength += hbug.getLength();
				accCells += static_cast<float>(hbug.getNCellsProcessed())/map->getSizeCell() * 100;
				if(!plot) {cout << ".";flush(cout);}
				else
				{
					cout << "Path: " << idx << " " << *i << endl;
					cout << "Nº of cells processed: " << hbug.getNCellsProcessed() << "/" << map->getSizeCell();
					cout << " - " <<  fixed << setprecision(2) << static_cast<float>(hbug.getNCellsProcessed())/map->getSizeCell() * 100 << "%" << endl;
					cout << "Elapsed time: " << setprecision(6) << hbug.getComputationTime() << " s." << endl;
					cout << "Length: " << hbug.getLength() << endl;
					cout << endl;
					cv::waitKey();
				}
			}

//			if(plot)
//			{
//				cout << "Path: " << *i << endl;
//				cout << "Nº of cells processed: " << hbug.getNCellsProcessed() << "/" << map->getSizeCell();
//				cout << " - " <<  fixed << setprecision(2) << static_cast<float>(hbug.getNCellsProcessed())/map->getSizeCell() * 100 << "%" << endl;
//				cout << "Elapsed time: " << setprecision(6) << hbug.getComputationTime() << " s." << endl;
//				cout << "Length: " << hbug.getLength() << endl;
//				cout << endl;
//				cv::waitKey();
//			}
//			else
			{
				//cout << idx << " " << setprecision(6) << hbug.getComputationTime()  << " " << fixed << hbug.getLength() << " %" << *i << endl;
				cout 	<<endl;
				cout 	<< idx << " " << setprecision(6) << accTime / numIterations  << " "
						<< fixed << accLength / numIterations << " "
						<< accCells / numIterations << " %" << *i << endl;
			}
			idx++;
		}

	}


//  A* --------------------------------------------------------------
//	CAStar astar(map, tspace.getRFPlot(), plot);
//	astar.computePath(map->metric2Cell(start), map->metric2Cell(goal));
//	if(plot)
//	{
//		cout << "Nº of cells processed: " << astar.getNCellsProcessed() << "/" << map->getSizeCell();
//		cout << " - " <<  fixed << setprecision(2) << static_cast<float>(astar.getNCellsProcessed())/map->getSizeCell() * 100 << "%" << endl;
//		cout << "Elapsed time: " << setprecision(6) << astar.getComputationTime() << " s." << endl;
//		cout << "Length: " << astar.getLength() << endl;
//		cv::waitKey();
//	}
//	else
//		cout << setprecision(6) << astar.getComputationTime()  << " " << fixed << astar.getLength() << endl;

//  RRT --------------------------------------------------------------
//	float accCells = 0.0f;
//	float accLength = 0.0f;
//	float accTime = 0.0f;
//	cout << "RRT-Path " << "-------------" << endl;
//	if(!plot) cout << "%";
//	for(unsigned int it = 0; it < numIterations; ++it)
//	{
//		CRRT rrt(map, tspace.getRFPlot(), plot);
//		rrt.computePath(	map->metric2Cell(start),
//							map->metric2Cell(goal),
//							round(5 / map->getResolution()),
//							0.05,//0.01,
//							5 / map->getResolution()
//							//round(0.5 / map->getResolution()),
////							20,
////							0.001,
////							20
//							);
//							//0.1 / map->getResolution());
//		accTime += rrt.getComputationTime();
//		accLength += rrt.getLength();
//		accCells += static_cast<float>(rrt.getNCellsProcessed())/map->getSizeCell() * 100;
//		if(!plot) {cout << ".";flush(cout);}
//	}
//	cout 	<<endl;
//	cout 	<< setprecision(6) << accTime / numIterations  << " "
//			<< fixed << accLength / numIterations << " "
//			<< accCells / numIterations << endl;

//  Bug2 --------------------------------------------------------------
//	float accCells = 0.0f;
//	float accLength = 0.0f;
//	float accTime = 0.0f;
//	std::vector<bug::EDirection> d = {bug::left, bug::left, bug::left};
//
//	if(!plot) cout << "%";
//	for(unsigned int it = 0; it < numIterations; ++it)
//	{
//		CBug2 bug2(map, tspace.getRFPlot(), plot);
//		bug2.computePath(	map->metric2Cell(start),
//							map->metric2Cell(goal),
//							d);
//		accTime += bug2.getComputationTime();
//		accLength += bug2.getLength();
//		accCells += static_cast<float>(bug2.getNCellsProcessed())/map->getSizeCell() * 100;
//		if(!plot) {cout << ".";flush(cout);}
//	}
//
////			if(plot)
////			{
////				cout << "Path: " << *i << endl;
////				cout << "Nº of cells processed: " << hbug.getNCellsProcessed() << "/" << map->getSizeCell();
////				cout << " - " <<  fixed << setprecision(2) << static_cast<float>(hbug.getNCellsProcessed())/map->getSizeCell() * 100 << "%" << endl;
////				cout << "Elapsed time: " << setprecision(6) << hbug.getComputationTime() << " s." << endl;
////				cout << "Length: " << hbug.getLength() << endl;
////				cout << endl;
////				cv::waitKey();
////			}
////			else
//	{
//		//cout << idx << " " << setprecision(6) << hbug.getComputationTime()  << " " << fixed << hbug.getLength() << " %" << *i << endl;
//		cout 	<<endl;
//		cout 	<< setprecision(6) << accTime / numIterations  << " "
//				<< fixed << accLength / numIterations << " "
//				<< accCells / numIterations << endl;
//	}



	if(plot)
	{
		cv::imwrite("test/cobstacle.tiff", map->getCObstacle());
		cv::waitKey();
	}

	cout << "Done" << endl;

	return 0;
}
