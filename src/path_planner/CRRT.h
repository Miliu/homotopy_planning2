// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CRRT.h
 *
 *  Created on: 13/11/2009
 *      Author: Emili
 */

#ifndef CRRT_H_
#define CRRT_H_

#include "CPoint2D.h"

typedef CPoint2D<int> CPoint2Di;

class CRRTNode
{
public:
	CRRTNode(	const long int& id,
				const CPoint2Di& point)
	:
		id_(id),
		point_(point)
	{}
	CRRTNode(const CRRTNode& n)
	:
		id_(n.id_),
		point_(n.point_)
	{}

	void operator=(const CRRTNode& n)
	{
		id_ = n.id_;
		point_ = n.point_;
	}

	long int getId() const {return id_;}
	CPoint2Di getPoint() const {return point_;}
	friend std::ostream& operator<<(std::ostream& cout, const CRRTNode& n)
	{
		cout << "[" << n.getId() << ","<< n.getPoint() << "]";
		return cout;
	}

private:
	long int 	id_; 		// added for representing results
	CPoint2Di 	point_;
};

namespace rrt
{
	const unsigned int maxNTries = 250;
	const float maxCellsFactor = 0.3; // 0..1
	const long int costCorrection = 100; // useful to operate with integers
	const unsigned char explored = 255;
	const unsigned char notExplored = 0;

}

#include "CMap.h"
#include "tree/tree.hh"
#include <boost/date_time/posix_time/posix_time.hpp>

class CRRT
{
public:
	CRRT(CMapPtr map, const cv::Mat& rf, const bool& plot);

	bool computePath(	const CPoint2Di& start,
						const CPoint2Di target,
						const unsigned int& stepi,
						const float& goalSamplingPrb,
						const float& distanceThresholdf);

	float getComputationTime() const {return computationTime_;}
	float getLength() const {return length_;}
	const long int getNCellsProcessed() const {return nCellsProcessed_;}

private:
	bool computeQRand(const CPoint2Di& goal, const float& goalSamplingPrb, CPoint2Di& qRand);
	tree<CRRTNode>::iterator nearestNeighbor(tree<CRRTNode>& T, const CPoint2Di& qRand);
	bool computeQNew(const CPoint2Di& qNearest, const CPoint2Di& qRand, const unsigned int& step, CPoint2Di& qNew);
	bool extend(	tree<CRRTNode>& T,
					const CRRTNode& goalNode,
					const unsigned int& step,
					const float& goalSamplingPrb,
					const float& distanceThreshold,
					tree<CRRTNode>::iterator& iNearest);

	// functions to show results
	float computePathLength(tree<CRRTNode> T, tree<CRRTNode>::iterator i);
	void savePathCoordinates(tree<CRRTNode> T, tree<CRRTNode>::iterator i, const std::string& filename);
	void saveTreeCoordinates(tree<CRRTNode> T, const std::string& filename);

	CMapPtr map_;
	CPoint2Di mapSize_;
	CPoint2Di start_, goal_;

	float computationTime_;
	float length_;
	unsigned long int nCellsProcessed_;
	unsigned long int maxCellsToProcess_;
	long int idCounter_;

	cv::Mat explored_;
	// plot flag
	bool plot_;

	cv::Mat rfPlot_;
	cv::Mat mapPlot_;
	void plotPath(tree<CRRTNode> T, tree<CRRTNode>::iterator i);
};

#endif /* CRRT_H_ */
