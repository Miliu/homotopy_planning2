// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CAStar.cpp
 *
 *  Created on: 08/04/2010
 *      Author: Emili
 *  La idea no funciona:
 *   - Falta saber per cada estat la part del channel que es necessita per calcular el LB
 *   - Sembla que cal controlar els estats open, close, etc.
	fal
 */



#include "CAStar.h"
#include "debug.h"

using namespace std;
using namespace boost::posix_time;

CAStar::CAStar(CMapPtr map, const cv::Mat& rf, const bool& plot)
:
		map_(map),
		mapSize_(map->getVSizeCell()),
		plot_(plot),
		rfPlot_(rf)
{
	if(plot_)
	{
		CPoint2Di size = map_->getVSizeCell();
		rfPlot_.copyTo(mapPlot_);
		cv::namedWindow("A*path", CV_WINDOW_AUTOSIZE);
		cvMoveWindow("A*path", 4 * size.x() + 100, 100);
		imshow("A*path", mapPlot_);
	}
}

CAStar::~CAStar()
{}

bool CAStar::computePath(const CPoint2Di& start, const CPoint2Di& goal)
{
	//cout << "computePath --------------------------------" << endl;
	start_ = start;
	goal_ = goal;

	bool pathFound = false;
	length_ = 0;
	nCellsProcessed_ = 0;

	if(!map_->isFree(start_))
	{
		//cout << "START POINT OCCUPIED" << start_ << " value: " << static_cast<int>(map_->getOccupancy(start_)) << endl;
		throw runtime_error("Start point occupied.");
	}
	if(!map_->isFree(goal_))
	{
		//cout << "GOAL POINT OCCUPIED" << endl;
		throw runtime_error("Goal point occupied.");
	}

	visitedNodes.clear();
	visitedNodes.reserve(map_->getSizeCell());
	visitedNodes.resize(map_->getSizeCell());

	status.reserve(map_->getSizeCell());
	status.resize(map_->getSizeCell());
	std::fill(status.begin(), status.end(), astar::notProcessed);

	vector<CAStarNode*> Q;
	Q.reserve(map_->getSizeCell());

	ptime time1(microsec_clock::universal_time());

	CAStarNode *currNode, *newNode;
	vector<CPoint2Di> neighbor;
	neighbor.reserve(8);
	long int cost;

	currNode = new CAStarNode(start_, 0, computeHeuristic(start_));
	Q.push_back(currNode);
	make_heap(Q.begin(), Q.end(), SOfRawPtrGreater<CAStarNode>());

//	debug::printVectorPtr("Q-add   :", "", Q, false);
//	cout << endl;

	status.at(sub2Ind(start_, mapSize_)) = astar::open;
	visitedNodes.at(sub2Ind(start_, mapSize_)) = currNode;
	nCellsProcessed_++;

	do
	{
		currNode = Q.front();
//		cout << " CURR: " << (*currNode) << endl;
		pop_heap(Q.begin(), Q.end(), SOfRawPtrGreater<CAStarNode>());
		Q.pop_back();
		status.at(sub2Ind(currNode->getPoint(), mapSize_)) = astar::closed;

		if(currNode->getH() == 0)
		{
			pathFound = true;
		}
		else
		{
			getNeighbors(currNode->getPoint(), neighbor);
			for(vector<CPoint2Di>::iterator i = neighbor.begin(); i < neighbor.end(); ++i)
			{
				if (map_->isFree(*i) && (status.at(sub2Ind(*i, mapSize_)) != astar::closed))
				{
					cost = currNode->getG() + computeCost(currNode->getPoint(), *i);
					if(status.at(sub2Ind(*i, mapSize_)) == astar::notProcessed) //add to heap
					{
						newNode = new CAStarNode(*i, cost, computeHeuristic(*i), currNode);
						Q.push_back(newNode);
						push_heap(Q.begin(), Q.end(), SOfRawPtrGreater<CAStarNode>());
						make_heap(Q.begin(), Q.end(), SOfRawPtrGreater<CAStarNode>());

//						debug::printVectorPtr("Q-add   :", "", Q, false);
//						cout << endl;

						status.at(sub2Ind(*i, mapSize_)) = astar::open;
						visitedNodes.at(sub2Ind(*i, mapSize_)) = newNode;
						nCellsProcessed_++;
					}
					else //if(status.at(sub2Ind(*i, mapSize_)) == astar::open) // update heap
					{
						// Check g and update if necessary
						if (cost < visitedNodes.at(sub2Ind(*i, mapSize_))->getG())
						{
							visitedNodes.at(sub2Ind(*i, mapSize_))->setG(cost);
							visitedNodes.at(sub2Ind(*i, mapSize_))->setParent(currNode);
							make_heap(Q.begin(), Q.end(), SOfRawPtrGreater<CAStarNode>());

//							debug::printVectorPtr("Q-update:", "", Q, false);
//							cout << endl;

						}
					}
				}
			}
		}
	}
	while(!Q.empty() && !pathFound);

	ptime time2(microsec_clock::universal_time());
	time_duration diff(time2 - time1);
//	computationTime_ = diff.total_milliseconds() / 1000.0f;
	computationTime_ = diff.total_microseconds() / 1000000.0f;

	if(pathFound)
	{
		cout << "A*. Path found" << endl;
		length_ = static_cast<double>((visitedNodes.at(sub2Ind(goal_,mapSize_)))->getG()) / astar::costCorrection;
	}
	else
		cout << "A*. Path not found." << endl;

	if(plot_)
	{
		rfPlot_.copyTo(mapPlot_);
//		saveG("test/ASTAR-g.txt");
//		saveH("test/ASTAR-h.txt");
		if(pathFound)
		{
			//savePath("test/ASTAR-path.txt");
			savePathCoordinates("test/ASTAR-pathcoord.txt");
			plotPath();
		}
		// start
		cv::line(mapPlot_,Point2i(start_.x()-10, start_.y()),Point2i(start_.x()+10, start_.y()),cv::Scalar(0,0,255));
		cv::line(mapPlot_,Point2i(start_.x(), start_.y()-10),Point2i(start_.x(), start_.y()+10),cv::Scalar(0,0,255));

		// goal
		cv::line(mapPlot_,Point2i(goal_.x()-10, goal_.y()),Point2i(goal_.x()+10, goal_.y()),cv::Scalar(0,255,0));
		cv::line(mapPlot_,Point2i(goal_.x(), goal_.y()-10),Point2i(goal_.x(), goal_.y()+10),cv::Scalar(0,255,0));
		imshow("A*path", mapPlot_);
	}

	// free memory
	for(vector<CAStarNode*>::iterator i = visitedNodes.begin(); i < visitedNodes.end(); ++i)
		delete *i;
	visitedNodes.clear();
	for(vector<CAStarNode*>::iterator i = Q.begin(); i < Q.end(); ++i)
		if(*i == NULL) delete *i;
	Q.clear();

	//cout << "computePath ------------------------------FI--" << endl;
	return pathFound;
}

float CAStar::getComputationTime() const
{
	return computationTime_;
}

float CAStar::getLength() const
{
	return length_;
}

long int CAStar::getNCellsProcessed() const
{
	return nCellsProcessed_;
}

long int CAStar::computeHeuristic(const CPoint2Di& point)
{
	// Euclidean distance that takes into account 1 step cost between cells
	long int d1 = abs(point.x() - goal_.x());
	long int d2 = abs(point.y() - goal_.y());

	if(d1 < d2)
		swap(d1, d2);
	return (d2 * (cmap::diagonalCost * astar::costCorrection) + (d1-d2) * (cmap::straightCost * astar::costCorrection));
}

void CAStar::getNeighbors(const CPoint2Di& point, std::vector<CPoint2Di>& neighbor)
{
	int x = point.x(), y = point.y();
	neighbor.clear();

	if (map_->isInside(x, y + 1)) 		neighbor.push_back(CPoint2Di(x, y + 1));
	if (map_->isInside(x + 1, y + 1)) 	neighbor.push_back(CPoint2Di(x + 1, y + 1));
	if (map_->isInside(x + 1, y)) 		neighbor.push_back(CPoint2Di(x + 1, y));
	if (map_->isInside(x + 1, y - 1)) 	neighbor.push_back(CPoint2Di(x + 1, y - 1));
	if (map_->isInside(x, y - 1)) 		neighbor.push_back(CPoint2Di(x, y - 1));
	if (map_->isInside(x - 1, y - 1)) 	neighbor.push_back(CPoint2Di(x - 1, y - 1));
	if (map_->isInside(x - 1, y)) 		neighbor.push_back(CPoint2Di(x - 1, y));
	if (map_->isInside(x - 1, y + 1)) 	neighbor.push_back(CPoint2Di(x - 1, y + 1));
}

long int CAStar::computeCost(const CPoint2Di& n1, const CPoint2Di& n2)
{
	if(!map_->isFree(n1) || !map_->isFree(n2)) return cmap::maxCost;

	CPoint2Di p(n1);
	if(p.distance2(n2) == 1) return (cmap::straightCost * astar::costCorrection);
	return (cmap::diagonalCost * astar::costCorrection);
}

void CAStar::saveH(const string& filename)
{
	ofstream f;
	f.open(filename.c_str(), ios::out);

	int i, j;
	unsigned int idx = 0;
	for(i = 0; i < mapSize_.y(); i++)
	{
		for(j = 0; j < mapSize_.x(); j++)
		{
			(visitedNodes.at(idx))? f << setw(4) << visitedNodes.at(idx)->getH() << " ": f << setw(4) << " NaN ";
			idx++;
		}
		f << endl;
	}
	f.close();
}

void CAStar::saveG(const string& filename)
{
	ofstream f;
	f.open(filename.c_str(), ios::out);

	int i, j;
	unsigned int idx = 0;
	for(i = 0; i < mapSize_.y(); i++)
	{
		for(j = 0; j < mapSize_.x(); j++)
		{
			(visitedNodes.at(idx))? f << setw(4) << visitedNodes.at(idx)->getG() << " ": f << " NaN ";
			idx++;
		}
		f << endl;
	}
	f.close();
}


void CAStar::savePath(const string& filename)
{
/*
	CMap out(*map_);
	CCell cell;

	CAStarNodePtr node = visitedNodes.at(sub2Ind(goal_, mapSize_));
	while(node->getPoint() != start_)
	{
		cell = out.getCell(node->getPoint());
		cell.setOccupancy(cell::path);
		out.setCell(node->getPoint(), cell);

		node = node->getParent();
	}
	cell = out.getCell(node->getPoint());
	cell.setOccupancy(cell::path);
	out.setCell(node->getPoint(), cell);

	length_ = (visitedNodes.at(sub2Ind(goal_,mapSize_)))->getG() / astar::costCorrection;
	out.save(filename);
	*/
	cout << "SavePath: doing nothing" << endl;
}

void CAStar::plotPath()
{
	CAStarNode *node = visitedNodes.at(sub2Ind(goal_, mapSize_));
	Point2i p1, p2;
	do
	{
		p1.x = node->getPoint().x();
		p1.y = node->getPoint().y();

		p2.x = node->getParent()->getPoint().x();
		p2.y = node->getParent()->getPoint().y();
		cv::line(mapPlot_, p1, p2, cv::Scalar(255,0,255));
		node = node->getParent();
	}
	while(node->getPoint() != start_);
}


void CAStar::savePathCoordinates(const string& filename)
{
	ofstream f;
	f.open(filename.c_str());

	CAStarNode *node = visitedNodes.at(sub2Ind(goal_, mapSize_));
	while(node->getPoint() != start_)
	{
		f << node->getPoint() << endl;
		node = node->getParent();

	}
	f << node->getPoint() << endl;
	f.close();
}

CPoint2Di CAStar::getNextPose()
{
	CAStarNode *node = visitedNodes.at(sub2Ind(goal_, mapSize_));
	return node->getParent()->getPoint();
}
