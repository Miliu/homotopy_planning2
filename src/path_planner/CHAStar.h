// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CHAStar.h
 *
 *  Created on: 13/04/2010
 *  Homotopic A*
 *
 *  Modified on: 28/04/2010
 *  	- Used class CTString instead TString
 *      Author: Emili
 */

#ifndef CHASTAR_H_
#define CHASTAR_H_

#include "CPoint2D.h"
#include "CTStringExt.h"

typedef CPoint2D<int> CPoint2Di;
typedef CPoint2D<double> CPoint2Df;

class CHAStarNode
{

public:
	CHAStarNode(	const CPoint2Di& point,
					const long int& g,
					const long int& h,
					CTStringExt tPath)
	:
		point_(point),
		g_(g),
		h_(h),
		f_(g + h),
		parent_(NULL),
		tPath_(tPath)
	{}

	CHAStarNode(	const CPoint2Di& point,
					const long int& g,
					const long int& h,
					CTStringExt tPath,
					CHAStarNode* parent)
	:
		point_(point),
		g_(g),
		h_(h),
		f_(g + h),
		parent_(parent),
		tPath_(tPath)
	{}

	CHAStarNode(const CHAStarNode& n)
	:
		point_(n.point_),
		g_(n.g_),
		h_(n.h_),
		f_(n.g_ + n.h_),
		parent_(n.parent_),
		tPath_(n.tPath_)
	{}

	void operator=(const CHAStarNode& n)
	{
		point_ = n.point_;
		g_ = n.g_;
		h_ = n.h_;
		f_ = n.f_;
		parent_ = n.parent_;
		tPath_ = n.tPath_;
	}

	bool operator>(const CHAStarNode& n) const {return (f_ > n.f_);}

	CPoint2Di getPoint() const {return point_;}
	long int getF() const {return f_;}
	long int getH() const {return h_;}
	long int getG() const {return g_;}

	void setG(const long int g)
	{
		g_ = g;
		f_ = g_ + h_;
	}

	CHAStarNode* getParent() const {return parent_;}
	void setParent(CHAStarNode* parent) {parent_ = parent;}
	CTStringExt getTPath() const {return tPath_;}
	void setTPath(CTStringExt tPath) {tPath_ = tPath;}

	friend std::ostream& operator<<(std::ostream& cout, const CHAStarNode& n)
	{
		cout << "[" << n.getPoint() << "," << n.getG() << "+" << n.getH() << "=" << n.getF() << ",[" << n.getTPath() << "]]";
		return cout;
	}

private:
	CPoint2Di 		point_;
	long int 		g_;
	long int 		h_;
	long int 		f_;
	CHAStarNode* 	parent_;
	CTStringExt 	tPath_; 	// topological path from the initial point to the node
};

#include "CMap.h"
#include "SOfPtr.h"
#include "CRFSegment.h"

#include <vector>
#include <algorithm>
#include <time.h>
#include <boost/date_time/posix_time/posix_time.hpp>

namespace hastar
{
	enum EStatus{notProcessed = 0, open, closed};
	const long int costCorrection = 100; // useful to operate with integers
};

class CHAStar
{
public:
	CHAStar(CMapPtr map, const std::vector<CRFSegment>& segments, const cv::Mat& rf, const bool& plot);
	//~CHAStar();
	bool computePath(const CPoint2Di& start, const CPoint2Di& goal, const CTStringR& homotopyClass);

	float getComputationTime() const {return computationTime_;}
	float getLength() const {return length_;}
	const long int getNCellsProcessed() const {return nCellsProcessed_;}

private:
	long int euclideanDistance(const CPoint2Di& p1, const CPoint2Di& p2);
	long int computeHeuristic(const CPoint2Di& point);
	unsigned int sub2Ind(const CPoint2Di& sub, const CPoint2Di& size){return (sub.y() * size.x() + sub.x());};
	void getNeighbors(const CPoint2Di& point, std::vector<CPoint2Di>& neighbor);
	long int computeCost(const CPoint2Di& n1, const CPoint2Di& n2);

	void saveH(const std::string& filename);
	void saveG(const std::string& filename);
	void saveF(const std::string& filename);
	void savePath(const std::string& filename);
	void savePathCoordinates(const std::string& filename);

	bool updateTopologicalPath(const std::vector<CRFSegmentIndex>& intersection, CTStringExt& path);

	std::vector<CRFSegmentIndex> findIntersection(const CPoint2Di& p1, const CPoint2Di& p2, const std::vector<CRFSegment>& segments);
	CHAStarNode* ptrToNodeWithSameTPath(const unsigned int& i, const  CTStringExt& path);

	CMapPtr map_;
	CPoint2Di mapSize_;
	CPoint2Di start_, goal_;
	std::vector<CRFSegment> segments_;
	CTStringR homotopyClass_;

	std::vector<std::vector<hastar::EStatus> > status;
	std::vector<std::vector<CHAStarNode*> > visitedNodes_;

	float computationTime_;
	float length_;
	long int nCellsProcessed_;

	static int id_;

	bool plot_; // plot flag

	cv::Mat rfPlot_;
	cv::Mat mapPlot_;
	void plotPath();
};

#endif /* CHASTAR_H_ */
