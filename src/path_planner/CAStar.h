// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CAStar.h
 *
 *  Created on: 08/04/2010
 *  Modified on: 05/10/2010
 *  	- Switch to shared_prt to raw_ptr for efficiency purposes
 *  	- Q is sorted using make_heap
 *  Based on the CAStar of Javier Antich
 *      Author: Emili
 */

#ifndef CASTAR_H_
#define CASTAR_H_

#include "CPoint2D.h"
typedef CPoint2D<int> CPoint2Di;

class CAStarNode
{

public:
	CAStarNode(	const CPoint2Di& point,
				const long int& g,
				const long int& h)
	:
		point_(point),
		g_(g),
		h_(h),
		f_(g + h),
		parent_(NULL)
	{}

	CAStarNode(	const CPoint2Di& point,
				const long int& g,
				const long int& h,
				CAStarNode* parent)
	:
		point_(point),
		g_(g),
		h_(h),
		f_(g + h),
		parent_(parent)
	{}

	CAStarNode(const CAStarNode& n)
	:
		point_(n.point_),
		g_(n.g_),
		h_(n.h_),
		f_(n.g_ + n.h_),
		parent_(n.parent_)
	{}

	void operator=(const CAStarNode& n)
	{
		point_ = n.point_;
		g_ = n.g_;
		h_ = n.h_;
		f_ = n.f_;
		parent_ = n.parent_;
	}
	bool operator<(const CAStarNode& n) const {return (f_ < n.f_);}
	bool operator>(const CAStarNode& n) const {return (f_ > n.f_);}

	CPoint2Di getPoint() const {return point_;}
	long int getF() const {return f_;}
	long int getH() const {return h_;}
	long int getG() const {return g_;}

	void setG(const long int g)
	{
		g_ = g;
		f_ = g_ + h_;
	}

	CAStarNode* getParent() const {return parent_;}
	void setParent(CAStarNode* parent) {parent_ = parent;}

	friend std::ostream& operator<<(std::ostream& cout, const CAStarNode& n)
	{
		cout << "[" << n.getPoint() << "," << n.getG() << "+" << n.getH() << "=" << n.getF() << "]";
		return cout;
	}

private:
	CPoint2Di point_;
	long int g_;				// Path cost
	long int h_;				// Heuristic estimation
	long int f_;				// f = g + h
	CAStarNode* parent_;		// Backpointer

};

#include "CMap.h"
#include "SOfPtr.h"

#include <vector>
#include <list>
#include <queue>
#include <algorithm>
#include <limits.h>
#include <time.h>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/pending/mutable_queue.hpp>

namespace astar
{
	enum EStatus{notProcessed = 0, open, closed};
	const double costCorrection = 100; // useful to operate with integers
};

class CAStar
{
public:
	CAStar(CMapPtr map, const cv::Mat& rf, const bool& plot);
	~CAStar();
	bool computePath(const CPoint2Di& start, const CPoint2Di& goal);

	float getComputationTime() const;
	float getLength() const;
	long int getNCellsProcessed() const;

	CPoint2Di getNextPose();

private:
	long int computeHeuristic(const CPoint2Di& point);
	unsigned int sub2Ind(const CPoint2Di& sub, const CPoint2Di& size){return (sub.y() * size.x() + sub.x());};
	void getNeighbors(const CPoint2Di& point, std::vector<CPoint2Di>& neighbor);
	long int computeCost(const CPoint2Di& n1, const CPoint2Di& n2);

	void saveH(const std::string& filename);
	void saveG(const std::string& filename);
	void savePath(const std::string& filename);
	void savePathCoordinates(const std::string& filename);

	CMapPtr map_;
	CPoint2Di mapSize_;
	CPoint2Di start_, goal_;

	std::vector<astar::EStatus> status;
	std::vector<CAStarNode*> visitedNodes;

	float computationTime_;
	float length_;
	long int nCellsProcessed_;

	bool plot_; // plot flag

	cv::Mat rfPlot_;
	cv::Mat mapPlot_;
	void plotPath();
};

#endif /* CASTAR_H_ */
