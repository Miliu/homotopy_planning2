// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CHBUG.h
 *
 *  Created on: 16/12/2010
 *      Author: emili
 */

#ifndef CHBUG_H_
#define CHBUG_H_

#include "CTStringExt.h"
#include "CMap.h"
#include "CRFPoint.h"
#include "CTSpace.h"

#include <vector>
#include <algorithm>
#include <time.h>
#include <boost/date_time/posix_time/posix_time.hpp>

namespace hbug
{
	enum EDirection {left = 0, right, parallel};
	enum EPointType {hit = 0, leave, other};
}

class CHLPoint
{
public:
	CHLPoint()
	:
		obstacle_(-1),
		point_(),
		type_(hbug::other)
	{}

	CHLPoint(	const int& obstacle,
				const CPoint2Di& point,
				const hbug::EPointType& type)
	:
		obstacle_(obstacle),
		point_(point),
		type_(type)
	{}

	CHLPoint(	const CHLPoint& p)
	:
		obstacle_(p.obstacle_),
		point_(p.point_),
		type_(p.type_)
	{}

	int getObstacle() const {return obstacle_;}
	CPoint2Di getPoint() const {return point_;}
	hbug::EPointType getType() const {return type_;}

	void setHLPoint(	const int& obstacle,
						const CPoint2Di& point,
						const hbug::EPointType& type)
	{
		obstacle_ = obstacle;
		point_ = point;
		type_ = type;
	}

	friend std::ostream& operator<<(std::ostream& cout, const CHLPoint& p)
	{
		cout << p.getObstacle() << ":" << p.getPoint();
		if(p.getType() == hbug::hit) cout << " hit";
		else if(p.getType() == hbug::leave) cout << " leave";
		else cout << " other";
		return cout;
	}

private:
	int 				obstacle_;
	CPoint2Di 			point_;
	hbug::EPointType	type_; // point type
};

class CHBugNode : public CHLPoint
{
public:
	CHBugNode(	const int& obstacle,
				const CPoint2Di& point,
				const hbug::EPointType& type,
				const CEdge& edge,
				const int& index)
	:
		CHLPoint(obstacle, point, type),
		edge_(edge),
		index_(index)
	{}

	CHBugNode(	const CHLPoint& p,
				const CEdge& edge,
				const int& index)
	:
		CHLPoint(p),
		edge_(edge),
		index_(index)
	{}

	CHBugNode(	const CHBugNode& n)
	:
		CHLPoint(n.getObstacle(), n.getPoint(), n.getType()),
		edge_(n.edge_),
		index_(n.index_)
	{}

	CEdge getEdge() const {return edge_;}
	int getIndex() const {return index_;}

	friend std::ostream& operator<<(std::ostream& cout, const CHBugNode& n)
	{
		cout << n.getObstacle() << ":" << n.getPoint();
		n.getType() == hbug::hit ? 	cout << "   hit;": cout << " leave;";
		cout << n.getEdge() << ";" << n.getIndex();
		return cout;
	}

private:
	CEdge 				edge_;
	int 				index_;
};

class CHBugNodeWithEdgeNot
{
public:
	CHBugNodeWithEdgeNot(const CEdge& e)
	:
		e_(e)
	{}
	bool operator() (const CHBugNode& n){return (n.getEdge() != e_);}

private:
	CEdge e_;
};

class CHBugObstacle
{
public:
	CHBugObstacle(	const int& id,
					const CPoint2Di& begin,
					const CPoint2Di& end,
					const hbug::EDirection& direction)
	:
		id_(id),
		begin_(begin),
		end_(end),
		direction_(direction)
	{}

	int getId() const {return id_;}
	CPoint2Di getBegin() const {return begin_;}
	CPoint2Di getEnd() const {return end_;}
	hbug::EDirection getDirection() const {return direction_;}


	friend std::ostream& operator<<(std::ostream& cout, const CHBugObstacle& o)
	{
		cout << o.getId() << ":" << o.getBegin() << "," << o.getEnd();
		if(o.getDirection() == hbug::left)
			cout << " lft";
		else if(o.getDirection() == hbug::right)
			cout << " rgt";
		else
			cout << " par";
		return cout;
	}

private:
	int 				id_;
	CPoint2Di 			begin_;
	CPoint2Di 			end_;
	hbug::EDirection	direction_;
};

class CHBug
{
public:
	CHBug(	CMapPtr map,
			const TSegmentMap& segments,
			const cv::Mat& rf,
			const bool& plot = false);

	bool computePath(	const std::vector<CRFPoint>& lbPath,
						const CTStringR& homotopyClass);

	float getComputationTime() const {return computationTime_;}
	float getLength() const {return length_;}
	const long int getNCellsProcessed() const {return nCellsProcessed_;}

private:
	void buildPath(const std::vector<CHBugObstacle>& o);
	std::vector<CHBugObstacle> computeObstacles(const std::vector<CHBugNode>& n);
	std::vector<CHBugNode> computeHBugPoints(const std::vector<CRFPoint>& path);
	bool isHLPoint(const CRFPoint& p, CHLPoint& o);

	hbug::EDirection computeDirection(	const std::vector<CHBugNode>& n,
										std::vector<CHBugNode>::const_iterator& hit,
										std::vector<CHBugNode>::const_iterator& leave,
										std::vector<CHBugNode>::const_iterator& currl,
										std::vector<CHBugNode>::const_iterator& nextl);

	std::vector<CHLPoint> computeHLPoints(	const CRFPoint& p1,
											const CRFPoint& p2,
											bool& insideObstacle);
	std::vector<Point2i> computeContour(const CHBugObstacle& o);
	unsigned int l_kEdgesCounter(	const unsigned int& subindex,
									const unsigned int& position,
									unsigned int& first);
	hbug::EDirection getDirection(	const CPoint2Di& a,
									const CPoint2Di& b,
									const CPoint2Di& c);
	void savePathCoordinates(const std::string& filename);
	void plotPath();

	float computeA8distance(	const CPoint2Di& p1,
								const CPoint2Di& p2);

	CMapPtr map_;
	//CPoint2Di mapSize_;
	CPoint2Di start_, goal_;
	std::vector<CRFPoint> lbPath_; // lower bound path
	TSegmentMap segments_;
	std::vector<ccl::SObstacleCoord> obstacleCoord_;
	CTStringR homotopyClass_;

	float computationTime_;
	float length_;
	long int nCellsProcessed_;

	static int id_;

	cv::Mat label_;
	bool plot_; // plot flag

	cv::Mat rfPlot_;
	cv::Mat mapPlot_;

	std::vector<Point2i> path_;
};

#endif /* CHBUG_H_ */
