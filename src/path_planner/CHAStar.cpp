// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CHAStar.cpp
 *
 *  Created on: 13/04/2010
 *      Author: Emili
 */

#include "CHAStar.h"
#include "debug.h"

using namespace std;
using namespace boost::posix_time;

int CHAStar::id_ = 1;

CHAStar::CHAStar(CMapPtr map, const vector<CRFSegment>& segments, const cv::Mat& rf, const bool& plot)
:
		map_(map),
		mapSize_(map->getVSizeCell()),
		segments_(segments),
		plot_(plot),
		rfPlot_(rf)
{
	if(plot_)
	{
		CPoint2Di size = map_->getVSizeCell();
		rfPlot_.copyTo(mapPlot_);
		cv::namedWindow("HA*path", CV_WINDOW_AUTOSIZE);
		cvMoveWindow("HA*path", 4 * size.x() + 100, 100);
		imshow("HA*path", mapPlot_);
	}
}

bool CHAStar::computePath(const CPoint2Di& start, const CPoint2Di& goal, const CTStringR& homotopyClass)
{
	start_ = start;
	goal_ = goal;
	homotopyClass_ = homotopyClass;

	bool pathFound = false;
	length_ = 0;
	nCellsProcessed_ = 0;

	if(!map_->isFree(start_))
	{
		//cout << "START POINT OCCUPIED" << start_ << " value: " << static_cast<int>(map_->getOccupancy(start_)) << endl;
		throw runtime_error("Start point occupied.");
	}
	if(!map_->isFree(goal_))
	{
		//cout << "GOAL POINT OCCUPIED" << endl;
		throw runtime_error("Goal point occupied.");
	}

	visitedNodes_.reserve(map_->getSizeCell());
	visitedNodes_.resize(map_->getSizeCell());

	vector<CHAStarNode*> Q;

	ptime time1(microsec_clock::universal_time());

	CHAStarNode *currNode, *newNode, *prevNode;
	vector<CPoint2Di> neighbor;
	neighbor.reserve(8);
	long int cost, h;
	//CEdgePtr nextSegmentId;
	vector<CRFSegment>::iterator nextSegment;
	vector<CRFSegmentIndex> I;

	CTStringExt tpath;
	CTStringExt eString; //empty string

	h = computeHeuristic(start_);
	currNode = new CHAStarNode(start_, 0, h, eString);
	Q.push_back(currNode);
	make_heap(Q.begin(), Q.end(), SOfRawPtrGreater<CHAStarNode>());

//	debug::printVectorPtr("Q-add   :", "", Q, false);
//	cout << endl;

	//status.at(sub2Ind(start_, mapSize_)) = hastar::open;
	visitedNodes_.at(sub2Ind(start_, mapSize_)).push_back(currNode);
	nCellsProcessed_++;

//	int count = 0;
	unsigned int idx;
	do
	{
		currNode = Q.front();
		pop_heap(Q.begin(), Q.end(), SOfRawPtrGreater<CHAStarNode>());
		Q.pop_back();
		//cout << "CURR: " << *currNode << endl;

		if(currNode->getH() == 0 && currNode->getTPath().size() == homotopyClass_.size())//currNode->getTPath() == homotopyClass_)
		{
			pathFound = true;
		}
		else
		{
			getNeighbors(currNode->getPoint(), neighbor);
			for(vector<CPoint2Di>::iterator i = neighbor.begin(); i < neighbor.end(); ++i)
			{
				if (map_->isFree(*i))
				{
					I = findIntersection(currNode->getPoint(), *i, segments_);
					tpath = currNode->getTPath();
//					cout << "prevTPath: " << tpath << endl;
					if(updateTopologicalPath(I, tpath))
					{
//						cout << "currTPath: " << tpath << endl;
						idx = sub2Ind(*i, mapSize_);
						prevNode = ptrToNodeWithSameTPath(idx, tpath);

						cost = currNode->getG() + computeCost(currNode->getPoint(), *i);
						if(prevNode == NULL)
						{
							//cout << "no prevNode" << endl;
							h = computeHeuristic(*i);
							newNode = new CHAStarNode(*i, cost, h, tpath, currNode);

							Q.push_back(newNode);
							push_heap(Q.begin(), Q.end(), SOfRawPtrGreater<CHAStarNode>());
							make_heap(Q.begin(), Q.end(), SOfRawPtrGreater<CHAStarNode>());

//							debug::printVectorPtr("Q-add   :", "", Q, false);
//							cout << endl;
//							getchar();

							visitedNodes_.at(idx).push_back(newNode);
							nCellsProcessed_++;
						}
						else
						{
							//cout << "si prevNode" << endl;
							if (cost < prevNode->getG())
							{
								prevNode->setG(cost);
								prevNode->setTPath(tpath);
								prevNode->setParent(currNode);
								make_heap(Q.begin(), Q.end(), SOfRawPtrGreater<CHAStarNode>());

//								debug::printVectorPtr("Q-update:", "", Q, false);
//								cout << endl;
//								getchar();
							}
//							else
//							{
//								cout << "no update" << endl;
//							}
						}

					}
				}
			}
			//uncoment to generate images for video
//			if((count % 100) == 0)
//			{
//				std::ostringstream countStream;
//				countStream << "_" << count;
//				cout << "COUNT: " << count << endl;
//				saveF("test/HASTAR-f" + countStream.str() + ".txt");
//			}
//			count++;

		}
	}
	while(!Q.empty() && !pathFound);

	ptime time2(microsec_clock::universal_time());
	time_duration diff(time2 - time1);
	//computationTime_ = diff.total_milliseconds() / 1000.0f;
	computationTime_ = diff.total_microseconds() / 1000000.0f;

	std::ostringstream idStream;
	idStream << "_" << id_;


	if(pathFound)
	{
		length_ = static_cast<double>((visitedNodes_.at(sub2Ind(goal_,mapSize_)).back())->getG()) / hastar::costCorrection;
	}
	else
		cout << "HA*. Path not found." << endl;

	if(pathFound)
	{
		savePathCoordinates("test/HASTAR-pathcoord" + idStream.str() + ".txt");
	}

	if(plot_)
	{
		cout << "HA*. Path found" << endl;
		rfPlot_.copyTo(mapPlot_);

		//saveG("test/HASTAR-g" + idStream.str() + ".txt");
		//saveG("test/HASTAR-g_FINAL.txt");
		saveF("test/HASTAR-f_FINAL.txt");
		//saveH("test/HASTAR-h" + idStream.str() + ".txt");

		if(pathFound)
		{
			//savePath("test/HASTAR-path" + idStream.str() + ".txt");
			savePathCoordinates("test/HASTAR-pathcoord" + idStream.str() + ".txt");
			plotPath();
		}
		// start
		cv::line(mapPlot_,Point2i(start_.x()-10, start_.y()),Point2i(start_.x()+10, start_.y()),cv::Scalar(0,0,255));
		cv::line(mapPlot_,Point2i(start_.x(), start_.y()-10),Point2i(start_.x(), start_.y()+10),cv::Scalar(0,0,255));

		// goal
		cv::line(mapPlot_,Point2i(goal_.x()-10, goal_.y()),Point2i(goal_.x()+10, goal_.y()),cv::Scalar(0,255,0));
		cv::line(mapPlot_,Point2i(goal_.x(), goal_.y()-10),Point2i(goal_.x(), goal_.y()+10),cv::Scalar(0,255,0));
		imshow("HA*path", mapPlot_);
	}
	id_++;

	// free memory
	for(vector<vector<CHAStarNode*> >::iterator i = visitedNodes_.begin(); i < visitedNodes_.end(); ++i)
	{
		for(vector<CHAStarNode*>::iterator j = i->begin(); j < i->end(); ++j)
		{
			delete *j;
		}
		i->clear();
	}
	visitedNodes_.clear();
	for(vector<CHAStarNode*>::iterator i = Q.begin(); i < Q.end(); ++i)
		if(*i == NULL) delete *i;
	Q.clear();

	return pathFound;
}


long int CHAStar::computeHeuristic(const CPoint2Di& point)
{
	// Euclidean distance that takes into account 1 step cost between cells
	long int d1 = abs(point.x() - goal_.x());
	long int d2 = abs(point.y() - goal_.y());

	if(d1 < d2)
		swap(d1, d2);
	return (d2 * (cmap::diagonalCost * hastar::costCorrection) + (d1-d2) * (cmap::straightCost * hastar::costCorrection));
}

void CHAStar::getNeighbors(const CPoint2Di& point, std::vector<CPoint2Di>& neighbor)
{
	int x = point.x(), y = point.y();
	neighbor.clear();

	if (map_->isInside(x, y + 1)) 		neighbor.push_back(CPoint2Di(x, y + 1));
	if (map_->isInside(x + 1, y + 1)) 	neighbor.push_back(CPoint2Di(x + 1, y + 1));
	if (map_->isInside(x + 1, y)) 		neighbor.push_back(CPoint2Di(x + 1, y));
	if (map_->isInside(x + 1, y - 1)) 	neighbor.push_back(CPoint2Di(x + 1, y - 1));
	if (map_->isInside(x, y - 1)) 		neighbor.push_back(CPoint2Di(x, y - 1));
	if (map_->isInside(x - 1, y - 1)) 	neighbor.push_back(CPoint2Di(x - 1, y - 1));
	if (map_->isInside(x - 1, y)) 		neighbor.push_back(CPoint2Di(x - 1, y));
	if (map_->isInside(x - 1, y + 1)) 	neighbor.push_back(CPoint2Di(x - 1, y + 1));
}

long int CHAStar::computeCost(const CPoint2Di& n1, const CPoint2Di& n2)
{
	if(!map_->isFree(n1) || !map_->isFree(n2)) return cmap::maxCost;

	CPoint2Di p(n1);
	if(p.distance2(n2) == 1) return cmap::straightCost * hastar::costCorrection;
	return cmap::diagonalCost * hastar::costCorrection;
}

void CHAStar::saveH(const string& filename)
{
	ofstream f;
	f.open(filename.c_str(), ios::out);

	int i, j;
	unsigned int idx = 0;
	for(i = 0; i < mapSize_.y(); i++)
	{
		for(j = 0; j < mapSize_.x(); j++)
		{
			// only saved last node info
			(!visitedNodes_.at(idx).empty())? f << setw(4) << visitedNodes_.at(idx).back()->getH() << " ": f << setw(4) << " NaN ";
			idx++;
		}
		f << endl;
	}
	f.close();
}

void CHAStar::saveF(const string& filename)
{
	ofstream f;
	f.open(filename.c_str(), ios::out);

	int i, j;
	unsigned int idx = 0;
	for(i = 0; i < mapSize_.y(); i++)
	{
		for(j = 0; j < mapSize_.x(); j++)
		{
			// only saved last node info
			(!visitedNodes_.at(idx).empty())? f << setw(4) << visitedNodes_.at(idx).back()->getH()+visitedNodes_.at(idx).back()->getG() << " ": f << setw(4) << " NaN ";
			idx++;
		}
		f << endl;
	}
	f.close();
}

void CHAStar::saveG(const string& filename)
{
	ofstream f;
	f.open(filename.c_str(), ios::out);

	int i, j;
	unsigned int idx = 0;
	for(i = 0; i < mapSize_.y(); i++)
	{
		for(j = 0; j < mapSize_.x(); j++)
		{
			// only saved last node info
			(!visitedNodes_.at(idx).empty())? f << setw(4) << visitedNodes_.at(idx).back()->getG() << " ": f << " NaN ";
			idx++;
		}
		f << endl;
	}
	f.close();
}

void CHAStar::savePath(const string& filename)
{
	/*
	CMap out(*map_);
	CCell cell;

	CHAStarNodePtr node = visitedNodes_.at(sub2Ind(goal_, mapSize_));
	while(node->getPoint() != start_)
	{
		cell = out.getCell(node->getPoint());
		cell.setOccupancy(cell::path);
		out.setCell(node->getPoint(), cell);

		node = node->getParent();
	}
	cell = out.getCell(node->getPoint());
	cell.setOccupancy(cell::path);
	out.setCell(node->getPoint(), cell);

	length_ = (visitedNodes_.at(sub2Ind(goal_,mapSize_)))->getG() / hastar::costCorrection;
	out.save(filename);
	*/
	cout << "SavePath: doing nothing" << endl;
}

void CHAStar::savePathCoordinates(const string& filename)
{
	ofstream f;
	f.open(filename.c_str());

	CHAStarNode *node = visitedNodes_.at(sub2Ind(goal_, mapSize_)).back();
	while(node->getPoint() != start_)
	{
		f << node->getPoint() << endl;
		node = node->getParent();

	}
	f << node->getPoint() << endl;
	f.close();
}

void CHAStar::plotPath()
{
	CHAStarNode *node = visitedNodes_.at(sub2Ind(goal_, mapSize_)).back();
	Point2i p1, p2;
	do
	{
		p1.x = node->getPoint().x();
		p1.y = node->getPoint().y();

		p2.x = node->getParent()->getPoint().x();
		p2.y = node->getParent()->getPoint().y();
		cv::line(mapPlot_, p1, p2, cv::Scalar(255,0,255));
		node = node->getParent();
	}
	while(node->getPoint() != start_);
}

//bool CHAStar::updateTopologicalPath(const vector<CSegmentDistance> intersection, CTStringExt& path)
//{
//	// No intersections
//	if(intersection.empty())
//		return true;
//
//	// Too much intersections to generate homotopyClass_
//	if(	path.size() == homotopyClass_.size() ||
//		(path.size() + intersection.size()) > homotopyClass_.size())
//		return false;
//
//
//	CTStringExt pathTmp = path;
//	CEdgePtr next;
//	bool pathOk = true;
//	vector<CSegmentDistance>::const_iterator i = intersection.begin();
//	while(pathOk && i < intersection.end())
//	{
//		next = homotopyClass_.at(pathTmp.size());
//		if(*(i->getIterator()->getEdge()) == *next)
//			pathTmp.add(next);
//		else
//			pathOk = false;
//
//		i++;
//	}
//	if(pathOk)
//	{
//		path = pathTmp;
//		return true;
//	}
//	return false;
//}

bool CHAStar::updateTopologicalPath(const vector<CRFSegmentIndex>& intersection, CTStringExt& path)
{
//	cout << "updateTP------------------" << endl;
//	debug::printVector("I:", "", intersection, false);
//	cout << " -> tpath: " << path << endl;
	// No intersections
	if(intersection.empty())
		return true;

	// Too much intersections to generate homotopyClass_
	if(	path.size() == homotopyClass_.size() ||
		(path.size() + intersection.size()) > homotopyClass_.size())
		return false;

	CTStringExt pathTmp = path;
//	cout << "is reversed:" << pathTmp.isReversed() << endl;
	//CEdgePtr next;
	bool pathOk = true;
	unsigned int pos;

	vector<CRFSegmentIndex>::const_iterator i = intersection.begin();
	while(pathOk && i < intersection.end())
	{
		pos = pathTmp.size();
//		cout << "pos: " << pos << endl;

		if(	!pathTmp.isReversed() && homotopyClass_.isReversible() &&
			homotopyClass_.inReversibleSubstring(pos))
		{
//			cout << "opcio1" << endl;
			// check the two possible candidate
			if(segments_.at(i->getIndex()).getEdge() == homotopyClass_.at(pos))
			{
//				cout << "----1" << endl;
				pathTmp.add(homotopyClass_.at(pos));
			}
			else if(segments_.at(i->getIndex()).getEdge() == homotopyClass_.atR(pos))
			{
//				cout << "----2" << endl;
				pathTmp.add(homotopyClass_.atR(pos));
				pathTmp.setReversed(true);
			}
			else
			{
//				cout << "----3" << endl;
				pathOk = false;
			}
		}
		else if(pathTmp.isReversed() && homotopyClass_.inReversibleSubstring(pos))
		{
//			cout << "opcio2" << endl;
			// check candidate of the reverse substring
			if(segments_.at(i->getIndex()).getEdge() == homotopyClass_.atR(pos))
				pathTmp.add(homotopyClass_.atR(pos));
			else
				pathOk = false;
		}
		else //if(!pathTmp.isReversed())
		{
//			cout << "opcio3" << endl;
			// out of the reversible substring
			if(segments_.at(i->getIndex()).getEdge() == homotopyClass_.at(pos))
				pathTmp.add(homotopyClass_.at(pos));
			else
				pathOk = false;
		}
//		if(	homotopyClass_.isReversible() &&
//			homotopyClass_.inReversibleSubstring(pathTmp.size()) &&
//			!pathTmp.isReversed())
//		{
//
//			else
//				pathOk = false;
//		}
//		else if(*(i->getIterator()->getEdge()) == *homotopyClass_.at(pathTmp.size()))
//			pathTmp.add(homotopyClass_.at(pathTmp.size()));
//		else
//			pathOk = false;


//		next = homotopyClass_.at(pathTmp.size());
//		if(*(i->getIterator()->getEdge()) == *next)
//			pathTmp.add(next);
//		else
//			pathOk = false;

		i++;
	}

	if(pathOk)
	{
		path = pathTmp;
//		cout << "updateTP--------------FI true----" << endl;
		return true;
	}
//	cout << "updateTP--------------FI false----" << endl;
	return false;
}

vector<CRFSegmentIndex>
CHAStar::findIntersection(const CPoint2Di& p1, const CPoint2Di& p2, const vector<CRFSegment>& segments)
{
	CPoint2Df point;
	vector<CRFSegmentIndex> result;

	double distance;
	for(vector<CRFSegment>::const_iterator  i = segments.begin(); i < segments.end(); ++i)
	{
		//cout << "Segment: " << *i << endl;
		if(util::intersection<double, int>(p1, p2, i->getP1(), i->getP2(), point))
		{
			distance = point.distance(CPoint2Df(p1.x(), p1.y()));
			if(distance > 0.0001)
				result.push_back(CRFSegmentIndex(i - segments.begin(), distance * hastar::costCorrection));
		}
	}
	sort(result.begin(), result.end());
	return result;
}

CHAStarNode*
CHAStar::ptrToNodeWithSameTPath(const unsigned int& idx, const  CTStringExt& path)
{
//	cout << "ptrToNode -----------------" << endl;
	vector<CHAStarNode*>::const_reverse_iterator i = visitedNodes_.at(idx).rbegin();
	while(i < visitedNodes_.at(idx).rend())
	{
		if(path.size() == (*i)->getTPath().size())
		{
//			cout << "ptrToNode -------------FI1----" << endl;
			return *i;
		}
		else if(path.size() < (*i)->getTPath().size())
			++i;
		else
			break;
	}
//	cout << "ptrToNode --------------FI2---" << endl;
	return NULL;
}
