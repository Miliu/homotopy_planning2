// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CHBUG.cpp
 *
 *  Created on: 16/12/2010
 *      Author: emili
 */

#include "CHBug.h"
#include "debug.h"

using namespace std;
using namespace boost::posix_time;

int CHBug::id_ = 1;

CHBug::CHBug(	CMapPtr map,
				const TSegmentMap& segments,
				const cv::Mat& rf,
				const bool& plot)
:
		map_(map),
		//mapSize_(map->getVSizeCell()),
		segments_(segments),
		obstacleCoord_(map->getObstacleCoordinates()),
		label_(map->getCCLabel()),
		plot_(plot),
		rfPlot_(rf)
{
	if(plot_)
	{
		CPoint2Di size = map_->getVSizeCell();
		rfPlot_.copyTo(mapPlot_);
		cv::namedWindow("HBugPath", CV_WINDOW_AUTOSIZE);
		//cvMoveWindow("HBugPath", 4 * size.x() + 100, 100);
		cvMoveWindow("HBugPath", 2 * size.x() + 100, 2 * size.y() + 100);
		imshow("HBugPath", mapPlot_);
	}

	// XAPUSSA PER CALCULAR COSTOS Bug2 sense impelmentar --------BORRAM
/*	Point2i h1(500, 163);
	Point2i l1(500, 233);
	Point2i h2(500, 454);
	Point2i l2(500, 490);
	Point2i h3(500, 661);
	Point2i l3(500, 822);
	vector<Point2i>::iterator it1, it2;

	cout << "SIZE: --------: " << obstacleCoord_.size() << endl;

	for(vector<Point2i>::iterator i = obstacleCoord_.at(10).externalContour_.begin();
					i <= obstacleCoord_.at(10).externalContour_.end(); ++i)
	{
		if(i->x == 500)
			cout << i->x << ", " << i->y << " " ;
	}
	cout << endl;
	it1 = find(obstacleCoord_.at(2).externalContour_.begin(), obstacleCoord_.at(2).externalContour_.end(), h1);
	if(it1 != obstacleCoord_.at(2).externalContour_.end())
		cout << "h1: " << it1 - obstacleCoord_.at(2).externalContour_.begin() << endl;
	else
		cout << "NO TROBAT" << endl;
	it2 = find(obstacleCoord_.at(2).externalContour_.begin(), obstacleCoord_.at(2).externalContour_.end(), l1);
	if(it2 != obstacleCoord_.at(2).externalContour_.end())
		cout << "l1: " << it2 - obstacleCoord_.at(2).externalContour_.begin() << endl;
	else
		cout << "NO TROBAT" << endl;
	cout << "Obst2: Total, dist h-l:" << obstacleCoord_.at(2).externalContour_.size() << ", " << it2 - it1 << endl;

	cout << endl;
		it1 = find(obstacleCoord_.at(7).externalContour_.begin(), obstacleCoord_.at(7).externalContour_.end(), h2);
		if(it1 != obstacleCoord_.at(7).externalContour_.end())
			cout << "h2: " << it1 - obstacleCoord_.at(7).externalContour_.begin() << endl;
		else
			cout << "NO TROBAT" << endl;
		it2 = find(obstacleCoord_.at(7).externalContour_.begin(), obstacleCoord_.at(7).externalContour_.end(), l2);
		if(it2 != obstacleCoord_.at(7).externalContour_.end())
			cout << "l2: " << it2 - obstacleCoord_.at(7).externalContour_.begin() << endl;
		else
			cout << "NO TROBAT" << endl;
		cout << "Obst7: Total, dist h-l:" << obstacleCoord_.at(7).externalContour_.size() << ", " << it1 - it2 << endl;


		cout << endl;
				it1 = find(obstacleCoord_.at(10).externalContour_.begin(), obstacleCoord_.at(10).externalContour_.end(), h3);
				if(it1 != obstacleCoord_.at(10).externalContour_.end())
					cout << "h3: " << it1 - obstacleCoord_.at(10).externalContour_.begin() << endl;
				else
					cout << "NO TROBAT" << endl;
				it2 = find(obstacleCoord_.at(10).externalContour_.begin(), obstacleCoord_.at(10).externalContour_.end(), l3);
				if(it2 != obstacleCoord_.at(10).externalContour_.end())
					cout << "l3: " << it2 - obstacleCoord_.at(10).externalContour_.begin() << endl;
				else
					cout << "NO TROBAT" << endl;
				cout << "Obst10: Total, dist h-l:" << obstacleCoord_.at(10).externalContour_.size() << ", " << it1 - it2 << endl;
//	contour.insert(contour.end(), idx_b, obstacleCoord_.at(o.getId()).externalContour_.end());

	getchar();
	*/
}

bool
CHBug::computePath(	const vector<CRFPoint>& lbPath,
					const CTStringR& homotopyClass)
{
	lbPath_ = lbPath;
	homotopyClass_ = homotopyClass;

	length_ = 0;
	nCellsProcessed_ = 0;
	start_ = lbPath_.front();
	goal_ = lbPath_.back();
	if(!map_->isFree(start_))	throw runtime_error("Start point occupied.");
	if(!map_->isFree(goal_))	throw runtime_error("Goal point occupied.");
	path_.clear();

	ptime time1(microsec_clock::universal_time());

	if(plot_)
	{
		for(vector<CRFPoint>::iterator j = lbPath_.begin(); j < lbPath_.end() - 1; ++j)
			cv::line(mapPlot_, Point2i(j->x(), j->y()), Point2i((j+1)->x(), (j+1)->y()), cv::Scalar(0, 127, 255), 1); //taronja
	}

#ifdef _NDEBUG
	debug::printVector("lbPath:", "", lbPath_);
#endif

	vector<CHBugNode> nodes = computeHBugPoints(lbPath);
#ifdef _NDEBUG
	debug::printVector("nodes:", "", nodes);
	cout << "------------------------------------------------------------------------------" << endl;
#endif

	vector<CHBugObstacle> obstacles = computeObstacles(nodes);
#ifdef _NDEBUG
	debug::printVector("obstacles:", "", obstacles);
	cout << "------------------------------------------------------------------------------" << endl;
#endif

	buildPath(obstacles);

	ptime time2(microsec_clock::universal_time());
	time_duration diff(time2 - time1);
	//computationTime_ = diff.total_milliseconds() / 1000.0f;
	computationTime_ = diff.total_microseconds() / 1000000.0f;

	std::ostringstream idStream;
	idStream << "_" << id_;

	savePathCoordinates("test/HBug-pathcoord" + idStream.str() + ".txt");
	if(plot_)
	{
		cout << "HBug. Path found" << endl;
		//rfPlot_.copyTo(mapPlot_);

		savePathCoordinates("test/HBug-pathcoord" + idStream.str() + ".txt");
		plotPath();

		// start
		cv::line(mapPlot_,Point2i(start_.x()-10, start_.y()),Point2i(start_.x()+10, start_.y()),cv::Scalar(0,0,255));
		cv::line(mapPlot_,Point2i(start_.x(), start_.y()-10),Point2i(start_.x(), start_.y()+10),cv::Scalar(0,0,255));

		// goal
		cv::line(mapPlot_,Point2i(goal_.x()-10, goal_.y()),Point2i(goal_.x()+10, goal_.y()),cv::Scalar(0,255,0));
		cv::line(mapPlot_,Point2i(goal_.x(), goal_.y()-10),Point2i(goal_.x(), goal_.y()+10),cv::Scalar(0,255,0));
		imshow("HBugPath", mapPlot_);
	}
	id_++;
	return true;
}

vector<CHBugNode>
CHBug::computeHBugPoints(const vector<CRFPoint>& path)
{
	vector<CHBugNode> p;

	bool insideObstacle = false;
	for(vector<CRFPoint>::const_iterator i = path.begin(); i < path.end() - 1; ++i)
	{
//		cout << "idx: " << i - path.begin() << " ->";
		vector<CHLPoint> pts = computeHLPoints(*i, *(i + 1), insideObstacle);
		vector<CEdge> edge(pts.size());
		vector<int> index(pts.size(), -1);

		if(pts.empty())
		{
//			cout << "--point to be added " << i - path.begin() << " - " << (i+1) - path.begin() << ": " << endl;

			CHLPoint aux;
			if(isHLPoint(*i, aux))
			{
//				cout << "P1 : " << aux << endl;
				pts.push_back(aux);
				edge.push_back(path.at(i - path.begin()).getEdge());
				index.push_back(path.at(i - path.begin()).getIndex());
//				auxEdge = path.at(i - path.begin()).getEdge();
//				auxIndex = path.at(i - path.begin()).getIndex();

			}
			//if(isHLPoint(*(i + 1), aux)) cout << "P2 : " << aux << endl;
		}

//		cout << "POINTS: " << *i << ", " << *(i+1) << "(" << pts.size() << ")" << endl;

		// can be optimized ??
//		for(vector<CHLPoint>::iterator j = pts.begin(); j < pts.end(); ++j)
//		{
//			if(j->getPoint() == *i)
//			{
//				edge.at(j - pts.begin()) = path.at(i - path.begin()).getEdge();
//				index.at(j - pts.begin()) = path.at(i - path.begin()).getIndex();
//			}
//			if(j->getPoint() == *(i+1))
//			{
//				edge.at(j - pts.begin()) = path.at(i + 1 - path.begin()).getEdge();
//				index.at(j - pts.begin()) = path.at(i + 1 - path.begin()).getIndex();
//			}
//		}

		if(!pts.empty())
		{
			if(pts.front().getPoint() == *i)
			{
				edge.at(0) = path.at(i - path.begin()).getEdge();
				index.at(0) = path.at(i - path.begin()).getIndex();
			}
			if(pts.back().getPoint() == *(i+1))
			{
				edge.at(pts.size() - 1) = path.at(i + 1 - path.begin()).getEdge();
				index.at(pts.size() - 1) = path.at(i + 1 - path.begin()).getIndex();
			}
//			cout << "!pts.empty" << endl;
		}
//		else
//			cout << "pts.empty" << endl;


//		if(pointAdded && !p.empty() && p.back().getPoint() != pts.back().getPoint())
//		{
////			p.push_back(CHBugNode(pts.front(), edge.front(), index.front()));
////			cout << "AUX POINT ADDED: " << p.back() << endl;
//		}

		unsigned int sz = pts.size();
		//cout << "sz: " << sz << endl;


//		if(sz == 1 && pts.back().getPoint() != p.back().getPoint()) //PETA AKI QUAN P ESTA EMPTY
//		{
//			cout << "opcio1" << endl;
//			p.push_back(CHBugNode(pts.front(), edge.front(), index.front()));
////			cout << "AUX POINT ADDED: " << p.back() << endl;
//		}

		if(sz == 1 && p.empty()) // mod 20120428 -> maybe can be written better
		{
			//cout << "opcio0" << endl;
			p.push_back(CHBugNode(pts.front(), edge.front(), index.front()));
//			cout << "AUX POINT ADDED: " << p.back() << endl;
		}
		else if(sz == 1 && !p.empty() && pts.back().getPoint() != p.back().getPoint()) //PETA AKI QUAN P ESTA EMPTY
		{
//			cout << "opcio1" << endl;
			p.push_back(CHBugNode(pts.front(), edge.front(), index.front()));
//			cout << "AUX POINT ADDED: " << p.back() << endl;
		}
		else if(sz > 1)
		{
//			cout << "opcio2" << endl;
			for(vector<CHLPoint>::iterator j = pts.begin(); j < pts.end(); ++j)
			{
//				cout << "POINT ADDED: " << *j << endl;
				p.push_back(CHBugNode(*j, edge.at(j - pts.begin()), index.at(j - pts.begin())));
	//			cout << p.back() << endl;
			}
		}

		//cout << "------------" << endl;
	}

	//cout << "---before plot ------" << endl;
	if(plot_)
	{
		for(vector<CHBugNode>::iterator j = p.begin(); j < p.end(); ++j)
		{
			CPoint2Di p = j->getPoint();
			cv::Scalar color;
			if(j->getType() == hbug::hit) color = cv::Scalar(255,0,0);
			else if(j->getType() == hbug::leave) color = cv::Scalar(0,0,255);
			else color = cv::Scalar(128,128,128);
			cv::line(mapPlot_,Point2i(p.x()-10, p.y()),Point2i(p.x()+10, p.y()),color);
			cv::line(mapPlot_,Point2i(p.x(), p.y()-10),Point2i(p.x(), p.y()+10),color);
			imshow("HBugPath", mapPlot_);

		}
	}
//	cout << "computeHBugPoints -----------FI----" << endl;
	return p;
}

bool
CHBug::isHLPoint(const CRFPoint& p, CHLPoint& o)
{
	if(label_.at<int>(p.y(), p.x()) == -1) // contour
	{
		if(label_.at<int>(p.y(), p.x() - 1) > 0)
		{
			o.setHLPoint(label_.at<int>(p.y(), p.x() - 1), p, hbug::leave);
			return true;
		}
		if(label_.at<int>(p.y() - 1, p.x()) > 0)
		{
			o.setHLPoint(label_.at<int>(p.y() - 1, p.x()), p, hbug::leave);
			return true;
		}
		if(label_.at<int>(p.y(), p.x() + 1) > 0)
		{
			o.setHLPoint(label_.at<int>(p.y(), p.x() + 1), p, hbug::leave);
			return true;
		}
		if(label_.at<int>(p.y() + 1, p.x()) > 0)
		{
			o.setHLPoint(label_.at<int>(p.y() + 1, p.x()), p, hbug::leave);
			return true;
		}

		if(label_.at<int>(p.y() - 1, p.x() - 1) > 0)
		{
			o.setHLPoint(label_.at<int>(p.y() - 1, p.x() - 1), p, hbug::leave);
			return true;
		}
		if(label_.at<int>(p.y() - 1, p.x() + 1) > 0)
		{
			o.setHLPoint(label_.at<int>(p.y() - 1, p.x() + 1), p, hbug::leave);
			return true;
		}
		if(label_.at<int>(p.y() + 1, p.x() + 1) > 0)
		{
			o.setHLPoint(label_.at<int>(p.y() + 1, p.x() + 1), p, hbug::leave);
			return true;
		}
		if(label_.at<int>(p.y() + 1, p.x() - 1) > 0)
		{
			o.setHLPoint(label_.at<int>(p.y() + 1, p.x() - 1), p, hbug::leave);
			return true;
		}
	}
	return false;
}

vector<CHLPoint>
CHBug::computeHLPoints(	const CRFPoint& p1,
						const CRFPoint& p2,
						bool& insideObstacle)
{
	vector<CHLPoint> p;
	int offset;
	int lprev = -2, lcurr = -2;
	Point2i prev, curr;
	prev.x = p1.x();
	prev.y = p1.y();

	cv::LineIterator iterator(label_, Point2i(p1.x(), p1.y()), Point2i(p2.x(), p2.y()), 8);
	for(int i = 0; i < iterator.count; i++, ++iterator)
	{
		offset = iterator.ptr - (uchar*)(label_.data);
		curr.y = offset / label_.step;
		curr.x = (offset - curr.y * label_.step) / (sizeof(int)); // size of pixel

		if(insideObstacle)
		{
			if(label_.at<int>(curr.y, curr.x) == -1) // contour
			{
				//cout << "outObst" << endl;
				insideObstacle = false;

				lprev = label_.at<int>(prev.y, prev.x);
				if(lprev > 0)
					p.push_back(CHLPoint(lprev, CPoint2Di(curr.x, curr.y), hbug::leave));
				else
					throw runtime_error("HBug::computeHLPoints: previous not an obstacle");
			}
		}
		else
		{
			lcurr = label_.at<int>(curr.y, curr.x);
			if(lcurr > 0)
			{
				//cout << "inObst" << endl;
				insideObstacle = true;
				if(label_.at<int>(prev.y, prev.x) == -1)
					p.push_back(CHLPoint(lcurr, CPoint2Di(prev.x, prev.y), hbug::hit));
				else
					throw runtime_error("HBug::computeHLPoints: previous not contour");
			}
		}
		prev = curr;

		nCellsProcessed_++;
	}
	return p;
}

//vector<CHBugObstacle>
//CHBug::computeObstacles(const vector<CHBugNode>& n)
//{
//	vector<CHBugObstacle> o;
//
//	unsigned int idx = 0;
//	CEdge currl, nextl;
//	unsigned int nextl_idx, currl_idx = 0;
//	int obstacle = 0;
//
//	for(vector<CHBugNode>::const_iterator i = n.begin(); i < n.end(); i += 2)
//	{
//		if(n.at(idx).getEdge() != edge::e)
//		{
//			currl = n.at(idx).getEdge();
//			currl_idx = idx;
//		}
//
//		vector<CHBugNode>::const_iterator pos = find_if(n.begin() + idx + 1, n.end(), CHBugNodeWithEdgeNot(edge::e));
//		if(pos != n.end())
//		{
//			nextl = pos->getEdge();
//			nextl_idx = pos - n.begin();
//		}
//		else // can be avoided
//		{
//			nextl = edge::e;
//			nextl_idx = -1;
//		}
//
//		if(i->getObstacle() != obstacle) // set direction
//		{
//			obstacle = i->getObstacle();
//
//			hbug::EDirection direction = hbug::parallel;
////			cout << "currl, nextl, obstacle: " << currl << ", " << nextl << ", " << obstacle << " -> ";
//
//			if(		currl.getSubindex() == nextl.getSubindex() &&
//					obstacle == currl.getSubindex()) // parallel
//			{
////				cout << "parallel " << endl;
//				direction = hbug::parallel;
//
////				cout << "NEXTL_IDX, HLINDEX(NEXTL_IDX): " << nextl_idx << ", " << hlindex.at(nextl_idx) << endl;
//				int counter = 0;
//				unsigned int idxFst;
//				l_kEdgesCounter(nextl.getSubindex(), n.at(nextl_idx).getIndex(), counter, idxFst);
//
////				cout << "l_kEdgesCounter RESULT ---------------------------------------" << endl;
////				cout << "tpath: " << homotopyClass_ << endl;
////				cout << "edge: "<< nextl << " -> " << "pos: " << hlindex.at(nextl_idx) << endl;
////				cout << "first edge: " << homotopyClass_.at(idxFst) << " -> pos: " << idxFst << endl;
////				cout << "counter: " << counter << endl;
////				cout << "--------------------------------------------------------------" << endl;
//
////				// set initial direction
//				Point2i mc = obstacleCoord_.at(obstacle).massCentre_;
//				direction = getDirection(	lbPath_.front(),
//											segments_[homotopyClass_.at(idxFst)].getP1(),
//											CPoint2Di(mc.x, mc.y));
////				cout << "INITIAL DIR: " << direction << endl;
//				if((counter % 2) == 0)
//				{
//					if(direction == hbug::left) direction = hbug::right;
//					else if(direction == hbug::right) direction = hbug::left;
//					else direction = hbug::parallel;
//				}
////				cout << "CURRENT DIR: " << direction << endl;
//			}
//			else
//			{ 	// aquesta part sembla correcta
////				cout << "NO parallel ";
//				Point2i mc = obstacleCoord_.at(i->getObstacle()).massCentre_;
//				direction = getDirection(	i->getPoint(),
//											(i+1)->getPoint(),
//											CPoint2Di(mc.x, mc.y));
//
//			}
////			cout << "OBST, DIR: " << obstacle << ", ";// << direction << endl;
////			if(direction == hbug::left) cout << "left" << endl;
////			if(direction == hbug::right) cout << "right" << endl;
////			if(direction == hbug::parallel) cout << "parallel" << endl;
//
//			// look for begin/end points of the obstacle
//			CPoint2Di begin = i->getPoint();
//			CPoint2Di end;
//			vector<CHBugNode>::const_iterator j = i + 1;
//			do
//			{
//				end = j->getPoint();
//				j += 2;
//			}
//			while(j < n.end() && j->getObstacle() == obstacle);
//			o.push_back(CHBugObstacle(obstacle, begin, end, direction));
//		}
//		idx += 2;
//	}
//	return o;
//}

vector<CHBugObstacle>
CHBug::computeObstacles(const vector<CHBugNode>& n)
{
	vector<CHBugObstacle> o;

	int obstacle = 0;
	bool hfound = false;
	bool lfound = false;

//	vector<CHBugNode>::const_iterator currl = n.begin();
//	vector<CHBugNode>::const_iterator nextl;

	vector<CHBugNode>::const_iterator hit;
	vector<CHBugNode>::const_iterator leave;

	for(vector<CHBugNode>::const_iterator i = n.begin(); i < n.end(); ++i)
	{
		if(!hfound && i->getType() == hbug::hit && i->getObstacle() != obstacle)
		{
			hit = i;
			hfound = true;
		}
		if(!lfound && hfound && i->getType() == hbug::leave)
		{
			leave = i;
			lfound = true;
		}

		// set direction based on hit/leave points
		if(hfound && lfound && i->getObstacle() != obstacle)
		{
			vector<CHBugNode>::const_iterator currl;
			vector<CHBugNode>::const_iterator nextl;

			int idx = hit - n.begin();
			while(idx >= 0 && n.at(idx).getEdge() == edge::e) idx--;
			(idx >= 0)? currl = n.begin() + idx : currl = n.end();

			nextl = find_if(leave, n.end(), CHBugNodeWithEdgeNot(edge::e));

//			cout << "HLPOINTS: " << *hit << " < " << *leave << endl;
//			cout << "currl, nextl: ";
//			if(currl != n.end())
//				cout << *currl;
//			cout << " | ";
//			if(nextl != n.end())
//				cout << *nextl;
//			cout << " -------" <<  endl;


//			cout << "OBST: " << hit->getObstacle() << " " << hit->getPoint() << " -> " << leave->getPoint() << " EDGES: ";
//			if(currl != n.end())
//				cout << currl->getEdge();
//			else
//				cout << "(end)";
//			cout << ",";
//			if(nextl != n.end())
//				cout << nextl->getEdge();
//			else
//				cout << "(end)";
//			cout << endl;


			hbug::EDirection direction = computeDirection(n, hit, leave, currl, nextl);
			obstacle = i->getObstacle();

			// look for begin/end points of the obstacle
			CPoint2Di begin = hit->getPoint();
			CPoint2Di end;
			vector<CHBugNode>::const_iterator j = leave;
			do
			{
				end = j->getPoint();
				++j;
			}
			while(j < n.end() && j->getObstacle() == obstacle);

			o.push_back(CHBugObstacle(obstacle, begin, end, direction));
//			cout << "O: " << o.back() << endl;
			hfound = lfound = false;
		}



		/*
		// keep the first hit point of the obstacle
		if(i->getObstacle() != obst)
		{
			obst = i->getObstacle();
			init = i;
		}

		// get first hit/leave points of the obstacle
		if(!hfound && i->getType() == hbug::hit)
		{
			if(i->getEdge() != edge::e) curr = i;
			hfound = true;
		}
		else if(hfound && !lfound && i->getType() == hbug::leave) // NO ESTA Be!!!!!!!!!!!!!!!!!!!!!!!!
		{
//			vector<CHBugNode>::const_iterator pos = find_if(i, n.end(), CHBugNodeWithEdgeNot(edge::e));
//			if(pos != n.end()) next = i;
			next = find_if(i, n.end(), CHBugNodeWithEdgeNot(edge::e));
			lfound = true;
		}

		// set direction based on hit/leave points
		if(hfound && lfound && i->getObstacle() != obstacle)
		{

			obstacle = i->getObstacle();
			hbug::EDirection direction = hbug::parallel;

//			direction = computeDirection(obstacle, n, init, curr, next);
			//if(direction == hbug::left) cout << "left" << endl;
			//if(direction == hbug::right) cout << "right" << endl;
			//if(direction == hbug::parallel) cout << "parallel" << endl;

			// look for begin/end points of the obstacle
			CPoint2Di begin = init->getPoint();
			cout << begin << endl;
			CPoint2Di end;
			vector<CHBugNode>::const_iterator j = init + 1;
			do
			{
				end = j->getPoint();
				++j;
			}
			while(j < n.end() && j->getObstacle() == obstacle);

			o.push_back(CHBugObstacle(obstacle, begin, end, direction));
			hfound = lfound = false;
		}
		*/
	}
	return o;
}
void
CHBug::buildPath(const vector<CHBugObstacle>& o)
{
	CPoint2Di p;
	path_.push_back(Point2i(start_.x(), start_.y()));
	for(vector<CHBugObstacle>::const_iterator i = o.begin(); i < o.end(); ++i)
	{
//		cout << "CONTOUR OBSTACLE: " << *i << endl;
		p.setVector(path_.back().x, path_.back().y);
		//length_ += p.distance(i->getBegin());
		length_ += computeA8distance(p, i->getBegin()); // for fair comparison with HA*/HRRT
		vector<Point2i> tmp = computeContour(*i);
		path_.insert(path_.end(), tmp.begin(), tmp.end());
//		cout << "cost contour: " << tmp.size() << endl;
		length_ += tmp.size(); // contour in connectivity A4 -> cost 1
	}
	p.setVector(path_.back().x, path_.back().y);
	//length_ += p.distance(goal_);
	length_ += computeA8distance(p, goal_); // for fair comparison with HA*/HRRT
	path_.push_back(Point2i(goal_.x(), goal_.y()));
}

vector<Point2i>
CHBug::computeContour(const CHBugObstacle& o)
{
//	cout << "computeContour ----------------------" << endl;
	vector<Point2i> contour;

	// Look for begin end index of the obstacle contour
	bool beginFound = false, endFound = false;
	Point2i begin(o.getBegin().x(), o.getBegin().y());
	Point2i end(o.getEnd().x(), o.getEnd().y());
	vector<Point2i>::iterator idx_b, idx_e;
	for(vector<Point2i>::iterator i = obstacleCoord_.at(o.getId()).externalContour_.begin();
			i <= obstacleCoord_.at(o.getId()).externalContour_.end(); ++i)
	{
		if(!beginFound && (*i == begin))
		{
			idx_b = i;
			beginFound = true;
			if(endFound) break;
		}
		if(!endFound && (*i == end))
		{
			idx_e = i;
			endFound = true;
			if(beginFound) break;
		}
		nCellsProcessed_++;
	}

//	cout << "found(b, e): " << "(" << beginFound << ", " << endFound << "): "
//			<<  idx_b-obstacleCoord_.at(o.getId()).externalContour_.begin() << ", "
//			<< idx_e-obstacleCoord_.at(o.getId()).externalContour_.begin() << endl;

	// Insert the desired part of the contour into 'contour'
	if(o.getDirection() == hbug::right)
	{
		if(idx_b < idx_e)
			contour.insert(contour.end(), idx_b, idx_e);
		else
		{
			contour.insert(contour.end(), idx_b, obstacleCoord_.at(o.getId()).externalContour_.end());
			contour.insert(contour.end(), obstacleCoord_.at(o.getId()).externalContour_.begin(), idx_e);
		}
	}
	else if(o.getDirection() == hbug::left)
	{
		if(idx_b < idx_e)
		{
//			cout << "l1" << endl;

			int sz1 = idx_b - obstacleCoord_.at(o.getId()).externalContour_.begin();
			int sz2 = obstacleCoord_.at(o.getId()).externalContour_.end() - idx_e;

			contour.resize(sz1 + sz2);
//			cout << "SIZE: " << contour.size() << endl;
			reverse_copy(obstacleCoord_.at(o.getId()).externalContour_.begin(), idx_b, contour.begin());

//			for(vector<Point2i>::iterator it = contour.begin(); it < contour.end(); ++it)
//				cout << "(" << it->x << ","  << it->y << ") ";
//			cout << "segona part: " << endl;
			reverse_copy(idx_e, obstacleCoord_.at(o.getId()).externalContour_.end() , contour.begin() + sz1);
//			for(vector<Point2i>::iterator it = contour.begin()+sz1; it < contour.end(); ++it)
//							cout << "(" << it->x << ","  << it->y << ") ";
		}
		else
		{
			int sz = idx_b-idx_e;
//			cout << "l2 b,e: " << idx_b-obstacleCoord_.at(o.getId()).externalContour_.begin() << ", " << idx_e - obstacleCoord_.at(o.getId()).externalContour_.begin() << "(" << sz << ")" << endl;
			contour.resize(sz);
			reverse_copy(idx_e, idx_b, contour.begin());
		}

	}
	else if(o.getDirection() == hbug::parallel)
	{
		cout << "PARALLEL -> NO FAIG RES" << endl;
		contour.push_back(Point2i(o.getBegin().x(), o.getBegin().y()));
		contour.push_back(Point2i(o.getEnd().x(), o.getEnd().y()));
	}
	return contour;
//	cout << "computeContour ------------------FI----" << endl;
}

unsigned int
CHBug::l_kEdgesCounter(	const unsigned int& subindex,
						const unsigned int& position,
						unsigned int& first)
{
	bool fstSet = false;
	unsigned int counter = 0;
	for(unsigned int i = 0; i < position; ++i)
	{
		if(homotopyClass_.at(i).getSubindex() == subindex)
		{
			counter++;
			if(!fstSet)
			{
				first = i;
				fstSet = true;
			}
		}
	}
	if(!fstSet) runtime_error("No first edge found for parallel segments");
	return counter;
}

hbug::EDirection
CHBug::getDirection(const CPoint2Di& a,
					const CPoint2Di& b,
					const CPoint2Di& c)
{
	CPoint2Di v1(a); v1 -= c;
	CPoint2Di v2(b); v2 -= c;
	int value = util::perpDotProduct<int>(v1, v2);
	if(value < 0) // CCW
		return hbug::left;
	else if(value > 0) // CW
		return hbug::right;
	else // value == 0 -> paral·lel. Never should be reached
		runtime_error("HBug. Cannot set direction 2");
	return hbug::parallel;
}

hbug::EDirection
CHBug:: computeDirection(	const vector<CHBugNode>& n,
							vector<CHBugNode>::const_iterator& hit,
							vector<CHBugNode>::const_iterator& leave,
							vector<CHBugNode>::const_iterator& currl,
							vector<CHBugNode>::const_iterator& nextl)
{
	hbug::EDirection direction = hbug::parallel;
	Point2i mc;

	if(		currl != n.end() &&
			nextl != n.end() &&
			currl->getEdge().getSubindex() == nextl->getEdge().getSubindex() &&
			currl->getEdge().getSubindex() == static_cast<unsigned int>(hit->getObstacle())) // parallel
	{
//		cout << "Parallel" << endl;
		unsigned int first;
		int counter = l_kEdgesCounter(	nextl->getEdge().getSubindex(),
										nextl->getIndex(),
										first);

//		cout << "l_kEdgesCounter RESULT ---------------------------------------" << endl;
//		cout << "tpath: " << homotopyClass_ << endl;
//		cout << "edge: "<< nextl->getEdge() << " -> " << "pos: " << nextl - n.begin() << endl;
//		cout << "first edge: " << homotopyClass_.at(first) << " -> pos: " << first << endl;
//		cout << "counter: " << counter << endl;
//		cout << "--------------------------------------------------------------" << endl;

		// set initial direction (S - first edge that belongs to k)
		mc = obstacleCoord_.at(hit->getObstacle()).massCentre_;
		direction = getDirection(	lbPath_.front(),
									segments_[homotopyClass_.at(first)].getP1(),
									CPoint2Di(mc.x, mc.y));
		// cout << "initial dir: " << direction << endl;
		if((counter % 2) == 0)
		{
			if(direction == hbug::left) direction = hbug::right;
			else if(direction == hbug::right) direction = hbug::left;
			else direction = hbug::parallel;
		}
		//	cout << "current dir: " << direction << endl;
	}
	else
	{ 	// aquesta part sembla correcta
//		cout << "NO parallel " << endl;
		mc = obstacleCoord_.at(hit->getObstacle()).massCentre_;
		direction = getDirection(	hit->getPoint(), leave->getPoint(),	CPoint2Di(mc.x, mc.y));
	}
	return direction;
}

void
CHBug::savePathCoordinates(const string& filename)
{
	ofstream f;
	f.open(filename.c_str());

	for(vector<Point2i>::iterator i = path_.begin(); i < path_.end(); ++i)
		f << "[" << i->x << ";" << i->y << "]" << endl;
	f.close();
}

void
CHBug::plotPath()
{
	for(vector<Point2i>::iterator i = path_.begin(); i < path_.end()-1; ++i)
		cv::line(mapPlot_, *i, *(i+1), cv::Scalar(255,0,255));
}


float
CHBug::computeA8distance(	const CPoint2Di& p1,
							const CPoint2Di& p2)
{
	// Euclidean distance that takes into account 1 step cost between cells
	long int d1 = abs(p1.x() - p2.x());
	long int d2 = abs(p1.y() - p2.y());

	if(d1 < d2)
		swap(d1, d2);
	return (static_cast<float>(d2) * cmap::diagonalCost + static_cast<float>(d1-d2) * cmap::straightCost);
}
