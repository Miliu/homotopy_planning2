// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CBug2.cpp
 *
 *  Created on: 01/08/2011
 *      Author: emili
 */

#include "CBug2.h"
#include "debug.h"

using namespace std;
using namespace boost::posix_time;

int CBug2::id_ = 1;

CBug2::CBug2(	CMapPtr map,
				const cv::Mat& rf,
				const bool& plot)
:
		map_(map),
		//mapSize_(map->getVSizeCell()),
		obstacleCoord_(map->getObstacleCoordinates()),
		label_(map->getCCLabel()),
		plot_(plot),
		rfPlot_(rf)
{
	if(plot_)
	{
		CPoint2Di size = map_->getVSizeCell();
		rfPlot_.copyTo(mapPlot_);
		cv::namedWindow("Bug2Path", CV_WINDOW_AUTOSIZE);
		cvMoveWindow("Bug2Path", 4 * size.x() + 100, 100);
		imshow("Bug2Path", mapPlot_);
	}
}

bool
CBug2::computePath(	const CPoint2Di& start,
					const CPoint2Di& goal,
					const std::vector<bug::EDirection>& direction)
{
	length_ = 0;
	nCellsProcessed_ = 0;
	start_ = start;
	goal_ = goal;
	if(!map_->isFree(start_))	throw runtime_error("Start point occupied.");
	if(!map_->isFree(goal_))	throw runtime_error("Goal point occupied.");
	direction_ = direction;
	path_.clear();

	ptime time1(microsec_clock::universal_time());

	vector<CHLPoint> nodes = computeBug2Points();
#ifdef _NDEBUG
	debug::printVector("nodes:", "", nodes);
	cout << "------------------------------------------------------------------------------" << endl;
#endif

	vector<CBugObstacle> obstacles = computeObstacles(nodes);
#ifdef _NDEBUG
	debug::printVector("obstacles:", "", obstacles);
	cout << "------------------------------------------------------------------------------" << endl;
#endif

	buildPath(obstacles);

	ptime time2(microsec_clock::universal_time());
	time_duration diff(time2 - time1);
	//computationTime_ = diff.total_milliseconds() / 1000.0f;
	computationTime_ = diff.total_microseconds() / 1000000.0f;

	std::ostringstream idStream;
	idStream << "_" << id_;

	savePathCoordinates("test/Bug2-pathcoord" + idStream.str() + ".txt");
	if(plot_)
	{
		cout << "Bug2. Path found" << endl;
		//rfPlot_.copyTo(mapPlot_);

		savePathCoordinates("test/Bug2-pathcoord" + idStream.str() + ".txt");
		plotPath();

		// start
		cv::line(mapPlot_,Point2i(start_.x()-10, start_.y()),Point2i(start_.x()+10, start_.y()),cv::Scalar(0,0,255));
		cv::line(mapPlot_,Point2i(start_.x(), start_.y()-10),Point2i(start_.x(), start_.y()+10),cv::Scalar(0,0,255));

		// goal
		cv::line(mapPlot_,Point2i(goal_.x()-10, goal_.y()),Point2i(goal_.x()+10, goal_.y()),cv::Scalar(0,255,0));
		cv::line(mapPlot_,Point2i(goal_.x(), goal_.y()-10),Point2i(goal_.x(), goal_.y()+10),cv::Scalar(0,255,0));
		imshow("Bug2Path", mapPlot_);
	}
	id_++;
	return true;
}

vector<CHLPoint>
CBug2::computeBug2Points()
{
//	vector<CHLPoint> p;

	bool insideObstacle = false;
	//for(vector<CRFPoint>::const_iterator i = path.begin(); i < path.end() - 1; ++i)
	//{
		vector<CHLPoint> pts = computeHLPoints(start_, goal_, insideObstacle);
//		vector<CEdge> edge(pts.size());
//		vector<int> index(pts.size(), -1);

		if(pts.empty())
		{
//			cout << "--point to be added " << i - path.begin() << " - " << (i+1) - path.begin() << ": " << endl;

			CHLPoint aux;
			if(isHLPoint(start_, aux))
			{
//				cout << "P1 : " << aux << endl;
				pts.push_back(aux);
//				edge.push_back(path.at(i - path.begin()).getEdge());
//				index.push_back(path.at(i - path.begin()).getIndex());
//				auxEdge = path.at(i - path.begin()).getEdge();
//				auxIndex = path.at(i - path.begin()).getIndex();

			}
			//if(isHLPoint(*(i + 1), aux)) cout << "P2 : " << aux << endl;
		}

//		cout << "POINTS: " << *i << ", " << *(i+1) << "(" << pts.size() << ")" << endl;

		// can be optimized ??
//		for(vector<CHLPoint>::iterator j = pts.begin(); j < pts.end(); ++j)
//		{
//			if(j->getPoint() == *i)
//			{
//				edge.at(j - pts.begin()) = path.at(i - path.begin()).getEdge();
//				index.at(j - pts.begin()) = path.at(i - path.begin()).getIndex();
//			}
//			if(j->getPoint() == *(i+1))
//			{
//				edge.at(j - pts.begin()) = path.at(i + 1 - path.begin()).getEdge();
//				index.at(j - pts.begin()) = path.at(i + 1 - path.begin()).getIndex();
//			}
//		}

//		if(!pts.empty())
//		{
//			if(pts.front().getPoint() == *i)
//			{
//				edge.at(0) = path.at(i - path.begin()).getEdge();
//				index.at(0) = path.at(i - path.begin()).getIndex();
//			}
//			if(pts.back().getPoint() == *(i+1))
//			{
//				edge.at(pts.size() - 1) = path.at(i + 1 - path.begin()).getEdge();
//				index.at(pts.size() - 1) = path.at(i + 1 - path.begin()).getIndex();
//			}
//		}


//		if(pointAdded && !p.empty() && p.back().getPoint() != pts.back().getPoint())
//		{
////			p.push_back(CBug2Node(pts.front(), edge.front(), index.front()));
////			cout << "AUX POINT ADDED: " << p.back() << endl;
//		}

/* FA FALTA??????
		unsigned int sz = pts.size();
		if(sz == 1 && pts.back().getPoint() != p.back().getPoint())
		{
			p.push_back(CBug2Node(pts.front(), edge.front(), index.front()));
//			cout << "AUX POINT ADDED: " << p.back() << endl;
		}
		else if(sz > 1)
		{
			for(vector<CHLPoint>::iterator j = pts.begin(); j < pts.end(); ++j)
			{
//				cout << "POINT ADDED: " << *j << endl;
				p.push_back(CBug2Node(*j, edge.at(j - pts.begin()), index.at(j - pts.begin())));
	//			cout << p.back() << endl;
			}
		}
*/
//		cout << "------------" << endl;
//	}

	if(plot_)
	{
		for(vector<CHLPoint>::iterator j = pts.begin(); j < pts.end(); ++j)
		{
			CPoint2Di p = j->getPoint();
			cv::Scalar color;
			if(j->getType() == bug::hit) color = cv::Scalar(255,0,0);
			else if(j->getType() == bug::leave) color = cv::Scalar(0,0,255);
			else color = cv::Scalar(128,128,128);
			cv::line(mapPlot_,Point2i(p.x()-10, p.y()),Point2i(p.x()+10, p.y()),color);
			cv::line(mapPlot_,Point2i(p.x(), p.y()-10),Point2i(p.x(), p.y()+10),color);
			imshow("Bug2Path", mapPlot_);

		}
	}
	return pts;
}

bool
CBug2::isHLPoint(const CRFPoint& p, CHLPoint& o)
{
	if(label_.at<int>(p.y(), p.x()) == -1) // contour
	{
		if(label_.at<int>(p.y(), p.x() - 1) > 0)
		{
			o.setHLPoint(label_.at<int>(p.y(), p.x() - 1), p, bug::leave);
			return true;
		}
		if(label_.at<int>(p.y() - 1, p.x()) > 0)
		{
			o.setHLPoint(label_.at<int>(p.y() - 1, p.x()), p, bug::leave);
			return true;
		}
		if(label_.at<int>(p.y(), p.x() + 1) > 0)
		{
			o.setHLPoint(label_.at<int>(p.y(), p.x() + 1), p, bug::leave);
			return true;
		}
		if(label_.at<int>(p.y() + 1, p.x()) > 0)
		{
			o.setHLPoint(label_.at<int>(p.y() + 1, p.x()), p, bug::leave);
			return true;
		}

		if(label_.at<int>(p.y() - 1, p.x() - 1) > 0)
		{
			o.setHLPoint(label_.at<int>(p.y() - 1, p.x() - 1), p, bug::leave);
			return true;
		}
		if(label_.at<int>(p.y() - 1, p.x() + 1) > 0)
		{
			o.setHLPoint(label_.at<int>(p.y() - 1, p.x() + 1), p, bug::leave);
			return true;
		}
		if(label_.at<int>(p.y() + 1, p.x() + 1) > 0)
		{
			o.setHLPoint(label_.at<int>(p.y() + 1, p.x() + 1), p, bug::leave);
			return true;
		}
		if(label_.at<int>(p.y() + 1, p.x() - 1) > 0)
		{
			o.setHLPoint(label_.at<int>(p.y() + 1, p.x() - 1), p, bug::leave);
			return true;
		}
	}
	return false;
}

vector<CHLPoint>
CBug2::computeHLPoints(	const CRFPoint& p1,
						const CRFPoint& p2,
						bool& insideObstacle)
{
	vector<CHLPoint> p;
	int offset;
	int lprev = -2, lcurr = -2;
	Point2i prev, curr;
	prev.x = p1.x();
	prev.y = p1.y();

	cv::LineIterator iterator(label_, Point2i(p1.x(), p1.y()), Point2i(p2.x(), p2.y()), 8);
	for(int i = 0; i < iterator.count; i++, ++iterator)
	{
		offset = iterator.ptr - (uchar*)(label_.data);
		curr.y = offset / label_.step;
		curr.x = (offset - curr.y * label_.step) / (sizeof(int)); // size of pixel

		if(insideObstacle)
		{
			if(label_.at<int>(curr.y, curr.x) == -1) // contour
			{
				//cout << "outObst" << endl;
				insideObstacle = false;

				lprev = label_.at<int>(prev.y, prev.x);
				if(lprev > 0)
					p.push_back(CHLPoint(lprev, CPoint2Di(curr.x, curr.y), bug::leave));
				else
					throw runtime_error("Bug2::computeHLPoints: previous not an obstacle");
			}
		}
		else
		{
			lcurr = label_.at<int>(curr.y, curr.x);
			if(lcurr > 0)
			{
				//cout << "inObst" << endl;
				insideObstacle = true;
				if(label_.at<int>(prev.y, prev.x) == -1)
					p.push_back(CHLPoint(lcurr, CPoint2Di(prev.x, prev.y), bug::hit));
				else
					throw runtime_error("Bug2::computeHLPoints: previous not contour");
			}
		}
		prev = curr;

		nCellsProcessed_++;
	}
	return p;
}


vector<CBugObstacle>
CBug2::computeObstacles(const vector<CHLPoint>& n)
{
	vector<CBugObstacle> o;

	int obstacle = 0;
	bool hfound = false;
	bool lfound = false;

	unsigned int idir = 0;

//	vector<CBug2Node>::const_iterator currl = n.begin();
//	vector<CBug2Node>::const_iterator nextl;

	vector<CHLPoint>::const_iterator hit;
	vector<CHLPoint>::const_iterator leave;

	for(vector<CHLPoint>::const_iterator i = n.begin(); i < n.end(); ++i)
	{
		if(!hfound && i->getType() == bug::hit && i->getObstacle() != obstacle)
		{
			hit = i;
			hfound = true;
		}
		if(!lfound && hfound && i->getType() == bug::leave)
		{
			leave = i;
			lfound = true;
		}

		// set direction based on hit/leave points (first hit/leave)
		if(hfound && lfound && i->getObstacle() != obstacle)
		{
//			vector<CBug2Node>::const_iterator currl;
//			vector<CBug2Node>::const_iterator nextl;
//
//			int idx = hit - n.begin();
//			while(idx >= 0 && n.at(idx).getEdge() == edge::e) idx--;
//			(idx >= 0)? currl = n.begin() + idx : currl = n.end();
//
//			nextl = find_if(leave, n.end(), CBug2NodeWithEdgeNot(edge::e));

//			cout << "HLPOINTS: " << *hit << " < " << *leave << endl;
//			cout << "currl, nextl: ";
//			if(currl != n.end())
//				cout << *currl;
//			cout << " | ";
//			if(nextl != n.end())
//				cout << *nextl;
//			cout << " -------" <<  endl;


//			cout << "OBST: " << hit->getObstacle() << " " << hit->getPoint() << " -> " << leave->getPoint() << " EDGES: ";
//			if(currl != n.end())
//				cout << currl->getEdge();
//			else
//				cout << "(end)";
//			cout << ",";
//			if(nextl != n.end())
//				cout << nextl->getEdge();
//			else
//				cout << "(end)";
//			cout << endl;


//			hbug::EDirection direction = computeDirection(n, hit, leave, currl, nextl);
			obstacle = i->getObstacle();

			// look for begin/end points of the obstacle
			CPoint2Di begin = hit->getPoint();
			CPoint2Di end;
			vector<CHLPoint>::const_iterator j = leave;
			do
			{
				end = j->getPoint();
				++j;
			}
			while(j < n.end() && j->getObstacle() == obstacle);


			bug::EDirection direction;
			if(idir < direction_.size())
			{
				direction = direction_.at(idir);
				idir++;
			}
			else
			{
				direction = direction_.back();
				//cout << "Bug2. Taking last known direction" << endl;
			}

			if(direction == bug::parallel)
				cout << "Bug2. Parallel direction changed to rigth" << endl;

			o.push_back(CBugObstacle(obstacle, begin, end, direction));
//			cout << "O: " << o.back() << endl;
			hfound = lfound = false;
		}
	}
	return o;
}


void
CBug2::buildPath(const vector<CBugObstacle>& o)
{
	CPoint2Di p;
	path_.push_back(Point2i(start_.x(), start_.y()));
	for(vector<CBugObstacle>::const_iterator i = o.begin(); i < o.end(); ++i)
	{
//		cout << "CONTOUR OBSTACLE: " << *i << endl;
		p.setVector(path_.back().x, path_.back().y);
		length_ += p.distance(i->getBegin());
		vector<Point2i> tmp = computeContour(*i);
		path_.insert(path_.end(), tmp.begin(), tmp.end());
//		cout << "cost contour: " << tmp.size() << endl;
		length_ += tmp.size(); // contour in connectivity A4 -> cost 1
	}
	p.setVector(path_.back().x, path_.back().y);
	length_ += p.distance(goal_);
	path_.push_back(Point2i(goal_.x(), goal_.y()));
}

vector<Point2i>
CBug2::computeContour(const CBugObstacle& o)
{
//	cout << "computeContour ----------------------" << endl;
	vector<Point2i> contour;

	// Look for begin end index of the obstacle contour
	bool beginFound = false, endFound = false;
	Point2i begin(o.getBegin().x(), o.getBegin().y());
	Point2i end(o.getEnd().x(), o.getEnd().y());
	vector<Point2i>::iterator idx_b, idx_e;
	for(vector<Point2i>::iterator i = obstacleCoord_.at(o.getId()).externalContour_.begin();
			i <= obstacleCoord_.at(o.getId()).externalContour_.end(); ++i)
	{
		if(!beginFound && (*i == begin))
		{
			idx_b = i;
			beginFound = true;
			if(endFound) break;
		}
		if(!endFound && (*i == end))
		{
			idx_e = i;
			endFound = true;
			if(beginFound) break;
		}
		nCellsProcessed_++;
	}

//	cout << "found(b, e): " << "(" << beginFound << ", " << endFound << "): "
//			<<  idx_b-obstacleCoord_.at(o.getId()).externalContour_.begin() << ", "
//			<< idx_e-obstacleCoord_.at(o.getId()).externalContour_.begin() << endl;

	// Insert the desired part of the contour into 'contour'
	if(o.getDirection() == bug::right)
	{
		if(idx_b < idx_e)
			contour.insert(contour.end(), idx_b, idx_e);
		else
		{
			contour.insert(contour.end(), idx_b, obstacleCoord_.at(o.getId()).externalContour_.end());
			contour.insert(contour.end(), obstacleCoord_.at(o.getId()).externalContour_.begin(), idx_e);
		}
	}
	else if(o.getDirection() == bug::left)
	{
		if(idx_b < idx_e)
		{
//			cout << "l1" << endl;

			int sz1 = idx_b - obstacleCoord_.at(o.getId()).externalContour_.begin();
			int sz2 = obstacleCoord_.at(o.getId()).externalContour_.end() - idx_e;

			contour.resize(sz1 + sz2);
//			cout << "SIZE: " << contour.size() << endl;
			reverse_copy(obstacleCoord_.at(o.getId()).externalContour_.begin(), idx_b, contour.begin());

//			for(vector<Point2i>::iterator it = contour.begin(); it < contour.end(); ++it)
//				cout << "(" << it->x << ","  << it->y << ") ";
//			cout << "segona part: " << endl;
			reverse_copy(idx_e, obstacleCoord_.at(o.getId()).externalContour_.end() , contour.begin() + sz1);
//			for(vector<Point2i>::iterator it = contour.begin()+sz1; it < contour.end(); ++it)
//							cout << "(" << it->x << ","  << it->y << ") ";
		}
		else
		{
			int sz = idx_b-idx_e;
//			cout << "l2 b,e: " << idx_b-obstacleCoord_.at(o.getId()).externalContour_.begin() << ", " << idx_e - obstacleCoord_.at(o.getId()).externalContour_.begin() << "(" << sz << ")" << endl;
			contour.resize(sz);
			reverse_copy(idx_e, idx_b, contour.begin());
		}

	}
	else if(o.getDirection() == bug::parallel)
	{
		cout << "PARALLEL -> NO FAIG RES" << endl;
		contour.push_back(Point2i(o.getBegin().x(), o.getBegin().y()));
		contour.push_back(Point2i(o.getEnd().x(), o.getEnd().y()));
	}
	return contour;
//	cout << "computeContour ------------------FI----" << endl;
}

void
CBug2::savePathCoordinates(const string& filename)
{
	ofstream f;
	f.open(filename.c_str());

	for(vector<Point2i>::iterator i = path_.begin(); i < path_.end(); ++i)
		f << "[" << i->x << ";" << i->y << "]" << endl;
	f.close();
}

void
CBug2::plotPath()
{
	for(vector<Point2i>::iterator i = path_.begin(); i < path_.end()-1; ++i)
		cv::line(mapPlot_, *i, *(i+1), cv::Scalar(255,0,255));
}

