// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CBug2.h
 *
 *  Created on: 01/08/2011
 *      Author: emili
 *
 *  Bug2 using the CCL contours to compute the path to surround obstacles
 *  - Maybe it's not fully operational
 *  - CHLPoint redefined in CHBug -> change in the future
 */

#ifndef CBUG2_H_
#define CBUG2_H_

#include "CTStringExt.h"
#include "CMap.h"
#include "CRFPoint.h"
#include "CTSpace.h"

#include <vector>
#include <algorithm>
#include <time.h>
#include <boost/date_time/posix_time/posix_time.hpp>

namespace bug
{
	enum EDirection {left = 0, right, parallel};
	enum EPointType {hit = 0, leave, other};
}

class CHLPoint
{
public:
	CHLPoint()
	:
		obstacle_(-1),
		point_(),
		type_(bug::other)
	{}

	CHLPoint(	const int& obstacle,
				const CPoint2Di& point,
				const bug::EPointType& type)
	:
		obstacle_(obstacle),
		point_(point),
		type_(type)
	{}

	CHLPoint(	const CHLPoint& p)
	:
		obstacle_(p.obstacle_),
		point_(p.point_),
		type_(p.type_)
	{}

	int getObstacle() const {return obstacle_;}
	CPoint2Di getPoint() const {return point_;}
	bug::EPointType getType() const {return type_;}

	void setHLPoint(	const int& obstacle,
						const CPoint2Di& point,
						const bug::EPointType& type)
	{
		obstacle_ = obstacle;
		point_ = point;
		type_ = type;
	}

	friend std::ostream& operator<<(std::ostream& cout, const CHLPoint& p)
	{
		cout << p.getObstacle() << ":" << p.getPoint();
		if(p.getType() == bug::hit) cout << " hit";
		else if(p.getType() == bug::leave) cout << " leave";
		else cout << " other";
		return cout;
	}

private:
	int 				obstacle_;
	CPoint2Di 			point_;
	bug::EPointType	type_; // point type
};

class CBugObstacle
{
public:
	CBugObstacle(	const int& id,
					const CPoint2Di& begin,
					const CPoint2Di& end,
					const bug::EDirection& direction)
	:
		id_(id),
		begin_(begin),
		end_(end),
		direction_(direction)
	{}

	int getId() const {return id_;}
	CPoint2Di getBegin() const {return begin_;}
	CPoint2Di getEnd() const {return end_;}
	bug::EDirection getDirection() const {return direction_;}


	friend std::ostream& operator<<(std::ostream& cout, const CBugObstacle& o)
	{
		cout << o.getId() << ":" << o.getBegin() << "," << o.getEnd();
		if(o.getDirection() == bug::left)
			cout << " lft";
		else if(o.getDirection() == bug::right)
			cout << " rgt";
		else
			cout << " par";
		return cout;
	}

private:
	int 				id_;
	CPoint2Di 			begin_;
	CPoint2Di 			end_;
	bug::EDirection		direction_;
};

class CBug2
{
public:
	CBug2(	CMapPtr map,
			const cv::Mat& rf,
			const bool& plot = false);

	bool computePath(	const CPoint2Di& start,
						const CPoint2Di& goal,
						const std::vector<bug::EDirection>& direction);

	float getComputationTime() const {return computationTime_;}
	float getLength() const {return length_;}
	const long int getNCellsProcessed() const {return nCellsProcessed_;}

private:
	void buildPath(const std::vector<CBugObstacle>& o);
	std::vector<CBugObstacle> computeObstacles(const std::vector<CHLPoint>& n);
	std::vector<CHLPoint> computeBug2Points();
	bool isHLPoint(const CRFPoint& p, CHLPoint& o);

	std::vector<CHLPoint> computeHLPoints(	const CRFPoint& p1,
											const CRFPoint& p2,
											bool& insideObstacle);
	std::vector<Point2i> computeContour(const CBugObstacle& o);
	void savePathCoordinates(const std::string& filename);
	void plotPath();

	CMapPtr map_;
	//CPoint2Di mapSize_;
	CPoint2Di start_, goal_;
	std::vector<ccl::SObstacleCoord> obstacleCoord_;

	std::vector<bug::EDirection> direction_;

	float computationTime_;
	float length_;
	long int nCellsProcessed_;

	static int id_;

	cv::Mat label_;
	bool plot_; // plot flag

	cv::Mat rfPlot_;
	cv::Mat mapPlot_;

	std::vector<Point2i> path_;
};
#endif /* CBUG2_H_ */
