// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CRRT.cpp
 *
 *  Created on: 13/11/2009
 *  Modified on: 03/05/2010
 *  	- Using STL trees from Kasper Peeters
 *      Author: Emili
 */

#include "CRRT.h"

using namespace std;
using namespace boost::posix_time;

CRRT::CRRT(	CMapPtr map,
			const cv::Mat& rf,
			const bool& plot)
:
			map_(map),
			mapSize_(map->getVSizeCell()),
			plot_(plot),
			rfPlot_(rf)
{
	srand(static_cast<unsigned int>(time(0)));
//	srand(1);
	explored_.create(mapSize_.y(), mapSize_.x(), CV_8U);

	if(plot_)
	{
		CPoint2Di size = map_->getVSizeCell();
		rfPlot_.copyTo(mapPlot_);
		cv::namedWindow("RRTpath", CV_WINDOW_AUTOSIZE);
		cvMoveWindow("RRTpath", 4 * size.x() + 100, 100);
		imshow("RRTpath", mapPlot_);
	}
}

bool
CRRT::computePath(	const CPoint2Di& start,
					const CPoint2Di goal,
					const unsigned int& stepi,
					const float& goalSamplingPrb,
					const float& distanceThresholdf)
{
	start_ = start;
	goal_ = goal;

	bool pathFound = false;

	if(!map_->isFree(start_))
	{
		throw runtime_error("Start point occupied.");
	}
	if(!map_->isFree(goal_))
	{
		throw runtime_error("Goal point occupied.");
	}

	idCounter_ = 0;
	length_ = 0;
	maxCellsToProcess_ = map_->getSizeCell() * rrt::maxCellsFactor;
	nCellsProcessed_ = 0;

	CRRTNode startNode(idCounter_, start_);
	CRRTNode goalNode(numeric_limits<long int>::max(), goal_);
//	cout << "Start: " << startNode << endl;
//	cout << "Goal: " << goalNode << endl;

	tree<CRRTNode> T;
	tree<CRRTNode>::iterator it;
	it = T.begin();
	it = T.insert(it, startNode);
	idCounter_++;
	nCellsProcessed_++;

	unsigned int step;
	(stepi == 0) ? step = 1 : step = stepi;
	float distanceThreshold = distanceThresholdf;

	explored_ = cv::Scalar(rrt::notExplored);
	ptime time1(microsec_clock::universal_time());
	do
	{
		pathFound = extend(T, goalNode, step, goalSamplingPrb, distanceThreshold, it);
		if(nCellsProcessed_ >= maxCellsToProcess_)
			break;
	}
	while (!pathFound);
	ptime time2(microsec_clock::universal_time());
	time_duration diff(time2 - time1);
	computationTime_ = diff.total_milliseconds() / 1000.0f;

	string filename;
	if(pathFound)
	{
		length_ = computePathLength(T, it);
		savePathCoordinates(T, it, "test/RRT-pathcoord.txt");
		filename = "test/RRT-treecoord.txt";
	}
	else
	{
		cout << "RRT. Path not found." << endl;
		filename = "test/RRT-treecoord_notFound.txt";
	}
	saveTreeCoordinates(T, filename);

	if(plot_)
	{
		cout << "RRT. Path found" << endl;
		rfPlot_.copyTo(mapPlot_);
		if(pathFound)
		{
			plotPath(T, it);
		}
		// start
		cv::line(mapPlot_,Point2i(start_.x()-10, start_.y()),Point2i(start_.x()+10, start_.y()),cv::Scalar(0,0,255));
		cv::line(mapPlot_,Point2i(start_.x(), start_.y()-10),Point2i(start_.x(), start_.y()+10),cv::Scalar(0,0,255));

		// goal
		cv::line(mapPlot_,Point2i(goal_.x()-10, goal_.y()),Point2i(goal_.x()+10, goal_.y()),cv::Scalar(0,255,0));
		cv::line(mapPlot_,Point2i(goal_.x(), goal_.y()-10),Point2i(goal_.x(), goal_.y()+10),cv::Scalar(0,255,0));
		imshow("RRTpath", mapPlot_);
	}

	T.clear();
	return pathFound;

		/*
	std::ostringstream idStream;
	if(pathFound)
	{
		cout << "RRT. Path found" << endl;
		cout << "Cells: " << nCellsProcessed_ << endl;
		savePathCoordinates(T, it, "test/RRT-pathcoord" + idStream.str()+ ".txt");
		saveTreeCoordinates(T, "test/RRT-treecoord" + idStream.str()+ ".txt");
	}
	else
	{
		cout << "RRT. Path not found" << endl;
		idStream << "_not found";
	}
	saveTree(T, "test/RRT-tree" + idStream.str() + ".txt");
	savePath(T, it, "test/RRT-path" + idStream.str() + ".txt");

	T.clear();
	return pathFound;
	*/
}

bool
CRRT::extend(	tree<CRRTNode>& T,
				const CRRTNode& goalNode,
				const unsigned int& step,
				const float& goalSamplingPrb,
				const float& distanceThreshold,
				tree<CRRTNode>::iterator& iNearest)
{
	CPoint2Di qRand, qNew, qNearest;

	if(computeQRand(goalNode.getPoint(), goalSamplingPrb, qRand))
	{
		iNearest = nearestNeighbor(T, qRand);
		qNearest = iNearest->getPoint();
		if(computeQNew(qNearest, qRand, step, qNew))
		{
 			iNearest = T.append_child(iNearest, CRRTNode(idCounter_, qNew));
 			if(plot_)
			{
				plotPath(T, iNearest);
				imshow("RRTpath", mapPlot_);
				cv::waitKey(50);
			}
			idCounter_++;
			nCellsProcessed_++;

			if(qNew.distance(goalNode.getPoint()) <= distanceThreshold)
			{
				iNearest = T.append_child(iNearest, CRRTNode(idCounter_, goalNode.getPoint()));
				if(plot_)
				{
					imshow("RRTpath", mapPlot_);
					cv::waitKey(50);
				}
				idCounter_++;
				return true;
			}
		}
 	}
	return false;
}

bool CRRT::computeQRand(const CPoint2Di& goal, const float& goalSamplingPrb, CPoint2Di& qRand)
{
	float p = rand() / static_cast<float>(RAND_MAX);
	if (p < goalSamplingPrb)
	{
		qRand = goal;
		return true; //qRandFound;
	}

	int x, y;
	unsigned int i = 0;
	do
	{
		x = static_cast<int>(mapSize_.x() * (rand() / (RAND_MAX + 1.0f)));
		y = static_cast<int>(mapSize_.y() * (rand() / (RAND_MAX + 1.0f)));
		qRand.setVector(x, y);
		i++;
	}
	while( (!map_->isFree(qRand) || explored_.at<uchar>(qRand.y(), qRand.x()) == 255) &&
				i < rrt::maxNTries);

	if(i == rrt::maxNTries)
		return false; //qRandFound = false;
	return true; //return qRandFound;
}

tree<CRRTNode>::iterator
CRRT::nearestNeighbor(tree<CRRTNode>& T, const CPoint2Di& qRand)
{
	CPoint2Di p(qRand);
	tree<CRRTNode>::pre_order_iterator i;
	tree<CRRTNode>::iterator nearest = T.begin();
	unsigned long distance = std::numeric_limits<unsigned long>::max();
	unsigned long tmp;

	i = T.begin();
	if(!T.is_valid(i))
	{
		cout << "Error. NearestNeighbor: iterator not valid.";
		abort();
	}
	while(i != T.end())
	{
		tmp = p.distance2(i->getPoint());
		if(tmp < distance)
		{
			nearest = i;
			distance = tmp;
		}
		++i;
	}
	return nearest;
}

bool
CRRT::computeQNew(const CPoint2Di& qNearest, const CPoint2Di& qRand, const unsigned int& step, CPoint2Di& qNew)
{
	int x1 = qNearest.x(), y1 = qNearest.y();
	const int x2 = qRand.x(), y2 = qRand.y();

	int xdelta = x2 - x1;
	int ydelta = y2 - y1;
	int xstep;
	int ystep;
	int change;
	unsigned int stepAux = 0;

	if(xdelta < 0)
	{
		xdelta = -xdelta;
		xstep = -1;
	}
	else
		xstep = 1;

	if(ydelta < 0)
	{
		ydelta = -ydelta;
		ystep = -1;
	}
	else
		ystep = 1;

	if(xdelta > ydelta)
	{
		change = xdelta >> 1;
		while (x1 != x2 && stepAux < step)
		{
			if(!map_->isFree(x1, y1)) return false;
			stepAux++;
			x1 += xstep;
			change += ydelta;
			if (change > xdelta)
			{
				y1 += ystep;
				change -= xdelta;
			}
		}
	}
	else
	{
		change = ydelta >> 1;
		while (y1 != y2 && stepAux < step)
		{
			if(!map_->isFree(x1, y1)) return false;
			stepAux++;
			y1 += ystep;
			change += xdelta;
			if (change > ydelta)
			{
				x1 += xstep;
				change -= ydelta;
			}
		}
	}

	if(map_->isFree(x1, y1) && explored_.at<uchar>(y1, x1) == rrt::notExplored)
	{
		qNew.setVector(x1, y1);
		explored_.at<uchar>(y1, x1) = rrt::explored;
		return true;
	}
	return false;
}

float
CRRT::computePathLength(tree<CRRTNode> T, tree<CRRTNode>::iterator i)
{
	CPoint2Di curr, prev;
	long int length = 0;
	long int d1, d2;

	if(!T.is_valid(i))
	{
		cout << "Error. ComputePathLength: iterator not valid.";
		return 0.0f;
	}

	curr = i->getPoint();
	do
	{
		prev = curr;
		curr = i->getPoint();

		d1 = abs(curr.x() - prev.x());
		d2 = abs(curr.y() - prev.y());

		if(d1 < d2)
			swap(d1, d2);
		length += (d2 * (cmap::diagonalCost * rrt::costCorrection) + (d1-d2) * (cmap::straightCost * rrt::costCorrection));
		i = T.parent(i);

	}
	while(T.is_valid(i));

	return (length / static_cast<float>(rrt::costCorrection));
}

void
CRRT::savePathCoordinates(tree<CRRTNode> T, tree<CRRTNode>::iterator i, const string& filename)
{
	CPoint2Di curr, prev;
	ofstream f;

	f.open(filename.c_str());
	if(!f.is_open())
	{
		cout << "Error. " << filename.c_str() << " cannot be created." << endl;
		abort();
	}

	curr = i->getPoint();
	f << curr << endl;
	do
	{
		prev = curr;
		curr = i->getPoint();
		f << curr << endl;
		i = T.parent(i);
	}
	while(T.is_valid(i));
	f.close();
}

void CRRT::saveTreeCoordinates(tree<CRRTNode> T, const string& filename)
{
	tree<CRRTNode>::pre_order_iterator i;
	ofstream f;

	f.open(filename.c_str());
	if(!f.is_open())
	{
		cout << "Error. " << filename.c_str() << " cannot be created." << endl;
		abort();
	}

	i = T.begin();
	if(!T.is_valid(i))
	{
		cout << "Error. SavePath: iterator not valid.";
		return;
	}

	// head coordinate
	f <<  i->getId() << " - " << -1 << " " << i->getPoint() << endl;
	i++;
	while(i != T.end())
	{
		f <<  i->getId() << " - " << T.parent(i)->getId()  << " " << i->getPoint() << endl;
		++i;
	}
	f.close();
}

void
CRRT::plotPath(tree<CRRTNode> T, tree<CRRTNode>::iterator i)
{
	CPoint2Di curr, prev;
	Point2i p1, p2;

	if(!T.is_valid(i))
	{
		cout << "Error. SavePath: iterator not valid.";
		return;
	}

	curr = i->getPoint();
	do
	{
		prev = curr;
		curr = i->getPoint();

		p1.x = prev.x();
		p1.y = prev.y();

		p2.x = curr.x();
		p2.y = curr.y();
		cv::line(mapPlot_, p1, p2, cv::Scalar(255,0,255));
		i = T.parent(i);

	}
	while(T.is_valid(i));
}
