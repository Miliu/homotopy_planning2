// Copyright (c) 2009-2018 Emili Hernandez
/*
  * CHRRT.h
 *  Class Homotopy RRT. RRT algorithm using an homotopy class to guide the tree
 *  Created on: 23/11/2009
 *
 *  Modified on: 28/04/2010
 *  	- Used class CTString instead TString
 *  	- Using STL trees from Kasper Peeters
 *      Author: Emili
 */

#ifndef CHRRT_H_
#define CHRRT_H_

#include "CPoint2D.h"
#include "CMap.h"
#include "CTStringExt.h"
#include "CRFSegment.h"

#include "tree/tree.hh"
#include "tree/tree_util.hh"
#include <sstream>
#include <iostream>
//#include <ctime>
#include <limits>
#include <boost/date_time/posix_time/posix_time.hpp>

typedef CPoint2D<int> CPoint2Di;
typedef CPoint2D<double> CPoint2Df;

class CHRRTNode
{
public:
	CHRRTNode(	const long int& id,
				const CPoint2Di& point,
				const CTStringExt& tPath)
	:
		id_(id),
		point_(point),
		tPath_(tPath)
	{}

	CHRRTNode(const CHRRTNode& n)
	:
		id_(n.id_),
		point_(n.point_),
		tPath_(n.tPath_)
	{}

	void operator=(const CHRRTNode& n)
	{
		id_ = n.id_;
		point_ = n.point_;
		tPath_ = n.tPath_;
	}

	long int getId() const {return id_;}
	CPoint2Di getPoint() const {return point_;}
	CTStringExt getTPath() const {return tPath_;}
	void setTPath(CTStringExt tPath) {tPath_ = tPath;}

	friend std::ostream& operator<<(std::ostream& cout, const CHRRTNode& n)
	{
		cout << "[" << n.getId() << ","<< n.getPoint() << ",[" << n.getTPath() << "]]";
		return cout;
	}

private:
	long int 	id_; // added for representing results
	CPoint2Di 	point_;
	CTStringExt tPath_;
};


namespace hrrt
{
	const unsigned int maxNTries = 250;
	const float maxCellsFactor = 0.3; //0..1
	const long int costCorrection = 100; // useful to operate with integers
	const uchar explored = 255;
	const uchar notExplored = 0;
}

class CHRRT
{
public:
	CHRRT(CMapPtr map, const std::vector<CRFSegment>& segments, const cv::Mat& rf, const bool& plot);

	bool computePath(	const CPoint2Di& start,
						const CPoint2Di& goal,
						const unsigned int& stepi,
						const float& goalSamplingPrb,
						const float& distanceThresholdf,
						const CTStringR& homotopyClass);

	float getComputationTime() const {return computationTime_;}
	float getLength() const {return length_;}
	const long int getNCellsProcessed() const {return nCellsProcessed_;}

private:
	bool computeQRand(const CPoint2Di& goal, const float& goalSamplingPrb, CPoint2Di& qRand);
	tree<CHRRTNode>::iterator nearestNeighbor(tree<CHRRTNode>& T, const CPoint2Di& qRand);
	bool computeQNew(const CPoint2Di& qNearest, const CPoint2Di& qRand, const unsigned int& step, CPoint2Di& qNew);
	bool extend(	tree<CHRRTNode>& T,
					const CPoint2Di& goal,
					const unsigned int& step,
					const float& goalSamplingPrb,
					const float& distanceThreshold,
					tree<CHRRTNode>::iterator& Tnearest);

	bool updateTopologicalPath(const std::vector<CRFSegmentIndex>& intersection, CTStringExt& path);
	std::vector<CRFSegmentIndex> findIntersection(const CPoint2Di& p1, const CPoint2Di& p2, const std::vector<CRFSegment>& segments);

	// functions to show results
	float computePathLength(tree<CHRRTNode> T, tree<CHRRTNode>::iterator i);
	void savePathCoordinates(tree<CHRRTNode> T, tree<CHRRTNode>::iterator i, const std::string& filename);
	void saveTreeCoordinates(tree<CHRRTNode> T, const std::string& filename);

	std::vector<CRFSegment> segments_;
	CPoint2Di start_, goal_;
	CTStringR homotopyClass_;

	CMapPtr map_;
	CPoint2Di mapSize_;

	float computationTime_;
	float length_;
	unsigned long int nCellsProcessed_;
	unsigned long int nCellsDiscarted_, nCellsAccepted_;
	unsigned long int maxCellsToProcess_;

	static int id_;
	long int idCounter_;

	cv::Mat explored_;
	// plot flag
	bool plot_;

	cv::Mat rfPlot_;
	cv::Mat mapPlot_;
	void plotPath(tree<CHRRTNode> T, tree<CHRRTNode>::iterator i);
};

#endif /* CHRRT_H_ */
