// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CHRRT.cpp
 *
 *  Created on: 23/11/2009
 *      Author: Emili
 */

#include "CHRRT.h"

using namespace std;
using namespace boost::posix_time;

int CHRRT::id_ = 1;

CHRRT::CHRRT(	CMapPtr map,
				const vector<CRFSegment>& segments,
				const cv::Mat& rf,
				const bool& plot)
:
				segments_(segments),
				map_(map),
				mapSize_(map->getVSizeCell()),
				plot_(plot),
				rfPlot_(rf)
{
	srand(static_cast<unsigned int>(time(0)));
	//srand(1);
	explored_.create(mapSize_.y(), mapSize_.x(), CV_8U);

	if(plot_)
	{
		CPoint2Di size = map_->getVSizeCell();
		rfPlot_.copyTo(mapPlot_);
		cv::namedWindow("HRRTpath", CV_WINDOW_AUTOSIZE);
		cvMoveWindow("HRRTpath", 4 * size.x() + 100, 100);
		imshow("HRRTpath", mapPlot_);
	}
}

bool
CHRRT::computePath(	const CPoint2Di& start,
					const CPoint2Di& goal,
					const unsigned int& stepi,
					const float& goalSamplingPrb,
					const float& distanceThresholdf,
					const CTStringR& homotopyClass)
{
	start_ = start;
	goal_ = goal;
	homotopyClass_ = homotopyClass;

	bool pathFound = false;

	if(!map_->isFree(start_))
	{
		throw runtime_error("Start point occupied.");
	}
	if(!map_->isFree(goal_))
	{
		throw runtime_error("Goal point occupied.");
	}

	idCounter_ = 0;
	length_ = 0;
	maxCellsToProcess_ = map_->getSizeCell() * hrrt::maxCellsFactor;
	nCellsProcessed_ = nCellsDiscarted_ = nCellsAccepted_ = 0;

	CHRRTNode startNode(idCounter_, start_, CTStringExt());

	tree<CHRRTNode> T;
	tree<CHRRTNode>::iterator it;
	it = T.begin();
	it = T.insert(it, startNode);
	idCounter_++;
	nCellsProcessed_++;
	nCellsAccepted_++;

	unsigned int step;
	(stepi == 0) ? step = 1 : step = stepi;
	float distanceThreshold = distanceThresholdf;

	explored_ = cv::Scalar(0);//cv::Scalar(hrrt::notExplored);
	ptime time1(microsec_clock::universal_time());
	do
	{
		pathFound = extend(T, goal_, step, goalSamplingPrb, distanceThreshold, it);
		//cout << "EXTEND " << nCellsProcessed_ << " " << hrrt::maxCellsProcessed  << endl;
		if(nCellsProcessed_ >= maxCellsToProcess_)
			break;
	}
	while (!pathFound);
	ptime time2(microsec_clock::universal_time());
	time_duration diff(time2 - time1);
//	computationTime_ = diff.total_milliseconds() / 1000.0f;
	computationTime_ = diff.total_microseconds() / 1000000.0f;

	std::ostringstream idStream;
	idStream << "_" << id_;
	string filename;

	if(pathFound)
	{
		length_ = computePathLength(T, it);
		savePathCoordinates(T, it, "test/HRRT-pathcoord" + idStream.str()+ ".txt");
		filename = "test/HRRT-treecoord" + idStream.str()+ ".txt";
	}
	else
	{
		cout << "HRRT. Path not found." << endl;
		filename = "test/HRRT-treecoord" + idStream.str()+ "_notFound.txt";
	}
	saveTreeCoordinates(T, filename);

	if(plot_)
	{
		cout << "HRRT. Path found" << endl;
		rfPlot_.copyTo(mapPlot_);
		if(pathFound)
		{
			cout << "Cells (accepted + discarded): " << nCellsAccepted_ << " + " << nCellsDiscarted_ << " = "  << nCellsProcessed_ << endl;
//			savePathCoordinates(T, it, "test/HRRT-pathcoord" + idStream.str()+ ".txt");
//			saveTreeCoordinates(T, "test/HRRT-treecoord" + idStream.str()+ ".txt");
			plotPath(T, it);
		}
		// start
		cv::line(mapPlot_,Point2i(start_.x()-10, start_.y()),Point2i(start_.x()+10, start_.y()),cv::Scalar(0,0,255));
		cv::line(mapPlot_,Point2i(start_.x(), start_.y()-10),Point2i(start_.x(), start_.y()+10),cv::Scalar(0,0,255));

		// goal
		cv::line(mapPlot_,Point2i(goal_.x()-10, goal_.y()),Point2i(goal_.x()+10, goal_.y()),cv::Scalar(0,255,0));
		cv::line(mapPlot_,Point2i(goal_.x(), goal_.y()-10),Point2i(goal_.x(), goal_.y()+10),cv::Scalar(0,255,0));
		imshow("HRRTpath", mapPlot_);
	}
	id_++;

	T.clear();
	return pathFound;
}

bool
CHRRT::extend(	tree<CHRRTNode>& T,
				const CPoint2Di& goal,
				const unsigned int& step,
				const float& goalSamplingPrb,
				const float& distanceThreshold,
				tree<CHRRTNode>::iterator& iNearest)
{
	CPoint2Di qRand, qNew, qNearest;
	vector<CRFSegmentIndex> I;
	CTStringExt tpath;

	if(computeQRand(goal, goalSamplingPrb, qRand))
	{
		//cout << "Qrand :" << qRand << endl;
		iNearest = nearestNeighbor(T, qRand);
		qNearest = iNearest->getPoint();
		//cout << "Qnear :" << qNearest << endl;
		if(computeQNew(qNearest, qRand, step, qNew))
		{
//			cout << ">>>>>>>>>>>>>>Qnew : " << qNew << endl;
			I = findIntersection(qNearest, qNew, segments_);
			tpath = iNearest->getTPath();
			if(updateTopologicalPath(I, tpath))
			{
				iNearest = T.append_child(iNearest, CHRRTNode(idCounter_, qNew, tpath));
				if(plot_)
				{
					plotPath(T, iNearest);
					imshow("HRRTpath", mapPlot_);
					cv::waitKey(50);
				}
				idCounter_++;
				nCellsAccepted_++;
			}
			else
			{
//				cout << "update tp fail" << endl;
				nCellsDiscarted_++;
			}
			nCellsProcessed_++;
			if(qNew.distance(goal) <= distanceThreshold && tpath.size() == homotopyClass_.size())//tpath == goalNode.getTPath())
			{
				//iNearest = T.append_child(iNearest, CHRRTNode(idCounter_, goalNode.getPoint(), goalNode.getTPath()));
				iNearest = T.append_child(iNearest, CHRRTNode(idCounter_, goal, tpath));
				if(plot_)
				{
					imshow("HRRTpath", mapPlot_);
					cv::waitKey(50);
				}
				idCounter_++;

				return true;
			}
			else
			{
//				cout << "Goal not reached" << endl;
				return false;
			}
		}
 		else
 		{

// 			cout << "No qNew" << endl;
 			return false;
 		}
	}
	else
	{
//		cout << "No qRand" << endl;
		return false;
	}
}

bool
CHRRT::computeQRand(const CPoint2Di& goal, const float& goalSamplingPrb, CPoint2Di& qRand)
{
	float p = rand() / static_cast<float>(RAND_MAX);
	if (p < goalSamplingPrb)
	{
		qRand = goal;
		return true; //qRandFound;
	}

	int x, y;
	unsigned int i = 0;
	do
	{
		x = static_cast<int>(mapSize_.x() * (rand() / (RAND_MAX + 1.0f)));
		y = static_cast<int>(mapSize_.y() * (rand() / (RAND_MAX + 1.0f)));
		qRand.setVector(x, y);
		i++;
	}
	while( (!map_->isFree(qRand) || explored_.at<uchar>(qRand.y(), qRand.x()) == 255) &&
			i < hrrt::maxNTries);
	//map_->getCell(qRand).getValue() == cell::explored) &&
	if(i == hrrt::maxNTries)
		return false; //qRandFound = false;

//		explored_.at<uchar>(qRand.y(), qRand.x()) = hrrt::explored;
//		imshow("HRRTpath", explored_);
//		cv::waitKey(50);
//		//getchar();
	return true; //return qRandFound;
}

tree<CHRRTNode>::iterator
CHRRT::nearestNeighbor(tree<CHRRTNode>& T, const CPoint2Di& qRand)
{
	CPoint2Di p(qRand);
	tree<CHRRTNode>::pre_order_iterator i;
	tree<CHRRTNode>::iterator nearest = T.begin();
	unsigned long distance = std::numeric_limits<unsigned long>::max();
	unsigned long tmp;

	i = T.begin();
	if(!T.is_valid(i))
	{
		cout << "Error. NearestNeighbor: iterator not valid.";
		abort();
	}
	while(i != T.end())
	{
		tmp = p.distance2(i->getPoint());
		if(	(i->getTPath().size() > nearest->getTPath().size()) ||
			(i->getTPath().size() == nearest->getTPath().size() && tmp < distance))
		{
			nearest = i;
			distance = tmp;
		}
		++i;
	}
	return nearest;
}

bool
CHRRT::computeQNew(const CPoint2Di& qNearest, const CPoint2Di& qRand, const unsigned int& step, CPoint2Di& qNew)
{
	//cout << "ComputeQNew - nearest rand :" << qNearest << ", " << qRand << endl;
	int x1 = qNearest.x(), y1 = qNearest.y();
	const int x2 = qRand.x(), y2 = qRand.y();

	int xdelta = x2 - x1;
	int ydelta = y2 - y1;
	int xstep;
	int ystep;
	int change;
	unsigned int stepAux = 0;

	if(xdelta < 0)
	{
		xdelta = -xdelta;
		xstep = -1;
	}
	else
		xstep = 1;

	if(ydelta < 0)
	{
		ydelta = -ydelta;
		ystep = -1;
	}
	else
		ystep = 1;

	if(xdelta > ydelta)
	{
		change = xdelta >> 1;
		while (x1 != x2 && stepAux < step)
		{
			//if(!map_->getCell(x1, y1).isFree()) return false;
			if(!map_->isFree(x1, y1)) return false;
			stepAux++;
			x1 += xstep;
			change += ydelta;
			if (change > xdelta)
			{
				y1 += ystep;
				change -= xdelta;
			}
		}
	}
	else
	{
		change = ydelta >> 1;
		while (y1 != y2 && stepAux < step)
		{
			if(!map_->isFree(x1, y1)) return false;
			stepAux++;
			y1 += ystep;
			change += xdelta;
			if (change > ydelta)
			{
				x1 += xstep;
				change -= ydelta;
			}
		}
	}

//	CCell c = map_->getCell(x1, y1);
//	//cout << "ComputeQNew - occupancy: " << x1 << ", " << y1 << " " << static_cast<unsigned int>(map_->getCell(x1,y1).getOccupancy()) << endl;
//	//if(c.getValue() == cell::unexplored)
//	if(c.getOccupancy() == cell::free && c.getValue() == cell::unexplored)
//	{
//		c.setValue(cell::explored);
//		qNew.setVector(x1, y1);
//		map_->setCell(qNew, c);
//		return true;
//	}

//	imshow("HRRTpath", explored_);
//	cv::waitKey(50);
//	//getchar();
//	cout << "Dins Qnew (" << x1 << "," << y1 <<")FI (" << map_->isFree(x1, y1) << ", " << static_cast<unsigned int>(explored_.at<uchar>(y1, x1)) << ")->";


	if(map_->isFree(x1, y1) && explored_.at<uchar>(y1, x1) == 0)//hrrt::notExplored)
	{
//		cout << "OK" << endl; // Nota per l'ICRA no es feia cas del explored!!! -> comprovar si fa falta treure-ho o no!!!
		qNew.setVector(x1, y1);
		explored_.at<uchar>(y1, x1) = 255;//hrrt::explored;
		return true;
	}
//	cout << "FAIL" << endl;


 return false;
}

bool
CHRRT::updateTopologicalPath(const vector<CRFSegmentIndex>& intersection, CTStringExt& path)
{
	// No intersections
	if(intersection.empty())
		return true;

	// Too much intersections to generate homotopyClass_
	if(	path.size() == homotopyClass_.size() ||
		(path.size() + intersection.size()) > homotopyClass_.size())
		return false;

	CTStringExt pathTmp = path;
	bool pathOk = true;
	unsigned int pos;

	vector<CRFSegmentIndex>::const_iterator i = intersection.begin();
	while(pathOk && i < intersection.end())
	{
		pos = pathTmp.size();

		if(	!pathTmp.isReversed() && homotopyClass_.isReversible() &&
			homotopyClass_.inReversibleSubstring(pos))
		{
//			cout << "opcio1" << endl;
			// check the two possible candidate
			if(segments_.at(i->getIndex()).getEdge() == homotopyClass_.at(pos))
			{
//				cout << "----1" << endl;
				pathTmp.add(homotopyClass_.at(pos));
			}
			else if(segments_.at(i->getIndex()).getEdge() == homotopyClass_.atR(pos))
			{
//				cout << "----2" << endl;
				pathTmp.add(homotopyClass_.atR(pos));
				pathTmp.setReversed(true);
			}
			else
			{
//				cout << "----3" << endl;
				pathOk = false;
			}
		}
		else if(pathTmp.isReversed() && homotopyClass_.inReversibleSubstring(pos))
		{
//			cout << "opcio2" << endl;
			// check candidate of the reverse substring
			if(segments_.at(i->getIndex()).getEdge() == homotopyClass_.atR(pos))
				pathTmp.add(homotopyClass_.atR(pos));
			else
				pathOk = false;
		}
		else //if(!pathTmp.isReversed())
		{
//			cout << "opcio3" << endl;
			// out of the reversible substring
			if(segments_.at(i->getIndex()).getEdge() == homotopyClass_.at(pos))
				pathTmp.add(homotopyClass_.at(pos));
			else
				pathOk = false;
		}
		i++;
	}

	if(pathOk)
	{
		path = pathTmp;
		return true;
	}
	return false;
}

vector<CRFSegmentIndex>
CHRRT::findIntersection(const CPoint2Di& p1, const CPoint2Di& p2, const vector<CRFSegment>& segments)
{
	CPoint2Df point;
	vector<CRFSegmentIndex> result;

	double distance;
	for(vector<CRFSegment>::const_iterator  i = segments.begin(); i < segments.end(); ++i)
	{
		//cout << "Segment: " << *i << endl;
		if(util::intersection<double, int>(p1, p2, i->getP1(), i->getP2(), point))
		{
			distance = point.distance(CPoint2Df(p1.x(), p1.y()));
			if(distance > 0.0001)
				result.push_back(CRFSegmentIndex(i - segments.begin(), distance * hrrt::costCorrection));
		}
	}
	sort(result.begin(), result.end());
	return result;
}

float
CHRRT::computePathLength(tree<CHRRTNode> T, tree<CHRRTNode>::iterator i)
{
	CPoint2Di curr, prev;
	long int length = 0;
	long int d1, d2;

	if(!T.is_valid(i))
	{
		cout << "Error. ComputePathLength: iterator not valid.";
		return 0.0f;
	}

	curr = i->getPoint();
	do
	{
		prev = curr;
		curr = i->getPoint();

		d1 = abs(curr.x() - prev.x());
		d2 = abs(curr.y() - prev.y());

		if(d1 < d2)
			swap(d1, d2);
		length += (d2 * (cmap::diagonalCost * hrrt::costCorrection) + (d1-d2) * (cmap::straightCost * hrrt::costCorrection));
		i = T.parent(i);

	}
	while(T.is_valid(i));

	return (length / static_cast<float>(hrrt::costCorrection));
}


void
CHRRT::savePathCoordinates(tree<CHRRTNode> T, tree<CHRRTNode>::iterator i, const string& filename)
{
	CPoint2Di curr, prev;
	ofstream f;

	f.open(filename.c_str());
	if(!f.is_open())
	{
		cout << "Error. " << filename.c_str() << " cannot be created." << endl;
		abort();
	}

	curr = i->getPoint();
	f << curr << endl;
	do
	{
		prev = curr;
		curr = i->getPoint();
		f << curr << endl;
		i = T.parent(i);
	}
	while(T.is_valid(i));
	f.close();
}

void
CHRRT::saveTreeCoordinates(tree<CHRRTNode> T, const string& filename)
{
	tree<CHRRTNode>::pre_order_iterator i;
	ofstream f;

	f.open(filename.c_str());
	if(!f.is_open())
	{
		cout << "Error. " << filename.c_str() << " cannot be created." << endl;
		abort();
	}

	i = T.begin();
	if(!T.is_valid(i))
	{
		cout << "Error. SavePath: iterator not valid.";
		return;
	}

	// head coordinate
	f <<  i->getId() << " - " << -1 << " " << i->getPoint() << endl;
	i++;
	while(i != T.end())
	{
		f <<  i->getId() << " - " << T.parent(i)->getId()  << " " << i->getPoint() << endl;
		++i;
	}
	f.close();
}

void
CHRRT::plotPath(tree<CHRRTNode> T, tree<CHRRTNode>::iterator i)
{
	CPoint2Di curr, prev;
	Point2i p1, p2;

	if(!T.is_valid(i))
	{
		cout << "Error. SavePath: iterator not valid.";
		return;
	}

	curr = i->getPoint();
	do
	{
		prev = curr;
		curr = i->getPoint();

		p1.x = prev.x();
		p1.y = prev.y();

		p2.x = curr.x();
		p2.y = curr.y();
		cv::line(mapPlot_, p1, p2, cv::Scalar(255,0,255));
		i = T.parent(i);

	}
	while(T.is_valid(i));
}
