/*
 * util.h
 *
 *  Created on: 31/05/2010
 *      Author: emili
 */

#ifndef UTIL_H_
#define UTIL_H_

#include "CPoint2D.h"
#include "CPose2D.h"
#include "CSegment2D.h"

namespace util
{
	extern const double pi;
	extern const double _2pi;

	template<typename T>
	T deg2rad(const T& a) {return (a * pi / 180.0);}

	template<typename T>
	T rad2deg(const T& a) {return (a * 180.0 / pi);}

	// gradients to radiants fuctions. Note: pi = 200 grad
	template<typename T>
	T grad2rad(const T& a) {return (a * pi / 200.0);}

	template<typename T>
	T rad2grad(const T& a) {return (a * 200.0 / pi);}

	// return angle between range [-pi pi]
	template<typename T>
	T normalize(const T& angle)
	{
		return (angle + _2pi * floor((pi - angle) / _2pi));
	}

	template<typename T>
	CPoint2D<T> composition(const CPose2D<T>& pose, const CPoint2D<T>& data)
	{
		const T cosy = cos(pose.yaw());
		const T siny = sin(pose.yaw());
		return 	CPoint2D<T>(	data.x() * cosy - data.y() * siny + pose.x(),
								data.x() * siny + data.y() * cosy + pose.y());
	}

	template<typename T>
	CPose2D<T> composition(const CPose2D<T>& pose1, const CPose2D<T>& pose2)
	{
		const T cosy = cos(pose1.yaw());
		const T siny = sin(pose1.yaw());
		return 	CPose2D<T>(	pose2.x() * cosy - pose2.y() * siny + pose1.x(),
							pose2.x() * siny + pose2.y() * cosy + pose1.y(),
							normalize<T>(pose1.yaw() + pose2.yaw()));
	}

	template<typename S, typename T>
	bool intersection(const CSegment2D<T>& s1, const CSegment2D<T>& s2)
	{
		S r, s, d;

		S x1 = s1.getP1().x();
		S y1 = s1.getP1().y();
		S x2 = s1.getP2().x();
		S y2 = s1.getP2().y();
		S x3 = s2.getP1().x();
		S y3 = s2.getP1().y();
		S x4 = s2.getP2().x();
		S y4 = s2.getP2().y();

		//Make sure the lines aren't parallel
		if ((y2 - y1) / (x2 - x1) != (y4 - y3) / (x4 - x3))
		{
			d = (((x2 - x1) * (y4 - y3)) - (y2 - y1) * (x4 - x3));
			if (d != 0)
			{
				r = (((y1 - y3) * (x4 - x3)) - (x1 - x3) * (y4 - y3)) / d;
				s = (((y1 - y3) * (x2 - x1)) - (x1 - x3) * (y2 - y1)) / d;
				if (r >= 0 && r <= 1)
				{
					if (s >= 0 && s <= 1)
						return true;
				}
			}
		}
		return false;
	}

	template<typename S, typename T>
	bool intersection(const CSegment2D<T>& s1, const CSegment2D<T>& s2, CPoint2D<S>& intersection)
	{
		S r, s, d;

		S x1 = s1.getP1().x();
		S y1 = s1.getP1().y();
		S x2 = s1.getP2().x();
		S y2 = s1.getP2().y();
		S x3 = s2.getP1().x();
		S y3 = s2.getP1().y();
		S x4 = s2.getP2().x();
		S y4 = s2.getP2().y();

		//Make sure the lines aren't parallel
		if ((y2 - y1) / (x2 - x1) != (y4 - y3) / (x4 - x3))
		{
			d = (((x2 - x1) * (y4 - y3)) - (y2 - y1) * (x4 - x3));
			if (d != 0)
			{
				r = (((y1 - y3) * (x4 - x3)) - (x1 - x3) * (y4 - y3)) / d;
				s = (((y1 - y3) * (x2 - x1)) - (x1 - x3) * (y2 - y1)) / d;
				if (r >= 0 && r <= 1)
				{
					if (s >= 0 && s <= 1)
					{
						x1 += r * (x2 - x1);
						y1 += r * (y2 - y1);
						intersection.setVector(x1, y1);
						return true;
					}
				}
			}
		}
		return false;
	}

	template<typename S, typename T>
	bool intersection(	const CPoint2D<T>& p1, const CPoint2D<T>& p2,
						const CPoint2D<T>& p3, const CPoint2D<T>& p4,
						CPoint2D<S>& intersection)
	{
		S r, s, d;

		S x1 = p1.x();
		S y1 = p1.y();
		S x2 = p2.x();
		S y2 = p2.y();
		S x3 = p3.x();
		S y3 = p3.y();
		S x4 = p4.x();
		S y4 = p4.y();

		//Make sure the lines aren't parallel
		if ((y2 - y1) / (x2 - x1) != (y4 - y3) / (x4 - x3))
		{
			d = (((x2 - x1) * (y4 - y3)) - (y2 - y1) * (x4 - x3));
			if (d != 0)
			{
				r = (((y1 - y3) * (x4 - x3)) - (x1 - x3) * (y4 - y3)) / d;
				s = (((y1 - y3) * (x2 - x1)) - (x1 - x3) * (y2 - y1)) / d;
				if (r >= 0 && r <= 1)
				{
					if (s >= 0 && s <= 1)
					{
						x1 += r * (x2 - x1);
						y1 += r * (y2 - y1);
						intersection.setVector(x1, y1);
						return true;
					}
				}
			}
		}
		return false;
	}

	// s'ha de passar els float a typename S pero hi ha un merder amb les distnacies del point2d que fa que no funcioni!!
	template<typename T>
	float distance2PointToSegment(const CPoint2D<T>& p, const CSegment2D<T>& s, CPoint2D<T>& po)
	{
		float a = p.x() - s.getP1().x();
		float b = p.y() - s.getP1().y();
		float c = s.getP2().x() - s.getP1().x();
		float d = s.getP2().y() - s.getP1().y();

		float dot = a * c + b * d;
		float lenght2 = c * c + d * d;
		float param = dot / lenght2;

		float xx, yy;

		if(param < 0)
		{
			xx = s.getP1().x();
			yy = s.getP1().y();
		}
		else if(param > 1)
		{
			xx = s.getP2().x();
			yy = s.getP2().y();
		}
		else
		{
			xx = s.getP1().x() + param * c;
			yy = s.getP1().y() + param * d;
		}
		po.setVector(xx, yy);

		xx -= p.x();
		yy -= p.y();
		return (xx * xx + yy * yy);
	}

	template<typename T>
	T perpDotProduct(const CPoint2D<T>& v1, const CPoint2D<T>& v2)
	{
		return -v1.y() * v2.x() + v1.x() * v2.y();
	}
}

#endif /* UTIL_H_ */
