/*
 * SOfPtr.h
 *  Struct of Pointers:
 *  	Useful, when you want to use std::sort to sort a vector of pointers *
 *
 *  Created on: 28/01/2010
 *      Author: Emili
 */

#ifndef SOFPTR_H_
#define SOFPTR_H_

//template <class T>
//struct SOfPtrLess
//{
//    bool operator()(const boost::shared_ptr<T>& a, const boost::shared_ptr<T>& b)
//    {
//        return *a < *b;
//    }
//};
//
//template <class T>
//struct SOfPtrGreater
//{
//    bool operator()(const boost::shared_ptr<T>& a, const boost::shared_ptr<T>& b)
//    {
//        return *a > *b;
//    }
//};

template <class T>
struct SOfRawPtrGreater
{
    bool operator()(const T* a, const T* b)
    {
        return *a > *b;
    }
};


#endif /* SOFPTR_H_ */
