/*
 * util.cpp
 *
 *  Created on: 31/05/2010
 *      Author: emili
 */

#include "util.h"
#include <math.h>

const double util::pi =  3.14159265358979;
const double util::_2pi = 2 * util::pi;

