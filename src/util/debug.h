/*
 * debug.h
 *
 *  Created on: 22/09/2010
 *      Author: emili
 */

#ifndef DEBUG_H_
#define DEBUG_H_

//#define _NDEBUG 1
//#define _LBDEBUG 1

#include <string>
#include <vector>
#include <iostream>
namespace debug
{
	template<typename T>
	void printVector(const std::string& msg, const std::string& msg2, const std::vector<T>& v, const bool& changeLine = true)
	{
		if(msg.size() > 0) std::cout << msg << std::endl;
		unsigned int idx = 0;
		typename std::vector<T>::const_iterator i;
		for(i = v.begin(); i < v.end(); ++i)
		{
			if(msg2.size() > 0) std::cout << msg2 << idx << ":";
			std::cout << *i;
			if(changeLine)
				std::cout << std::endl;
			else
				std::cout << " ";
			idx++;
		}

	}

	template<typename T>
	void printVectorOfVector(const std::string& msg, const std::string& msg2, const std::vector<std::vector<T> >& v)
	{
		if(msg.size() > 0) std::cout << msg << std::endl;
		unsigned int idx = 0;
		typename std::vector<std::vector<T> >::const_iterator i;
		typename std::vector<T>::const_iterator j;
		for(i = v.begin(); i < v.end(); ++i)
		{
			if(msg2.size() > 0) std::cout << msg2 << idx << ":";
			for(j = i->begin(); j < i->end(); ++j)
				std::cout << *j << " ";
			std::cout << std::endl;
			idx++;
		}
	}

	template<typename T>
	void printVectorPtr(const std::string& msg, const std::string& msg2, const std::vector<T>& v, const bool& changeLine = true)
	{
		if(msg.size() > 0) std::cout << msg << std::endl;
		unsigned int idx = 0;
		typename std::vector<T>::const_iterator i;
		for(i = v.begin(); i < v.end(); ++i)
		{
			if(msg2.size() > 0) std::cout << msg2 << idx << ":";
			std::cout << **i;
			if(changeLine)
				std::cout << std::endl;
			else
				std::cout << " ";
			idx++;
		}

	}
}

#endif /* DEBUG_H_ */
