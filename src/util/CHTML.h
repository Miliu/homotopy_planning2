/*
 * CHTML.h
 *
 *  Created on: 26/05/2011
 *      Author: emili
 */

#ifndef CHTML_H_
#define CHTML_H_

#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>

#include "CTString.h"


class CHTML
{
public:
	CHTML(const std::string& title, const std::string& envId)
	:
		envId_(envId)
	{
		strm_ << "<html>" << std::endl;
		strm_ << "<body>" << std::endl;
		setHead();
		strm_ << "<br>" << std::endl;
		strm_ << "<h1>" << title.c_str() << "</h1>" << std::endl;
		strm_ << "<br>" << std::endl;
		strm_ << "Scenario: " << envId_.c_str() << std::endl;
		strm_ << "<br><br>" << std::endl;
	}

	void addHomotopyClasses(	const std::vector<CIdxValue<unsigned int, float> >& idxValue,
								const std::vector<CTString>& hc)
	{
		strm_ << "<table class=\"table1\">" << std::endl;
		strm_ << "<thead>" << std::endl;
			strm_ << "<tr>" << std::endl;
				strm_ << "<th scope=\"col\" abbr=\"Idx\">Index</th>" << std::endl;
				strm_ << "<th scope=\"col\" abbr=\"hc\">Homotopy Class</th>" << std::endl;
				strm_ << "<th scope=\"col\" abbr=\"LB\">Lower Bound (pix)</th>" << std::endl;
			strm_ << "</tr>" << std::endl;
		strm_ << "</thead>" << std::endl;
		strm_ << "<tbody>" << std::endl;

		for(unsigned int i = 0; i < idxValue.size(); ++i)
			setHCRow(idxValue.at(i).getIndex(), idxValue.at(i).getValue(), hc.at(idxValue.at(i).getIndex()));

		strm_ << "</tbody>" << std::endl;
		strm_ << "</table>" << std::endl;
	}

	void save(const std::string& filename)
	{
		strm_ << "</body>" << std::endl;
		strm_ << "</html>" << std::endl;

		//std::cout << strm_.str() << std::endl;

		std::ofstream file (filename.c_str());
		if(!file.is_open())
		{
			std::cout << "Error. Cannot open " << filename << std::endl;
			return;
		}

		file << strm_.str();
		file.close();
	}


private:
	std::string envId_;
	std::ostringstream strm_;


	void setHead()
	{
		strm_ << "<head>" << std::endl;
		strm_ << "\t<title>Generation of Homotopy Classes</title>" << std::endl;
		strm_ << "\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>" << std::endl;
		strm_ << "\t<meta name=\"description\" content=\"Homotpy classes\" />" << std::endl;
		strm_ << "\t<link rel=\"stylesheet\" href=\"css/style.css\" type=\"text/css\" media=\"screen\"/>" << std::endl;
		strm_ << "</head>" << std::endl;
	}

	void setHCRow(	const unsigned int& idx,
					const float& value,
					const CTString& hc)
	{
		strm_ << "<tr>" << std::endl;
			strm_ << "<td>" << idx << "</td>" << std::endl;
			strm_ << "<td>";
					toHTML(strm_, hc);
			strm_ << "</td>" << std::endl;
			strm_ << "<td><div style=\"float:right;width:100%;\">" << std::fixed << std::setprecision(2) << value	<< "</div></td>" << std::endl;
		strm_ << "</tr>" << std::endl;
	}

};

#endif /* CHTML_H_ */
