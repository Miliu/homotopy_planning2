// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CISSegmentation.h
 *
 *  Created on: 26/08/2010
 *      Author: emili
 */

#ifndef CISSEGMENTATION_H_
#define CISSEGMENTATION_H_

#include "CPolar2D.h"
#include <vector>

class CISSegmentation
{
public:
	CISSegmentation()
	:
		radius1_(0),
		radius2_(CPolar2D<double>()),
		distanceBetweenMax_(0),
		hThreshold_(0),
		lThreshold_(0)
	{}

	CISSegmentation(	const double& radius1,
						const CPolar2D<double>& radius2,
						const double& distanceBetweenMax,
						const unsigned int& hThreshold,
						const unsigned int& lThreshold)
	:
		radius1_(radius1),
		radius2_(radius2),
		distanceBetweenMax_(distanceBetweenMax),
		hThreshold_(hThreshold),
		lThreshold_(lThreshold)
	{}


	double getRadius1() const {return radius1_;}
	CPolar2D<double> getRadius2() const {return radius2_;}
	double getDistanceBetweenMax() const {return distanceBetweenMax_;}
	unsigned int getHThreshold() const {return hThreshold_;}
	unsigned int getLThreshold() const {return lThreshold_;}

	void operator=(const CISSegmentation& c)
	{
		radius1_ = c.radius1_;
		radius2_ = c.radius2_;
		distanceBetweenMax_ = c.distanceBetweenMax_;
		hThreshold_ = c.hThreshold_;
		lThreshold_ = c.lThreshold_;
	}

	friend std::ostream &operator<<(std::ostream& cout, const CISSegmentation& c)
	{
		cout 	<< "radius1: " << c.getRadius1() << std::endl
				<< "radius2: " << c.getRadius2() << std::endl
				<< "distanceBetweenMax: " << c.getDistanceBetweenMax() << std::endl
				<< "hThreshold: " << c.getHThreshold() << std::endl
				<< "lThreshold: " << c.getLThreshold() << std::endl;
		return cout;
	}

	// returns offset for each beam referenced to the sensor
	std::vector<CPolar2D<double> >
	computeOffset(const unsigned int& nBeams, const double& thetaStep)
	{
		std::vector<CPolar2D<double> > offset;

		unsigned int idxRadius2 = round(radius2_.getTheta() / thetaStep);
		unsigned int idxLast = nBeams - 1;
		double radius, angle = 0;
		for(unsigned int i = 0; i < nBeams; i++)
		{
			if(((i < idxRadius2) || (i > (idxLast - idxRadius2))) && nBeams > 180)
				radius = radius2_.getRho();
			else
				radius = radius1_;

			offset.push_back(CPolar2D<double>(radius, angle));
			angle += thetaStep;
		}
		return offset;
	}

private:
	double radius1_;				// in meters. used to discard the corresponding bins
	CPolar2D<double> radius2_;				// rho & theta. used to discard more bins that robotRadius1 for an specic theta width.
									// PERMET DESCARTAR ELS BINS DE DARRERA ON EL SPARUS ES DETECTA A SI MATEIX
	double distanceBetweenMax_;		// distance betwwen IS local max in meters
	unsigned int hThreshold_; 		// high threshold 0..255
	unsigned int lThreshold_; 		// low threshold0..255
};

#endif /* CISSEGMENTATION_H_ */
