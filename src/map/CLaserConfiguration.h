// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CLaserConfiguration.h
 *
 *  Created on: 17/09/2010
 *      Author: emili
 */

#ifndef CLASERCONFIGURATION_H_
#define CLASERCONFIGURATION_H_

#include "CPose2D.h"

class CLaserConfiguration
{
public:
	CLaserConfiguration()
	:
		pose_(CPose2D<double>()),
		resolution_(0),
		maxRange_(0)
	{}

	CLaserConfiguration(const CPose2D<double>& pose, const double& resolution, const double& maxRange)
	:
		pose_(pose),
		resolution_(resolution),
		maxRange_(maxRange)
	{}

	CPose2D<double> getPose() const {return pose_;}
	double getResolution() const {return resolution_;}
	double getMaxRange() const {return maxRange_;}

	void operator=(const CLaserConfiguration& c)
	{
		pose_ = c.pose_;
		resolution_ = c.resolution_;
		maxRange_ = c.maxRange_;
	}

private:
	CPose2D<double> 	pose_; 			// respect to the vehiche
	double 			resolution_; 	// in meters
	double 			maxRange_; 		// in meters
};


#endif /* CLASERCONFIGURATION_H_ */
