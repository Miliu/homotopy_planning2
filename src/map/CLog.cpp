// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CLog.cpp
 *
 *  Created on: 23/09/2010
 *      Author: emili
 */

#include "CLog.h"

using namespace std;

CLog::CLog(const long long& startTime)
:
startTime_(startTime)
{
}

CLog::~CLog()
{}

void
CLog::processNavigatorFile(const std::string& filename)
{
	vector<vector<long long> > navRawTime;
	readNavigatorFile(filename, navRawTime, navData_);
	navTime_ = processTime(navRawTime);
}

// for time(ms) x(m),y(m),yaw(rad) format
void
CLog::processNavigatorFile2(const std::string& filename)
{
	readNavigatorFile2(filename, navTime_, navData_);
}

void
CLog::processISFile(const std::string& filename,
					const std::vector<CPolar2D<double> >& offset,
					CISSegmentation segmentation,
					CISConfiguration& configuration)
{
	isConfiguration_ = configuration;

	std::vector<std::vector<long long> > isRawTime;
	std::vector<std::vector<unsigned int> > isRawData;
	readISFile(filename, isRawTime, isRawData);
	isTime_ = processTime(isRawTime);
	isData_ = processISData(isRawData,
							offset,
							segmentation.getHThreshold(),
							segmentation.getLThreshold(),
							segmentation.getDistanceBetweenMax(),
							isConfiguration_.getResolution());
}

void
CLog::processISFile2(const std::string& filename,
					const std::vector<CPolar2D<double> >& offset,
					CISSegmentation segmentation,
					CISConfiguration& configuration)
{
	isConfiguration_ = configuration;

	std::vector<std::vector<unsigned int> > isRawData;
	readISFile2(filename, isTime_, isRawData);
	isData_ = processISData(isRawData,
							offset,
							segmentation.getHThreshold(),
							segmentation.getLThreshold(),
							segmentation.getDistanceBetweenMax(),
							isConfiguration_.getResolution());
}

void
CLog::readNavigatorFile(const std::string& filename,
						std::vector<std::vector<long long> >& time,
						std::vector<std::vector<double> >& data)
{
	std::string line;
	ifstream file (filename.c_str());
	if(!file.is_open())
	{
		cout << "Error. Cannot open " << filename << endl;
		return;
	}
	while(getline (file,line,'\n'))//!file.eof())
	{
		if(line.at(0) != '%')
		{
			//cout << line << endl;
			std::stringstream    str(line);
			std::string          numberOnlyText;

			if(getline(str,numberOnlyText,'%'))
			{
				//cout << "NUMS:" << numberOnlyText << endl;
				long long lvalue;
				double fvalue;
				std::stringstream    str2(numberOnlyText);

				// time
				//cout << "Time: ";
				std::vector<long long> t(7);
				unsigned int counter = 0;
				while(!str2.eof() && counter < 7)
				{
					str2 >> lvalue;
					//cout << lvalue << endl;
					t.at(counter) = lvalue;
					counter++;
				}
				time.push_back(t);

				// navigator data
				//cout << "Data: ";
				std::vector<double> d(9);
				counter = 0;
				while(!str2.eof() && counter < 9)
				{
					str2 >> fvalue;
					//cout << fvalue << endl;
					d.at(counter) = fvalue;
					counter++;
				}
				data.push_back(d);
			}
		}
	}
	file.close();
}

//file format: time(ms) x(m) y(m) yaw(rad)
void
CLog::readNavigatorFile2(const std::string& filename,
						std::vector<long long>& time,
						std::vector<std::vector<double> >& data)
{
	std::string line;
	ifstream file (filename.c_str());
	if(!file.is_open())
	{
		cout << "Error. Cannot open " << filename << endl;
		return;
	}
	while(getline (file,line,'\n'))//!file.eof())
	{
		if(line.at(0) != '%')
		{
			//cout << line << endl;
			std::stringstream    str(line);
			std::string          numberOnlyText;

			if(getline(str,numberOnlyText,'%'))
			{
				//cout << "NUMS:" << numberOnlyText << endl;
//				long long lvalue;
				long double fvalue;
				std::stringstream    str2(numberOnlyText);

				// time
				//cout << "Time: ";
//				std::vector<long long> t(7);
//				unsigned int counter = 0;
//				while(!str2.eof() && counter < 7)
//				{
//					str2 >> lvalue;
//					//cout << lvalue << endl;
//					t.at(counter) = lvalue;
//					counter++;
//				}
				str2 >> fvalue;
//				cout << fixed << fvalue << endl;
//				fvalue = fvalue*1000.0;
//				long long val = static_cast<long long>(fvalue);
//				cout << "mod: " <<  val << endl;
				time.push_back(static_cast<long long>(fvalue*1000.0));
//				getchar();

				// navigator data
				//cout << "Data: ";
				std::vector<double> d(9,0.0);
				str2 >> fvalue;
				d.at(0) = fvalue;
				str2 >> fvalue;
				d.at(1) = fvalue;
				str2 >> fvalue;
				d.at(5) = fvalue;
//				counter = 0;
//				while(!str2.eof() && counter < 9)
//				{
//					str2 >> fvalue;
//					//cout << fvalue << endl;
//					d.at(counter) = fvalue;
//					counter++;
//				}
				data.push_back(d);
			}
		}
	}
	file.close();
}

void
CLog::readISFile(	const std::string& filename,
			std::vector<std::vector<long long> >& time,
			std::vector<std::vector<unsigned int> >& data)
{
	std::string line;
	ifstream file (filename.c_str());
	unsigned int desiredNBins = round(isConfiguration_.getRange() / isConfiguration_.getResolution());
	bool rangeChecked = false;

	if(!file.is_open())
	{
		cout << "Error. Cannot open " << filename << endl;
		return;
	}
	while(getline (file,line,'\n'))//!file.eof())
	{
		if(line.at(0) != '%')
		{
			//cout << line << endl;
			std::stringstream    str(line);
			std::string          numberOnlyText;

			if(getline(str,numberOnlyText,'%'))
			{
				//cout << "NUMS:" << numberOnlyText << endl;
				long long lvalue;
				unsigned int uivalue;
				std::stringstream    str2(numberOnlyText);

				// time
				//cout << "Time: ";
				std::vector<long long> t(7);
				unsigned int counter = 0;
				while(!str2.eof() && counter < 7)
				{
					str2 >> lvalue;
					//cout << lvalue << endl;
					t.at(counter) = lvalue;
					counter++;
				}
				time.push_back(t);

				// is data
				//cout << "Data: ";
				//cout << "bearing & nbins: ";
				std::vector<unsigned int> d(2);
				counter = 0;
				while(!str2.eof() && counter < 2)
				{
					str2 >> uivalue;
					//cout << uivalue << endl;
					d.at(counter) = uivalue;
					counter++;
				}

				//cout << "bins: ";
				unsigned int nbins = std::min(d.back(), desiredNBins);
				if(!rangeChecked)
				{
					if(nbins == d.back()) // check range (maybe is not the same we want because the round of the internal electronics of the is)
					{
						double range = nbins * isConfiguration_.getResolution();
						if(abs(range - isConfiguration_.getRange()) > 0.0001)
						{
							cout << "Warning: changing range from " << isConfiguration_.getRange() << " to " << range << endl;
							CISConfiguration isConfigurationTmp(	isConfiguration_.getPose(), 		// pose
																	range,							// range
																	isConfiguration_.getResolution(),// resolution
																	isConfiguration_.getBeamWidth(),	// beamWidth
																	isConfiguration_.getSector(),	// sector
																	isConfiguration_.getThetaStep());// thetaStep
							isConfiguration_ = isConfigurationTmp;
						}
					}
					rangeChecked = true;
				}
				counter = 0;
				while(!str2.eof() && counter < nbins)
				{
					str2 >> uivalue;
					//cout << uivalue << endl;
					d.push_back(uivalue);
					counter++;
				}
				data.push_back(d);
				//cout << "counter: " << counter << endl;
			}
		}
//		getchar();
	}
	file.close();
}


void
CLog::readISFile2(	const std::string& filename,
			std::vector<long long>& time,
			std::vector<std::vector<unsigned int> >& data)
{
	std::string line;
	ifstream file (filename.c_str());
	unsigned int desiredNBins = round(isConfiguration_.getRange() / isConfiguration_.getResolution());
	bool rangeChecked = false;

	if(!file.is_open())
	{
		cout << "Error. Cannot open " << filename << endl;
		return;
	}
	while(getline (file,line,'\n'))//!file.eof())
	{
		if(line.at(0) != '%')
		{
			//cout << line << endl;
			std::stringstream    str(line);
			std::string          numberOnlyText;

			if(getline(str,numberOnlyText,'%'))
			{
				//cout << "NUMS:" << numberOnlyText << endl;
//				long long lvalue;
				double fvalue;
				unsigned int uivalue;
				std::stringstream    str2(numberOnlyText);

//				cout << str2.str() << endl;
				// time
				//cout << "Time: ";
//				std::vector<long long> t(7);
				unsigned int counter = 0;
//				while(!str2.eof() && counter < 7)
//				{
//					str2 >> lvalue;
//					//cout << lvalue << endl;
//					t.at(counter) = lvalue;
//					counter++;
//				}
				str2 >> fvalue;
				time.push_back(static_cast<long long>(fvalue*1000.0));
//				cout << str2.str() << endl;
				// is data
				//cout << "Data: ";
//				cout << "bearing & nbins: ";
				std::vector<unsigned int> d(2);
				counter = 0;
				while(!str2.eof() && counter < 2)
				{
					str2 >> uivalue;
					//cout << uivalue << endl;
					d.at(counter) = uivalue;
					counter++;
				}

//				cout << "bins: ";
				unsigned int nbins = std::min(d.back(), desiredNBins);
				if(!rangeChecked)
				{
					if(nbins == d.back()) // check range (maybe is not the same we want because the round of the internal electronics of the is)
					{
						double range = nbins * isConfiguration_.getResolution();
						if(abs(range - isConfiguration_.getRange()) > 0.0001)
						{
							cout << "Warning: changing range from " << isConfiguration_.getRange() << " to " << range << endl;
							CISConfiguration isConfigurationTmp(	isConfiguration_.getPose(), 		// pose
																	range,							// range
																	isConfiguration_.getResolution(),// resolution
																	isConfiguration_.getBeamWidth(),	// beamWidth
																	isConfiguration_.getSector(),	// sector
																	isConfiguration_.getThetaStep());// thetaStep
							isConfiguration_ = isConfigurationTmp;
						}
					}
					rangeChecked = true;
				}
				counter = 0;
				while(!str2.eof() && counter < nbins)
				{
					str2 >> uivalue;
					//cout << uivalue << endl;
					d.push_back(uivalue);
					counter++;
				}
				data.push_back(d);
//				cout << "counter: " << counter << endl;
			}
		}
//		getchar();
	}
	file.close();
}


std::vector<long long>
CLog::processTime(const std::vector<std::vector<long long> >& time)
{
	std::vector<long long> t;
	t.reserve(time.size());
	std::vector<std::vector<long long> >::const_iterator i;
	for(i = time.begin(); i < time.end(); i++)
		t.push_back((i->at(3) * 3600 + i->at(4) * 60 + i->at(5)) * 1000 + i->at(6));
	return t;
}



double
CLog::computeBeamMaximum(	const std::vector<unsigned int>& beam,
							const unsigned int& offset,
							const unsigned int& threshold)
{
	std::vector<unsigned int>::const_iterator i;
	unsigned int infoOffset = 2; //angle & nbins

	std::vector<unsigned int>::const_iterator largest =
				    std::max_element( beam.begin() + infoOffset + offset, beam.end() );

	//cout << "The largest number is " << *largest << "\n";
	//cout << "It is located at index " << largest - numbers.begin() << "\n";
	if(*largest < threshold)
	{
		//cout << "max range" << endl;
		return isConfiguration_.getRange();
	}
	else
	{
		unsigned int idx = largest - (beam.begin() + infoOffset);
		//cout << "MAX & IDX: " << *largest << " " << idx << endl;
		return (idx + 1) * isConfiguration_.getResolution();
	}
}

// Segmentation procedure applied to the MSISPIC
std::vector<double>
CLog::computeBeamRho(	const std::vector<unsigned int>& beam,
						const unsigned int& offset,
						const unsigned int& hThreshold,
						const unsigned int& lThreshold,
						const unsigned int& distance,
						const double& resolution,
						bool& free)
{
	std::vector<double> rho;
	std::vector<SCandidate> candidate;

	free = true;
	std::vector<unsigned int>::const_iterator i;

	//for(i = beam.begin() + offset; i < beam.begin()+100; i++)
	for(i = beam.begin() + offset; i < beam.end(); i++)
	{
		if(free && *i > lThreshold)
		{
			free = false;
		}
		if(*i > hThreshold)
		{
			SCandidate c;
			c.intensity = *i;
			c.index = i-beam.begin();
			candidate.push_back(c);
		}
	}

	if(!candidate.empty())
	{
		std::sort(candidate.begin(), candidate.end(), SSortCandidate());
		std::vector<bool> localMax(candidate.size(), true);
		unsigned int a, b;
		bool exit;
		int v;
		for(a = 0; a < candidate.size(); a++)
		{
			b = 0; exit = false;
			while(b < a && !exit)
			{
				v = candidate.at(a).index - candidate.at(b).index;
				if(static_cast<unsigned int>(abs(v)) < distance)
				{
					localMax.at(a) = false;
					exit = true;
				}
				b+=1;
			}
		}

		for(unsigned int i = 0; i  < localMax.size(); i--)
		{
			if(localMax.at(i) == true)
			{
				rho.push_back((candidate.at(i).index + 1) * resolution);
				std::sort(rho.begin(), rho.end());
			}
		}

//		for(std::vector<SCandidate>::iterator i = candidate.begin(); i < candidate.end(); i++)
//			cout << "(" << i->intensity << "," << i->index << ") " << localMax.at(i-candidate.begin()) << endl;
	}

	return rho;
}

std::vector<CPolar2D<double> >
CLog::processISData(const std::vector<std::vector<unsigned int> >& data,
					const std::vector<CPolar2D<double> >& offset,
					const unsigned int& hThreshold,
					const unsigned int& lThreshold,
					const double& distance,
					const double& resolution)
{
	std::vector<CPolar2D<double> > d;
	double rho, theta;
	bool free;
	unsigned int boffset;
	unsigned int iDistance = round(distance * resolution);

//	ofstream file;
//	file.open("test/isDataProcessada.txt", ios::out);






	std::vector<double> vRho;
	std::vector<std::vector<unsigned int> >::const_iterator i;
	for(i = data.begin(); i < data.end(); i++)
	{
//		cout << "BEAM ----------------------------------------" << endl;
//		debug::printVector("","",*i,false);//printBeam(*i);

		theta = util::grad2rad<double>(i->at(0) / 16); // 16's of gradients to radiants
		boffset = round(offset.at(round(theta / isConfiguration_.getThetaStep())).getRho()  / resolution); // comprovar-ho

		// simple processing
		//vRho = computeMaximum(*i, offset, hThreshold);

		// MSISPIC processing
		vRho = computeBeamRho(*i , boffset + 2, hThreshold, lThreshold, iDistance, resolution, free);
		if(!vRho.empty())
			rho = vRho.at(0);
		else if(free)
		{
			rho = 0.0;
		}
		else
			rho = isConfiguration_.getRange();

//		cout << "rho,theta : " << rho << ", " << theta << endl;
		d.push_back(CPolar2D<double>(rho, theta));
//		file << d.back() << endl;
//		cout << "BEAM ------------------------------------FI----" << endl;
//		getchar();
	}
//	file.close();
	return d;
}
