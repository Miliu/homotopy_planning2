// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CLog.h
 *
 *  Created on: 23/09/2010
 *      Author: emili
 */

#ifndef CLOG_H_
#define CLOG_H_

#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>

#include "util.h"
#include "debug.h"
#include "CISConfiguration.h"
#include "CISSegmentation.h"

struct SCandidate
{
	unsigned int intensity;
	unsigned int index;
};

struct SSortCandidate
{
	bool operator () (const SCandidate& s1 , const SCandidate& s2)
	{
		return s2.intensity < s1.intensity;
	}
};

class CLog
{
public:
	CLog(const long long& startTime = 0);
	virtual ~CLog();

	void processNavigatorFile(const std::string& filename);
	void processNavigatorFile2(const std::string& filename);

	void processISFile(	const std::string& filename,
						const std::vector<CPolar2D<double> >& offset,
						CISSegmentation segmentation,
						CISConfiguration& configuration);
	void processISFile2(	const std::string& filename,
							const std::vector<CPolar2D<double> >& offset,
							CISSegmentation segmentation,
							CISConfiguration& configuration);

	std::vector<long long> getISTime() const {return isTime_;}
	std::vector<CPolar2D<double> > getISData() const {return isData_;}
	std::vector<long long> getNavigatorTime() const {return navTime_;}
	std::vector<std::vector<double> > getNavigatorData() const {return navData_;}
	CISConfiguration getISConfiguration() const {return isConfiguration_;}
private:
	long long startTime_;

	void readNavigatorFile(	const std::string& filename,
							std::vector<std::vector<long long> >& time,
							std::vector<std::vector<double> >& data);
	void readNavigatorFile2(	const std::string& filename,
							std::vector<long long>& time,
							std::vector<std::vector<double> >& data);

	void readISFile(	const std::string& filename,
						std::vector<std::vector<long long> >& time,
						std::vector<std::vector<unsigned int> >& data);
	void readISFile2(	const std::string& filename,
							std::vector<long long>& time,
							std::vector<std::vector<unsigned int> >& data);

	std::vector<long long> processTime(const std::vector<std::vector<long long> >& time);

	double computeBeamMaximum(	const std::vector<unsigned int>& beam,
								const unsigned int& offset,
								const unsigned int& threshold);

	// Segmentation procedure applied to the MSISPIC
	std::vector<double> computeBeamRho(	const std::vector<unsigned int>& beam,
										const unsigned int& offset,
										const unsigned int& hThreshold,
										const unsigned int& lThreshold,
										const unsigned int& distance,
										const double& resolution,
										bool& free);

	std::vector<CPolar2D<double> >processISData(	const std::vector<std::vector<unsigned int> >& data,
												const std::vector<CPolar2D<double> >& offset,
												const unsigned int& hThreshold,
												const unsigned int& lThreshold,
												const double& distance,
												const double& resolution);
	CISConfiguration isConfiguration_;

	std::vector<long long> navTime_;
	std::vector<std::vector<double> > navData_;

	std::vector<long long> isTime_;
	std::vector<CPolar2D<double> > isData_;
};

#endif /* CLOG_H_ */
