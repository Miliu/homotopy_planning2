// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CEnvironment.h
 *
 *  Created on: 23/09/2010
 *      Author: emili
 */

#ifndef CENVIRONMENT_H_
#define CENVIRONMENT_H_

#include <iostream>

#include "util.h"
#include "CLog.h"
#include "CMap.h"
#include "CMapConfiguration.h"
#include "CISConfiguration.h"
#include "CISSegmentation.h"

#include <boost/date_time/posix_time/posix_time.hpp>

namespace environment
{
	enum EType {image = 0, log, log2};
}
class CEnvironment
{
public:
	CEnvironment(const unsigned int& idx, const bool& plot = false);
	void build();
	CMapPtr getMap() const {return map_;};
	CPoint2D<double> getC() const {return c_;}
	CPoint2D<double> getStart() const {return start_;}
	CPoint2D<double> getGoal() const {return goal_;}
	std::string getRoot() const {return root_;}
	virtual ~CEnvironment();
private:
	environment::EType type_;
	unsigned int idx_;
	CMapPtr map_;
	bool plot_;

	CPoint2D<double> start_;
	CPoint2D<double> goal_;
	CPoint2D<double> c_;
	std::string root_;

	// log-type parameters
	CMapConfiguration mapConfiguration_;
	CISConfiguration isConfiguration_;
	CISSegmentation isSegmentation_;
	std::string navFilename_;
	std::string sensorFilename_;
	long int initialTime_;
};

#endif /* CENVIRONMENT_H_ */
