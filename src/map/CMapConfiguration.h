// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CMapConfiguration.h
 *
 *  Created on: 28/08/2010
 *      Author: emili
 */

#ifndef CMAPCONFIGURATION_H_
#define CMAPCONFIGURATION_H_

class CMapConfiguration
{
public:
	CMapConfiguration()
	:
		size_(CPoint2D<int>()),
		resolution_(0),
		offset_(CPoint2D<int>()),
		nBeamsToUpdate_(0)
		{}

	CMapConfiguration(	const CPoint2D<int>& size,
						const double resolution,
						const CPoint2D<int>& offset,
						const unsigned int& nBeamsToUpdate)
	:
		size_(size),
		resolution_(resolution),
		offset_(offset),
		nBeamsToUpdate_(nBeamsToUpdate)
		{}

	CPoint2D<int> getSize() const { return size_; }
	double getResolution() const { return resolution_; }
	CPoint2D<int> getOffset() const { return offset_; }
	unsigned int getNBeamsToUpdate() const { return nBeamsToUpdate_; }

	void operator=(const CMapConfiguration& c)
	{
		size_ = c.size_;
		resolution_ = c.resolution_;
		offset_ = c.offset_;
		nBeamsToUpdate_ = c.nBeamsToUpdate_;;
	}

	friend std::ostream &operator<<(std::ostream& cout, const CMapConfiguration& c)
	{
		cout 	<< "size: " << c.getSize() << std::endl
				<< "resolution: " << c.getResolution() << std::endl
				<< "offset: " << c.getOffset() << std::endl
				<< "nBeamsToUpdate: " << c.getNBeamsToUpdate() << std::endl;
		return cout;
	}
private:
	CPoint2D<int> size_;		// in meters
	double resolution_;		// in meters
	CPoint2D<int> offset_;		// in meters
	unsigned int nBeamsToUpdate_;	// min num. of beams required to apply processing algorigthms

};

#endif /* CMAPCONFIGURATION_H_ */
