// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CMap.cpp
 *
 *  Created on: 04/06/2010
 *  Modified on: 20/09/2010
 *      Author: emili
 */

#include "CMap.h"

using namespace std;

CMap::CMap(const string& filename, const double& resolution, const bool& plot)
:
	resolution_(resolution),
	offset_(CPoint2Di()),
	robotRadius_(0),
	nBeamsToUpdate_(0),
	plot_(plot)
{
	map_ = cv::imread(filename.c_str(), 0);
	if( map_.data == NULL )
	{
		string msg= filename;
		msg += " cannot be loaded.";
		throw runtime_error(msg.c_str());
	}

	vsize_.setVector(map_.cols, map_.rows);
	size_ = vsize_.x() * vsize_.y();

	// map parameters in cells/pixels
	vsizeCell_ = vsize_ / resolution_;
	sizeCell_ = vsizeCell_.x() * vsizeCell_.y();

	cv::Size2i s(0,0);
	double factor = 1 / resolution_;
	cv::resize(map_, cobstacle_, s, factor, factor);

	ccl_.reset(new CCL(cobstacle_, vsizeCell_, 0, 0, plot_));
	label_ = ccl_->getMap();

	prevBPoints_.clear();

	if(plot_)
	{
		cv::namedWindow("ogmMap", CV_WINDOW_AUTOSIZE);
		cvMoveWindow("ogmMap", 0 * vsizeCell_.x() + 100, 100);

		cv::namedWindow("cobstacle", CV_WINDOW_AUTOSIZE);
		cvMoveWindow("cobstacle", 1 * vsizeCell_.x() + 100, 100);

		cv::namedWindow("cclPlot", CV_WINDOW_AUTOSIZE);
		cvMoveWindow("cclPlot", 2 * vsizeCell_.x() + 100, 100);
		labelPlot_ = ccl_->getColorMap();

		cv::imwrite("test/cobstacle.tiff", cobstacle_);
	}
}

CMap::CMap(	const CPoint2Di& size,
			const double& resolution,
			const CPoint2Di& offset,
			const double& robotRadius,
			const unsigned int& nBeamsToUpdate,
			const bool& plot)
:
	vsize_(size),
	size_(size.x() * size.y()),
	resolution_(resolution),
	offset_(offset),
	robotRadius_(robotRadius),
	nBeamsToUpdate_(nBeamsToUpdate),
	plot_(plot)
{
//	cout << "CMap --------------------------------" << endl;
	// map parameters in cells/pixels
	vsizeCell_ = vsize_ / resolution_;
	offsetCell_ = offset_ / resolution_;
	sizeCell_ = vsizeCell_.x() * vsizeCell_.y();
	robotRadiusCell_ = round(robotRadius_/ resolution);

	nBeamsCounter_ = 0;

//	cout << "----1" << endl;
	//ogm_.reset(new COccupancyGridMap(vsize_.x(), vsize_.y(), resolution_, offset_.x(), offset_.y()));
	ogm_.reset(new COccupancyGridMap(vsize_, resolution_, offset_, robotRadiusCell_));
	logoddsMap_ = ogm_->getLogoddMap();
	map_ = ogm_->getMap();
	map_.copyTo(mapDebug_); // for debug

//	cout << "----2" << endl;

	cobstacle_.create(map_.size(), CV_8U);
	cobstacle_ = ogm::unknown;

//	cout << "----3" << endl;
	// robot polygon in meters
	robotMetric_.push_back(CPoint2Df(0.625, 0));
	robotMetric_.push_back(CPoint2Df(-0.625, 0.11));
	robotMetric_.push_back(CPoint2Df(-0.625, -0.11));

	double minArea = 0.3*0.3; // in m^2
	unsigned int minAreaPixels = round((minArea)/ (resolution_ * resolution_));

	nIterationsErode_ = ceilf(robotRadius_/ resolution_);
	cout << "robotRadius, res: " << robotRadius_ << " , " << resolution_ << endl;
	cout << "nIterationsErode: " << nIterationsErode_ << endl;
	cout << "Min area (m^2, pix): " << minArea << ", " << minAreaPixels << endl;
	//ccl_.reset(new CCL(map_, vsizeCell_.x(), vsizeCell_.y(), minAreaPixels, plot_));
	//ccl_.reset(new CCL(map_, vsizeCell_.x(), vsizeCell_.y(), 20, plot_));

//	cout << "----4" << endl;
	ccl_.reset(new CCL(cobstacle_, vsizeCell_, 20, 1, plot_));
//	cout << "----5" << endl;
	label_ = ccl_->getMap();


	prevBPoints_.clear();

	if(plot_)
	{
		cv::namedWindow("ogmMap", CV_WINDOW_AUTOSIZE);
		cvMoveWindow("ogmMap", 0 * vsizeCell_.x() + 150, 150);

		cv::namedWindow("ogmMapDebug", CV_WINDOW_AUTOSIZE);
		cvMoveWindow("ogmMapDebug", 0 * vsizeCell_.x() + 100, 1 * vsizeCell_.y() + 100);

		cv::namedWindow("cobstacle", CV_WINDOW_AUTOSIZE);
		cvMoveWindow("cobstacle", 1 * vsizeCell_.x() + 100, 100);


		cv::namedWindow("cclPlot", CV_WINDOW_AUTOSIZE);
		cvMoveWindow("cclPlot", 2 * vsizeCell_.x() + 150, 150);
		labelPlot_ = ccl_->getColorMap();


		cv::imshow("ogmMap", map_);
		cv::imshow("ogmMapDebug", mapDebug_);
		cv::imshow("cobstacle", cobstacle_);
		cv::imshow("cclPlot", labelPlot_);
	}
//	cout << "CMap -----------------------------FI---" << endl;
}

CMap::~CMap()
{
	if(ogm_) ogm_->save("test/occmap.txt");
}

// for static known map
void
CMap::update(const bool& deleteCoordinates)
{
	ccl_->update(deleteCoordinates);
	if(plot_)
	{
		cv::imshow("ogmMap", map_);
		cv::imshow("cobstacle", cobstacle_);
		cv::imshow("cclPlot", labelPlot_);
	}
}

void
CMap::setLaserConfiguration(const CLaserConfiguration& configuration)
{
	ogm_->setLaserConfiguration(configuration);
}

void
CMap::updateLaserSensor(const CPose2Df& pose, const std::vector<CPolar2Df>& data)
{
	ogm_->updateLaserSensor(pose, data);
	roi_ = ogm_->getROI();

	cv::Mat mapROI(map_, roi_);
	cv::Mat cobstacleROI(cobstacle_, roi_);
	//cv::erode(map_, cobstacle_, cv::Mat(), Point2i(-1, -1), 2);
	cv::erode(mapROI, cobstacleROI, cv::Mat(), Point2i(-1, -1), nIterationsErode_);

	ccl_->update();

	cv::rectangle(labelPlot_, roi_.tl(), roi_.br(), cv::Scalar(0,0,255));
	//ccl_->save("test/ccllabel.txt");

	if(plot_)
	{
		cv::imshow("ogmMap", map_);
		cv::imshow("cclPlot", labelPlot_);
		cv::imshow("cobstacle", cobstacle_);
	}
}

void
CMap::setSonarConfiguration(const CISConfiguration& configuration)
{
	ogm_->setSonarConfiguration(configuration);
}

void
CMap::updateSonarSensor(const CPose2Df& pose, const std::vector<CPolar2Df>& data)
{
	//cout << "update sonar sensor----------------------"<< endl;
	ogm_->updateSonarSensor(pose, data);
	cout << "pose: " << pose << endl;
	cout << "data: ";
	for(std::vector<CPolar2Df>::const_iterator i = data.begin(); i < data.end(); i++)
		cout << *i << " ";
	cout << endl;
	roi_ = ogm_->getROI();
//	roi_.x -= 5;
//	roi_.y -= 5;
//	roi_.height += 5;
//	roi_.width += 5;

	cv::Mat mapROI(map_, roi_);
	cv::Mat cobstacleROI(cobstacle_, roi_);
//	cv::dilate(mapROI, cobstacleROI, cv::Mat(), Point2i(-1, -1), 1);
//	cv::erode(cobstacleROI, cobstacleROI, cv::Mat(), Point2i(-1, -1), 1);

	// sembla que funciona millor així-> Falta buscar ROI
	cv::dilate(map_, cobstacle_, cv::Mat(), Point2i(-1, -1), 1);
	cv::erode(cobstacle_, cobstacle_, cv::Mat(), Point2i(-1, -1), 2);
//
	//cv::erode(mapROI, cobstacleROI, cv::Mat(), Point2i(-1, -1), nIterationsErode_);
	nBeamsCounter_ += data.size();
	if(nBeamsCounter_ >= nBeamsToUpdate_)
	{
		nBeamsCounter_ = 0;
		cout << "CCL update -----------------" << endl;
		time1 = boost::posix_time::microsec_clock::universal_time();
		ccl_->update();
		time2 = boost::posix_time::microsec_clock::universal_time();
		diff = (time2 - time1);
		cout << " CCL update: = " << diff.total_milliseconds() / 1000.0f << " s." << endl;

		//cout << "CCL update ------------DONE-" << endl;

	//	cv::rectangle(labelPlot_, roi_.tl(), roi_.br(), cv::Scalar(0,0,255));
	//	//ccl_->save("test/ccllabel.txt");
	}

	if(plot_)
	{
//		cv::imshow("log-oddMap", logoddsMap_);
		cv::imshow("ogmMap", map_);

		map_.copyTo(mapDebug_); // for debug
		CPoint2Di tmp;
		for(vector<CPose2Df>::iterator i = poseDebug_.begin(); i < poseDebug_.end(); i++)
		{
			tmp = metric2Cell(i->x(), i->y());
			mapDebug_.at<uchar>(tmp.y(), tmp.x()) = 0;
		}

		vector<cv::Point2i> range = computeInitRangeCell(poseDebug_.back(), initRangeMetric_);
//		for(unsigned int i = 0; i < range.size()-1; i++)
//			cv::line(mapDebug_, range.at(i), range.at(i+1),0);
		//cv::polylines(mapDebug_, &range[0], range.size(), 0, true, cv::Scalar(128));
		cv::fillConvexPoly(mapDebug_, &range[0], range.size(), 128);

		robotCell_.clear();
		for(vector<CPoint2Df>::iterator i = robotMetric_.begin(); i < robotMetric_.end(); i++)
		{
			tmp = metric2Cell(util::composition<double>(poseDebug_.back(), *i));
			robotCell_.push_back(Point2i(tmp.x(), tmp.y()));
		}
//		for(unsigned int i = 0; i < robotCell_.size(); i++)
//		{
//			cv::line(mapDebug_, robotCell_.at(i), robotCell_.at((i+1)%robotCell_.size()),0);
//		}
		cv::fillConvexPoly(mapDebug_, &robotCell_[0], robotCell_.size(), 0);



		cv::imshow("ogmMapDebug", mapDebug_);
		cv::imshow("cobstacle", cobstacle_);
		cv::imshow("cclPlot", labelPlot_);
		cv::waitKey(50);
	}
	//cout << "update sonar sensor----------------FI------"<< endl;
}

void
CMap::updateRobotPose(const CPose2Df& pose)
{
	poseDebug_.push_back(pose);
}

void
CMap::setInitRange(const CPose2Df& pose, const std::vector<CPolar2Df>& range)
{
	std::vector<CPolar2Df>::const_iterator i;
	for(i = range.begin(); i < range.end(); i++)
	{
		// reference init range to the robot
		initRangeMetric_.push_back(util::composition<double>(pose, i->toCart()));
	}
}

std::vector<cv::Point2i>
CMap::computeInitRangeCell(const CPose2Df& pose, const std::vector<CPoint2Df>& range)
{
	std::vector<cv::Point2i> rangeCell;
	CPoint2Di v;
	std::vector<CPoint2Df>::const_iterator i;
	for(i = range.begin(); i < range.end(); i++)
	{
		// reference to the current position of the robot
		v = metric2Cell(util::composition<double>(pose, *i));
		rangeCell.push_back(Point2i(v.x(), v.y()));
	}
	return rangeCell;
}

// Compute subsegments from p1 to p2
// Begin and end segment points are inside obstacles when they are not bounded by the map size
vector<CRFSegment>
CMap::computeSubsegments(const CPoint2Di& p1, const CPoint2Di& p2)
{
	vector<CRFSegment> subsegments;
	int offset;
	int lprev = -1, lcurr = -1; // previous & current label
	Point2i prev, curr;
	prev.x = p1.x();
	prev.y = p1.y();

	bool insideObstacle = false;
	CPoint2Di p1tmp, p2tmp;
	p1tmp = p1;


	cv::LineIterator iterator(label_, prev, Point2i(p2.x(), p2.y()), 8);
	for(int i = 0; i < iterator.count; i++, ++iterator)
	{
		offset = iterator.ptr - (uchar*)(label_.data);
		curr.y = offset / label_.step;
		curr.x = (offset - curr.y * label_.step)/(sizeof(int)); // size of pixel

		if(insideObstacle)
		{
			//if(isFree(curr.x, curr.y))
			if(cobstacle_.at<uchar>(curr.y, curr.x) >= 127)
			{
				//cout << "outObst" << endl;
				insideObstacle = false;
				p1tmp.setVector(prev.x, prev.y); // Test -> uncomment for HASTAR/ HRRT
				//p1tmp.setVector(curr.x, curr.y); // FOR HBUG
				lprev = lcurr;
			}
		}
		else
		{
			//if(isOccupied(curr.x, curr.y) && label_.at<int>(curr) != 1)
			//if(cobstacle_.at<uchar>(curr.y, curr.x) < 127 && label_.at<int>(curr) != 1)
			if(cobstacle_.at<uchar>(curr.y, curr.x) < 127 && label_.at<int>(curr) != ccl_->getFirstObstacleId())
			{
				//cout << "inObst" << endl;
				lcurr = label_.at<int>(curr);
				if(lprev != lcurr)
				{
					insideObstacle = true;
					p2tmp.setVector(curr.x, curr.y); // FOR HRRT ASTAR
					//p2tmp.setVector(prev.x, prev.y); // FOR HBUG
					//cout << "New subsegment: " << p1tmp << ", " << p2tmp << endl;
					subsegments.push_back(CRFSegment(p1tmp, p2tmp, CEdge(), static_cast<int>(*iterator.ptr)));
				}
				else
					insideObstacle = false;

			}
		}

		prev = curr;

		//cout << static_cast<int>(*iterator.ptr) << "[" << prev.x << "," << prev.y << "]";
		//cout << static_cast<int>(*iterator) <<
	}

	if(!insideObstacle)
	{
		p2tmp.setVector(curr.x, curr.y);
		subsegments.push_back(CRFSegment(p1tmp, p2tmp));
		//subsegments.push_back(CBoundedSegment(p1tmp, p2tmp, -1));
		//cout << "New subsegment: " << p1tmp << ", " << p2tmp << endl;
	}

	return subsegments;
}

CRFSegment
CMap::computeBoundarySubsegment(const CRFSegment& s)
{
	CPoint2Di p1(s.getP1());
	CPoint2Di p2(s.getP2());
	Point2i prev, curr;
	int offset;

	bool p1Found = false;
	bool p2Found = false;

	cv::LineIterator iterator(label_, Point2i(p1.x(), p1.y()), Point2i(p2.x(), p2.y()), 8);
	for(int i = 0; i < iterator.count; i++, ++iterator)
	{
		offset = iterator.ptr - (uchar*)(label_.data);
		curr.y = offset / label_.step;
		curr.x = (offset - curr.y * label_.step)/(sizeof(int)); // size of pixel

		if(!p1Found)
		{
			if(label_.at<int>(curr.y, curr.x) <= 0)
			{
				p1.setVector(curr.x, curr.y);
				p1Found = true;
			}
		}
		else
		{
			if(label_.at<int>(curr.y, curr.x) <= 0)
			{
				p2.setVector(curr.x, curr.y);
				p2Found = true;
			}
		}
	}
	if(!p2Found) p2 = p1;
	return CRFSegment(p1, p2, s.getEdge());
}

bool
CMap::isBoundaryPoint(const CPoint2Di& p)
{
	if(p.x() == 0) return true;
	if(p.y() == 0) return true;
	if(p.x() == vsizeCell_.x() - 1) return true;
	if(p.y() == vsizeCell_.y() - 1) return true;
	return false;
}

//vector<CBoundedSegment>
//CMap::computeSubsegments(const CPoint2Di& p1, const CPoint2Di& p2)
//{
//	//vector<CSegment2> subsegments;
//	vector<CBoundedSegment> subsegments;
//	int offset;
//	int lprev, lcurr; // previous & current label
//	Point2i prev, curr;
//	prev.x = p1.x();
//	prev.y = p1.y();
//
//	bool insideObstacle = false;
//	CPoint2Di p1tmp, p2tmp;
//	p1tmp = p1;
//
////	cout << "line: " << p1 << ", " << p2 << endl;
//
//	cv::LineIterator iterator(label_, prev, Point2i(p2.x(), p2.y()), 8);
//	for(int i = 0; i < iterator.count; i++, ++iterator)
//	{
//		offset = iterator.ptr - (uchar*)(label_.data);
//		curr.y = offset / label_.step;
//		curr.x = (offset - curr.y * label_.step)/(sizeof(int) /* size of pixel */);
//
////		if(insideObstacle)
////		{
////			if(label_.at<int>(curr) <= 0)
////			{
////				//cout << "outObst" << endl;
////				insideObstacle = false;
////				p1tmp.setVector(prev.x, prev.y);
////			}
////		}
////		else
////		{
////			if(label_.at<int>(curr) > 0)
////			{
////				//cout << "inObst" << endl;
////				insideObstacle = true;
////				p2tmp.setVector(curr.x, curr.y);
////				cout << "New subsegment: " << p1tmp << ", " << p2tmp << endl;
////				subsegments.push_back(CBoundedSegment(p1tmp, p2tmp, static_cast<int>(*iterator.ptr)));
////			}
////		}
//
//		if(insideObstacle)
//		{
//			//if(isFree(curr.x, curr.y))
//			if(cobstacle_.at<uchar>(curr.y, curr.x) >= 127)
//			{
//				//cout << "outObst" << endl;
//				insideObstacle = false;
//				p1tmp.setVector(prev.x, prev.y);
//				lprev = lcurr;
//			}
//		}
//		else
//		{
//			//if(isOccupied(curr.x, curr.y) && label_.at<int>(curr) != 1)
//			if(cobstacle_.at<uchar>(curr.y, curr.x) < 127 && label_.at<int>(curr) != 1)
//			{
//				//cout << "inObst" << endl;
//				lcurr = label_.at<int>(curr);
//				if(lprev != lcurr)
//				{
//					insideObstacle = true;
//					p2tmp.setVector(curr.x, curr.y);
//					//cout << "New subsegment: " << p1tmp << ", " << p2tmp << endl;
//					subsegments.push_back(CBoundedSegment(p1tmp, p2tmp, static_cast<int>(*iterator.ptr)));
//				}
//				else
//					insideObstacle = false;
//
//			}
//		}
//
//		prev = curr;
//
//		//cout << static_cast<int>(*iterator.ptr) << "[" << prev.x << "," << prev.y << "]";
//		//cout << static_cast<int>(*iterator) <<
//	}
//
//	if(!insideObstacle)
//	{
//		p2tmp.setVector(curr.x, curr.y);
//		subsegments.push_back(CBoundedSegment(p1tmp, p2tmp, -1));
//		//cout << "New subsegment: " << p1tmp << ", " << p2tmp << endl;
//	}
//
//	return subsegments;
//}

/*
vector<CRFSegment>
CMap::computeSubsegments(const CRFSegment& s)
{
	vector<CRFSegment> subsegments;
	const CVector2 p1 = s.getP1();
	const CVector2 p2 = s.getP2();
	int offset;
	int lprev, lcurr; // previous & current label
	Point2i prev, curr;
	prev.x = p1.x();
	prev.y = p1.y();

	bool insideObstacle = false;
	CVector2 p1tmp, p2tmp;
	p1tmp = p1;

	cv::LineIterator iterator(label_, prev, Point2i(p2.x(), p2.y()), 8);
	for(int i = 0; i < iterator.count; i++, ++iterator)
	{
		offset = iterator.ptr - (uchar*)(label_.data);
		curr.y = offset / label_.step;
		curr.x = (offset - curr.y * label_.step)/(sizeof(int));


		if(insideObstacle)
		{
			//if(isFree(curr.x, curr.y))
			if(cobstacle_.at<uchar>(curr.y, curr.x) >= 127)
			{
				//cout << "outObst" << endl;
				insideObstacle = false;
				p1tmp.setVector(prev.x, prev.y);
				lprev = lcurr;
			}
		}
		else
		{
			//if(isOccupied(curr.x, curr.y) && label_.at<int>(curr) != 1)
			if(cobstacle_.at<uchar>(curr.y, curr.x) < 127 && label_.at<int>(curr) != 1)
			{
				//cout << "inObst" << endl;
				lcurr = label_.at<int>(curr);
				if(lprev != lcurr)
				{
					insideObstacle = true;
					p2tmp.setVector(curr.x, curr.y);
					//cout << "New subsegment: " << p1tmp << ", " << p2tmp << endl;
					subsegments.push_back(CBoundedSegment(p1tmp, p2tmp, static_cast<int>(*iterator.ptr)));
				}
				else
					insideObstacle = false;

			}
		}

		prev = curr;

		//cout << static_cast<int>(*iterator.ptr) << "[" << prev.x << "," << prev.y << "]";
		//cout << static_cast<int>(*iterator) <<
	}

	if(!insideObstacle)
	{
		p2tmp.setVector(curr.x, curr.y);
		subsegments.push_back(CBoundedSegment(p1tmp, p2tmp, -1));
		//cout << "New subsegment: " << p1tmp << ", " << p2tmp << endl;
	}

	return subsegments;
}
*/

vector<CBPointChange>
CMap::getChanges()
{
	cout << "CMap::getChanges --------------------------" << endl;
	vector<CBPointChange> changes;

	currBPoints_ = getBPoints();
	sort(currBPoints_.begin(), currBPoints_.end());
	unsigned int szCurr = currBPoints_.size();
	unsigned int szPrev = prevBPoints_.size();

	unsigned int i;
	cout << "sizes: " << szCurr << ", " << szPrev << endl;
	if(szCurr >= szPrev)
	{
		cout << szCurr - szPrev << " obstacles added" << endl;
		for(i = 0; i < prevBPoints_.size(); i++)
		{
			if(currBPoints_.at(i).getPoint() != prevBPoints_.at(i).getPoint())
			{
				cout << "Obstacle " << currBPoints_.at(i).getId() << " modified" << endl;
				changes.push_back(CBPointChange(CBPointPtr(new CBPoint(currBPoints_.at(i))), b_updated));
			}
		}
		for(i = prevBPoints_.size(); i < currBPoints_.size(); i++)
		{
			cout  << "Obstacle " <<  currBPoints_.at(i).getId() << " added" << endl;
			changes.push_back(CBPointChange(CBPointPtr(new CBPoint(currBPoints_.at(i))), b_added));
		}


	}
	else if(szCurr < szPrev)
	{
		cout << szPrev - szCurr << " obstacles deleted" << endl;
		cout << "NOT IMPLEMENTED. aborting" << endl;
		abort();
	}
//	else
//	{
//		cout << "Same number of obstacles" << endl;
//		for(i = prevBPoints_.begin(); i < prevBPoints_.end(); i++)
//		{
//			if(i->getPoint() != currBPoints_.at(i - prevBPoints_.begin()).getPoint())
//			{
//				cout << "Obstacle " << i->getId() << " modified" << endl;
//			}
//		}
//	}

	prevBPoints_ = currBPoints_;
	cout << "CMap::getChanges ----------------------FI----" << endl;
	return changes;
}

void
CMap::saveRobotPose(std::string filename)
{
	ofstream f;
	f.open(filename.c_str(), ios::out);

	if(!f.is_open())
	{
		cout << "Error. " << filename << " cannot be created." << endl;
		abort();
	}

	CPoint2Di tmp;
	for(vector<CPose2Df>::iterator i = poseDebug_.begin(); i < poseDebug_.end(); i++)
	{
		tmp = metric2Cell(i->x(), i->y());
		f << tmp.x() << " " << tmp.y() << endl;
	}
	f.close();
}
