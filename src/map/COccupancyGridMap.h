// Copyright (c) 2009-2018 Emili Hernandez
/*
 * COccupancyGridMap.h
 *
 *  Created on: 31/05/2010
 *  Modified on:17/09/2010
 *      Author: emili
 */

#ifndef COCCUPANCYGRIDMAP_H_
#define COCCUPANCYGRIDMAP_H_

#include "util.h"
#include "CPoint2D.h"
#include "CPose2D.h"
#include "CPolar2D.h"
#include "CISConfiguration.h"
#include "CLaserConfiguration.h"

#include <vector>
#include <math.h>
#include <iomanip>
#include <fstream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv/cv.hpp>

#ifndef BASIC_TYPES
#define BASIC_TYPES
typedef cv::Point_<int> Point2i; // openCV
typedef CPoint2D<int> CPoint2Di;
typedef CPoint2D<double> CPoint2Df;
typedef CPose2D<double> CPose2Df;
typedef CPolar2D<double> CPolar2Df;
#endif

namespace ogm
{
	const double probFree = 0.3f;
	const double probUnknown = 0.5f;
	const double probOcc = 0.9f;

	const double lunknown = log(probUnknown) - log(1 - probUnknown);
	const double lfree = log(probFree) - log(1 - probFree) - lunknown;
	const double locc = log(probOcc) - log(1 - probOcc) - lunknown;

	const unsigned char unknown = (1 - probUnknown) * 255;
	const unsigned char free = (1 - probFree) * 255;
	const unsigned char occ = (1 - probOcc) * 255;

	const double alpha = 0.1; // meters
	//const float alphaDiv2 = alpha / 2.0f;

	//TODO: posar-ho a un altre lloc
	struct CLaserConfiguration
	{
		int alpha_; // in cells;
		//int maxRange_;
	};

	struct CSonarConfiguration
	{
		int alpha_; // in cells;
		//int maxRange_;
	};

	enum { XY_SHIFT = 16, XY_ONE = 1 << XY_SHIFT, DRAWING_STORAGE_BLOCK = (1<<12) - 256 };
}

#include <fstream>
#include <sstream>
#include <iostream>

class COccupancyGridMap
{
public:
	COccupancyGridMap(const CPoint2Di size, const double& resolution,const CPoint2Di& offset = CPoint2Di(), const unsigned int& robotRadius = 0);
	virtual ~COccupancyGridMap();

	double getResolution() const {return resolution_;}
	CPoint2Di getVSize() const {return CPoint2Di(round(vsize_.x()), round(vsize_.y()));}
	unsigned long int getSize() const {return size_;}
	cv::Mat getMap() const {return map_;}
	cv::Mat getLogoddMap() const {return logoddsMap_;}
	cv::Rect getROI() const {return roi_;}

	void setLaserConfiguration(const CLaserConfiguration& configuration)
	{
		laserConfiguration_ = configuration;
	}
	void updateLaserSensor(const CPose2Df& pose, const std::vector<CPolar2Df>& data);

	void setSonarConfiguration(const CISConfiguration& configuration)
	{
		sonarConfiguration_ = configuration;
	}
	void updateSonarSensor(const CPose2Df& pose, const std::vector<CPolar2Df>& data);

	void save(const std::string& filename);

private:
	double resolution_;
	CPoint2Di vsize_;
	unsigned long int size_;
	CPoint2Di offset_;
	unsigned int robotRadius_;

	// sensors configuration
	CLaserConfiguration laserConfiguration_;
	ogm::CLaserConfiguration laser_;

	CISConfiguration sonarConfiguration_;
	ogm::CSonarConfiguration sonar_;


	std::vector<Point2i> rposition_;

	cv::Mat logoddsMap_;
	cv::Mat map_;

	// updated ROI of the map
	int xmin_, xmax_, ymin_, ymax_;
	cv::Rect roi_;

	CPoint2Di metric2Cell(const CPoint2Df& p)
	{
		return CPoint2Di(	p.x() / resolution_ + offset_.x(),
							p.y() / resolution_ + offset_.y());
	}

	void fan2DInverseModel(const Point2i& position, const Point2i& up, const Point2i& data, const Point2i& down, const int& alpha);
	void fan2DInverseModel(const Point2i& position, const Point2i& up, const Point2i& data, const Point2i& down, const float& value);
	void fan2DInverseModel(const Point2i& position, const Point2i& up, const Point2i& data, const Point2i& down, const int& radius, const int& distance);


	void lineInverseModel(cv::Point pt1, cv::Point pt2, const int& alpha);

	// with update
	void myFillConvexPoly(cv::Mat& img, cv::Mat& updated, const cv::Point* v, int npts, int line_type, int shift );
	void lineInverseModel(cv::Mat& img, cv::Mat& updated, cv::Point pt1, cv::Point pt2, const int& alpha);

	void myFillConvexPoly(cv::Mat& img, cv::Mat& updated, const cv::Point* v, int npts, int line_type, int shift, const float& value);
	void lineInverseModel(const Point2i& position, const Point2i& data, const float& value);
	void hLineInverseModel(cv::Mat& img, cv::Mat& updated, uchar *ptr, int xl, int xr, const float& value);

	// with update and taking into account robot "radius"
	void myFillConvexPoly(cv::Mat& img, cv::Mat& updated, const cv::Point* v, int npts, int line_type, int shift, const int& alpha, cv::Point2i sensor, int freeRange);
	void hLineInverseModel(cv::Mat& img, cv::Mat& updated, uchar *ptr, int xl, int xr, cv::Point2i sensor, int freeRange);

	cv::Mat updatedCells_;

	std::ofstream file;

};

#endif /* COCCUPANCYGRIDMAP_H_ */
