// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CEnvironment.cpp
 *
 *  Created on: 23/09/2010
 *      Author: emili
 */

#include "CEnvironment.h"

using namespace std;
using namespace boost;
using namespace boost::posix_time;

CEnvironment::CEnvironment(const unsigned int& idx, const bool& plot)
:
idx_(idx),
plot_(plot)
{
	cout << "Setup environment " << idx_ << ": ";
	switch(idx_)
	{
	case 0:
		root_ = "map/basic1.tiff";

		mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
												1,					// resolution
												CPoint2D<int>(),	// offset
												0);
//		start_.setVector(100, 0);
//		goal_.setVector(100, 195);
		start_.setVector(70, 0);
		goal_.setVector(70, 195);
		c_.setVector(75, 100);

		type_ = environment::image;
		break;
	case 1:
		root_ = "map/O2CA2Session_2010-09-03_16-28-05/"; // traj. quadrada i passa pel mig dels dos obstacles, is: nbins = 200 range = 10m
		navFilename_ = root_ + "sparus_navigator_procunit.log";
		sensorFilename_ = root_ + "is_sensor.log";
		initialTime_ = (16 * 3600 + 28 * 60 + 57) * 1000 + 448;

		isSegmentation_ = CISSegmentation(	0.35, 										// radius 1
											CPolar2D<double>(1.25, util::deg2rad<double>(20)),	// radius 2
											2,											// distanceBetweenMax,
											110,										// hThreshold,
											60);										// lThreshold
		isConfiguration_ = CISConfiguration(CPose2D<double>(0.47, 0.0, util::pi), 	// pose
											5,									// range to be used during the process (<= isRange)
											10/200.0,	 						// resolution
											util::deg2rad<double>(3),					// beamWidth
											util::deg2rad<double>(360),					// sector
											util::deg2rad<double>(1.8));				// thetaStep
		mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(18, 16), 	// size
												0.1,						// resolution
												CPoint2D<int>(5, 9),		// offset
												200);						// nBeamsToUpdate

		start_.setVector(1, 1);
		goal_.setVector(6.5, -4);
		c_.setVector(3.5, -2.5);

		type_ = environment::log;
		break;
	case 2:
		root_ = "map/stress_big_13.tiff";

		mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
												1,					// resolution
												CPoint2D<int>(),	// offset
												0);
		start_.setVector(500, 10);
		goal_.setVector(500, 960);
		c_.setVector(500, 350);

		type_ = environment::image;
		break;
	case 3:
		root_ = "map/mini.tiff";
		mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
												1,					// resolution
												CPoint2D<int>(),	// offset
												0);
		start_.setVector(5, 0);
		goal_.setVector(5, 9);
		c_.setVector(3, 3);

		type_ = environment::image;
		break;
	case 4:
		root_ = "map/mini2.tiff";
		mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
												1,					// resolution
												CPoint2D<int>(),	// offset
												0);
		start_.setVector(5, 0);
		goal_.setVector(5, 9);
		c_.setVector(6, 4);

		type_ = environment::image;
		break;

	case 5:
		root_ = "map/test_iros10_2.tiff";
		mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
												1,					// resolution
												CPoint2D<int>(),	// offset
												0);
		start_.setVector(10, 0);
		//goal_.setVector(10, 19);
		goal_.setVector(2, 8);
		c_.setVector(10, 5);

		type_ = environment::image;
		break;
	case 6:
		root_ = "map/test_iros10_2_big_mod.tiff";
		mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
												1,					// resolution
												CPoint2D<int>(),	// offset
												0);
		start_.setVector(100, 0);
		//goal_.setVector(100, 190);
		goal_.setVector(20, 80);
		c_.setVector(100, 50);

		type_ = environment::image;
		break;
	case 7:
		root_ = "map/empty.tiff";
		mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
												1,					// resolution
												CPoint2D<int>(),	// offset
												0);
		start_.setVector(10, 0);
		goal_.setVector(10, 19);
		c_.setVector(10, 5);

		type_ = environment::image;
		break;
	case 8:
		root_ = "map/h.tiff";
		mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
												1,					// resolution
												CPoint2D<int>(),	// offset
												0);
		start_.setVector(0, 0);
		goal_.setVector(19, 19);
		c_.setVector(9, 5);

		type_ = environment::image;
		break;
	case 9:
		root_ = "map/concave.tiff";
		mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
												1,					// resolution
												CPoint2D<int>(),	// offset
												0);
		start_.setVector(100, 10);
		goal_.setVector(100, 190);
		c_.setVector(100, 100);

		type_ = environment::image;
		break;
	case 10:
		root_ = "map/stress_big_15_icaps.tiff";

		mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
												1,					// resolution
												CPoint2D<int>(),	// offset
												0);
		start_.setVector(500, 10);
		goal_.setVector(500, 960);
		c_.setVector(500, 350);

		type_ = environment::image;
		break;
	case 11:
		root_ = "map/test_iros10_2_big_mod2.tiff";
		mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
												1,					// resolution
												CPoint2D<int>(),	// offset
												0);
		start_.setVector(100, 0);
		//goal_.setVector(100, 190);
		goal_.setVector(20, 90);
		c_.setVector(100, 50);

		type_ = environment::image;
		break;
	case 12:
		root_ = "map/cluttered.tiff";
		mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
												1,					// resolution
												CPoint2D<int>(),	// offset
												0);
		start_.setVector(10, 10);
		goal_.setVector(190, 190);
		c_.setVector(30, 170);

		type_ = environment::image;
		break;
	case 13:
		root_ = "map/cluttered_convex.tiff";
		mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
												1,					// resolution
												CPoint2D<int>(),	// offset
												0);
		start_.setVector(10, 10);
		goal_.setVector(190, 190);
		c_.setVector(30, 170);

		type_ = environment::image;
		break;
	case 14:
		root_ = "map/cirs_2obstacles_without_contour.tiff";
		mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
												1,					// resolution
												CPoint2D<int>(),	// offset
												0);
		start_.setVector(60, 100);
		goal_.setVector(115, 50);
		c_.setVector(85, 65);

		type_ = environment::image;
		break;

	case 15:
		root_ = "map/basic1_rotated.tiff";

		mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
												1,					// resolution
												CPoint2D<int>(),	// offset
												0);
//		start_.setVector(100, 0);
//		goal_.setVector(100, 195);

		start_.setVector(70, 195);
		goal_.setVector(190, 125);
		c_.setVector(75, 150);

		type_ = environment::image;
		break;

	case 16:
//		root_ = "map/formigues-cspace.tiff";
//
//		mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
//												1,					// resolution
//												CPoint2D<int>(),	// offset
//												0);
////		start_.setVector(100, 0);
////		goal_.setVector(100, 195);
//
//		//start_.setVector(300, 100);
//		start_.setVector(50, 218);
//		goal_.setVector(950, 625);
//		c_.setVector(400, 400);

		root_ = "map/formigues_scale5.tiff";

		mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
												1,					// resolution
												CPoint2D<int>(),	// offset
												0);
//		start_.setVector(100, 0);
//		goal_.setVector(100, 195);

		//start_.setVector(300, 100);
		start_.setVector(10, 70);
		goal_.setVector(460, 260);
		c_.setVector(295, 80);


		type_ = environment::image;
		break;

	case 17:
		root_ = "map/formigues_scale5.tiff";

		mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
												1,					// resolution
												CPoint2D<int>(),	// offset
												0);

		start_.setVector(10, 70);
		goal_.setVector(415, 85);
		c_.setVector(295, 80);


		type_ = environment::image;
		break;

	case 18:
//			root_ = "map/example_small.tiff";
//			mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
//													1,					// resolution
//													CPoint2D<int>(),	// offset
//													0);
//			start_.setVector(118/2, 290/2);
//			goal_.setVector(110/2, 28/2);
//			c_.setVector(113/2, 161/2);

			root_ = "map/example.tiff";
			mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
													1,					// resolution
													CPoint2D<int>(),	// offset
													0);
			start_.setVector(118, 290);
			goal_.setVector(110, 28);
			c_.setVector(113, 161);

			type_ = environment::image;
			break;

	case 19:
			root_ = "map/mini3.tiff";
			mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
													1,					// resolution
													CPoint2D<int>(),	// offset
													0);
			start_.setVector(5, 0);
			goal_.setVector(5, 9);
			c_.setVector(6, 5);

			type_ = environment::image;
			break;

	case 20:
			root_ = "map/cirs_homotopies_sm/"; // traj. quadrada i passa pel mig dels dos obstacles corregida amb scan matching (pIC Aggelos), is: nbins = 200 range = 10m
			//navFilename_ = root_ + "deadreckoning.txt";
			navFilename_ = root_ + "sm.txt";
			//navFilename_ = root_ + "sm_interp_dr_yaw.txt";
			//navFilename_ = root_ + "sm_interp_rot_spline.txt";
//			navFilename_ = root_ + "sm_interp_rot_lineal.txt";
			sensorFilename_ = root_ + "old_format/is_sensor_of2.log";
			initialTime_ = 0;

			isSegmentation_ = CISSegmentation(	0.45, 										// radius 1
												CPolar2D<double>(1.25, util::deg2rad<double>(20)),	// radius 2
												2,											// distanceBetweenMax,
												110,										// hThreshold,
												60);										// lThreshold
			isConfiguration_ = CISConfiguration(CPose2D<double>(0.47, 0.0, util::pi), 	// pose
												5,									// range to be used during the process (<= isRange)
												10/200.0,	 						// resolution
												util::deg2rad<double>(3),					// beamWidth
												util::deg2rad<double>(360),					// sector
												util::deg2rad<double>(1.8));				// thetaStep
			mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(18, 16), 	// size
													0.1,						// resolution
													CPoint2D<int>(11, 10),//CPoint2D<int>(5, 9),		// offset
													200);						// nBeamsToUpdate

			start_.setVector(1, -0.5);
			goal_.setVector(-5, -4);
			c_.setVector(-2.5, -1.5);

			type_ = environment::log2;
			break;
	case 21:
				//root_ = "map/deadreckoning_without_contour.tiff";
				root_ = "map/deadreckoning_mod.tiff";
				mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
														1,					// resolution
														CPoint2D<int>(),	// offset
														0);
				start_.setVector(120, 95);
				goal_.setVector(60, 60);
				c_.setVector(85, 85);

				type_ = environment::image;
				break;

	case 22:
			root_ = "map/cluttered_mod.tiff";
			mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
													1,					// resolution
													CPoint2D<int>(),	// offset
													0);
			start_.setVector(10, 10);
			goal_.setVector(190, 190);
			c_.setVector(30, 170);

			type_ = environment::image;
			break;


	case 23:
			root_ = "map/basic1.tiff";

			mapConfiguration_ = CMapConfiguration(	CPoint2D<int>(), 	// size
													1,					// resolution
													CPoint2D<int>(),	// offset
													0);
			start_.setVector(70, 0);
			goal_.setVector(70, 195);
			c_.setVector(100, 100);
			//c_.setVector(70, 100);
			type_ = environment::image;
			break;

	default:
		throw runtime_error("Environment does not exists.");
		break;
	}
	cout << root_ << endl << "Done" << endl;
}

void
CEnvironment::build()
{
	switch(type_)
	{
	case environment::image:
	{
		cout << "environment image!" << endl;
		ptime time1(microsec_clock::universal_time());
		map_.reset(new CMap(root_, mapConfiguration_.getResolution(), plot_));
		map_->update(false); // Test for HBug
		ptime time2(microsec_clock::universal_time());
		time_duration diff(time2 - time1);
		cout << "CCL: " <<  diff.total_microseconds() / 1000000.0f << endl;
	}
		break;
	case environment::log:
	{
		unsigned int nBeamsPerScan = round(isConfiguration_.getSector() / isConfiguration_.getThetaStep());
		std::vector<CPolar2D<double> > offset = isSegmentation_.computeOffset(nBeamsPerScan, isConfiguration_.getThetaStep());

		map_.reset(new CMap(mapConfiguration_.getSize(),
							mapConfiguration_.getResolution(),
							mapConfiguration_.getOffset(),
							0.3,
							mapConfiguration_.getNBeamsToUpdate(),
							plot_));

		CLog log(initialTime_);
		log.processNavigatorFile(navFilename_);
		log.processISFile(sensorFilename_, offset, isSegmentation_, isConfiguration_);
		vector<long long> navTime  = log.getNavigatorTime();
		vector<vector<double> >navData = log.getNavigatorData();
		vector<long long> isTime = log.getISTime();
		vector<CPolar2D<double> > isData = log.getISData();

		map_->setInitRange(isConfiguration_.getPose(), offset);
		map_->setSonarConfiguration(isConfiguration_);

		vector<CPolar2D<double> > data;
		unsigned int iNavTime = 0;
		unsigned int idxISTime = 0;

		std::ofstream file;
		file.open("test/pose.txt", ios::out);
//		for(vector<vector<double> >::iterator i = navData.begin(); i < navData.end(); i++)
//		{
//			for(vector<double>::iterator j = i->begin(); j < i->end(); j++)
//				file << *j << " ";
//			file << endl;
//		}
//		file.close();

		// discard first beams
		if(iNavTime < navTime.size() - 1)
		{
			while(isTime.at(idxISTime) < navTime.at(iNavTime) && idxISTime < isTime.size())
				idxISTime++;
		}

		//cout << "InitialTime: " << initialTime << endl; getchar();
		for(iNavTime = 0; iNavTime < navTime.size(); iNavTime++)
		{
			cout << "NAV time - (X,Y): " << navTime.at(iNavTime) << " - " << navData.at(iNavTime).at(0) << ", " << navData.at(iNavTime).at(1) << endl;
			if(iNavTime < navTime.size() - 1)
			{
				data.clear();
				while((idxISTime + 1) < isTime.size() && isTime.at(idxISTime) < navTime.at(iNavTime+1))
				{
					//All staff here
					data.push_back(isData.at(idxISTime));

					cout << "IS time - data: " << isTime.at(idxISTime) << " - " << isData.at(idxISTime) << endl;
					idxISTime++;
				}

				if(navTime.at(iNavTime) >= initialTime_)
				{
					CPose2D<double> pose(navData.at(iNavTime).at(0), navData.at(iNavTime).at(1), navData.at(iNavTime).at(5));

					//map->updateLaserSensor(coord::composition(pose, isConfiguration.pose_), data);
					//cout << pose << endl;
					map_->updateRobotPose(pose);
					map_->updateSonarSensor(util::composition<double>(pose, isConfiguration_.getPose()), data);

					//file << pose << " " << isConfiguration_.getPose() <<  endl;
					file << pose << " " << isConfiguration_.getPose() << " " << util::composition<double>(pose, isConfiguration_.getPose()) << endl;
					//cv::waitKey();
				}
			}
		}
		file.close();
	}
		break;

	case environment::log2:
		{
			unsigned int nBeamsPerScan = round(isConfiguration_.getSector() / isConfiguration_.getThetaStep());
			std::vector<CPolar2D<double> > offset = isSegmentation_.computeOffset(nBeamsPerScan, isConfiguration_.getThetaStep());

			map_.reset(new CMap(mapConfiguration_.getSize(),
								mapConfiguration_.getResolution(),
								mapConfiguration_.getOffset(),
								0.3,
								mapConfiguration_.getNBeamsToUpdate(),
								plot_));

			CLog log(initialTime_);
			log.processNavigatorFile2(navFilename_);
			log.processISFile2(sensorFilename_, offset, isSegmentation_, isConfiguration_);
			vector<long long> navTime  = log.getNavigatorTime();
			vector<vector<double> >navData = log.getNavigatorData();
			vector<long long> isTime = log.getISTime();
			vector<CPolar2D<double> > isData = log.getISData();


			map_->setInitRange(isConfiguration_.getPose(), offset);
			map_->setSonarConfiguration(isConfiguration_);

			vector<CPolar2D<double> > data;
			unsigned int iNavTime = 0;
			unsigned int idxISTime = 0;

			std::ofstream file;
			file.open("test/pose.txt", ios::out);
	//		for(vector<vector<double> >::iterator i = navData.begin(); i < navData.end(); i++)
	//		{
	//			for(vector<double>::iterator j = i->begin(); j < i->end(); j++)
	//				file << *j << " ";
	//			file << endl;
	//		}
	//		file.close();

			// discard first beams
			if(iNavTime < navTime.size() - 1)
			{
				while(isTime.at(idxISTime) < navTime.at(iNavTime) && idxISTime < isTime.size())
					idxISTime++;
			}

			//cout << "InitialTime: " << initialTime << endl; getchar();
			for(iNavTime = 0; iNavTime < navTime.size(); iNavTime++)
			{
				cout << "NAV time - (X,Y): " << navTime.at(iNavTime) << " - " << navData.at(iNavTime).at(0) << ", " << navData.at(iNavTime).at(1) << endl;
				if(iNavTime < navTime.size() - 1)
				{
					data.clear();
					while((idxISTime + 1) < isTime.size() && isTime.at(idxISTime) < navTime.at(iNavTime+1))
					{
						//All staff here
						data.push_back(isData.at(idxISTime));

						cout << "IS time - data: " << isTime.at(idxISTime) << " - " << isData.at(idxISTime) << endl;
						idxISTime++;
					}

					if(navTime.at(iNavTime) >= initialTime_)
					{
						CPose2D<double> pose(navData.at(iNavTime).at(0), navData.at(iNavTime).at(1), navData.at(iNavTime).at(5));

						//map->updateLaserSensor(coord::composition(pose, isConfiguration.pose_), data);
						//cout << pose << endl;
						map_->updateRobotPose(pose);
						map_->updateSonarSensor(util::composition<double>(pose, isConfiguration_.getPose()), data);

						//file << pose << " " << isConfiguration_.getPose() <<  endl;
						file << pose << " " << isConfiguration_.getPose() << " " << util::composition<double>(pose, isConfiguration_.getPose()) << endl;
						//cv::waitKey();
					}
				}

			}
			file.close();
		}
		break;
	default:
		throw runtime_error("Environment type does not exists.");
		break;
	}
}

CEnvironment::~CEnvironment()
{
	// TODO Auto-generated destructor stub
}
