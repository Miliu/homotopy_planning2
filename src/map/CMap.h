// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CMap.h
 * Map for dynamic environments
 *
 *  Created on: 04/06/2010
 *      Author: emili
 */

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "CPoint2D.h"
#include "COccupancyGridMap.h"
#include "CCL.h"
#include "CRFSegment.h"
#include <iostream>
#include <limits>

#ifndef CMAP_H_
#define CMAP_H_

namespace cmap
{
	const double straightCost = 1;
	const double diagonalCost = 1.4; // sqrt(2)
	const unsigned int	maxCost = std::numeric_limits<unsigned int>::max();
	const uchar occUnknown = 127;
};



class CMap
{
public:
	CMap(	const std::string& filename,
			const double& resolution = 1,
			const bool& plot = false);
	CMap(	const CPoint2Di& size,
			const double& resolution,
			const CPoint2Di& offset = CPoint2Di(),
			const double& robotRadius = 0.2,
			const unsigned int& nBeamsToUpdate = 0,
			const bool& plot = false);
	virtual ~CMap();

	long unsigned int getSize() const {return size_;}
	CPoint2Di getVSize() const {return vsize_;}
	double getResolution() const {return resolution_;}

	unsigned long int getSizeCell() const {return sizeCell_;}
	CPoint2Di getVSizeCell() const {return vsizeCell_;}
	CPoint2Di getOffsetCell() const {return offsetCell_;}

	// coordinates in cells
	bool isOccupied(const unsigned int& x, const unsigned int& y) const
	{
		return (cobstacle_.at<uchar>(y, x) < cmap::occUnknown);
	}

	bool isOccupied(const CPoint2Di& p) const
	{
		return (cobstacle_.at<uchar>(p.y(), p.x()) < cmap::occUnknown);
	}

	bool isFree(const unsigned int& x, const unsigned int& y) const
	{
		return (cobstacle_.at<uchar>(y, x) >= cmap::occUnknown);
	}

	bool isFree(const CPoint2Di& p) const
	{
		return (cobstacle_.at<uchar>(p.y(), p.x()) >= cmap::occUnknown);
	}

	bool isInside(const unsigned int& x, const unsigned int& y) const
	{
		return (x >= 0 && x < static_cast<unsigned int>(cobstacle_.cols) && y >= 0 && y < static_cast<unsigned int>(cobstacle_.rows));
	}

	bool isInside(const CPoint2Di& p) const
	{
		return (p.x() >= 0 && p.x() < cobstacle_.cols && p.y() >= 0 && p.y() < cobstacle_.rows);
	}

	uchar getOccupancy(const unsigned int& x, const unsigned int& y) const
	{
		return cobstacle_.at<uchar>(y, x);
	}

	uchar getOccupancy(const CPoint2Di& p) const
	{
		return cobstacle_.at<uchar>(p.y(), p.x());
	}

	int getLabel(const unsigned int& x, const unsigned int& y) const
	{
		return label_.at<int>(y, x);
	}

	int getLabel(const CPoint2Di& p) const
	{
		return label_.at<int>(p.y(), p.x());
	}


	// Conversion methods
	CPoint2Di metric2Cell(const CPoint2Df& p)
	{
		return CPoint2Di(	round(p.x() / resolution_) + offsetCell_.x(),
							round(p.y() / resolution_) + offsetCell_.y());
	}

	CPoint2Di metric2Cell(const double& x, const double& y)
	{
		return CPoint2Di(	round(x / resolution_) + offsetCell_.x(),
							round(y / resolution_) + offsetCell_.y());
	}

	CPoint2Df cell2Metric(const CPoint2Di& p)
	{
		return CPoint2Df(	static_cast<double>(p.x() - offsetCell_.x()) * resolution_,
							static_cast<double>(p.y() - offsetCell_.y()) * resolution_);
	}

	CPoint2Df cell2Metric(const int& x, const int& y)
	{
		return CPoint2Df(	static_cast<double>(x - offsetCell_.x()) * resolution_,
							static_cast<double>(y - offsetCell_.y()) * resolution_);
	}

	void update(const bool& deleteCoordinates = true);

	void setLaserConfiguration(const CLaserConfiguration& configuration);
	void updateLaserSensor(const CPose2Df& pose, const std::vector<CPolar2Df>& data);

	void setSonarConfiguration(const CISConfiguration& configuration);
	void updateSonarSensor(const CPose2Df& pose, const std::vector<CPolar2Df>& data);

	// for debug purposes
	void updateRobotPose(const CPose2Df& pose);
	void setInitRange(const CPose2Df& pose, const std::vector<CPolar2Df>& range);

	std::vector<CBPoint> getBPoints() const {return ccl_->getBPoints();}
	void setBPoints(std::vector<CBPoint> p) {ccl_->setBPoints(p);} // xapussa temporal!! Emili 08-10-2010
	std::vector<CBPointChange> getChanges();


	//std::vector<CBoundedSegment> computeSubsegments(const CPoint2Di& p1, const CPoint2Di& p2);
	std::vector<CRFSegment> computeSubsegments(const CPoint2Di& p1, const CPoint2Di& p2);
	//std::vector<CRFSegment> computeSubsegments(const CRFSegment& s);
	CRFSegment computeBoundarySubsegment(const CRFSegment& s);

	// for plotting purposes
	cv::Mat getCObstacle() const {return cobstacle_;}
	cv::Mat getCCLabel() const {return label_;}
	cv::Mat getMap() const {return map_;}
	void saveRobotPose(std::string filename);

	std::vector<ccl::SObstacleCoord> getObstacleCoordinates() const {return ccl_->getObstacleCoordinates();}

private:
	bool isBoundaryPoint(const CPoint2Di& p);

	// map parameters in meters
	CPoint2Di vsize_;
	long unsigned int size_;
	double resolution_;
	CPoint2Di offset_;
	double robotRadius_;

	// map parameters in cells/pixels
	CPoint2Di vsizeCell_;
	long unsigned int sizeCell_;
	CPoint2Di offsetCell_;
	unsigned int robotRadiusCell_;

	unsigned int nBeamsToUpdate_;
	unsigned int nBeamsCounter_;

	// change detection
	std::vector<CBPoint> currBPoints_;
	std::vector<CBPoint> prevBPoints_;

	// plot flag
	bool plot_;

	boost::shared_ptr<COccupancyGridMap> ogm_;
	cv::Mat logoddsMap_;
	cv::Mat map_;
	cv::Mat mapDebug_;
	std::vector<CPose2Df> poseDebug_;
	std::vector<CPoint2Df> robotMetric_;
	std::vector<cv::Point2i> robotCell_;
	std::vector<CPoint2Df> initRangeMetric_;
	//std::vector<cv::Point2i> initRangeCell_;
	std::vector<cv::Point2i> computeInitRangeCell(const CPose2Df& pose, const std::vector<CPoint2Df>& range);

	cv::Mat cobstacle_;
	cv::Rect roi_;

	boost::shared_ptr<CCL> ccl_;
	cv::Mat label_, labelPlot_;

	int nIterationsErode_;

	boost::posix_time::ptime time1, time2;
	boost::posix_time::time_duration diff;
};

#include <boost/shared_ptr.hpp>
typedef boost::shared_ptr<CMap> CMapPtr;

#endif /* CMAP_H_ */
