// Copyright (c) 2009-2018 Emili Hernandez
/*
 * COccupancyGridMap.cpp
 *
 *  Created on: 31/05/2010
 *  Modified on: 17/09/2010
 *      Author: Emili Hernandez
 */

#include "COccupancyGridMap.h"

using namespace std;

COccupancyGridMap::COccupancyGridMap(const CPoint2Di size, const double& resolution,const CPoint2Di& offset, const unsigned int& robotRadius)
:
	resolution_(resolution),
	vsize_(size),
	offset_(offset),
	robotRadius_(robotRadius),
	xmin_(0), xmax_(0), ymin_(0), ymax_(0)
{
	vsize_ /= resolution_;
	offset_ /= resolution_;
	size_ = vsize_.x() * vsize_.y();

	logoddsMap_.create(vsize_.y(), vsize_.x(), CV_32F);
	logoddsMap_ = ogm::lunknown;
	map_.create(vsize_.y(), vsize_.x(), CV_8U);
	map_ = ogm::unknown;

	updatedCells_.create(vsize_.y(), vsize_.x(), CV_8U);
	updatedCells_ = cv::Scalar(0);

	laser_.alpha_ = 1;
	sonar_.alpha_ = 1;

	file.open("test/isocc.txt", ios::out);
}

COccupancyGridMap::~COccupancyGridMap()
{
	file.close();
}

// need to be tested
void
COccupancyGridMap::updateLaserSensor(const CPose2Df& pose, const vector<CPolar2Df>& data)
{
	Point2i position(	round(pose.x() / resolution_ + offset_.x()),
						round(pose.y() / resolution_ + offset_.y()));

	xmin_ = position.x;
	xmax_ = position.x;
	ymin_ = position.y;
	ymax_ = position.y;

	CPoint2Df dataCart;
	for(vector<CPolar2Df>::const_iterator i = data.begin(); i < data.end(); i++)
	{
		dataCart = util::composition<double>(pose, i->toCart()) / resolution_;
		Point2i data(round(dataCart.x() + offset_.x()),  round(dataCart.y() + offset_.y()));
		if(i->getRho() < laserConfiguration_.getMaxRange())
		{
			lineInverseModel(position, data, laser_.alpha_);
			if(data.x < xmin_) xmin_ = data.x;
			if(data.x > xmax_) xmax_ = data.x;
			if(data.y < ymin_) ymin_ = data.y;
			if(data.y > ymax_) ymax_ = data.y;
		}
	}
	roi_.x = xmin_;
	roi_.y = ymin_;
	roi_.width = xmax_ - xmin_ + 1;
	roi_.height = ymax_ - ymin_ + 1;
}

void
COccupancyGridMap::save(const string& filename)
{
	ofstream f;
	f.open(filename.c_str(), ios::out);

	if(!f.is_open())
	{
		cout << "COccupancyGridMap.Error. " << filename.c_str() << " cannot be created." << endl;
		return;
	}

	int r, c;
	for(r = 0; r < map_.rows; r++)
	{
		for(c = 0; c < map_.cols; c++)
		{
			f << setw(4) << static_cast<unsigned int>(map_.at<uchar>(r, c)) << " ";
		}
		f << endl;
	}
	f.close();
}

// 3rd version 2.
void
COccupancyGridMap::updateSonarSensor(const CPose2Df& pose, const vector<CPolar2Df>& data)
{
	Point2i position(	round(pose.x() / resolution_ + offset_.x()),
						round(pose.y() / resolution_ + offset_.y()));
	xmin_ = position.x;
	xmax_ = position.x;
	ymin_ = position.y;
	ymax_ = position.y;

	CPoint2Di dataCart, upDataCart, downDataCart;
	int value;
	double rho, theta;
	const double beamWidthDiv2 = sonarConfiguration_.getBeamWidth() / 2;
	bool free;

	for(vector<CPolar2Df>::const_iterator i = data.begin(); i < data.end(); i++)
	{
		theta = i->getTheta();
		if(i->getRho() < 0.0001)
		{
			rho = sonarConfiguration_.getRange();
			free = true;
		}
		else
		{
			rho = i->getRho();
			free = false;
		}

		CPolar2Df d(rho, theta);
		dataCart = metric2Cell(util::composition<double>(pose, d.toCart()));
		CPolar2Df up(rho, theta - beamWidthDiv2);
		upDataCart = metric2Cell(util::composition<double>(pose, up.toCart()));
		CPolar2Df down(rho, theta + beamWidthDiv2);
		downDataCart = metric2Cell(util::composition<double>(pose, down.toCart()));

		file << pose << endl;
		//file << pose << " " << d << " " << up << " " << down << endl;
		//file << pose << " " << dataCart << " " << upDataCart << " " << downDataCart << endl;

		cout << "rho, sonarRange: " << rho << ", " << sonarConfiguration_.getRange() << endl;
		if(free || (rho < sonarConfiguration_.getRange()))
		{
			cv::Point2i data(dataCart.x(), dataCart.y());
			cv::Point2i up(upDataCart.x(), upDataCart.y());
			cv::Point2i down(downDataCart.x(), downDataCart.y());

			if(free)
			{
				fan2DInverseModel(position, up, data, down, static_cast<float>(ogm::lfree));
			}
			else
			{
	//			fan2DInverseModel(position, up, data, down, sonar_.alpha_);
				int freeRange = round(rho / resolution_) - robotRadius_;
				fan2DInverseModel(position, up, data, down, robotRadius_, freeRange);
			}

			value = std::min(data.x, std::min(up.x, std::min(down.x, position.x)));
			if(value < xmin_) xmin_ = value;
			value = std::max(data.x, std::max(up.x, std::max(down.x, position.x)));
			if(value > xmax_) xmax_ = value;
			value = std::min(data.y, std::min(up.y, std::min(down.y, position.y)));
			if(value < ymin_) ymin_ = value;
			value = std::max(data.y, std::max(up.y, std::max(down.y, position.y)));
			if(value > ymax_) ymax_ = value;
		}
	}
	roi_.x = xmin_;
	roi_.y = ymin_;
	roi_.width = xmax_ - xmin_ + 1;
	roi_.height = ymax_ - ymin_ + 1;
}


// Inverse models implmentation using modified OpenCV functions-----------------------------------------------
void
COccupancyGridMap::fan2DInverseModel(const Point2i& position, const Point2i& up, const Point2i& data, const Point2i& down, const int& alpha)
{
	cv::Point2i pts[] = {position, up, data, down};
	myFillConvexPoly( map_, updatedCells_, pts, 4, 8, 0);
}

void
COccupancyGridMap::fan2DInverseModel(const Point2i& position, const Point2i& up, const Point2i& data, const Point2i& down, const float& value)
{
	cv::Point2i pts[] = {position, up, data, down};
	myFillConvexPoly( map_, updatedCells_, pts, 4, 8, 0, value);
}

void
COccupancyGridMap::fan2DInverseModel(const Point2i& position, const Point2i& up, const Point2i& data, const Point2i& down, const int& radius, const int& distance)
{
	cv::Point2i pts[] = {position, up, data, down};
	myFillConvexPoly( map_, updatedCells_, pts, 4, 8, 0, radius, position, distance);
}

void
COccupancyGridMap::lineInverseModel(cv::Point pt1, cv::Point pt2, const int& alpha)
{
	int offset, x, y;
	int i, t;

	cv::LineIterator iterator(map_, pt1, pt2, 8);

	t = iterator.count-alpha;
	for(i = 0; i < t; i++, ++iterator)
	{
		offset = iterator.ptr - (uchar*)(map_.data);
		y = offset/map_.step;
		x = (offset - y*map_.step)/(sizeof(uchar));

		logoddsMap_.at<float>(y, x) += ogm::lfree;
		map_.at<uchar>(y, x) = round((1.0f / 1 - expf(logoddsMap_.at<float>(y, x))) * 255.0f); //ogm::free;

	}

	for(i = t; i < iterator.count; i++, ++iterator)
	{
		offset = iterator.ptr - (uchar*)(map_.data);
		y = offset/map_.step;
		x = (offset - y*map_.step)/(sizeof(uchar));

		logoddsMap_.at<float>(y, x) += ogm::locc;
		map_.at<uchar>(y, x) = round((1.0f / 1 - expf(logoddsMap_.at<float>(y, x))) * 255.0f); //ogm::occ;
	}
}

//filling convex polygon. v - array of vertices, ntps - number of points
void
COccupancyGridMap::myFillConvexPoly(cv::Mat& img, cv::Mat& updated, const cv::Point* v, int npts, int line_type, int shift )
{
    struct
    {
        int idx, di;
        int x, dx, ye;
    }
    edge[2];

    int delta = shift ? 1 << (shift - 1) : 0;
    int i, y, imin = 0, left = 0, right = 1, x1, x2;
    int edges = npts;
    int xmin, xmax, ymin, ymax;
    uchar* ptr = img.data;
    cv::Size size = img.size();
    //int pix_size = (int)img.elemSize();
    cv::Point p0;
    int delta1, delta2;

    if( line_type < CV_AA )
        delta1 = delta2 = ogm::XY_ONE >> 1;
//    else
//        delta1 = XY_ONE - 1, delta2 = 0;

    p0 = v[npts - 1];
    p0.x <<= ogm::XY_SHIFT - shift;
    p0.y <<= ogm::XY_SHIFT - shift;

    assert( 0 <= shift && shift <= ogm::XY_SHIFT );
    xmin = xmax = v[0].x;
    ymin = ymax = v[0].y;

    for( i = 0; i < npts; i++ )
    {
    	cv::Point p = v[i];
        if( p.y < ymin )
        {
            ymin = p.y;
            imin = i;
        }

        ymax = std::max( ymax, p.y );
        xmax = std::max( xmax, p.x );
        xmin = MIN( xmin, p.x );

        p.x <<= ogm::XY_SHIFT - shift;
        p.y <<= ogm::XY_SHIFT - shift;

       // if( line_type <= 8 )
       // {
            if( shift == 0 )
            {
                cv::Point pt0, pt1;
                pt0.x = p0.x >> ogm::XY_SHIFT;
                pt0.y = p0.y >> ogm::XY_SHIFT;
                pt1.x = p.x >> ogm::XY_SHIFT;
                pt1.y = p.y >> ogm::XY_SHIFT;
                //myLine( img, pt0, pt1, color, line_type );
                //myLine2(pt0, pt1, 1);
                //cv::line( img, pt0, pt1, (cv::Scalar)color, line_type );
            }
            else
            {
            	cout << "ERROR: shift != 0" << endl;
            	return;
            }
        //}
        p0 = p;
    }


    if(npts < 3)  return;

    // must be done in this order
    for( i = 1; i < npts-1; i++ )
    	lineInverseModel(img, updated, v[i], v[i+1], ogm::locc);
		//lineInverseModelOccupied(img, updated, v[i], v[i+1]);
    lineInverseModel(img, updated, v[0], v[1], 1);
    lineInverseModel(img, updated, v[0], v[i], 1);


    xmin = (xmin + delta) >> shift;
    xmax = (xmax + delta) >> shift;
    ymin = (ymin + delta) >> shift;
    ymax = (ymax + delta) >> shift;
    cv::Rect roi(xmin, ymin, xmax - xmin + 1, ymax - ymin +1) ;
    cv::Mat updatedROI(updated, roi);

    if( npts < 3 || xmax < 0 || ymax < 0 || xmin >= size.width || ymin >= size.height )
            return;

    ymax = MIN( ymax, size.height - 1 );
    edge[0].idx = edge[1].idx = imin;

    edge[0].ye = edge[1].ye = y = ymin;
    edge[0].di = 1;
    edge[1].di = npts - 1;

    ptr += img.step*y;

    do
    {
        if( line_type < CV_AA || y < ymax || y == ymin )
        {
            for( i = 0; i < 2; i++ )
            {
                if( y >= edge[i].ye )
                {
                    int idx = edge[i].idx, di = edge[i].di;
                    int xs = 0, xe, ye, ty = 0;

                    for(;;)
                    {
                        ty = (v[idx].y + delta) >> shift;
                        if( ty > y || edges == 0 )
                            break;
                        xs = v[idx].x;
                        idx += di;
                        idx -= ((idx < npts) - 1) & npts;   // idx -= idx >= npts ? npts : 0
                        edges--;
                    }

                    ye = ty;
                    xs <<= ogm::XY_SHIFT - shift;
                    xe = v[idx].x << (ogm::XY_SHIFT - shift);

                    // no more edges
                    if( y >= ye )
                    {
                    	updatedROI =  cv::Scalar(0);
                        return;
                    }

                    edge[i].ye = ye;
                    edge[i].dx = ((xe - xs)*2 + (ye - y)) / (2 * (ye - y));
                    edge[i].x = xs;
                    edge[i].idx = idx;
                }
            }
        }

        if( edge[left].x > edge[right].x )
        {
            left ^= 1;
            right ^= 1;
        }

        x1 = edge[left].x;
        x2 = edge[right].x;

        if( y >= 0 )
        {
            int xx1 = (x1 + delta1) >> ogm::XY_SHIFT;
            int xx2 = (x2 + delta2) >> ogm::XY_SHIFT;

            if( xx2 >= 0 && xx1 < size.width )
            {
                if( xx1 < 0 )
                    xx1 = 0;
                if( xx2 >= size.width )
                    xx2 = size.width - 1;
                hLineInverseModel(img, updated, ptr, xx1, xx2, ogm::lfree); // fills polygon
            }
        }

        x1 += edge[left].dx;
        x2 += edge[right].dx;

        edge[left].x = x1;
        edge[right].x = x2;
        ptr += img.step;
    }
    while( ++y <= ymax );
}

void
COccupancyGridMap::lineInverseModel(cv::Mat& img, cv::Mat& updated, cv::Point pt1, cv::Point pt2, const int& alpha)
{
	int offset, x, y;
	int i, t;

	cv::LineIterator iterator(img, pt1, pt2, 8);

	float occValue = ogm::lfree;
	t = iterator.count-alpha;
	for(i = 0; i < t; i++, ++iterator)
	{
		offset = iterator.ptr - (uchar*)(img.data);
		y = offset/img.step;
		x = (offset - y*img.step)/(sizeof(uchar));

		if(updated.at<uchar>(y, x) == 0)
		{
			logoddsMap_.at<float>(y, x) += ogm::lfree;
			img.at<uchar>(y, x) = round((1.0f / 1 - expf(logoddsMap_.at<float>(y, x))) * 255.0f); //ogm::free;
			updated.at<uchar>(y, x) = 255;
		}

	}

	occValue = ogm::locc;
	for(i = t; i < iterator.count; i++, ++iterator)
	{
		offset = iterator.ptr - (uchar*)(img.data);
		y = offset/img.step;
		x = (offset - y*img.step)/(sizeof(uchar));
		if(updated.at<uchar>(y, x) == 0)
		{
			logoddsMap_.at<float>(y, x) += occValue;
			img.at<uchar>(y, x) = round((1.0f / 1 - expf(logoddsMap_.at<float>(y, x))) * 255.0f); //ogm::free;
			updated.at<uchar>(y, x) = 255;
		}
	}
}

//filling convex polygon. v - array of vertices, ntps - number of points
void
COccupancyGridMap::myFillConvexPoly(cv::Mat& img, cv::Mat& updated, const cv::Point* v, int npts, int line_type, int shift, const int& alpha, cv::Point2i sensor, int freeRange)
{
	struct
    {
        int idx, di;
        int x, dx, ye;
    }
    edge[2];

    int delta = shift ? 1 << (shift - 1) : 0;
    int i, y, imin = 0, left = 0, right = 1, x1, x2;
    int edges = npts;
    int xmin, xmax, ymin, ymax;
    uchar* ptr = img.data;
    cv::Size size = img.size();
    //int pix_size = (int)img.elemSize();
    cv::Point p0;
    int delta1, delta2;

    if( line_type < CV_AA )
        delta1 = delta2 = ogm::XY_ONE >> 1;
//    else
//        delta1 = XY_ONE - 1, delta2 = 0;

    p0 = v[npts - 1];
    p0.x <<= ogm::XY_SHIFT - shift;
    p0.y <<= ogm::XY_SHIFT - shift;

    assert( 0 <= shift && shift <= ogm::XY_SHIFT );
    xmin = xmax = v[0].x;
    ymin = ymax = v[0].y;

    for( i = 0; i < npts; i++ )
    {
    	cv::Point p = v[i];
        if( p.y < ymin )
        {
            ymin = p.y;
            imin = i;
        }

        ymax = std::max( ymax, p.y );
        xmax = std::max( xmax, p.x );
        xmin = MIN( xmin, p.x );

        p.x <<= ogm::XY_SHIFT - shift;
        p.y <<= ogm::XY_SHIFT - shift;

       // if( line_type <= 8 )
       // {
            if( shift == 0 )
            {
                cv::Point pt0, pt1;
                pt0.x = p0.x >> ogm::XY_SHIFT;
                pt0.y = p0.y >> ogm::XY_SHIFT;
                pt1.x = p.x >> ogm::XY_SHIFT;
                pt1.y = p.y >> ogm::XY_SHIFT;
                //myLine( img, pt0, pt1, color, line_type );
                //myLine2(pt0, pt1, 1);
                //cv::line( img, pt0, pt1, (cv::Scalar)color, line_type );
            }
            else
            {
            	cout << "ERROR: shift != 0" << endl;
            	return;
            }
        //}
        p0 = p;
    }


    if(npts < 3)  return;

    // must be done in this order
    for( i = 1; i < npts-1; i++ )
    	lineInverseModel(img, updated, v[i], v[i+1], ogm::locc);
		//lineInverseModelOccupied(img, updated, v[i], v[i+1]);
    lineInverseModel(img, updated, v[0], v[1], alpha);
    lineInverseModel(img, updated, v[0], v[i], alpha);


    xmin = (xmin + delta) >> shift;
    xmax = (xmax + delta) >> shift;
    ymin = (ymin + delta) >> shift;
    ymax = (ymax + delta) >> shift;
    cv::Rect roi(xmin, ymin, xmax - xmin + 1, ymax - ymin +1) ;
    cv::Mat updatedROI(updated, roi);

    if( npts < 3 || xmax < 0 || ymax < 0 || xmin >= size.width || ymin >= size.height )
            return;

    ymax = MIN( ymax, size.height - 1 );
    edge[0].idx = edge[1].idx = imin;

    edge[0].ye = edge[1].ye = y = ymin;
    edge[0].di = 1;
    edge[1].di = npts - 1;

    ptr += img.step*y;

    do
    {
        if( line_type < CV_AA || y < ymax || y == ymin )
        {
            for( i = 0; i < 2; i++ )
            {
                if( y >= edge[i].ye )
                {
                    int idx = edge[i].idx, di = edge[i].di;
                    int xs = 0, xe, ye, ty = 0;

                    for(;;)
                    {
                        ty = (v[idx].y + delta) >> shift;
                        if( ty > y || edges == 0 )
                            break;
                        xs = v[idx].x;
                        idx += di;
                        idx -= ((idx < npts) - 1) & npts;   // idx -= idx >= npts ? npts : 0
                        edges--;
                    }

                    ye = ty;
                    xs <<= ogm::XY_SHIFT - shift;
                    xe = v[idx].x << (ogm::XY_SHIFT - shift);

                    // no more edges
                    if( y >= ye )
                    {
                    	updatedROI =  cv::Scalar(0);
                        return;
                    }

                    edge[i].ye = ye;
                    edge[i].dx = ((xe - xs)*2 + (ye - y)) / (2 * (ye - y));
                    edge[i].x = xs;
                    edge[i].idx = idx;
                }
            }
        }

        if( edge[left].x > edge[right].x )
        {
            left ^= 1;
            right ^= 1;
        }

        x1 = edge[left].x;
        x2 = edge[right].x;

        if( y >= 0 )
        {
            int xx1 = (x1 + delta1) >> ogm::XY_SHIFT;
            int xx2 = (x2 + delta2) >> ogm::XY_SHIFT;

            if( xx2 >= 0 && xx1 < size.width )
            {
                if( xx1 < 0 )
                    xx1 = 0;
                if( xx2 >= size.width )
                    xx2 = size.width - 1;
                hLineInverseModel(img, updated, ptr, xx1, xx2, sensor, freeRange*freeRange); // fills polygon
            }
        }

        x1 += edge[left].dx;
        x2 += edge[right].dx;

        edge[left].x = x1;
        edge[right].x = x2;
        ptr += img.step;
    }
    while( ++y <= ymax );
}

// all units in cells
void
COccupancyGridMap::hLineInverseModel(cv::Mat& img, cv::Mat& updated, uchar *ptr, int xl, int xr, cv::Point2i sensor, int freeRange)
{
	int offset, x, y;
    uchar* hline_ptr = (uchar*)(ptr) + xl;
    uchar* hline_max_ptr = (uchar*)(ptr) + xr;

    for( ; hline_ptr <= hline_max_ptr; hline_ptr ++)
    {
 		offset = hline_ptr - (uchar*)(img.data);
		y = offset/img.step;
		x = (offset - y*img.step)/(sizeof(uchar));
		if(updated.at<uchar>(y, x) == 0)
		{
			int dx = sensor.x - x;
			int dy = sensor.y - y;
			int d = dx * dx + dy * dy;
			if(d < freeRange)
				logoddsMap_.at<float>(y, x) += ogm::lfree;
			else
				logoddsMap_.at<float>(y, x) += ogm::locc;
			img.at<uchar>(y, x) = round((1.0f / 1 - expf(logoddsMap_.at<float>(y, x))) * 255.0f); //ogm::free;
			updated.at<uchar>(y, x) = 255;
		}
    }
}

//filling convex polygon. v - array of vertices, ntps - number of points
void
COccupancyGridMap::myFillConvexPoly(cv::Mat& img, cv::Mat& updated, const cv::Point* v, int npts, int line_type, int shift, const float& value )
{
    struct
    {
        int idx, di;
        int x, dx, ye;
    }
    edge[2];

    int delta = shift ? 1 << (shift - 1) : 0;
    int i, y, imin = 0, left = 0, right = 1, x1, x2;
    int edges = npts;
    int xmin, xmax, ymin, ymax;
    uchar* ptr = img.data;
    cv::Size size = img.size();
    //int pix_size = (int)img.elemSize();
    cv::Point p0;
    int delta1, delta2;

    if( line_type < CV_AA )
        delta1 = delta2 = ogm::XY_ONE >> 1;
//    else
//        delta1 = XY_ONE - 1, delta2 = 0;

    p0 = v[npts - 1];
    p0.x <<= ogm::XY_SHIFT - shift;
    p0.y <<= ogm::XY_SHIFT - shift;

    assert( 0 <= shift && shift <= ogm::XY_SHIFT );
    xmin = xmax = v[0].x;
    ymin = ymax = v[0].y;

    for( i = 0; i < npts; i++ )
    {
    	cv::Point p = v[i];
        if( p.y < ymin )
        {
            ymin = p.y;
            imin = i;
        }

        ymax = std::max( ymax, p.y );
        xmax = std::max( xmax, p.x );
        xmin = MIN( xmin, p.x );

        p.x <<= ogm::XY_SHIFT - shift;
        p.y <<= ogm::XY_SHIFT - shift;

       // if( line_type <= 8 )
       // {
            if( shift == 0 )
            {
                cv::Point pt0, pt1;
                pt0.x = p0.x >> ogm::XY_SHIFT;
                pt0.y = p0.y >> ogm::XY_SHIFT;
                pt1.x = p.x >> ogm::XY_SHIFT;
                pt1.y = p.y >> ogm::XY_SHIFT;
                //myLine( img, pt0, pt1, color, line_type );
                //myLine2(pt0, pt1, 1);
                //cv::line( img, pt0, pt1, (cv::Scalar)color, line_type );
            }
            else
            {
            	cout << "ERROR: shift != 0" << endl;
            	return;
            }
        //}
        p0 = p;
    }


    if(npts < 3)  return;

    // must be done in this order
    for( i = 1; i < npts-1; i++ )
		lineInverseModel(img, updated, v[i], v[i+1], value);
    lineInverseModel(img, updated, v[0], v[1], value);
    lineInverseModel(img, updated, v[0], v[i], value);


    xmin = (xmin + delta) >> shift;
    xmax = (xmax + delta) >> shift;
    ymin = (ymin + delta) >> shift;
    ymax = (ymax + delta) >> shift;
    cv::Rect roi(xmin, ymin, xmax - xmin + 1, ymax - ymin +1) ;
    cv::Mat updatedROI(updated, roi);

    if( npts < 3 || xmax < 0 || ymax < 0 || xmin >= size.width || ymin >= size.height )
            return;

    ymax = MIN( ymax, size.height - 1 );
    edge[0].idx = edge[1].idx = imin;

    edge[0].ye = edge[1].ye = y = ymin;
    edge[0].di = 1;
    edge[1].di = npts - 1;

    ptr += img.step*y;

    do
    {
        if( line_type < CV_AA || y < ymax || y == ymin )
        {
            for( i = 0; i < 2; i++ )
            {
                if( y >= edge[i].ye )
                {
                    int idx = edge[i].idx, di = edge[i].di;
                    int xs = 0, xe, ye, ty = 0;

                    for(;;)
                    {
                        ty = (v[idx].y + delta) >> shift;
                        if( ty > y || edges == 0 )
                            break;
                        xs = v[idx].x;
                        idx += di;
                        idx -= ((idx < npts) - 1) & npts;   // idx -= idx >= npts ? npts : 0
                        edges--;
                    }

                    ye = ty;
                    xs <<= ogm::XY_SHIFT - shift;
                    xe = v[idx].x << (ogm::XY_SHIFT - shift);

                    // no more edges
                    if( y >= ye )
                    {
                    	updatedROI =  cv::Scalar(0);
                        return;
                    }

                    edge[i].ye = ye;
                    edge[i].dx = ((xe - xs)*2 + (ye - y)) / (2 * (ye - y));
                    edge[i].x = xs;
                    edge[i].idx = idx;
                }
            }
        }

        if( edge[left].x > edge[right].x )
        {
            left ^= 1;
            right ^= 1;
        }

        x1 = edge[left].x;
        x2 = edge[right].x;

        if( y >= 0 )
        {
            int xx1 = (x1 + delta1) >> ogm::XY_SHIFT;
            int xx2 = (x2 + delta2) >> ogm::XY_SHIFT;

            if( xx2 >= 0 && xx1 < size.width )
            {
                if( xx1 < 0 )
                    xx1 = 0;
                if( xx2 >= size.width )
                    xx2 = size.width - 1;
                hLineInverseModel(img, updated, ptr, xx1, xx2, value); // fills polygon
            }
        }

        x1 += edge[left].dx;
        x2 += edge[right].dx;

        edge[left].x = x1;
        edge[right].x = x2;
        ptr += img.step;
    }
    while( ++y <= ymax );
}

void
COccupancyGridMap::lineInverseModel(const Point2i& position, const Point2i& data, const float& value)
{
	int offset, x, y;
	cv::LineIterator iterator(map_, position, data, 8);

	for(int i = 0; i < iterator.count; i++, ++iterator)
	{
		offset = iterator.ptr - (uchar*)(map_.data);
		y = offset/map_.step;
		x = (offset - y*map_.step)/(sizeof(uchar));

		logoddsMap_.at<float>(y, x) += value;
		map_.at<uchar>(y, x) = round((1.0f / 1 - expf(logoddsMap_.at<float>(y, x))) * 255.0f);
	}
}

void
COccupancyGridMap::hLineInverseModel(cv::Mat& img, cv::Mat& updated, uchar *ptr, int xl, int xr, const float& value)
{
	int offset, x, y;
    uchar* hline_ptr = (uchar*)(ptr) + xl;
    uchar* hline_max_ptr = (uchar*)(ptr) + xr;

    for( ; hline_ptr <= hline_max_ptr; hline_ptr ++)
    {
 		offset = hline_ptr - (uchar*)(img.data);
		y = offset/img.step;
		x = (offset - y*img.step)/(sizeof(uchar));
		if(updated.at<uchar>(y, x) == 0)
		{
			logoddsMap_.at<float>(y, x) += value;
			img.at<uchar>(y, x) = round((1.0f / 1 - expf(logoddsMap_.at<float>(y, x))) * 255.0f); //ogm::free;
			updated.at<uchar>(y, x) = 255;
		}
    }
}

