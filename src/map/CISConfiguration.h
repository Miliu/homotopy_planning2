// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CISConfiguration.h
 *
 *  Created on: 26/08/2010
 *  Modified on: 17/09/2010
 *      Author: emili
 */

#ifndef CISCONFIGURATION_H_
#define CISCONFIGURATION_H_

#include "CPose2D.h"

class CISConfiguration
{
public:
public:
	CISConfiguration()
	:
			pose_(CPose2D<double>()),
			range_(0),
			resolution_(0),
			beamWidth_(0),
			sector_(0),
			thetaStep_(0)
	{}

	CISConfiguration(	const CPose2D<double>& pose,
						const double& range,
						const double& resolution,
						const double& beamWidth,
						const double& sector,
						const double& thetaStep)
	:
		pose_(pose),
		range_(range),
		resolution_(resolution),
		beamWidth_(beamWidth),
		sector_(sector),
		thetaStep_(thetaStep)
	{}

	CPose2D<double> getPose() const {return pose_;}
	double getResolution() const {return resolution_;}
	double getRange() const {return range_;}
	double getBeamWidth() const {return beamWidth_;}
	double getSector() const {return sector_;}
	double getThetaStep() const {return thetaStep_;}

	void operator=(const CISConfiguration& c)
	{
		pose_ = c.pose_;
		range_ = c.range_;
		resolution_ = c.resolution_;
		beamWidth_ = c.beamWidth_;
		sector_ = c.sector_;
		thetaStep_ = c.thetaStep_;
	}

	friend std::ostream &operator<<(std::ostream& cout, const CISConfiguration& c)
	{
		cout 	<< "pose: " << c.getPose() << std::endl
				<< "range: " << c.getRange() << std::endl
				<< "resolution: " << c.getResolution() << std::endl
				<< "beamWidth: " << c.getBeamWidth() << std::endl
				<< "sector: " << c.getSector() << std::endl
				<< "thetaStep: " << c.getThetaStep() << std::endl;
		return cout;
	}

private:
	CPose2D<double> 	pose_; 			// respect to the vehicle
	double 			range_; 		// in meters
	double 			resolution_; 	// in meters
	double			beamWidth_;		// in radiants
	double			sector_;		// in radiants
	double			thetaStep_;		// in radiants
};

#endif /* CISCONFIGURATION_H_ */
