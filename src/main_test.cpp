/*
 * main.cpp
 *
 *  Created on: 17/09/2010
 *      Author: emili
 */

//#define _NDEBUG 1

#include "CMap.h"
#include "CEnvironment.h"
#include "CTSpace.h"

//#include "CAStar.h"
//#include "CHAStar.h"
//#include "CRRT.h"
//#include "CHRRT.h"

#include <iostream>
#include <boost/date_time/posix_time/posix_time.hpp>
using namespace boost::posix_time;





//struct hashstr
//{
//	unsigned int operator()(const CEdge& e) const
//	{
//		return static_cast<int>(e.getType())* 1000 + e.getSubsegment();
//	}
//};
//
//typedef std::unordered_map<CEdge, std::vector<unsigned int>, hashstr > tMap2;



using namespace std;
int main()
{

//	multimap<int, int> mm;
//
//	mm.insert(pair<int, int>(1,1));
//	mm.insert(pair<int, int>(1,3));
//	mm.insert(pair<int, int>(1,2));
//	mm.insert(pair<int, int>(1,-1));
//
//
//
//	pair<multimap<int,int>::iterator, multimap<int,int>::iterator> ii = mm.equal_range(1); //We get the first and last entry in ii;
//	multimap<int, int>::iterator it; //Iterator to be used along with ii
//	for(it = ii.first; it != ii.second; ++it)
//	{
//		cout<<"Key = "<<it->first<<"    Value = "<<it->second<<endl;
//	}
//	cout << "FET" << endl;

	bool plot = true;

	// 0 - "map/basic1.tiff";
	// 1 - "map/O2CA2Session_2010-09-03_16-28-05/"; // traj. quadrada i passa pel mig dels dos obstacles, is: nbins = 200 range = 10m
	// 2 - "map/stress_big_13.tiff";
	// 3 - "map/mini.tiff";
	// 4 - "map/mini2.tiff";
	// 5 - "map/test_iros10_2.tiff";
	// 6 - "map/empty.tiff";
	// 7 - "map/concave.tiff";

	unsigned int idx = 5;
	CEnvironment env(idx, plot);
	env.build();

	CMapPtr map = env.getMap();
	CPoint2D<double> start = env.getStart();
	CPoint2D<double> goal = env.getGoal();
	CPoint2D<double> c = env.getC();

	// xapussilla
	vector<CBPoint> obstacles = map->getBPoints();
	if(idx == 2)
	{
		CBPoint p;
		p = obstacles.at(5);
		obstacles.at(5) = CBPoint(p.getId(), p.getLabel(), CPoint2D<int>(514, 766), p.isInsideObstacel());
		p = obstacles.at(7);
		obstacles.at(7) = CBPoint(p.getId(), p.getLabel(), CPoint2D<int>(883, 547), p.isInsideObstacel());
	}
	map->setBPoints(obstacles);
/*

//	tMap mapa;
//	mapa[1];
//	mapa[1].push_back(1);


//	// test TPath
//	TEdgeMap edgeIdx_;
//	edgeIdx_[CEdge('A',1,0)];
//	edgeIdx_[CEdge('B',1,0)];
//	edgeIdx_[CEdge('A',2,0)];
//	edgeIdx_[CEdge('B',2,0)];
//	edgeIdx_[CEdge('A',3,0)];
//	edgeIdx_[CEdge('B',3,0)];
//
//	TVertexMap vertexMap_;
//	vertexMap_[0];
//	vertexMap_[1];
//	vertexMap_[2];
//	vertexMap_[3];
//	vertexMap_[4];
//	vertexMap_[5];
//	vertexMap_[6];
//	vertexMap_[7];
//	vertexMap_[8];

	CEdge a00('A',0,0);
	CEdge a01('A',0,1);
	CEdge a02('A',0,2);
	CEdge a03('A',0,3);
	CEdge a10('A',1,0);
	CEdge b11('B',1,1);
	CEdge b0_1('B',0,-1);
	CEdge a0_1('A',0,-1);
	CEdge a0_2('A',0,-2);
	CEdge b02('B',0,2);
	CEdge b03('B',0,3);
	CEdge b30('B',3,0);



	ptime time1(microsec_clock::universal_time());

	//CTPath path(CTString(), edgeIdx_, vertexMap_);
	CTPath path;
//	for(unsigned int i = 0; i < 1000000; ++i)
//	{
//		path.add(a01, 0);
//		path.add(a0_1, 1);
//		path.add(a0_2, 1);
//		path.add(a0_2, 1);
//		path.add(b30, 9);
//		path.add(a01, 0);
//	}

//	// wrap jenkins
//	path.add(a01, 0);
//	path.add(a0_1, 1);
//	path.add(a01, 0);

//	// wrap Emili
//	path.add(a01, 0);
//	path.add(b30,2);
//	path.add(a0_1,2);
//	path.add(a00, 1);
//	path.add(a0_1,2);
//	path.add(b30,2);
//	path.add(a02, 0);

//	// self-crossing Jenkins
//	path.add(a03, 0);
//	path.add(a0_1, 0);
//	path.add(a10, 2);
//
//	//path.add(b11 ,2);
//	path.add(a10, 2);
//	path.add(b30 ,2);
//	path.add(a0_2, 1);
//	path.add(a10, 2);
//
//	path.checkCriteria();
//	cout << path << endl;
//	ptime time2(microsec_clock::universal_time());
//	time_duration diff(time2 - time1);
//	cout << "tpath: " <<  diff.total_milliseconds() / 1000.0f << endl;
//

//	//test CTString.sortAlpha()
//	CTString s;
//	s.add(b11);
//	s.add(a03);
//	s.add(a02);
//	s.add(a01);
//	s.add(b0_1);
//	s.add(a02);
//	s.add(a01);
//
//	cout << "normal: " << s << endl;
//	s.sortAlpha();
//	cout << "sorted: " << s << endl;

*/

//	CPoint2D<int> p(0,0);
//	CPoint2D<int> p1(0,5);
//	CPoint2D<int> p2(10,0);
//	CSegment2D<int> s(p1, p2);
//	CPoint2D<int> po;
//	float d = util::distance2PointToSegment<int>(p, s, po);
//	cout << "po, d: " << po << ", " << d << endl;


	ptime time1(microsec_clock::universal_time());
	CTSpace tspace(map, map->metric2Cell(start), map->metric2Cell(goal), plot);
	try
	{
		tspace.build(map->metric2Cell(c));
	}
	catch ( std::exception& e )
	{
		cout << e.what() << endl;
	}
	vector<CTString> homotopyClasses;
	vector<CRFSegment> subsegments;
	tspace.computeHomotopyClasses(20);


	tspace.computeLowerBound();
	ptime time2(microsec_clock::universal_time());
	time_duration diff(time2 - time1);
	cout << "tspace & homotopy classes: " <<  diff.total_milliseconds() / 1000.0f << endl;

	homotopyClasses = tspace.getHomotopyClasses();
	subsegments = tspace.getRFSubsegments();

	if(plot)
	{
		cv::imwrite("test/cobstacle.tiff", map->getCObstacle());
		cv::waitKey();
	}

	cout << "Done" << endl;
	return 0;
}
