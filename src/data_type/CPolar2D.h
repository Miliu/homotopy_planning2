// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CPolar2D.h
 *
 *  Created on: 01/06/2010
 *      Author: emili
 */

#ifndef CPOLAR2D_H_
#define CPOLAR2D_H_

#include "CPoint2D.h"

template<typename T>
class CPolar2D
{
public:
	CPolar2D():rho_(0.0), theta_(0.0){}
	CPolar2D(const T& rho, const T& theta):rho_(rho), theta_(theta){}
	T getRho() const {return rho_;}
	T getTheta() const {return theta_;}
	CPoint2D<T> toCart() const {return CPoint2D<T>(rho_ * cos(theta_), rho_* sin(theta_));}

	friend std::ostream &operator<<(std::ostream& cout, const CPolar2D<T>& p)
	{
		cout << "[" << p.rho_ << ";" << p.theta_ << "]";
		return cout;
	}

private:
	T rho_;
	T theta_;
};

#endif /* CPOLAR2D_H_ */
