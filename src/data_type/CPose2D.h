// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CPose2D.h
 *
 *  Created on: 31/05/2010
 *      Author: emili
 */

#ifndef CPOSE2D_H_
#define CPOSE2D_H_

#include <iostream>

template<typename T>
class CPose2D
{
public:
	CPose2D():x_(0), y_(0), yaw_(0){}
	CPose2D(const T& x, const T& y, const T& yaw):x_(x), y_(y), yaw_(yaw)
	{
		//if(yaw_ < -util::pi || yaw_ > util::pi) yaw_ = coord::normalize(yaw_);
	}

	T x() const {return x_;}
	T y() const {return y_;}
	T yaw() const {return yaw_;}

	void operator=(const CPose2D<T>& p)
	{
		x_ = p.x_;
		y_ = p.y_;
		yaw_ = p.yaw_;
	}

	friend std::ostream &operator<<(std::ostream& cout, const CPose2D<T>& p)
	{
		cout << "[" << p.x_ << ";" << p.y_ << ";" << p.yaw_ << "]";
		return cout;
	}
private:
	T x_, y_, yaw_;
};

#endif /* CPOSE2D_H_ */
