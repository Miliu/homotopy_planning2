// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CPoint2D.cpp
 *
 *  Created on: 17/09/2010
 *      Author: emili
 */

#include "CPoint2D.h"

//template<T>
//void CPoint2D<int>::operator=(const CPoint2D<float>& v)
//{
//	x_ = round(v.x_);
//	y_ = round(v.y_);
//}

template<>
CPoint2D<int> CPoint2D<int>::operator/(const float& s)
{
	return CPoint2D<int>(round(x_ / s), round(y_ / s));
}

template<>
CPoint2D<int> CPoint2D<int>::operator/=(const float& s)
{
	std::cout << "AKEST" << std::endl;
	x_ = round(x_ / s);
	y_ = round(y_ / s);
	return *this;
}
