// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CVector2.h
 *
 *  Created on: 17/09/2010
 *      Author: emili
 */

#ifndef CPoint2D_H_
#define CPoint2D_H_

#include <cmath>
#include <iostream>

template <typename T>
class CPoint2D
{
public:
	CPoint2D():x_(0),y_(0){}
	CPoint2D(const T& x, const T& y):x_(x),y_(y){}
	CPoint2D(const CPoint2D<T>& v)
	{
		x_ = v.x_;
		y_ = v.y_;
	}
	void setVector(const T& x, const T& y)
	{
		x_ = x;
		y_ = y;
	}
	T x() const {return x_;}
	T y() const {return y_;}
	void setX(const T& x){x_ = x;}
	void setY(const T& y){y_ = y;}

	void operator=(const CPoint2D<T>& v)
	{
		x_ = v.x_;
		y_ = v.y_;
	}

	CPoint2D<T> operator+(const CPoint2D<T>& v)
	{
		return CPoint2D<T>(x_ + v.x_, y_ + v.y_);
	}
	CPoint2D<T> operator+=(const CPoint2D<T>& v)
	{
		x_ += v.x_;
		y_ += v.y_;
		return *this;
	}

	CPoint2D<T> operator-(const CPoint2D<T>& v)
	{
		return CPoint2D<T>(x_ - v.x_, y_ - v.y_);
	}

	CPoint2D<T> operator-() const
	{
		return CPoint2D<T>(-x_, -y_);
	}

	CPoint2D<T> operator-=(const CPoint2D<T>& v)
	{
		x_ -= v.x_;
		y_ -= v.y_;
		return *this;
	}

	bool operator==(const CPoint2D<T>& v)
	{
		return (x_ == v.x_ && y_ == v.y_);
	}
	bool operator!=(const CPoint2D<T>& v)
	{
		return (x_ != v.x_ || y_ != v.y_);
	}

	//distance^2
	T distance2(const CPoint2D<T>& v)
	{
		CPoint2D<T> copy(*this);
		copy -= v;
		return (copy.x_ * copy.x_ + copy.y_ * copy.y_);
	}

	float distance(const CPoint2D<int>& v)
	{
		CPoint2D<int> copy(*this);
		copy -= v;
		return (sqrt(static_cast<float>(copy.x_ * copy.x_ + copy.y_ * copy.y_)));
	}
	float distance(const CPoint2D<float>& v)
	{
		CPoint2D<float> copy(*this);
		copy -= v;
		return (sqrt(copy.x_ * copy.x_ + copy.y_ * copy.y_));
	}
	double distance(const CPoint2D<double>& v)
	{
		CPoint2D<double> copy(*this);
		copy -= v;
		return (sqrt(copy.x_ * copy.x_ + copy.y_ * copy.y_));
	}

	CPoint2D<T> operator*(const T& s)
	{
		return CPoint2D<T>(s * x_, s * y_);
	}

	CPoint2D<T> operator/(const float& s)
	{
		return CPoint2D<T>(x_ / s, y_ / s);
	}

//	CPoint2D<int> operator/(const float& s)
//	{
//		return CPoint2D<int>(x_ / s, y_ / s);
//	}

	CPoint2D<T> operator/=(const float& s)
	{
		std::cout << "NORMAL" << std::endl;
		x_ /= s;
		y_ /= s;
		return *this;
	}
//	friend CPoint2D<T> operator*(const CPoint2D<T>& v, const T& s)
//	{
//		return CPoint2D<T>(s * v.x_, s * v.y_);
//	}
//
//
//	friend CPoint2D<T> operator/(const CPoint2D<T>& v, const T& s)
//	{
//		return CPoint2D<T>(v.x_ / s, v.y_ / s);
//	}

	friend std::ostream &operator<<(std::ostream& cout, const CPoint2D<T>& v)
	{
		cout << "[" << v.x_ << ";" << v.y_ << "]";
		return cout;
	}

private:
	T x_, y_;
};

template<>
CPoint2D<int> CPoint2D<int>::operator/(const float& s);

template<>
CPoint2D<int> CPoint2D<int>::operator/=(const float& s);

//template<>
//void CPoint2D<int>::operator=(const CPoint2D<float>& v);

//{
//	return CPoint2D<int>(round(x_ / s), round(y_ / s));
//}

//template<>
//CPoint2D<int> CPoint2D<int>::operator/(const CPoint2D<int>& v, const int& s)
//{
//	return CPoint2D<int>(round(v.x_ / s), round(v.y_ / s));
//}


#endif /* CPOINT2D_H_ */
