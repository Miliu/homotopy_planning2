// Copyright (c) 2009-2018 Emili Hernandez
/*
 * CSegment2D.h
 *
 *  Created on: 20/09/2010
 *      Author: emili
 */

#ifndef CSEGMENT2D_H_
#define CSEGMENT2D_H_

#include "CPoint2D.h"

template <typename T>
class CSegment2D
{
public:
	CSegment2D()
	:
		p1_(CPoint2D<T>()),
		p2_(CPoint2D<T>())
	{}
	CSegment2D(	const CPoint2D<T>& p1,
				const CPoint2D<T>& p2)
	:
		p1_(p1),
		p2_(p2)
	{}
	CSegment2D(	const CSegment2D<T>& s)
		:
			p1_(s.p1_),
			p2_(s.p2_)
	{}
	CPoint2D<T> getP1() const {return p1_;}
	CPoint2D<T> getP2() const {return p2_;}
	void setP1(const CPoint2D<T>& p1) {p1_ = p1;}
	void setP2(const CPoint2D<T>& p2) {p2_ = p2;}

	void operator=(const CSegment2D<T>& s)
	{
		p1_ = s.p1_;
		p2_ = s.p2_;
	}

	friend std::ostream& operator<<(std::ostream& cout, const CSegment2D<T>& s)
	{
		cout << s.p1_ << ";" << s.p2_;
		return cout;
	}

protected:
	CPoint2D<T> p1_, p2_;
};

#endif /* CSEGMENT2D_H_ */
