Homotopy Planning2
==================

Path planners implemented during my PhD [Thesis](http://hdl.handle.net/10803/83568).

- List of algorithms:
	- A*
	- RRT
	- Bug2
	- Homotopic A*
	- Homotopic RRT
	- Homotopic Bug


------------------
## Buiild instructions
1. $ mkdir homotopy_planning2/build
2. $ cd homotopy_planning2/build
3. $ cmake ../src
4. $ make

### Run instructions
1. Ensure that ./test folder exists
2. Run ./homotopy_planning2 in the root folder
3. Press any key to while the focus is on the <Algorithm>Path window to visualize the path for each homotopy class.

**NOTE: **Enviroment selection and algorithm used is still hard coded in the main.cpp



------------
## License
Hoptopy Planning2 is open-sourced under the 3-clause BSD License. See the
[LICENSE](LICENSE) file for details.
