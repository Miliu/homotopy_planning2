% just change the is log to be as much similar as possible to the file
% required by the c++ code (homotopy_planning2)

close all; 
clear all;

addpath('log')
path = [''];

global PARAM;

%% PARAMETERS -------------------------------------------------------------
PARAM.msis.filename = [path 'is_sensor.log'];




%% MSIS
msis_raw = load(PARAM.msis.filename);
msis_t = realtime2unix(msis_raw(:,1:7));
%msis_angle =  grad2rad(msis_raw(:,8)/16);
msis_angle =  msis_raw(:,8);
msis_nbins = msis_raw(:,9);
msis_bins = msis_raw(:,10:end);
sz = size(msis_raw,1);
msis = [msis_t msis_angle msis_nbins msis_bins];

fid = fopen('is_sensor_of2.log', 'w');
s = ' %d';
s = repmat(s,1,size(msis_bins,2));
format = ['%f %d %d' s '\n'];
fprintf(fid, format, msis');
fclose(fid);





