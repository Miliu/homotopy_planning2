close all; 
clear all;

addpath('log')
path = ['../'];

global PARAM;

%% PARAMETERS -------------------------------------------------------------
PARAM.initial_time = (16 * 3600 + 28 * 60 + 57) * 1000 + 448;
PARAM.mti.filename = [path 'mti_sensor.log'];
PARAM.dvl.filename = [path 'linkquest_dvl_sensor.log'];
PARAM.msis.filename = [path 'is_sensor.log'];


%% DVL
dvl_raw = load(PARAM.dvl.filename);
dvl_t = realtime2unix(dvl_raw(:,1:7));

dvl_bottom_velocity = dvl_raw(:,8:11);
dvl_bottom_velocity(:,1:3) = dvl_bottom_velocity(:,1:3)*100; %cm/s

dvl_water_velocity = dvl_raw(:,12:15);
dvl_water_velocity(:,1:3) = dvl_water_velocity(:,1:3)*100; %cm/s

dvl_altitude = dvl_raw(:,16);
dvl_roll_pitch_yaw = dvl_raw(:,17:19);
dvl_temperature = dvl_raw(:,20);
dvl_pressure = dvl_raw(:,21);

sz = size(dvl_raw,1);
dvl=[dvl_t zeros(sz,6) dvl_water_velocity dvl_bottom_velocity zeros(sz,7) dvl_roll_pitch_yaw(:,3) dvl_roll_pitch_yaw(:,2) dvl_roll_pitch_yaw(:,1) dvl_temperature dvl_pressure zeros(sz,5)];

fz5 = ' %d %d %d %d %d';
fz6 = ' %d %d %d %d %d %d';
fz7 = ' %d %d %d %d %d %d %d';
format = ['%f' fz6 ' %.10f %.10f %.10f %d' ' %.10f %.10f %.10f %d' fz7 ' %.10f %.10f %.10f' ' %.1f' ' %.10f' fz5 '\n'];
fid = fopen('linkquest_dvl_sensor_of.log', 'w');
fprintf(fid, format, dvl');
fclose(fid);


% save('dvl_old.txt','dvl','-ascii', '-tabs');

%% MTi
mti_raw = load(PARAM.mti.filename);
mti_t = realtime2unix(mti_raw(:,1:7));

mti_roll_pitch_yaw = rad2deg(mti_raw(:,13:15));
mti_velocity = mti_raw(:,16:18);
mti_acceleration = mti_raw(:,19:21);
mti = [mti_t mti_roll_pitch_yaw mti_velocity mti_acceleration];

fid = fopen('mti_sensor_of.log', 'w');
fprintf(fid, '%f %.10f %.10f %.10f %.10f %.10f %.10f %.10f %.10f %.10f\n', mti');
fclose(fid);

%% MSIS
msis_raw = load(PARAM.msis.filename);
msis_t = realtime2unix(msis_raw(:,1:7));
msis_angle =  grad2rad(msis_raw(:,8)/16);
msis_bins = msis_raw(:,10:end);
sz = size(msis_raw,1);
msis = [msis_t zeros(sz,1) msis_angle msis_bins];

fid = fopen('is_sensor_of.log', 'w');
s = ' %d';
s = repmat(s,1,size(msis_bins,2));
format = ['%f %d %.10f' s '\n'];
fprintf(fid, format, msis');
fclose(fid);





