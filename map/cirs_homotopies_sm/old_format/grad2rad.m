function r = grad2rad(g)
% gradients to radiants fuctions. Note: pi = 200 grad
    r = g * pi / 200.0;
end