function [out,q,num_it,assoc] = pIC(scanK,scanK1,motion)

global PARAM;
global NUM_SM;

% Debug mode 3
%global handle_assoc_scanK;
% frame=1;

errorK1 = 1000000;
num_converged = 0;
num_it = 0;

% % Remove unnecessary points
% indexDelete = isnan(scanK.cart(1,:));
% scanK.cart(:,indexDelete) = [];
% scanK.P(:,:,indexDelete) = [];
% 
% indexDelete = isnan(scanK1.cart(1,:));
% scanK1.cart(:,indexDelete) = [];
% scanK1.P(:,:,indexDelete) = [];


if (PARAM.debug >= 1) && (NUM_SM >= PARAM.plotFrom)
    clf(figure(12))
%     scanK1_s=[];
    fig_sm=figure(12); 
    ax1 = gca; %get the current axes handle
    set(ax1,'XDir','reverse') %Invert X direction so Z look downward
    set(fig_sm,'CurrentAxes',ax1);
    axis equal;
    xlabel('X (m)'); ylabel('Y (m)');
    
    
    hold on;

    handle_scanK = plot(scanK.cart(1,:),scanK.cart(2,:),'black.','MarkerSize',5); %base
%     scanK1_r = Composition(motion.q,scanK1.cart);
    %handle_scanK1_r = plot(scanK1_r(1,:),scanK1_r(2,:),'r.');
    handle_scanK1_s = plot(NaN,NaN,'green.','MarkerSize',5);
    
    handle_association = plot(NaN,NaN,'red.');
%     handle_P_association = plot(NaN,NaN,'magenta.');
%     handle_assoc_scanK = plot(NaN,NaN,'-magenta');
%       
    %handle_P_scanK = draw_ellipse(scanK.cart',scanK.P,'black');
    handle_P_scanK1 = plot(NaN,NaN,'green');
    
    %handle_P_scanK1 = draw_ellipse(scanK1.cart',scanK1.P,'m');
    axis equal;
    %legend([handle_scanK,handle_scanK1_r,handle_scanK1_s],'scan k','skan k+1','solution',1);
    legend([handle_scanK,handle_scanK1_s,handle_association],'scan k','solution','association',1);
end

out = 0;
while(num_it < PARAM.sm.maxIterations && out == 0)
    
    %E_STEP
    [assoc,assoc_lines,scanK_P_index,scanK1_Pindex] = EStep(scanK, scanK1, motion);
    
    %Debug
    if PARAM.debug >= 2 && (NUM_SM >= PARAM.plotFrom)
        % Plot scanK1 referenced to r using scan matching estimation
        scanK1_s = Composition(motion.q,scanK1.cart);
        set(handle_scanK1_s,'XData',scanK1_s(1,:),'YData',scanK1_s(2,:));
        
        %pause;
        draw_ellipse_v2(scanK1_s',scanK1.P,'green',handle_P_scanK1);
        
        % Plot associations referenced to r 
        %set(handle_assoc_scanK,'XData',assoc_lines(1,:),'YData',assoc_lines(2,:));
        set(handle_association,'XData',assoc.a(1,:),'YData',assoc.a(2,:));
        drawnow      
%         pause;
    end;
    
    %M_STEP
    [out_MStep,solution,errorK1,num_converged] = MStep(assoc,motion,errorK1,num_converged);
    
    %Debug
    if PARAM.debug >= 2 && (NUM_SM >= PARAM.plotFrom)
        % Plot scanK1 referenced to r using scan matching estimation
        scanK1_s = Composition(solution,scanK1.cart);
        set(handle_scanK1_s,'XData',scanK1_s(1,:),'YData',scanK1_s(2,:));
        
        % Plot associations referenced to r 
        set(handle_association,'XData',assoc.a(1,:),'YData',assoc.a(2,:));
        drawnow
%         saveas(gcf, ['path', num2str(frame),'.jpg']);
%         frame = frame + 1;
%         pause;
    end;
    
    motion.q = solution;
   
    if out_MStep == 1
        out = 1;
    elseif out_MStep == -1
        out = -1;
    else
        num_it = num_it+1;
    end
end

q = motion.q;
if num_it == PARAM.sm.maxIterations
    out = 2;
end

if PARAM.debug >= 1 && NUM_SM >= PARAM.plotFrom
    % Plot scanK1 referenced to r using scan matching estimation
    scanK1_s = Composition(solution,scanK1.cart);
    set(handle_scanK1_s,'XData',scanK1_s(1,:),'YData',scanK1_s(2,:));
    hold off;
end
