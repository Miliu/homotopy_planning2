function [scan] = ScanForming_slam_v6(sensorData)

% Function ScanForming
%
% Given an IS data, global referenced positions of the robot at each beam
% and the scan points, the function returns a full scan referenced to
% 'ref_indx' in cartesian coordinates with its covariance matrix 'P'
%
% Input:
%  sensorData structure:
%       .seg = segmented miniking's data
%           .rho = range, sensor fram
%           .theta = bearing, sensor frame
%       .pos = position(x,y,yaw) of the robot for each beam, 1st EKF frame
%       .cov = covariance of the robot for each beam, 1st EKF frame
%       .ref_point = Index of the beam position to reference the scan
%
% Output:
%  scan: struct which contains
%    cart: scan's data in cartesian cooridates referenced to 'ref_indx'
%    P: covariance matrixes of each scan point

% Ver.6: added more debuging options

global PARAM;

%% ------------------------- Initializations ------------------------------
%sensor position in the robot frame
r_X_s = PARAM.sensor.position;

%robot position and cov at each beam
b_X_r = sensorData.pos;
P_br  = sensorData.cov;

%who knows why is that here?
b_X_r = repmat(b_X_r,1,PARAM.segmentation.maxRangesPerAngle);
P_br  = repmat(P_br,[1,1,PARAM.segmentation.maxRangesPerAngle]);

%reference point
ref_indx = sensorData.ref_point;
ref_X_b = b_X_r(:,ref_indx);
ref_P_b = P_br(:,:,ref_indx);

%the same in local frame
Rref = [0;0;ref_X_b(3)];
P_ref = zeros(3);
P_ref(3,3) = ref_P_b(3,3);

%sonar noice
P_psi = diag([PARAM.sensor.deviation(1) PARAM.sensor.deviation(2)])^2;

%Remove unnecessary scan points  
%(not positions because it may delete the reference point)
indexDelete = (sensorData.seg.rho == 0);
sensorData.seg.theta(indexDelete) = NaN;
sensorData.seg.rho(indexDelete) = NaN;
% b_X_r(:,indexDelete) = NaN;
% P_br(:,:,indexDelete) = NaN;

scan_size = size(b_X_r,2);

scan.cart = zeros(2,scan_size);
scan.P    = zeros(2,2,scan_size);
P_loc     = zeros(3,3,scan_size);
X_loc     = zeros(3,scan_size);

% scan.cart = zeros(2,scan_size);
% scan.P    = zeros(2,2,scan_size);
scan.debug.P_org     = zeros(2,2,scan_size);
scan.debug.X_org     = zeros(2,scan_size);


%% ---------------------------- Computations -------------------------------
for i = 1:scan_size
    
    %% P2C
    theta     = sensorData.seg.theta(i);
    rho       = sensorData.seg.rho(i);
    [x_s,y_s] = pol2cart(theta,rho);
    X_s       = [x_s;y_s];
    
    % Jacobian for pol2cart
    J_s = [cos(theta) -rho.*sin(theta);
           sin(theta) rho.*cos(theta)];
    
    % Covariance
    P_s = J_s * P_psi * J_s';
    
    %% Transform scan points from sonar to robot frame
    X_r  = compound(r_X_s, X_s);
    
    % Covariance
    P_r = X_r.Jb * P_s * X_r.Jb';
    
       
    %% Robot positions relative to reference position
    
    %for beams after the reference frame
    if i > ref_indx
        
        %find the linear traslations
        Bi = b_X_r(1:2,ref_indx+1);
        Pbi = P_br(1:2,1:2,ref_indx+1);
        for j = ref_indx+2 : i
            Bi = Bi + b_X_r(1:2,j);
            Pbi = Pbi + P_br(1:2,1:2,j);
        end
        Bi(3) = 0;
        Pbi(3,1:3) = [0 0 0];
        
        %position
        Rj = [0;0;b_X_r(3,i)];
        P_RJ = P_br(:,:,i);
        P_RJ(1:2,1:2)=0;
        
        invRi = invert(Rref);
        R_B = compound(invRi.x,Bi);
        X_ir = compound(R_B.x,Rj);
        
        %covariance
        PinvRi = invRi.oJ * P_ref * invRi.oJ';
        P_R_B = R_B.Ja*PinvRi*R_B.Ja' + R_B.Jb*Pbi*R_B.Jb';
        P_ir = X_ir.Ja*P_R_B*X_ir.Ja' + X_ir.Jb*P_RJ*X_ir.Jb';
        
    %for beams before the reference frame  
    elseif i < ref_indx
        
        %find the linear traslations
        Bi = -ref_X_b(1:2);
        Pbi = ref_P_b(1:2,1:2);
        for j = ref_indx-1: -1 : i+1
            Bi = Bi - b_X_r(1:2,j);
            Pbi = Pbi + P_br(1:2,1:2,j);
        end
        Bi(3) = 0;
        Pbi(3,1:3) = [0 0 0];

        %position
        Rj = [0;0;b_X_r(3,i)];
        P_RJ = P_br(:,:,i);
        P_RJ(1:2,1:2)=0;
        
        invRi = invert(Rref);
        R_B = compound(invRi.x,Bi);
        X_ir = compound(R_B.x,Rj);

        %covariance
        PinvRi = invRi.oJ * P_ref * invRi.oJ';
        P_R_B = R_B.Ja*PinvRi*R_B.Ja' + R_B.Jb*Pbi*R_B.Jb';
        P_ir = X_ir.Ja*P_R_B*X_ir.Ja' + X_ir.Jb*P_RJ*X_ir.Jb';
        
        %for the reference frame    
    elseif i == ref_indx
        X_ir.x = zeros(3,1);
        P_ir = zeros(3); %???? to be checked
    end
    
    %% Transform scan points from robot to reference frame
    res  = compound(X_ir.x, X_r.x);
    X_i  = res.x;
    
    % Covariance
    P_i = res.Ja*P_ir*res.Ja' + res.Jb*P_r*res.Jb';
    
    %% Transform scan points from robot to base frame at their original position
    % for debuging and ploting reasons
    res2  = compound(sensorData.pos(1:3,i), X_r.x);
    scan.debug.X_org(:,i)  = res2.x;
    
    % Covariance
    scan.debug.P_org(:,:,i) = ...
        res2.Ja*sensorData.cov(1:3,1:3,i)*res2.Ja' + res2.Jb*P_r*res2.Jb';
    
    %% Storage
    %output
    scan.cart(:,i) = X_i;
    scan.P(:,:,i) = P_i;
    
    %for ploting
    P_loc(:,:,i) = P_ir;
    X_loc(:,i) = X_ir.x;    
    scan.debug.P_loc(:,:,i) = P_ir;
    scan.debug.X_loc(:,i) = X_ir.x;    
end


%% Debug plots in local frame OFF-LINE
if PARAM.debug_scanforming
    clf(figure(20));
    figure(20);
    set(gca,'XDir','reverse') %Invert X direction so Z look downward
    axis equal;
    xlabel('X (m)'); ylabel('Y (m)');
    hold all
    
    %plot robot positions
    plot(X_loc(1,:), X_loc(2,:), 'm+');
    plot_origin(X_loc(1,ref_indx), X_loc(2,ref_indx), X_loc(3,ref_indx),.01,'m');
    for i = 1:10:scan_size;
        draw_ellipse_v2(X_loc(:,i)', P_loc(:,:,i), 'm');
    end
    %plot scan points
    plot(scan.cart(1,:), scan.cart(2,:), 'b.');
    draw_ellipse_v2(scan.cart', scan.P, 'g');
%     hold off
    drawnow;
end
