function [ device ] = read_dataset_sparus()
%[ device ] = read_dataset()
%from David Ribas
%Reads DVL, IS, DGPS and MTI data from files with its file id stored in the
%global variable FILE.fid

%Sparus update
%from Angelos Mallios
%Reads DVL, GPS, MTI from ROS architecture
%and IS from O2CA2 architecture
%and it outputs them in the same data stracture as the Marina dataset. Take care
%with some of the units

global FILE

if ~isfield(FILE,'files2read') %if it is the first access to the files initialize vars
    FILE.files2read               = FILE.fid>0;     %vector to store the files from wich to read new data
    FILE.time                     = repmat(NaN,1,length(FILE.fid));   %NaN indicates that a time tag is needed
    FILE.time(FILE.files2read==0) = Inf;  %Inf indicates that the file doesn't exist
    FILE.depth                    = 0;

end

while ~exist('device','var') %Do until a value is assigned to device
    
    if FILE.files2read(1) %read DVL==============================================================
       FILE.DVL_tline_raw      = fgetl(FILE.fid(1)); %Obtain data line from the file
        if ~isempty(FILE.DVL_tline_raw) %If the line contains time stamp
            format1        = repmat(' %f',[1,45]);
            FILE.DVL_tline_raw  = textscan(FILE.DVL_tline_raw, [format1 ' %*c' format1],'delimiter',',','commentstyle','matlab'); %read the content and interpret it following matlab rules (% = comment)
            FILE.DVL_tline_raw = cell2mat(FILE.DVL_tline_raw);
        end
        if isempty(FILE.DVL_tline_raw) %If nothing is read (because the line is commented) do
            FILE.files2read(1) = 1;  %This file has to be read in the next cicle         
            FILE.time(1)       = NaN; %This file needs a time tag           
        else %if something is read
            FILE.DVL_tline(1)  = FILE.DVL_tline_raw(1)/1000000000; %for the ROS architecture
            FILE.files2read(1) = 0; %This file does not need further reading
            FILE.time(1)       = FILE.DVL_tline(1);
            
            %convert to "marina" dataset (check velocities units)
            FILE.DVL_tline = [FILE.DVL_tline(1) 0 0 0 0 0 0 ... %time
                              FILE.DVL_tline_raw(75)*100 FILE.DVL_tline_raw(76)*100 FILE.DVL_tline_raw(77)*100 ...%w-vel (cm/s)
                              FILE.DVL_tline_raw(78) ...%w-vel status, 0=no vel, 1=ok
                              FILE.DVL_tline_raw(67)*100 FILE.DVL_tline_raw(68)*100 FILE.DVL_tline_raw(69)*100 ...%b-vel (cm/s)
                              FILE.DVL_tline_raw(70) ...%w-vel status, 0=no vel, 1=ok
                              FILE.DVL_tline_raw(51) FILE.DVL_tline_raw(52) FILE.DVL_tline_raw(53) ...%Bottom range (m)
                              0 0 0 0 ... 
                              rad2deg(FILE.DVL_tline_raw(85)) rad2deg(FILE.DVL_tline_raw(84)) rad2deg(FILE.DVL_tline_raw(83)) ...%YPR (deg)
                              FILE.DVL_tline_raw(87) ...% Temperature
                              FILE.depth ...%Pressure from MTI z axis in meters!!
                              0 0 0 0 0];
        end
    end
    
    if FILE.files2read(2) %read IS===============================================================
        FILE.IS_tline_raw     = fgetl(FILE.fid(2));
        if ~isempty(FILE.IS_tline_raw)
            FILE.IS_tline_raw       = textscan(FILE.IS_tline_raw, '%f', 'commentstyle','matlab');
            FILE.IS_tline_raw      = cell2mat(FILE.IS_tline_raw)';
            FILE.IS_tline_Utime = realtime2unix(FILE.IS_tline_raw(1:7)); %for the O2CA2 architecture
            
        end
        if isempty(FILE.IS_tline_raw)
            FILE.files2read(2) = 1;
            FILE.time(2)       = NaN;
        else
            %convert to "marina" dataset (take care: only 493 bins instead of 500)
            FILE.IS_tline = [FILE.IS_tline_Utime FILE.IS_tline_Utime ... %time
                             deg2rad((FILE.IS_tline_raw(8)/16)*0.9) ... %GRADIANS to degrees to radians
                             FILE.IS_tline_raw(10:end-1)]; %bins
            
            FILE.files2read(2) = 0;
            FILE.time(2)=FILE.IS_tline(1);
        end
    end
    
    if FILE.files2read(3) %read GPS=============================================================
        FILE.GPS_tline_raw        = fgetl(FILE.fid(3));
        if ~isempty(FILE.GPS_tline_raw)
%             format = '%u64,%u32,%u64,,%f,%d,%f,%d,%f,%d,%f,%f,%d %c,';% Be carefull * means avoid field    
            FILE.GPS_tline_raw     = textscan(FILE.GPS_tline_raw, '%f', 'delimiter', ',','commentstyle','matlab');
            FILE.GPS_tline_raw    = cell2mat(FILE.GPS_tline_raw)';
        end
        if isempty(FILE.GPS_tline_raw)
            FILE.files2read(3) = 1;
            FILE.time(3)       = NaN;
        else
            FILE.GPS_tline(1) = FILE.GPS_tline_raw(1)/1000000000; %for the ROS architecture            
            FILE.files2read(3) = 0;
            FILE.time(3)       = FILE.GPS_tline(1);
            
            %convert to "marina" dataset 
            FILE.GPS_tline = [FILE.GPS_tline(1) ... %time
                              FILE.GPS_tline_raw(5) ... %Lat ddmm.ffff (deg min fraction of min)
                              FILE.GPS_tline_raw(7) ... %Lon ddmm.ffff (deg min fraction of min)
                              FILE.GPS_tline_raw(10) ... %Status 1=gps, 2=dgps  
                              0 0 0 0 0 0];
        end
    end

    if FILE.files2read(4) %read MTI==============================================================
        FILE.MTI_tline_raw     = fgetl(FILE.fid(4));
        if ~isempty(FILE.MTI_tline_raw)
            FILE.MTI_tline_raw  = textscan(FILE.MTI_tline_raw, '%f', 'delimiter', ',','commentstyle','matlab');
            FILE.MTI_tline_raw = cell2mat(FILE.MTI_tline_raw)';
        end
        if isempty(FILE.MTI_tline_raw)
            FILE.files2read(4) = 1;           
            FILE.time(4)       = NaN;            
        else
            FILE.MTI_tline(1) = FILE.MTI_tline_raw(1)/1000000000; %for the ROS architecture
            FILE.files2read(4) = 0;
            FILE.time(4)       = FILE.MTI_tline(1);
            FILE.depth         = FILE.MTI_tline_raw(10);
            
            %convert MTi to "marina" dataset 
            FILE.MTI_tline = [FILE.MTI_tline(1) ... 
                              rad2deg(FILE.MTI_tline_raw(11)) ...%Roll deg 
                              rad2deg(FILE.MTI_tline_raw(12)) ...%Pitch deg 
                              rad2deg(FILE.MTI_tline_raw(13)) ...%Yaw deg
                              FILE.MTI_tline_raw(17) FILE.MTI_tline_raw(18) FILE.MTI_tline_raw(19) ...%Rate rad/s
                              FILE.MTI_tline_raw(53)*100 FILE.MTI_tline_raw(54)*100 FILE.MTI_tline_raw(55)*100];%Acc cm/s
%             %convert NAVIGATOR to "marina" dataset 
%             FILE.MTI_tline = [FILE.MTI_tline(1) ... 
%                               rad2deg(FILE.MTI_tline_raw(8)) ...%Roll deg 
%                               rad2deg(FILE.MTI_tline_raw(9)) ...%Pitch deg 
%                               rad2deg(FILE.MTI_tline_raw(10)-0.80) ...%Yaw deg
%                               FILE.MTI_tline_raw(14) FILE.MTI_tline_raw(15) FILE.MTI_tline_raw(16) ...%Rate rad/s
%                               FILE.MTI_tline_raw(17)*100 FILE.MTI_tline_raw(18)*100 FILE.MTI_tline_raw(19)*100];%Acc cm/s
        end
    end
    
    if all(FILE.time>0)%decide output device=====================================================
        [~,device]              = min([FILE.time]);
        FILE.files2read(device) = 1;
    end       
end