
function [handle] = plot_robot_v2(state, size, type, handle)
%[handle] = plot_robot(state, size, type)
%Creates a figure representing the AUVs and stores the coordinates
%in the global variable PLOT.ictineu_coord and PLOT.sparus_coord

%plot_robot(state, size, type, handle)
%actualizes the auv plot with a new position x,y,ang

%type: 1 for ictineu, 2 for sparus

x = state(1);
y = state(2);
ang = state(3);

global PLOT

%% Ictineu AUV
if type == 1
    if nargin==3
        
        PLOT.ictineu_coord(1,:)=[-0.3700    0.3700    0.3700   -0.3700   -0.3700    0.3700    0.3700   -0.3700    0.3579    0.3373    0.3052    0.2648    0.2200    0.1752    0.1348    0.1027...
            0.0821    0.0750    0.0821    0.1027    0.1348    0.1752    0.2200    0.2648    0.3052    0.3373    0.3579    0.3650   -0.3579   -0.3373   -0.3052   -0.2648...
            -0.2200   -0.1752   -0.1348   -0.1027   -0.0821   -0.0750   -0.0821   -0.1027   -0.1348   -0.1752   -0.2200   -0.2648   -0.3052   -0.3373   -0.3579   -0.3650...
            0.0713    0.0607    0.0441    0.0232         0   -0.0232   -0.0441   -0.0607   -0.0713   -0.0750   -0.0713   -0.0607   -0.0441   -0.0232         0    0.0232...
            0.0441    0.0607    0.0713    0.0750   -0.1500   -0.1732   -0.1941   -0.2107   -0.2213   -0.2250   -0.2213   -0.2107   -0.1941   -0.1732   -0.1500   -0.1000...
            -0.0951   -0.0809   -0.0588   -0.0309         0    0.0309    0.0588    0.0809    0.0951    0.1000    0.1500    0.1732    0.1941    0.2107    0.2213    0.2250...
            0.3700    0.3700   -0.3700   -0.3700    0.3700    0.3700    0.2250    0.2213    0.2107    0.1941    0.1732    0.1500    0.1000    0.0951    0.0809    0.0588...
            0.0309         0   -0.0309   -0.0588   -0.0809   -0.0951   -0.1000    0.1000    0.3000    0.3000    0.1000    0.1000    0.3000    0.3000    0.1000   -0.1000...
            -0.3000   -0.3000   -0.1000   -0.1000   -0.3000   -0.3000   -0.1000    0.2300    0.3300    0.3300    0.2300    0.3585    0.3543    0.3476    0.3393    0.3300...
            0.3207    0.3124    0.3057    0.3015    0.3000    0.3015    0.3057    0.3124    0.3207    0.3300    0.3393    0.3476    0.3543    0.3585    0.3600]*size;
        
        PLOT.ictineu_coord(2,:)=[-0.2625   -0.2625   -0.2425   -0.2425    0.2625    0.2625    0.2425    0.2425    0.0448    0.0852    0.1173    0.1379    0.1450    0.1379    0.1173    0.0852...
            0.0448         0   -0.0448   -0.0852   -0.1173   -0.1379   -0.1450   -0.1379   -0.1173   -0.0852   -0.0448         0    0.0448    0.0852    0.1173    0.1379...
            0.1450    0.1379    0.1173    0.0852    0.0448         0   -0.0448   -0.0852   -0.1173   -0.1379   -0.1450   -0.1379   -0.1173   -0.0852   -0.0448    0.0000...
            0.0232    0.0441    0.0607    0.0713    0.0750    0.0713    0.0607    0.0441    0.0232         0   -0.0232   -0.0441   -0.0607   -0.0713   -0.0750   -0.0713...
            -0.0607   -0.0441   -0.0232         0    0.0750    0.0713    0.0607    0.0441    0.0232         0   -0.0232   -0.0441   -0.0607   -0.0713   -0.0750   -0.0750...
            -0.1059   -0.1338   -0.1559   -0.1701   -0.1750   -0.1701   -0.1559   -0.1338   -0.1059   -0.0750   -0.0750   -0.0713   -0.0607   -0.0441   -0.0232    0.0000...
            0   -0.2425   -0.2425    0.2425    0.2425         0         0    0.0232    0.0441    0.0607    0.0713    0.0750    0.0750    0.1059    0.1338    0.1559...
            0.1701    0.1750    0.1701    0.1559    0.1338    0.1059    0.0750    0.2425    0.2425    0.1700    0.1700   -0.2425   -0.2425   -0.1700   -0.1700    0.2425...
            0.2425    0.1700    0.1700   -0.2425   -0.2425   -0.1700   -0.1700   -0.0300   -0.0300    0.0300    0.0300    0.0093    0.0176    0.0243    0.0285    0.0300...
            0.0285    0.0243    0.0176    0.0093         0   -0.0093   -0.0176   -0.0243   -0.0285   -0.0300   -0.0285   -0.0243   -0.0176   -0.0093         0]*size;
        
        x=x+PLOT.ictineu_coord(1,:)*cos(ang)-PLOT.ictineu_coord(2,:)*sin(ang);
        y=y+PLOT.ictineu_coord(1,:)*sin(ang)+PLOT.ictineu_coord(2,:)*cos(ang);
        
        handle =  fill(x(1:4),y(1:4),[0.8 0.8 0.8],...
            x(5:8),y(5:8),[0.8 0.8 0.8],...
            x(9:28),y(9:28),'k',...
            x(29:48),y(29:48),'k',...
            x(49:68),y(49:68),[0.8 0.8 0.8],...
            x(69:119),y(69:119),'r',...
            x(120:123),y(120:123),[0.5 0 0],...
            x(124:127),y(124:127),[0.5 0 0],...
            x(128:131),y(128:131),[0.5 0 0],...
            x(132:135),y(132:135),[0.5 0 0],...
            x(136:139),y(136:139),[0.5 0.5 0.5],...
            x(140:159),y(140:159),'k');
        set(handle(3),'EdgeColor','none');
        set(handle(4),'EdgeColor','none');
        
    elseif nargin==4
        
        x=x+PLOT.ictineu_coord(1,:)*cos(ang)-PLOT.ictineu_coord(2,:)*sin(ang);
        y=y+PLOT.ictineu_coord(1,:)*sin(ang)+PLOT.ictineu_coord(2,:)*cos(ang);
        
        set(handle(1),'XData',x(1:4),'YData',y(1:4));
        set(handle(2),'XData',x(5:8),'YData',y(5:8));
        set(handle(3),'XData',x(9:28),'YData',y(9:28));
        set(handle(4),'XData',x(29:48),'YData',y(29:48));
        set(handle(5),'XData',x(49:68),'YData',y(49:68));
        set(handle(6),'XData',x(69:119),'YData',y(69:119));
        set(handle(7),'XData',x(120:123),'YData',y(120:123));
        set(handle(8),'XData',x(124:127),'YData',y(124:127));
        set(handle(9),'XData',x(128:131),'YData',y(128:131));
        set(handle(10),'XData',x(132:135),'YData',y(132:135));
        set(handle(11),'XData',x(136:139),'YData',y(136:139));
        set(handle(12),'XData',x(140:159),'YData',y(140:159));
    end
end

%% Sparus AUV
if type == 2
    if nargin==3
        
        PLOT.sparus_coord(1,:)=[ 0.5600    0.5700    0.5800    0.5900     0.6000    0.6000    0.5900    0.5800... %body
             0.5700    0.5600   -0.4500   -0.6000   -0.6000   -0.4500... %body
             0.0713    0.0607    0.0441    0.0232    0        -0.0232    -0.0441   -0.0607   -0.0713   -0.0750... %center
            -0.0713   -0.0607   -0.0441   -0.0232    0         0.0232     0.0441    0.0607    0.0713    0.0750... %center
             0.4463    0.4357	 0.4191	   0.3982	 0.3750	   0.3518	  0.3309	0.3143    0.3037	0.3000... %dvl
             0.3037	   0.3143	 0.3309	   0.3518	 0.3750    0.3982	  0.4191	0.4357	  0.4463	0.4500... %dvl	    
             0.4985    0.4943    0.4876    0.4793    0.4700    0.4607     0.4524    0.4457    0.4415    0.4400... %sonar
             0.4415    0.4457    0.4524    0.4607    0.4700    0.4793     0.4876    0.4943    0.4985    0.5000... %sonar
             -0.4985    -0.4943    -0.4876    -0.4793    -0.4700    -0.4607     -0.4524    -0.4457    -0.4415    -0.4400... %gps
             -0.4415    -0.4457    -0.4524    -0.4607    -0.4700    -0.4793     -0.4876    -0.4943    -0.4985    -0.5000... %gps
            -0.1500   -0.2300   -0.4200   -0.4200   -0.1500   -0.2300    -0.4200   -0.4200... %fins
             ]*size;
        
        PLOT.sparus_coord(2,:)=[ 0.1100    0.1000    0.0900    0.0700    0.0300   -0.0300   -0.0700    -0.0900... %body
            -0.1000   -0.1100   -0.1100   -0.0100    0.0100    0.1100... %body
             0.0232    0.0441    0.0607    0.0713    0.0750    0.0713    0.0607    0.0441    0.0232     0     ... %center
            -0.0232   -0.0441   -0.0607   -0.0713   -0.0750   -0.0713   -0.0607   -0.0441   -0.0232     0     ... %center 
             0.0232    0.0441    0.0607    0.0713    0.0750    0.0713    0.0607    0.0441    0.0232     0     ... %dvl
            -0.0232   -0.0441   -0.0607   -0.0713   -0.0750   -0.0713   -0.0607   -0.0441   -0.0232     0     ... %dvl
             0.0093    0.0176    0.0243    0.0285    0.0300    0.0285    0.0243    0.0176    0.0093     0     ... %sonar
            -0.0093   -0.0176   -0.0243   -0.0285   -0.0300   -0.0285   -0.0243   -0.0176   -0.0093     0     ... %sonar
             0.0093    0.0176    0.0243    0.0285    0.0300    0.0285    0.0243    0.0176    0.0093     0     ... %gps
            -0.0093   -0.0176   -0.0243   -0.0285   -0.0300   -0.0285   -0.0243   -0.0176   -0.0093     0     ... %gps
             0.1100    0.2200    0.2200    0.1100   -0.1100   -0.2200   -0.2200   -0.1100... %fins
            ]*size;

        
        x=x+PLOT.sparus_coord(1,:)*cos(ang)-PLOT.sparus_coord(2,:)*sin(ang);
        y=y+PLOT.sparus_coord(1,:)*sin(ang)+PLOT.sparus_coord(2,:)*cos(ang);
        
        handle =  fill(x(1:14),y(1:14),'r',...      %body
            x(15:34),y(15:34),[0.8 0.8 0.8],...     %center
            x(35:54),y(35:54),[0.5 0 0],...         %dvl
            x(55:74),y(55:74),'k',...               %sonar
            x(75:94),y(75:94),'k',...               %gps
            x(95:98),y(95:98),[0.5 0.5 0.5],...     %fins
            x(99:102),y(99:102),[0.5 0.5 0.5]);     %fins
%         set(handle(3),'EdgeColor','none');
%         set(handle(4),'EdgeColor','none');
        
    elseif nargin==4
        
        x=x+PLOT.sparus_coord(1,:)*cos(ang)-PLOT.sparus_coord(2,:)*sin(ang);
        y=y+PLOT.sparus_coord(1,:)*sin(ang)+PLOT.sparus_coord(2,:)*cos(ang);
        
        set(handle(1),'XData',x(1:14),'YData',y(1:14));
        set(handle(2),'XData',x(15:34),'YData',y(15:34));
        set(handle(3),'XData',x(35:54),'YData',y(35:54));
        set(handle(4),'XData',x(55:74),'YData',y(55:74));
        set(handle(5),'XData',x(75:94),'YData',y(75:94));
        set(handle(6),'XData',x(95:98),'YData',y(95:98));
        set(handle(7),'XData',x(99:102),'YData',y(99:102));
%         set(handle(8),'XData',x(124:127),'YData',y(124:127));
%         set(handle(9),'XData',x(128:131),'YData',y(128:131));
%         set(handle(10),'XData',x(132:135),'YData',y(132:135));
%         set(handle(11),'XData',x(136:139),'YData',y(136:139));
%         set(handle(12),'XData',x(140:159),'YData',y(140:159));
    end
end




