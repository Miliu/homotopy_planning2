function [out]=plot_origin(x,y,angle,size,colr,handle)
%Two ways to call the function:
%
%[ObjectHandle]=plot_origin(x,y,angle,size,colr) 
%Plots a robot figure at the position [x,y,angle]                                          
%of size determined by [size]. Returns object handle.
%  
%[]=plot_origin(x,y,angle,size,handle) 
%Redefines position, orientation and size of a previously 
%defined object "handle". 

punts_x=[-1 0 1 NaN 0 0 NaN -1 0 1 NaN 0 0 6 NaN 5 6 5 NaN 7.5 9.5 NaN 7.5 9.5]*size;
punts_y=[10.5 9 10.5 NaN 9 7.5 NaN 5 6 5 NaN 6 0 0 NaN 1 0 -1 NaN 1.5 -1.5 NaN -1.5 1.5]*size;
punts_z=zeros(1,24);
uns=ones(1,24);

if nargin==5
    punts=[mrot(0,0,angle) [x;y;0]; 0 0 0 1]*[punts_x;punts_y;punts_z; uns];
    %out = plot([x+2*size*cos(angle) x+size*cos(angle-(3/4)*pi)
    %x+size*cos(angle+(3/4)*pi) x+2*size*cos(angle)],[y+2*size*sin(angle) y+size*sin(angle-(3/4)*pi) y+size*sin(angle+(3/4)*pi) y+2*size*sin(angle)]);
    out = plot(punts(1,:),punts(2,:),'LineWidth',2,'Color',colr);
elseif nargin==6
   punts=[mrot(0,0,angle) [x;y;0]; 0 0 0 1]*[punts_x;punts_y;punts_z; uns];
   set(handle,'XData',punts(1,:),'YData',punts(2,:));
end

