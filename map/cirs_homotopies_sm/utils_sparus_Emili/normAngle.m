function res = normAngle(angles)
% Normalizes angles to interval (-pi,pi]
%angles = pi - mod(pi-angles, 2*pi);

% Same result, more efficiently:
res = angles + (2*pi)*floor((pi-angles)/(2*pi));

% Another way:
% 	for i=1:size(theta,2)
% 		while theta(i)>pi
% 			 theta(i) = theta(i) - 2*pi;
% 		end
% 		
% 		while theta(i)<-pi
% 			 theta(i) = theta(i) + 2*pi;
% 		end
% 	end
% res = theta;