function [angle, offset] = compute_offset()
% compute offset to avoid robot detection
% WARNING: offest valid only for SPARUS
global PARAM;

nbeams = round(PARAM.msis.config.sector/PARAM.msis.config.theta_step);
angle = 0:PARAM.msis.config.theta_step:PARAM.msis.config.theta_step*(nbeams-1);

offset = PARAM.segmentation.bounding_radius;
offset = offset(:, ones(size(angle,2), 1));

idx = round(PARAM.segmentation.bounding_radius2_angle/PARAM.msis.config.theta_step);
offset(1:idx) = PARAM.segmentation.bounding_radius2;
offset(end-idx:end) = PARAM.segmentation.bounding_radius2;
end
