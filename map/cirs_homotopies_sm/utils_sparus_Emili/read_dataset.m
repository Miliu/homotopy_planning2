function [ device ] = read_dataset()
%[ device ] = read_dataset()
%
%Reads DVL, IS, DGPS and MTI data from files with its file id stored in the
%global variable FILE.fid

global FILE

if ~isfield(FILE,'files2read') %if it is the first acces to the files initialize vars
    FILE.files2read               = FILE.fid>0;     %vector to store the files from wich to read new data
    FILE.time                     = repmat(NaN,1,length(FILE.fid));   %NaN indicates that a time tag is needed
    FILE.time(FILE.files2read==0) = Inf;  %Inf indicates that the file doesn't exist
end

while ~exist('device') %Do until a value is assigned to device
    
    if FILE.files2read(1) %read DVL==============================================================
        FILE.DVL_tline = fgetl(FILE.fid(1)); %Obtain data line from the file
        if ~isempty(FILE.DVL_tline) %If the line contains something
            FILE.DVL_tline=strread(FILE.DVL_tline,'%f','delimiter',' ','commentstyle','matlab')'; %read the content and interpret it following matlab rules (% = comment)
        end
        if isempty(FILE.DVL_tline) %If nothing is read (because the line is commented) do
            FILE.files2read(1)=1;  %This file has to be read in the next cicle         
            FILE.time(1)=NaN; %This file needs a time tag           
        else %if something is read
            FILE.files2read(1)=0; %This file does not need further reading
            FILE.time(1)=FILE.DVL_tline(1); %Set time tag
        end
    end
    
    if FILE.files2read(2) %read IS===============================================================
        FILE.IS_tline = fgetl(FILE.fid(2));
        if ~isempty(FILE.IS_tline)
            FILE.IS_tline=strread(FILE.IS_tline,'%f','delimiter',' ','commentstyle','matlab')';
        end
        if isempty(FILE.IS_tline)
            FILE.files2read(2)=1;
            FILE.time(2)=NaN;
        else
            FILE.files2read(2)=0;
            FILE.time(2)=FILE.IS_tline(1);
        end
    end
    
    if FILE.files2read(3) %read DGPS=============================================================
        FILE.DGPS_tline = fgetl(FILE.fid(3));
        if ~isempty(FILE.DGPS_tline)
            FILE.DGPS_tline=strread(FILE.DGPS_tline,'%f','delimiter',' ','commentstyle','matlab')';
        end
        if isempty(FILE.DGPS_tline)
            FILE.files2read(3)=1;
            FILE.time(3)=NaN;
        else
            FILE.files2read(3)=0;
            FILE.time(3)=FILE.DGPS_tline(1);
        end
    end

    if FILE.files2read(4) %read MTI==============================================================
        FILE.MTI_tline = fgetl(FILE.fid(4));
        if ~isempty(FILE.MTI_tline)
            FILE.MTI_tline=strread(FILE.MTI_tline,'%f','delimiter',' ','commentstyle','matlab')';
        end
        if isempty(FILE.MTI_tline)
            FILE.files2read(4)=1;           
            FILE.time(4)=NaN;            
        else
            FILE.files2read(4)=0;
            FILE.time(4)=FILE.MTI_tline(1);
        end
    end
    if all(FILE.time>0)%decide output device=====================================================
        [dummy,device]=min([FILE.time]);
        FILE.files2read(device)=1;
    end       
end