function [ vx,vy,vz,roll,pitch,yaw ] = D2Dprima_sparus_DVL_v2(vx_dvl,vy_dvl,vz_dvl,roll_dvl,pitch_dvl,yaw_dvl)
%--------------------------------------------------------------------------
%[ vel,roll,pitch,yaw ] = D2Dprima(vx_dvl,vy_dvl,vz_dvl,roll_dvl,pitch_dvl,yaw_dvl)
%--------------------------------------------------------------------------
%
%ICTINEU ICTINEU ICTINEU ICTINEU ICTINEU ICTINEU ICTINEU ICTINEU ICTINEU ICTINEU
%ICTINEU ICTINEU ICTINEU ICTINEU ICTINEU ICTINEU ICTINEU ICTINEU ICTINEU ICTINEU
%
% vx_dvl,vy_dvl,vz_dvl -> velocity measurements in DVL coordinate frame
% roll_dvl,pitch_dvl,yaw_dvl -> angle measurements from DVL
%
%Transforms the velocities measured in the DVL coordinate frame {D} to a
%one {D'} coherent with the AUV's coordinate frame by means of a RPY rotation.
%The angular measurements are adapted to represent the rotation of the {D'}
%coordinate frame with respect to a NED (North, East, Down) coordinate
%frame.

rotacio.z=0; %Aqui configurem la orientacio del DVL respecte el sistema de coordenades del robot
rotacio.y=0; %Basicament es descriu les rotacions necessaries per convertir el sistema de coordenades
% rotacio.x=pi; %del robot al del DVL comenšant per Z, dp per Y i finalment rotant en X 
rotacio.x=0; %del robot al del DVL comenšant per Z, dp per Y i finalment rotant en X 

m_rot=mrot(rotacio.x,rotacio.y,rotacio.z);

vel=m_rot*[vx_dvl;vy_dvl;vz_dvl];

vx=vel(1); vy=vel(2); vz=vel(3);


roll=0;%-pitch_dvl;
pitch=0;%roll_dvl;
% yaw=yaw_dvl-rotacio.z;
yaw=yaw_dvl;

%MIRAR LA LLIBRETA PER MES DETALLS SOBRE LA TRANSFORAMACIO {D} -> {D'} 

% 
% 
% roll=roll_dvl;
% pitch=2*pi-pitch_dvl;
% yaw=2*pi-yaw_dvl;
% 
% m_rot2=mrot(roll_dvl,-pitch_dvl,-yaw_dvl);
% 
% aa=m_rot2*m_rot';
% 
% nx=aa(1,1);
% ny=aa(2,1);
% nz=aa(3,1);
% ox=aa(1,2);
% oy=aa(2,2);
% oz=aa(3,2);
% ax=aa(1,3);
% ay=aa(2,3);
% az=aa(3,3);
% 
% X=atan2(ny,nx) %Z
% Y=atan2(-nz,(nx*cos(X)+ny*sin(X)))
% Z=atan2((-ay*cos(X)-ax*sin(X)),(oy*cos(X)-ox*sin(X)))
% 






% % roll=pi;
% % pitch=0;
% % yaw=pi/2;
% 
% roll=0;
% pitch=0;
% yaw=-pi/2;
% 
% 
% rot=[1 sin(roll)*tan(pitch) cos(roll)*tan(pitch);
%  0 cos(roll)              -sin(roll);
%  0 sin(roll)/cos(pitch) cos(roll)/cos(pitch)]
% 
% 
% roll_dvl=pi/4; 
% pitch_dvl=0; 
% yaw_dvl=0;
% normalize(rot*[roll_dvl; 2*pi-pitch_dvl; 2*pi-yaw_dvl])