function [cov,error] = pIC_cov_weight_T_test(theta,Sr,assoc)


Nmp = size(assoc.a,2);
q_th = theta(3);
I = eye(3);
error = 0;

invSr = inv(Sr);
[L,rank] = chol(invSr);
 if (rank~=0)
     disp('Sr rank ~0');
      [E,Lambda]=eig(Sr);
      %error('Sigma must be positive semi-definite.')
      if (min(diag(Lambda))<0),error = 1; end
      L = sqrt(Lambda)*E';
  end;

% points and weights
% a_x = assoc.a(1,:);
% a_y = assoc.a(2,:);
% n_x = assoc.n(1,:);
% n_y = assoc.n(2,:);
% w1 = L(2*i-1,2*i-1);
% w2 = L(2*i-1,2*i);
% w3 = L(2*i,2*i-1);
% w4 = L(2*i,2*i);


% residuals
ci = Composition(theta, assoc.n);
Ri = assoc.a - ci;
R = reshape(Ri,2*Nmp,1);
R_hat = L * R;
% L = Sr;
% R_hat = R;

for i = 1:Nmp
    %Jacobian d_Ri/d_Theta
    % J_theta =
    %
    % [ -w1, -w2, w1*(n_y*cos(q_th) + n_x*sin(q_th)) - w2*(n_x*cos(q_th) - n_y*sin(q_th))]
    % [ -w3, -w4, w3*(n_y*cos(q_th) + n_x*sin(q_th)) - w4*(n_x*cos(q_th) - n_y*sin(q_th))]
    
    %constants
    w1 = L(2*i-1,2*i-1);    w2 = L(2*i-1,2*i);    w3 = L(2*i,2*i-1);    w4 = L(2*i,2*i);
    n_x = assoc.n(1,i);    n_y = assoc.n(2,i);
    
    %Jacobian d_Ri/d_Theta
    J_theta(2*i-1:2*i,:) = [ -w1, -w2, w1*(n_y*cos(q_th) + n_x*sin(q_th)) - w2*(n_x*cos(q_th) - n_y*sin(q_th));
                             -w3, -w4, w3*(n_y*cos(q_th) + n_x*sin(q_th)) - w4*(n_x*cos(q_th) - n_y*sin(q_th))];
end

%Jacobian d_Ri/d_X
% Xi = [assoc.a; assoc.n];
% X = reshape(Xi,4*Nmp,1);
% J_X = jacobian(Ri, Ui);

% J_X =
% [ w1, w2, - w1*cos(q_th) - w2*sin(q_th), w1*sin(q_th) - w2*cos(q_th)]
% [ w3, w4, - w3*cos(q_th) - w4*sin(q_th), w3*sin(q_th) - w4*cos(q_th)]
 
J_X = zeros(2*Nmp,4*Nmp);
for i = 0:Nmp-1
    %constants (to check)
    w1 = L(2*(i+1)-1,2*(i+1)-1);    w2 = L(2*(i+1)-1,2*(i+1));    w3 = L(2*(i+1),2*(i+1)-1);    w4 = L(2*(i+1),2*(i+1));

    J_X(i*2+1:i*2+2,i*4+1:i*4+4) = [w1, w2, - w1*cos(q_th) - w2*sin(q_th), w1*sin(q_th) - w2*cos(q_th);
                                    w3, w4, - w3*cos(q_th) - w4*sin(q_th), w3*sin(q_th) - w4*cos(q_th)];
%     J_X(i*2+1:i*2+2,i*4+1:i*4+4) = [L(2*(i+1)-1,2*(i+1)-1), L(2*(i+1)-1,2*(i+1)), - L(2*(i+1)-1,2*(i+1)-1)*cos(q_th) - L(2*(i+1)-1,2*(i+1))*sin(q_th), L(2*(i+1)-1,2*(i+1)-1)*sin(q_th) - L(2*(i+1)-1,2*(i+1))*cos(q_th);
%                                     L(2*(i+1),2*(i+1)-1), L(2*(i+1),2*(i+1)), - L(2*(i+1),2*(i+1)-1)*cos(q_th) - L(2*(i+1),2*(i+1))*sin(q_th), L(2*(i+1),2*(i+1)-1)*sin(q_th) - L(2*(i+1),2*(i+1))*cos(q_th)];
end

%% Hessian Calculation from Jacobian
%  with respect to theta%

% the jacobial from symbolics
% jacobian(J_theta,theta) =   
% [ 0, 0,                                                                       0]
% [ 0, 0,                                                                       0]
% [ 0, 0, w1*(n_x*cos(q_th) - n_y*sin(q_th)) + w2*(n_y*cos(q_th) + n_x*sin(q_th))]
% [ 0, 0,                                                                       0]
% [ 0, 0,                                                                       0]
% [ 0, 0, w3*(n_x*cos(q_th) - n_y*sin(q_th)) + w4*(n_y*cos(q_th) + n_x*sin(q_th))]
%
col33 = zeros(1,Nmp);
col63 = zeros(1,Nmp);
for i = 1:Nmp
    %constants
    w1 = L(2*i-1,2*i-1);    w2 = L(2*i-1,2*i);    w3 = L(2*i,2*i-1);    w4 = L(2*i,2*i);
    n_x = assoc.n(1,i);    n_y = assoc.n(2,i);
    
    col33(:,i) = w1*(n_x*cos(q_th) - n_y*sin(q_th)) + w2*(n_y*cos(q_th) + n_x*sin(q_th));
    col63(:,i) = w3*(n_x*cos(q_th) - n_y*sin(q_th)) + w4*(n_y*cos(q_th) + n_x*sin(q_th));
end
upper = [zeros(2,Nmp); col33; zeros(2,Nmp); col63];
col3 = reshape(upper,6*Nmp,1);
col12 = zeros(6*Nmp,2);
JHa = [col12 col3];

% the Kronecker tensor product
Ha = kron(R_hat',I) * JHa;

% And the Hessian d(g)/d(theta)
dg_dtheta = 2 * (J_theta' * J_theta) + 2*Ha;


%% Hessian Calculation from Jacobian
%  with respect to X (point and matches)
%  s2 = jacobian(J_theta,Ui) =
%  
% [ 0, 0,                           0,                           0]
% [ 0, 0,                           0,                           0]
% [ 0, 0, w1*sin(q_th) - w2*cos(q_th), w1*cos(q_th) + w2*sin(q_th)]
% [ 0, 0,                           0,                           0]
% [ 0, 0,                           0,                           0]
% [ 0, 0, w3*sin(q_th) - w4*cos(q_th), w3*cos(q_th) + w4*sin(q_th)]
   
JHx = zeros(6*Nmp,4*Nmp);
for i = 0:Nmp-1
    %constants (to check)
    w1 = L(2*(i+1)-1,2*(i+1)-1);  w2 = L(2*(i+1)-1,2*(i+1));    
    w3 = L(2*(i+1),2*(i+1)-1);    w4 = L(2*(i+1),2*(i+1));
    
    JHx(i*6+1:i*6+6,i*4+1:i*4+4)=[0, 0,                           0,                           0;
                                  0, 0,                           0,                           0;
                                  0, 0, w1*sin(q_th) - w2*cos(q_th), w1*cos(q_th) + w2*sin(q_th);
                                  0, 0,                           0,                           0;
                                  0, 0,                           0,                           0;
                                  0, 0, w3*sin(q_th) - w4*cos(q_th), w3*cos(q_th) + w4*sin(q_th)];
end

Hbx = kron(R_hat',I) * JHx;

dg_dX = 2 * (J_theta' * J_X) + 2*Hbx;


dg_dtheta = (dg_dtheta + dg_dtheta')./2; % force dg_dtheta to be symmetric
inv_dg_dtheta = inv(dg_dtheta);

Sx = zeros(4*Nmp,4*Nmp);
for i = 0:Nmp-1
    Sx(i*4+1:i*4+2,i*4+1:i*4+2)=assoc.Pa(:,:,i+1);
    Sx(i*4+3:i*4+4,i*4+3:i*4+4)=assoc.Pn(:,:,i+1);
end

% A = dg_dtheta \ dg_dX;
% B = dg_dX' / dg_dtheta;
% cov = A * Sx * B;
cov = inv_dg_dtheta * dg_dX * Sx * dg_dX' * inv_dg_dtheta;

[~,rank] = chol(cov);
 if (rank~=0)
       disp('cov rank ~0');
      [~,Lambda]=eig(cov);
      if (min(diag(Lambda))<0),error = 1;end
%       La = sqrt(Lambda)*E';
  end;


