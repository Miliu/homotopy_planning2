function [loop_index] = findloops_v4(x)
% function findloops_v4, returns the scans index of the positions that can close
%a loop from the current position. It close loop after a number of previous 
%scans (defined in CONFIG.state_dist)and with the ones that are inside a max 
%range (defined in CONFIG.max_range
%
% x = the full state vector, with x_i=[x,y,th]'
% loop_index = scans index starting from further to closer one.
%
%v4: look a window of scans

global CONFIG;

curX      = x(1:3);      %current position
N         = size(x,1);
nu_scan   = (N/3)-1;     %find the number of scans
prv_state = CONFIG.state_dist;% number of previous states to put in the loop
unsorted  = zeros(1,nu_scan);

for i = 1:(nu_scan)
    
    %position for compare
    elm_x = N -(3*i-1); elm_y = N -(3*i-2); elm_th = N -(3*i-3);
    testX = [x(elm_x); x(elm_y); x(elm_th)];
    
    %find euclidian distance
    pos_range = sqrt((curX(1)-testX(1))^2 + (curX(2)-testX(2))^2);
    
    %Check if in range and register
    if (pos_range < CONFIG.max_range) && (i <= nu_scan-prv_state) && (i~=nu_scan)
        unsorted(i) = pos_range;    
    end
    if i == nu_scan
        unsorted(i) = pos_range;    
    end
end
%add the previous scan also


[rng, loop_index]       = sort(unsorted,'descend');
indexDelete             = (rng == 0);
loop_index(indexDelete) = [];

