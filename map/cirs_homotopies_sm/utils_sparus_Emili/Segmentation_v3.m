function [scan] = Segmentation_v3(beams)
% Given a Minking's Imaging Sonar data returns an struct that contains
% theta (1xN) and range (nxN)
% Distance is computed according the local maxim intensities value
%
% Output: scan (2xN) matrix where   row1: theta
%                                   row2: distance
% If a theta have more than one distance the theta is duplicated for the
% rest of the distances.

% ver 3. minor corrections

global PARAM;

intensity = beams.int;
% Delete sonar view after the threshold
resolution = PARAM.IS_range_res;
if(PARAM.segmentation.maxRange < size(intensity,2)*resolution)
    intensity(:,floor(PARAM.segmentation.maxRange/resolution)+1:size(intensity,2)) = 0; %[] maybe faster
elseif PARAM.debug > 0
    disp('Range not modified');
end

% Delete sonar view before the threshold
% intensity(:,1:round(PARAM.segmentation.initRange/resolution)) = 0;
bins_offset = ceil(PARAM.segmentation.offset.rho/resolution);
for i = 1:size(intensity,2)
  intensity(i,1:bins_offset(i)) = 0;  
end    

% Segmentation
index = intensity<=PARAM.segmentation.threshold;
intensity(index) = 0;

% Look for local max
distance = PARAM.segmentation.maxDistance/resolution;

scan.theta = beams.angle';

scan.rho = zeros(PARAM.segmentation.maxRangesPerAngle,size(intensity,1));
indexDelete = ones(1,size(intensity,1));

for i = 1:size(intensity,1)
    candidate = find(intensity(i,:) > 0.5);
    [~,candidate] = SortCandidates(intensity(i,candidate),candidate);
    
    localMax = ones(size(candidate));
    for a = 1:length(candidate)
        b = 1; exit = 0;
        while (b<a  && exit == 0)
            if abs(candidate(a)-candidate(b)) < distance
                localMax(a) = 0;
                exit = 1;
            end
            b = b+1;
        end
    end
   
    index = find(localMax == 1);
    if(~isempty(index))
        j = 1:length(index);
        scan.rho(1:length(index),i) = (candidate(index(j))*resolution)';
        indexDelete(i) = 0;
    end
end

% % % Delete beams with no ranges
% % scan.theta(indexDelete) = [];
% % scan.rho(:,indexDelete) = [];

if(PARAM.segmentation.maxRangesPerAngle>1)
    scan.theta = repmat(scan.theta,1,PARAM.segmentation.maxRangesPerAngle);
    scan.rho = reshape(scan.rho',1,size(scan.rho,2)*PARAM.segmentation.maxRangesPerAngle);
    
% %     % Look for and delete values with rho 0
% %     indexDelete = (scan.rho == 0);
% %     scan.theta(indexDelete) = [];
% %     scan.rho(indexDelete) = [];
    
% %     % Needed to be sorted by theta??
end