function [votes,candidates] = SortCandidates(votes,candidates)
[vs,p] = sort(-votes);
votes = votes(p);
candidates = candidates(p);