function [hp] = plot_IS_v3(sonar_data, sonar_angle, sonar_range, position_data, mode, ObjectHandle)
%-------------------------------------------------------------------------
% Obtains a acoustic image from sonar data
% Arguments:
%   sonar_data (num_beams x measures_beam): acustic measurements along one complete scan
%   sonar_angle (num_beams): transducer angle in radians for each beam
%   sonar_range: maximum range of a beam
%   position_data(6/3 x num_beams): ROBOT odometry ([x y z roll pitch yaw] or [x y yaw]) at the time of each beam
%   mode: 0 faster but less accurate, 1 more accurated, represents each beam individually, 2 accurate leaveas the arc
%         after the threshold, 3 accurate leaveas the arc after the threshold 
%-------------------------------------------------------------------------

global PARAM CONFIG

if ~exist('PARAM','var') %In case I forget to set PARAM I set the basic transformation to represent the IS with the X looking forward
   PARAM.Tdvl2is=[0 0 0 0 0 pi]; 
end


if size(position_data,1)==6 %Build a [x y yaw] position vector
    position_data=position_data([1 2 6],:);
end


%transforms (given PARAM.Tdvl2is) the sonar data from the IS coordinate frame to the position's coordinate frame (DVL) 
position_data=[ position_data(1,:) + (PARAM.Tdvl2is(1).*cos(position_data(3,:))) - (PARAM.Tdvl2is(2).*sin(position_data(3,:))); 
                position_data(2,:) + (PARAM.Tdvl2is(1).*sin(position_data(3,:))) + (PARAM.Tdvl2is(2).*cos(position_data(3,:)));
                normAngle(position_data(3,:)+PARAM.Tdvl2is(6))];
base=zeros(6,1);%position_data(:,end); %IMPORTANT This is the base reference frame. As default set as the last (the actual) position of the sensor 

if mode==0 %faster but less accurate. Paints the stict point position but the sonar data is distorted
    n_bins=size(sonar_data,2); %define number of bins in a single beam
    n_beams=length(position_data(3,:)); %define number of beams
    distances=(sonar_range/n_bins):(sonar_range/n_bins):sonar_range; %position of the bins into de beam (rho)
    %Transformation to get the x-y coordinates of each point with respect the base reference frame
    th2=base(3);
    th=th2-repmat(position_data(3,:)'+sonar_angle,1,n_bins); 
    X=(repmat(position_data(1,:)',1,n_bins)-base(1)).*cos(th2)+(repmat(position_data(2,:)', 1, n_bins)-base(2)).*sin(th2)+repmat(distances,n_beams,1).*cos(th);
    Y=(repmat(-position_data(1,:)',1,n_bins)-base(1)).*sin(th2)+(repmat(position_data(2,:)', 1, n_bins)-base(2)).*cos(th2)-repmat(distances,n_beams,1).*sin(th);
    
    %pcolor do not accept matrixs with only one row. If we plot only a single sonar beam this happens
    if size(X,1)==1 %When only one single beam is plotted do
        X=[X;X];
        Y=[Y;Y];
        sonar_data=[sonar_data;sonar_data];
    end
    
elseif mode>0 %slower but more accurate. Represents beams as independent entities. Plots the data taking into account the beambidth and placing each sonar value in the correct position
    if ~exist('CONFIG','var') %In case I forget to set CONFIG I set the normal beamwidth for our IS
        CONFIG.beamwidth = 3*pi/180;
    end
    n_bins=size(sonar_data,2); %define number of bins in a single beam
    res=(sonar_range/n_bins); 
    distances=(res-(res/2)):(sonar_range/n_bins):(sonar_range+(res/2)); % limits of the bins into the beam (rho)
    %Reshape and modify the data to represent beams with their beamwidth and as independent objects separated by NaNs
    position_data=reshape([position_data;position_data;position_data],3,3*size(position_data,2));
    sonar_angle=reshape([sonar_angle,sonar_angle,sonar_angle]',3*length(sonar_angle),1)+repmat([-CONFIG.beamwidth/2; CONFIG.beamwidth/2; NaN],length(sonar_angle),1);
    sonar_data=[reshape([sonar_data'; sonar_data'; sonar_data'],size(sonar_data,2),3*size(sonar_data,1))' zeros(3*size(sonar_data,1),1)];
    n_bins=length(distances); %recalculate number of bins after the transformation
    n_beams=length(position_data(3,:)); %define number of beams
    %Transformation to get the x-y coordinates of each point with respect the base reference frame
    th2=base(3);
    th=th2-repmat(position_data(3,:)'+sonar_angle,1,n_bins); %thetas for each vote and each assigned cell
    X=(repmat(position_data(1,:)',1,n_bins)-base(1)).*cos(th2)+(repmat(position_data(2,:)', 1, n_bins)-base(2)).*sin(th2)+repmat(distances,n_beams,1).*cos(th);
    Y=(repmat(-position_data(1,:)',1,n_bins)-base(1)).*sin(th2)+(repmat(position_data(2,:)', 1, n_bins)-base(2)).*cos(th2)-repmat(distances,n_beams,1).*sin(th);
end


if nargin==5
    hp = pcolor(X,Y,sonar_data);
    shading interp
elseif nargin==6
    set(ObjectHandle, 'XData',X,'YData',Y,'ZData',sonar_data,'CData', sonar_data);
    [c,i]=max(sonar_data(1,:)); 
    %print the "arc"
    if c>=PARAM.segmentation.threshold && mode == 2; 
        plot(X(1:2,i),Y(1:2,i),'k'); 
    end
    %print the middle dot
    if c>=PARAM.segmentation.threshold && mode == 3; 
        plot((X(1,i)+X(2,i))/2,(Y(1,i)+Y(2,i))/2,'r'); 
    end 
end
