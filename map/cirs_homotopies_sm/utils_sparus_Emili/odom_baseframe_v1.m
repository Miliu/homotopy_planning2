function res = odom_baseframe_v1(beams1,beams2)
%
%The function returns the displacement X and its uncertainty P between two scans
%or the displacement of one scan


if nargin == 1
    x = beams1.pos(1,1);
    y = beams1.pos(2,1);
    %     th = beams1.pos(3,1);
    Pbi = beams1.cov(1:2,1:2,1);
    idx1 = beams1.ref_point;
    
    for i = 2:idx1
        x1 = beams1.pos(1,i);
        y1 = beams1.pos(2,i);
%         th1 = beams1.pos(3,i);
        Q = beams1.cov(1:2,1:2,i);
        
        x = x + x1;
        y = y + y1;
        %Uncertainty update
        Pbi = Pbi + Q;
    end
    Bi = [x y 0]';
    Pbi(3,1:3) = [0 0 0];
    
    %position
    Rj = [0;0;beams1.pos(3,idx1)];
    P_RJ = beams1.cov(:,:,idx1);
    P_RJ(1:2,1:2)=0;
    
%     invRi = invert(Rref);
%     R_B = compound(invRi.x,Bi);
    X_ir = compound(Bi,Rj);
    
    %covariance
%     PinvRi = invRi.oJ * P_ref * invRi.oJ';
%     P_R_B = R_B.Ja*PinvRi*R_B.Ja' + R_B.Jb*Pbi*R_B.Jb';
    P_ir = X_ir.Ja*Pbi*X_ir.Ja' + X_ir.Jb*P_RJ*X_ir.Jb';
    
    
    res.pos = X_ir.x;
    res.cov = P_ir;
end

if nargin == 2
    idx1 = beams1.ref_point;
    idx2 = beams2.ref_point;
    
    %reference point
    ref_X_b = beams1.pos(:,idx1);
    ref_P_b = beams1.cov(:,:,idx1);
    
    %the same in local frame
    Rref = [0;0;ref_X_b(3)];
    P_ref = zeros(3);
    P_ref(3,3) = ref_P_b(3,3);
    
    %find the linear traslations for the first half of the odometry
    Bi = beams1.pos(1:2,idx1+1);
    Pbi = beams1.cov(1:2,1:2,idx1+1);
    for i = idx1+2 : size(beams1.pos,2)
        Bi = Bi + beams1.pos(1:2,i);
        Pbi = Pbi + beams1.cov(1:2,1:2,i);
    end
    
    %and now the second half
    for i = 1:idx2
        Bi = Bi + beams2.pos(1:2,i);
        Pbi = Pbi + beams2.cov(1:2,1:2,i);
    end
    Bi(3) = 0;
    Pbi(3,1:3) = [0 0 0];
    
    %position
    Rj = [0;0;beams2.pos(3,idx2)];
    P_RJ = beams2.cov(:,:,idx2);
    P_RJ(1:2,1:2)=0;
    
%     invRi = invert(Rref);
%     R_B = compound(invRi.x,Bi);
%     X_ir = compound(R_B.x,Rj);
    X_ir = compound(Bi,Rj);
    
    %covariance
%     PinvRi = invRi.oJ * P_ref * invRi.oJ';
%     P_R_B = R_B.Ja*PinvRi*R_B.Ja' + R_B.Jb*Pbi*R_B.Jb';
%     P_ir = X_ir.Ja*P_R_B*X_ir.Ja' + X_ir.Jb*P_RJ*X_ir.Jb';
    P_ir = X_ir.Ja*Pbi*X_ir.Ja' + X_ir.Jb*P_RJ*X_ir.Jb';
    
    res.pos = X_ir.x;
    res.cov = P_ir;
end



