function [time] = realtime2unix(realtime)
% converts real world time to unix time system
% realtime = [yyyy mm dd hh mm ss fff]
% fff: fractions of seconds

time = realtime(1:6);
time = (datenum(time)-datenum(1970,01,01,00,00,00))*86400;
time = time+realtime(7)/1000;
