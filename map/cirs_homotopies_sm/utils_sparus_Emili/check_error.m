function check_error(traj)

% traj: trajectory [x,y,yaw]' in the global frame

dtraj = zeros(3,size(traj,2)-1);
dtraj(1,:) = diff(traj(1,:));
dtraj(2,:) = diff(traj(2,:));
dtraj(3,:) = normAngle(diff(traj(3,:)));

gt = load('Data\gps_mti_trajectory.mat');
n_beams = 200;
n_scans = size(gt.gps,2)/n_beams;
dgt = zeros(3,n_scans);
i = 1:n_beams:size(gt.gps,2);
dgt(1:2,:) = gt.gps(:,i);
dgt(3,:) = normAngle(gt.yaw_acc(i));

delta_gt = zeros(3,n_scans-1);
delta_gt(1,:) = diff(dgt(1,:));
delta_gt(2,:) = diff(dgt(2,:));
delta_gt(3,:) = normAngle(diff(dgt(3,:)));

err_dist = ((dtraj(1,:)-delta_gt(1,:)).^2+(dtraj(2,:)-delta_gt(2,:)).^2).^0.5;
err_yaw = ((dtraj(3,:)-delta_gt(3,:)).^2).^0.5;

err_xy = dtraj-delta_gt;

iscans= 1:size(err_dist,2);

figure
plot(err_xy(1,:),err_xy(2,:),'.b')

figure
histfit(err_xy(1,:))
%title 'Distance error';
ylabel('Number of scans'); xlabel('Error for x (m)');

figure
histfit(err_xy(2,:))
%title 'Distance error';
ylabel('Number of scans'); xlabel('Error for y (m)');

figure;
subplot(2,2,1);
bar(iscans,err_dist);
title 'Distance error';
xlabel('Scan matching'); ylabel('Error (m)');
subplot(2,2,2); 
hist(err_dist,30);
h = findobj(gca,'Type','patch');
set(h,'EdgeColor','w')
title 'Distance error';
xlabel('Error (m)'); ylabel('N� scan matchings');

subplot(2,2,3);
bar(iscans,rad2deg(err_yaw));
title 'Yaw error';
xlabel('Scan matching'); ylabel('Error (deg)');
subplot(2,2,4); 
hist(rad2deg(err_yaw));
title 'Yaw error';
xlabel('Error (deg)'); ylabel('N� scan matchings');


end