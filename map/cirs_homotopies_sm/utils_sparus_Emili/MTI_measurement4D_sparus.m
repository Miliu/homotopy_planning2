function [ z,H,R ] = MTI_measurement4D_sparus(line,config)
%Select the vehicle measurements from the DVL input line

global PARAM

if ~exist('config','var')
    config=[ 1 0 ];
end


H=[];
z=[];
R=[];

if config(1)
%     z=[z; normAngle(line(3)*(pi/180))];
%     H=[H; 0 0 0 1 0 0 0 0];
%     R=[R PARAM.SDmtiyaw^2];
    z=[z; line(1)];
    H=[H; 0 0 0 0 0 0 0 1];
    R=[R PARAM.SDmtivy^2];

end

if config(2)
    z=[z; normAngle(line(6))];
    H=[H; 0 0 0 0 0 0 0 1];
    R=[R PARAM.SDmtivy^2];
end

R=diag(R); %uncertainty