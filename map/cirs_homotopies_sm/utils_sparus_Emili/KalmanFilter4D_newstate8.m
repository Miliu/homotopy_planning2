function [X,P]=KalmanFilter4D_newstate8(state,P,At,Z,H,R)
%[X,P]=KalmanFilter(state,P,At,Z,H,R)
%
%If state, P and At are input of the function it performs a prediction.
%If Z,H and R are also present, an additional correction step is performed. 
%The function returns the new state X and its uncertainty P

global PARAM;

%--RENAME THE STATE VARS---------------------------------------------------
x=state(1);
y=state(2);
z=state(3);
yw=normAngle(state(4));
u=state(5);
v=state(6);
w=state(7);
vyw=state(8);
t=At;
tamany=length(state)-8;

%--PREDICTION--------------------------------------------------------------

%Constant velocity model
X = [x + (u*t)*cos(yw) - (v*t)*sin(yw);
     y + (u*t)*sin(yw) + (v*t)*cos(yw);
     z + w*t;
     normAngle(yw + vyw*t);
     u;
     v;
     w;
     vyw];

%The rest of the state (features) is constant and doesn't changes
X=[X; state(9:end)];

%Jacobian of partial derivates of the funcion respect to state vector X 
A=[ 1, 0, 0, -(u*t)*sin(yw)-(v*t)*cos(yw), t*cos(yw), -t*sin(yw), 0, 0; 
    0, 1, 0,  (u*t)*cos(yw)-(v*t)*sin(yw), t*sin(yw),  t*cos(yw), 0, 0; 
    0, 0, 1, 0, 0, 0, t, 0; 
    0, 0, 0, 1, 0, 0, 0, t; 
    0, 0, 0, 0, 1, 0, 0, 0; 
    0, 0, 0, 0, 0, 1, 0, 0; 
    0, 0, 0, 0, 0, 0, 1, 0; 
    0, 0, 0, 0, 0, 0, 0, 1];


%Jacobian of partial derivates of the funcion respect to noises 
%(noise propagated through linear and angular velocities)            
B=[ 1/2*t^2*cos(yw), -1/2*t^2*sin(yw), 0, -1/2*(u*t)*t^2*sin(yw)-1/2*(v*t)*t^2*cos(yw);
    1/2*t^2*sin(yw),  1/2*t^2*cos(yw), 0,  1/2*(u*t)*t^2*cos(yw)-1/2*(v*t)*t^2*sin(yw);
    0, 0, 1/2*t^2, 0;
    0, 0, 0, 1/2*t^2;
    t, 0, 0, 0;
    0, t, 0, 0;
    0, 0, t, 0;
    0, 0, 0, t];

%Model Uncertainty                     
Q=diag([PARAM.SDur^2 PARAM.SDvr^2 PARAM.SDwr^2 PARAM.SDvywr^2]);

%Build the Jacobians
% J1=A;
% J2=B;
J1=sparse(blkdiag(A, eye(tamany)));
J2=sparse([B;zeros(tamany,4)]);

%Uncertainty update
P=J1*P*J1'+J2*Q*J2';


%---CORRECTION-------------------------------------------------------------

if nargin==6
    H=[H zeros(size(H,1),tamany)];
    [files_ang,dummy]=find(H(:,4));
    Hx=H*X;
    Hx(files_ang)=normAngle(Hx(files_ang));
    dif=Z-Hx;
    dif(files_ang)=normAngle(dif(files_ang));    
    
    K=(P*H')*inv(H*P*H'+R);
    X=X+K*(dif);
%     P=(eye(length(X))-K*H)*P;
    P=(eye(length(X))-K*H)*P*(eye(length(X))-K*H)' + K*R*K'; %Joseph Form
    X(4)=normAngle(X(4));
end


%--APPENDIX (Simbolic calculation of the A and B matrices)-------------------------
%syms  x y z yw u v w vyw t su sv sw syw
% X = [x + (u*t+su*(t^2)/2)*cos(yw + syw*(t^2)/2) - (v*t+sv*(t^2)/2)*sin(yw + syw*(t^2)/2);
%      y + (u*t+su*(t^2)/2)*sin(yw + syw*(t^2)/2) + (v*t+sv*(t^2)/2)*cos(yw + syw*(t^2)/2);
%      z + (w*t+sw*(t^2)/2);
%      yw + (vyw*t+syw*(t^2)/2);
%      u + su*t;
%      v + sv*t;
%      w + sw*t;
%      vyw + syw*t];
%  A=[diff(X,x) diff(X,y) diff(X,z) diff(X,yw) diff(X,u) diff(X,v) diff(X,w) diff(X,vyw)]
%  B=[diff(X,su) diff(X,sv) diff(X,sw) diff(X,syw)]
