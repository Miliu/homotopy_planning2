function [q,scan] = ScanForming_old(sensorData, robotPosition, q)

% Function ScanForming
%
% Given an IS data, global referenced positions of the robot for each beam 
% and a scan, the function returns a full scan referenced to 
% 'q' in cartesian coordinates with each covariance matrix 'P'
% 
% Input:
%  sensorData: segmented miniking's data
%  robotPosition: position(x,y,yaw) of the robot for each beam
%
% Output:
%  q: estimation of the global position during the scan. 
%  scan: struct which contains
%    cart: scan's data in cartesian cooridates referenced to 'position'
%    P: covariance matrixes of each beam

global PARAM;
% PLOT.beam_unc=plot(NaN,NaN,'k');
% -------------------------- Initializations ------------------------------
r_X_s = PARAM.sensor.position';

b_X_r = [robotPosition.est];
P_br = reshape([robotPosition.cov],3,3,size(b_X_r,2));

b_X_r = repmat(b_X_r,1,PARAM.segmentation.maxRangesPerAngle);
P_br = repmat(P_br,[1,1,PARAM.segmentation.maxRangesPerAngle]);

% Remove unnecessary points
indexDelete = (sensorData.rho == 0);
sensorData.theta(indexDelete) = [];
sensorData.rho(indexDelete) = [];
b_X_r(:,indexDelete) = [];
P_br(:,:,indexDelete) = [];

% ---------------------------- Computations -------------------------------

P_psi = [PARAM.sensor.deviation(1)^2 0; 0 PARAM.sensor.deviation(2)^2];
P_psi = repmat(P_psi,[1,1,size(sensorData.theta,2)]);
% P2C ---------------------------------------------------------------------
[x_s,y_s] = pol2cart(sensorData.theta,sensorData.rho);
X_s = [x_s;y_s];

% Jacobian for pol2cart
J_s = zeros(2,2,size(sensorData.theta,2));
J_s(1,1,:) = cos(sensorData.theta);
J_s(1,2,:) = -sensorData.rho.*sin(sensorData.theta);
J_s(2,1,:) = sin(sensorData.theta);
J_s(2,2,:) = sensorData.rho.*cos(sensorData.theta);

% X_r ---------------------------------------------------------------------
X_r = Composition(r_X_s,X_s);

% Jacobian for r_X_s
J_r = [cos(r_X_s(3)) -sin(r_X_s(3)); sin(r_X_s(3)) cos(r_X_s(3))];

% X_b ---------------------------------------------------------------------
X_b = zeros(2,size(sensorData.theta,2));
for i=1:size(sensorData.theta,2) % vectorization not possible
    X_b(:,i) = Composition(b_X_r(:,i),X_r(:,i));
end

% Jacobians for b_X_r
J_b1 = zeros(2,3,size(sensorData.theta,2));
J_b1(1,1,:) = 1;
J_b1(2,2,:) = 1;
J_b1(1,3,:) = -X_r(1,i).*sin(b_X_r(3,i))-X_r(2,i).*cos(b_X_r(3,i));
J_b1(2,3,:) = X_r(1,i).*cos(b_X_r(3,i))-X_r(2,i).*sin(b_X_r(3,i));

J_b2 = zeros(2,2,size(sensorData.theta,2));
J_b2(1,1,:) = cos(b_X_r(3,:));
J_b2(1,2,:) = -sin(b_X_r(3,:));
J_b2(2,1,:) = sin(b_X_r(3,:));
J_b2(2,2,:) = cos(b_X_r(3,:));

% X_i ---------------------------------------------------------------------
i_X_b = [0;0;-q.est(3)];
X_i = Composition(i_X_b,X_b);

% Jacobians for i_X_b
J_i1 = zeros(2,3,size(sensorData.theta,2));
J_i1(1,1,:) = 1;
J_i1(2,2,:) = 1;
J_i1(1,3,:) = -X_b(1,i).*sin(i_X_b(3))-X_b(2,i).*cos(i_X_b(3));
J_i1(2,3,:) = X_b(1,i).*cos(i_X_b(3))-X_b(2,i).*sin(i_X_b(3));

J_i2 = zeros(2,2,size(sensorData.theta,2));
J_i2(1,1,:) = cos(i_X_b(3));
J_i2(1,2,:) = -sin(i_X_b(3));
J_i2(2,1,:) = sin(i_X_b(3));
J_i2(2,2,:) = cos(i_X_b(3));


% P_i: Uncertainty refernced to the frame i -------------------------------
P_ib = zeros(3,3);
P_ib(3,3) = q.cov(3,3);
% % P_ib
% % disp('--- P_ib----');
% % pause;

P_s = zeros(2,2);
P_r = zeros(2,2);
P_b = zeros(2,2);
P_i = zeros(2,2,size(sensorData.theta,2));
for i=1:size(sensorData.theta,2)
    P_s = J_s(:,:,i)*P_psi(:,:,i)*J_s(:,:,i)';
    P_r = J_r*P_s*J_r';
    P_b = J_b1(:,:,i)*P_br(:,:,i)*J_b1(:,:,i)'+J_b2(:,:,i)*P_r*J_b2(:,:,i)';
    P_i(:,:,i) = J_i1(:,:,i)*P_ib*J_i1(:,:,i)'+J_i2(:,:,i)*P_b*J_i2(:,:,i)';
%     draw_ellipse([X_i(1,i) X_i(2,i)], P_i(1:2,1:2), 'k',PLOT.beam_unc);
%     drawnow
end

% % P_i
% % disp('--- P_i -----');
% % pause;

% Output variables --------------------------------------------------------
scan.cart = X_i;
scan.P = P_i;

