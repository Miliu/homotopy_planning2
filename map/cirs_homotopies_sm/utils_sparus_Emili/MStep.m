function [out,solution,errorK1,nConverged] = MStep(assoc,motion,errorK1,nConverged)

global PARAM;

if (size(assoc.a,2) == 0)
%     error('No associations avaiable.');
out = -1;
solution=motion.q;
PARAM.sm.uncertainty4 = zeros(3);
return
end

E = zeros(2*size(assoc.a,2),1);
H = zeros(2*size(assoc.a,2),3);
C = zeros(2*size(assoc.a,2));

for i = 1:size(assoc.a,2)
    E(2*i-1:2*i) = (assoc.a(:,i)-assoc.c(:,i)) + assoc.Jq(:,:,i)*motion.q;
    H(2*i-1:2*i,:) = assoc.Jq(:,:,i);
    C(2*i-1:2*i,2*i-1:2*i) = assoc.Pe(:,:,i);
end
invC = inv(C);
invA = inv(H'*invC*H);
qmin = invA*H'*invC*E;

% Compute error
e = Composition(qmin,assoc.n);
e = (e - assoc.a).^2;
err = sum(sum(e,2));

% Convergence criteria
errorRatio = err/errorK1;
diff = abs(motion.q' - qmin');
% diff = abs(PARAM.realq' - qmin');
if (abs(1-errorRatio) <= PARAM.sm.errorRatio) || (diff(1) < PARAM.sm.error(1) && diff(2) < PARAM.sm.error(2) && diff(3) < PARAM.sm.error(3))
    nConverged = nConverged+1;
else
    nConverged = 0;
end

% Build solution
errorK1 = err;
solution = qmin;

% Smooth convergence criterion
if (nConverged > PARAM.sm.nIterationSmoothConvergence)
%     PARAM.sm.uncertainty = inv(H'*invC*H); %displacement uncertainty
%     PARAM.sm.uncertainty1 = pIC_cov(qmin,C,assoc);
%     PARAM.sm.uncertainty3 = pIC_cov(qmin,C,assoc);
%     lala=eye(size(C));
%     PARAM.sm.Censi_num = icp_covariance_agg(qmin,C,assoc);
%     PARAM.sm.unc_2LS2 = inv(H'*inv(2*(C^2))*H);
%     PARAM.sm.unc_2LS = inv(H'*inv(2*(C))*H);
    PARAM.sm.unc_Haralick = pIC_cov_weight_T_test(qmin,C,assoc);
%     PARAM.sm.unc_LS = 2*(err/(i-3))*invA;
    PARAM.sm.unc_LS = invA/2;
%     PARAM.sm.unc_LS = inv(H'*inv(2*(C)^2)*H);
%     PARAM.sm.uncertainty = invA;
%     PARAM.sm.unc_LS = [invA(2,2),invA(1,2),invA(2,3); invA(2,1),invA(1,1),invA(1,3); invA(3,2),invA(3,1),invA(3,3)];
    out = 1;
else
    out = 0;
end

