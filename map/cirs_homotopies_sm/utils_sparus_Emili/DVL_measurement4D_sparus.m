function [ z,H,R ] = DVL_measurement4D_sparus(line,config)
%[ z,H,R ] = DVL_measurement(line,config)
%
%Takes a data line from a log and extracts the measurement z, the measurement
%model H and its uncertainty R

global PARAM 

if ~exist('config','var') %if no configuration parameter is given, look for all available measurements
    config=[1 0 1 1];
end

H=[];
z=[];
R=[];

if line(14)>0 && config(1) %If status for bottom velocity is OK (cheat for sparus)
    %     x y z y u v w r 
    H=[H; 0 0 0 0 1 0 0 0;
          0 0 0 0 0 1 0 0; 
          0 0 0 0 0 0 1 0];
%     [vx,vy,vz] = D2Dprima_sparus(line(11)/100,line(12)/100,line(13)/100,NaN,NaN,NaN);
    [vx,vy,vz,roll,pitch,yaw] = D2Dprima_sparus_DVL_v2(line(11)/100,line(12)/100,line(13)/100,NaN,NaN,line(22));
    z=[z; vx; vy; vz];
    R=[R (PARAM.SDub^2) (PARAM.SDvb^2) (PARAM.SDwb^2)];
end

[ vx,vy,vz,roll,pitch,yaw ] = D2Dprima_sparus_DVL_v2(line(7)/100,line(8)/100,line(9)/100,line(24)*pi/180,line(23)*pi/180,line(22));            
profunditat=0.003772250*(line(26)-1440); %profunditat

if line(10)==1 && config(2) %If status for water velocity is OK
    %     x y z y u v w r
    H=[H; 0 0 0 0 1 0 0 0; 
          0 0 0 0 0 1 0 0; 
          0 0 0 0 0 0 1 0];
    z=[z; vx; vy; vz];
    R=[R (PARAM.SDuw^2) (PARAM.SDvw^2) (PARAM.SDww^2)];
end
if config(3) %yaw
    %     x y z y u v w r
    H=[H; 0 0 0 1 0 0 0 0];
    z=[z; (yaw)];
    R=[R (PARAM.SDyaw^2)];   
end
if config(4) %depth
    %     x y z y u v w r
    H=[H; 0 0 1 0 0 0 0 0];
    z=[z; profunditat];
    R=[R (PARAM.SDzp^2)];   
end

R=diag(R);
    
    
