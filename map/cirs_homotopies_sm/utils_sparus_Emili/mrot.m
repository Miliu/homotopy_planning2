function [R]=mrot(X,Y,Z)
%[R]=mrot(X,Y,Z)
%R matriu de rotaci�
%X rotacio a l'eix X en rad
%Y rotacio a l'eix Y en rad
%Z rotacio a l'eix Z en rad
%
%Matriu RPY que es fa servir per passar de XYZ a ENU
%Aquesta matriu es equivalent a fer una rotacio del sistema 1 en Z, despres rotar el
%sistema resultant en Y i finalment, el resultant en X. El sistema final
%seria el 2
%
%Aquesta matriu et permet tranformar una cosa referenciada en el sistema de coordenades
%2 a la mateixa cosa referenciada a 1.


R=[cos(Z)*cos(Y)  -sin(Z)*cos(X)+cos(Z)*sin(Y)*sin(X)   sin(Z)*sin(X)+cos(Z)*sin(Y)*cos(X);
   sin(Z)*cos(Y)   cos(Z)*cos(X)+sin(X)*sin(Z)*sin(Y)  -sin(X)*cos(Z)+sin(Z)*sin(Y)*cos(X);
  -sin(Y)          cos(Y)*sin(X)                                             cos(Y)*cos(X)];