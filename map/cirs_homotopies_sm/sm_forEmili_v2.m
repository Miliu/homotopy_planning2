fclose('all');
clear all;
close all;
clc;
addpath('utils_sparus_Emili');
% profile on

%LOAD EXPERIMENT===========================================================
experiment='old_format/';


global FILE PARAM CONFIG PLOT NUM_SM

%FILE======================================================================
%Ids for the opened files
FILE.fid(1) = fopen([experiment 'linkquest_dvl_sensor_of.log']);
FILE.fid(2) = fopen([experiment 'is_sensor_of.log']);
% FILE.fid(3) = fopen([experiment 'DGPS.log']);
FILE.fid(4) = fopen([experiment 'mti_vz.log']);

%% PARAMETERS===================================================================
%Global
PARAM.chi2          =  chi2inv(0.95,2);    %chi2 at a 95% confidence level for ellipse drawing 

%Vehicle
PARAM.Tdvl2is       =  [0.465 0 0.11 0 0 pi]';
%PARAM.Tdvl2is      =  [0.5 0 -0.2 0 0 pi]';

%Imaging sonar
IS_setup            =  [10 0.0505 pi];
PARAM.IS_range      =  IS_setup(1);     %maximum beam distance (m)
PARAM.IS_range_res  =  IS_setup(2);     %beam resolution
PARAM.IS_angle_res  =  IS_setup(3);     %resolucio angular
%[#_of_beams, #_of_bins]
PARAM.IS_size       =  [round(2*pi/IS_setup(3))...
                        round(PARAM.IS_range/PARAM.IS_range_res)]; 
CONFIG.beamwidth    = deg2rad(1.8);   %Horizontal beamwidth of the IS 

%Scan Matching Parameters 
PARAM.sm.estep_method                  = 0;   %0 nearest neighbour; 1 virtual point
PARAM.sm.step                          = 1;
PARAM.sm.maxIterations                 = 250;
PARAM.sm.errorRatio                    = 0.0000001;
PARAM.sm.error                         = [0.0001 0.0001 0.0001];
PARAM.sm.nIterationSmoothConvergence   = 2;
PARAM.sm.chi2value                     = chi2inv(0.70,2);
% PARAM.sm.uncertainty                   = [1.5 1.5 0.05];
% R_sm                                   = diag(PARAM.sm.uncertainty)^2;

% Motion -----------------------------------------------------------------
% motion std deviation [x(m) y(m) yaw(rad)]
% PARAM.motion.deviation                 = [0.1 0.1 deg2rad(10)]; 

% Sensor -----------------------------------------------------------------
%x,y,yaw.Position respect to the robot's zero (z = 0)
PARAM.sensor.position                  = [0.47 0 pi]'; 
% std deviation [range(m) angular(rad)] [+/-5cm +/-1.5deg]
PARAM.sensor.deviation                 = [0.1 deg2rad(3)]; 
% PARAM.sensor.range                     = 10;           
% PARAM.sensor.resolution                = 0.0505;
% PARAM.sensor.sector                    = deg2rad(360);
% PARAM.sensor.thetaStep                 = deg2rad(1.8);

% Segmentation -----------------------------------------------------------
PARAM.segmentation.threshold           = 110; %intensity threshold. keep bins after that
% PARAM.segmentation.initRange           = 1.5; %in meters. erase first measurements to avoid sonar head ringing
PARAM.segmentation.maxRange            = 10;   %in meters. erase last measurements to avoid multipaths
PARAM.segmentation.maxDistance         = IS_setup(1);  %to check if needed
PARAM.segmentation.maxRangesPerAngle   = ...
    ceil(PARAM.IS_range/PARAM.segmentation.maxDistance); 
% compute sonar's "blind" spots
PARAM.segmentation.bounding_radius     = 0.45; %discard data the first X meters
PARAM.segmentation.bounding_radius2    = 1.25;
PARAM.segmentation.bounding_radius2_angle = deg2rad(20); % apply radius2 [-20..0..+20]
PARAM.msis.config.sector       = deg2rad(360);
PARAM.msis.config.theta_step    = deg2rad(1.8);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DVL
PARAM.SDub    =  0.01;       %m/s    velocity x bottom
PARAM.SDvb    =  0.01;       %m/s    velocity y bottom
PARAM.SDwb    =  0.01;       %m/s    velocity z bottom
PARAM.SDuw    =  0.01;       %m/s    velocity x water
PARAM.SDvw    =  0.01;       %m/s    velocity y water
PARAM.SDww    =  0.01;       %m/s    velocity z water
PARAM.SDyaw   =  0.1;       %rad    yaw
PARAM.SDzp    =  0.01;       %m      depth (pressure)
%MTI
PARAM.SDmtiyaw = 0.1;       %rad    yaw
PARAM.SDmtivy  = 0.1;       %rad/s  yaw
%Model
PARAM.SDur    =  0.1;        %m/s^2   velocity x robot
PARAM.SDvr    =  0.1;        %m/s^2   velocity y robot
PARAM.SDwr    =  0.1;        %m/s^2   velocity z robot
PARAM.SDvywr  =  0.1;       %rad/s^2  ang. velocity yaw robot

%% CONFIGURATION================================================================
%Slam
CONFIG.plot_slam            = 1;  %If 1 plot slam online, 2 for offline
CONFIG.plot_odom            = 1;  %If 1 plot local odometry
CONFIG.plot_sonar           = 0;  %If 1 plot sonar beam
CONFIG.plot_nu_beams        = 1;  %number of sonar beams to plot. for full sector put 1000
CONFIG.plot_IS_mode         = 1;  %0 for fast, 1 accurate, 2 plot arc, 3 plot max point
CONFIG.plot_robot           = 1;  %If 1 plot robot icon on odometry, 2 on the slam
CONFIG.plot_robot_type      = 2;  %If 1 Ictineu, 2 Sparus
CONFIG.robot_size           = 1;  %x times the size of the robot icon (5 for the marina size)
CONFIG.plot_gps             = 0;  %If 1 plot gps trajectory
CONFIG.plot_ellipse_slam    = 1;  %If 1 plot slam uncertainty
CONFIG.plot_ellipse_odom    = 1;  %If 1 plot odometry uncertainty
CONFIG.plot_ellipse_sm      = 1;  %If 1 plot scan matching uncertainty
CONFIG.plot_scans           = 4;  %if 1 plot at the end, 2 plot last scan, 3 plot all scan points, 4 replot online
CONFIG.plot_grid            = 1;  %if 1 plot grid connections
CONFIG.display              = 1;  %if 1 display few debug messages, 2 for more, 3 for plot scan#
CONFIG.makeimages           = 0;  %if 1 create images for video
% CONFIG.min_range            = 0;  %minimum range for doing SLAM
CONFIG.max_range            = 0; %maximum range for doing SLAM
CONFIG.state_dist           = 0;  %maximum state vector distance after that, do SLAM
% CONFIG.state_dist_min       = 0;  %minimum previous state poses distance for doing SLAM
% CONFIG.sm_K                 = 1;  %sm uncertainty coinficiency factor
CONFIG.sm_only              = 1;  %compute scan matching trajectory only (CONFIG.state_dist should be 0)

% Debug switches
CONFIG.debug                = 0;  %if 1 store motion vectors, 2 display motion vectors
PARAM.debug_scanforming     = 0;  % 1 plot scan
PARAM.debug                 = 0;
PARAM.plotFrom              = 0;

%% INITIALIZE VARIABLES=========================================================
%SLAM algorithm
tam_xbeam=8; %size of the beams state vector
tam_x=3; %size of the scans state vector
max_rng = []; grid=[]; grid_fnl=[];

%DGPS
stored_gps_n=zeros(1,3020); stored_gps_e=zeros(1,3020); nu_gps=0;
index_gps=zeros(1,218); GPSstore=zeros(3,218);
stored_gps=zeros(3,3020);

%IS
buffer_IS=[]; buffer_pos=zeros(3,1);
% beam_pos=zeros(3,200); beams.angle=zeros(200,1); beams.int=zeros(200,500); 
% beam_cov=zeros(3,3,200); 
% mti_store=zeros(1,200);
beam_pos_index=[]; beam_cov_index=[];
scan_k.data=[]; scans=[];  scan_k.data=[]; scandata=[]; 
scan_data=[];
% scan_data(1,218)=struct('pos',[],'cov',[],'angle',[],'int',[],'ref_point',[],'seg',[],'scan',[]);
scanK1_qcov=[];  scan_cov=[]; traj_odom=[];
nu_scans=0; nu_beam=0; start=0;
IS=zeros(1,PARAM.IS_size(2));
sm_unc=[];

%Plots
xloc=[];
count=0; 

%General
stop=0; device=0;
PStore=zeros(3,218); PStore_mti=zeros(1,218); XErrStore=zeros(3,218); 
assoc=[]; nu_pic=0; frame=1;
error_P=[]; error_Pbeam=[]; error_scans =[];
motion_store=[];
correction_store=[];
metadata=[];
odom_store=[];
StoreTime=zeros(1,43614);
tot_beams=0;

%odometry
X_x=[]; X_y=[];

%mod emili 2011-11-30
deadreckoning = [];
time_emili =[];

%% initialize state vector
if FILE.fid(4)>0 %Exist a MTI measurement
    while device~=4 %searchs for the first MTI measurement.
        [device] = read_dataset;
    end
    time=FILE.MTI_tline(1); %obtains the initial time
    
    %start from specific time
    while time < 1283531341.048
        [device] = read_dataset;
        time=FILE.MTI_tline(1); %obtains the initial time
    end
%     
%     MTI=FILE.MTI_tline(2:end); %obtains the first MTI measurement
%     %obtain measurements from MTI data
%     [z_mti,H,R] = MTI_measurement4D_sparus(MTI);    
% elseif FILE.fid(1)>0 %Exist a DVL measurement
    while device~=1 %searchs for the first DVL measurement.
        [device] = read_dataset;
    end
    time=FILE.DVL_tline(1); %obtains the initial time
    DVL=FILE.DVL_tline(2:end); %obtains the first DVL measurement
    DVL([10 14])=0; %set velocity status to 0
    %obtain measurements from DVL data
    [z,H,R] = DVL_measurement4D_sparus(DVL,[1 1 1 1]);       
    z_mti = z(1); R=0.2.^2;
% else %There is no orientation measurement available
%     z=0'; R=0.5.^2;
%     [device] = read_dataset;
%     if isfield(FILE,'IS_tline')
%         time=FILE.IS_tline(1);
%     elseif isfield(FILE,'DGPS_tline')
%         time=FILE.DGPS_tline(1);
%     end
end
x_beam = zeros(tam_xbeam,1); %initialize the vehicle position at (0,0)
%initialize the correct orientation for the vehicle in world coordinates
x_beam(4)= z_mti; 
P_beam = zeros(1,tam_xbeam);
P_beam(4) = R; %initialize uncertainty for angular measurements
P_beam = diag(P_beam); 
xbeam_odom=x_beam;
Pbeam_odom=P_beam;

% compute sonar's "blind" spots
[PARAM.segmentation.offset.theta, PARAM.segmentation.offset.rho] = compute_offset();

%% INITIALIZE PLOTS=============================================================
if CONFIG.plot_slam
    %Create figure and axes for scan representation
    scrsz = get(0,'ScreenSize');
    %Mirar "Printing from Z-Buffer.
%     PLOT.fig_scan=figure('Position',[(scrsz(3)/3)+60 30 (scrsz(3)/1.5)-63...
%     scrsz(4)-104],'Renderer','zbuffer','doublebuffer','on'); 
    PLOT.fig_scan=figure('Position',scrsz,'Renderer','zbuffer','doublebuffer','on'); 
    set(gca,'XDir','reverse') %Invert X direction so Z look downward
    axis equal;
%     axis([-300 50  -50 200]);
%     axis([-290 40  -50 185]);
%     axis([-25 5  1 24]);
    xlabel('X (m)'); ylabel('Y (m)');
    hold all 
   
    %Defining objects in axes
%     PLOT.scan_sector=plot_IS_v2(zeros(1,PARAM.IS_size(2)), 0, PARAM.IS_range, x([1; 2; 4]), 0);
    if CONFIG.plot_sonar; 
        PLOT.scan_sector = plot_IS_v3(zeros(1,PARAM.IS_size(2)),0,PARAM.IS_range,x_beam,0); 
    end
    %DGPS trajectory in green (Ground Truth)
    PLOT.dgps_GT = plot(NaN,NaN,'g','DisplayName','DGPS ground truth','LineWidth',3); 
    %DGPS trajectory in green (Real)
    PLOT.dgps_RL = plot(NaN,NaN,'r','DisplayName','DGPS Scur','LineWidth',3); 
    %DGPS corresponding points to scans
%     PLOT.dgps_ScnPos = plot(NaN,NaN,'k+','DisplayName','dgps'); 
    %robot slam trajectory in blue
    PLOT.traj_slam = plot(NaN,NaN,'--b','DisplayName','SLAM trajectory','LineWidth',3);   
     %grid connections
    if CONFIG.plot_grid; PLOT.grid = plot(NaN,NaN,'k','DisplayName','Constrains','LineWidth',1); end
    %scan points
    if CONFIG.plot_scans > 1;
        PLOT.handle_scan = plot(NaN,NaN,'Color',[0.2 0.2 0.2],'Marker','.',...
            'LineStyle','none','MarkerSize',0.1,'DisplayName','Scan points'); 
        PLOT.handle_scan1 = plot(NaN,NaN,'.b','DisplayName','Current scan'); 
        PLOT.handle_scan2 = plot(NaN,NaN,'.r','DisplayName','Reference scan'); 
    end 
    %robot slam trajectory in magenta
    if CONFIG.plot_odom;   PLOT.traj_odom = plot(NaN,NaN,'m','DisplayName','odometry'); end  
    %robot   
    if CONFIG.plot_robot;        PLOT.ictineu    = plot_robot_v2(x_beam([1 2 4]),CONFIG.robot_size,CONFIG.plot_robot_type); end 
    %robot uncertainty
    if CONFIG.plot_ellipse_slam; PLOT.slam_unc   = plot(NaN,NaN,'b','DisplayName','SLAM cov (3\sigma)'); end 
    %odometry uncertainty
    if CONFIG.plot_ellipse_odom; PLOT.odom_unc   = plot(NaN,NaN,'m','DisplayName','odom\_unc'); end 
    %scan matching uncertainty
    if CONFIG.plot_ellipse_sm; 
        PLOT.sm_unc_LS       = plot(NaN,NaN,'c','DisplayName','sm\_unc\_LS');
        PLOT.sm_unc_Haralick = plot(NaN,NaN,'r','DisplayName','sm\_unc\_Haralick');
%         PLOT.sm_unc_fix      = plot(NaN,NaN,'g--','DisplayName','sm\_unc\_fix');
    end 
    if CONFIG.debug;  %motion vectors           
        PLOT.motionq     = plot(NaN,NaN,'+k--','DisplayName','motion.q'); 
        PLOT.qpic        = plot(NaN,NaN,'+r--','DisplayName','q\_pic'); 
        PLOT.res         = plot(NaN,NaN,'+g--','DisplayName','displacement'); 
        PLOT.motionq_unc = plot(NaN,NaN,'k','DisplayName','motion.q unc'); 
        PLOT.qpic_unc    = plot(NaN,NaN,'r','DisplayName','q\_pic unc'); 
        PLOT.res_unc     = plot(NaN,NaN,'g','DisplayName','displacement unc'); 
    end     
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% MAIN=========================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic;
last = 0;
while stop==0
    
    [device] = read_dataset;
    
    
    switch device
        case 1 %DVL measurement available=======================================
            At=(FILE.DVL_tline(1)-time); %time interval
            time=FILE.DVL_tline(1); %store time for next iteration
            DVL=FILE.DVL_tline(2:end); %store sensor raw data
            %if there is no MTI available obtain measurements from DVL data
            if FILE.fid(4)<0 
                [z,H,R] = DVL_measurement4D_sparus(DVL);         
            else
                [z,H,R] = DVL_measurement4D_sparus(DVL,[1 0 1 1]);         
            end
            z(2)=z(2)-(z_mti*0.375);
          
            %make local state prediction and update
            [x_beam,P_beam]=KalmanFilter4D_newstate8(x_beam,P_beam,At,z,H,R);    
            [xbeam_odom,Pbeam_odom]=KalmanFilter4D_newstate8(xbeam_odom,Pbeam_odom,At,z,H,R);    
            
        case 2 %IS measurement available========================================
            %% Gathering beams
            At = (FILE.IS_tline(1)-time); %time interval
            time = FILE.IS_tline(1); %store time for next iteration
            IS = FILE.IS_tline(2:end); %store sensor raw data
%             IS(3:10) = 0; %erase first measurements to avoid sonar head ringing
%             IS(100:end) = 0; %erase first measurements to avoid sonar head ringing
            
            %make local state prediction 
            [x_beam,P_beam] = KalmanFilter4D_newstate8(x_beam,P_beam,At);
            [xbeam_odom,Pbeam_odom]=KalmanFilter4D_newstate8(xbeam_odom,Pbeam_odom,At);    
            
            deadreckoning = [deadreckoning; [time xbeam_odom(1) xbeam_odom(2) xbeam_odom(4)]];
            
            %augment the state vector
            [x_beam,P_beam] = Cloning(x_beam,P_beam,tam_xbeam);   
            
            %Store the beams data until complete a full scan
            nu_beam                = nu_beam + 1;
            beams.pos(:,nu_beam)   = x_beam([1 2 4]);             
            beams.cov(:,:,nu_beam) = P_beam([1 2 4], [1 2 4]);
            beams.angle(nu_beam,1) = IS(2);
            beams.int(nu_beam,:)   = IS(3:end);
            
            %store ground truth data
            tot_beams              = tot_beams +1;
%             beams.GT_mti(nu_beam)  = GT.is.mti(tot_beams);
%             beams.GT_gpsN(nu_beam) = GT.is.gpsN(tot_beams);
%             beams.GT_gpsE(nu_beam) = GT.is.gpsE(tot_beams);
%             beams.GT_time(nu_beam) = GT.is.time(tot_beams);
            
            %once to check sync
%             StoreTime(tot_beams) = time - GT.is.time(tot_beams);
%             mti_store(nu_beam)     = z_mti; 
            
            %Reset varables for starting new scan
            x_beam(1:3)     = 0;
            P_beam(1:3,1:3) = 0;
            
            %reset the state vector
            x_beam(9:end) = [];
            P_beam        = P_beam(1:8,1:8);
           
            %% Full Scan
            if IS(2) == 0 %if a full scan complete do SLAM
%             if nu_beam == 200 %if a full scan complete do SLAM
                %counters for the full scans
                nu_scans = nu_scans+1; 
                NUM_SM = nu_scans;
                if CONFIG.display > 0, disp(['Scan # ' num2str(nu_scans)]); end
                if nu_scans == 22
                    lala=0;
                end
                time_emili = [time_emili time];
                %find the middle point to reference the scan
                beams.ref_point = floor(nu_beam/2);
%                 beams.ref_point = 1;

%===============================================================================                
                % build the scan
                [beams.seg] = Segmentation_v3(beams);
                [beams.scan] = ScanForming_slam_v6(beams);

%                 % test with old scanforming
%                 robotPosition.est = beams.pos;
%                 robotPosition.cov = beams.cov;
%                 q.est = beams.pos(:,1);
%                 q.cov = beams.cov(:,:,1);
%                 [q,scan] = ScanForming_old(beams.seg, robotPosition, q); 
%                 beams.scan = scan;
              
                %store scans data
%                 scan_data(nu_scans) = beams;
                scan_data = [scan_data beams];
                
                %Store the gps indexes 
                index_gps(nu_scans+1) = nu_gps;   %at the next scan
                gps_GT_k1 = index_gps(nu_scans);   %at the current scan
                
                %if this is not the first scan do SLAM
                if start == 1                     
                    %find the full odometry
                    odom_k = odom_baseframe_v1(scan_data(nu_scans-1),scan_data(nu_scans));
 
                    %augment the state vector
                    [x,P] = Cloning(x,P,tam_x);
                     
                    %predict the next global position
                    [x,P] = Prediction_ScanMatching_v1(x,P,odom_k); 
                    
                    %find candidates for loop closing
                    [loop_inx] = findloops_v4(x);

                    %initialize current variables
                    N    = size(x,1);
                    curX = x(1:3); %current position

                    %do Scan matching and SLAM
                    for i = 1:size(loop_inx,2)
                        %test scan number
                        j = loop_inx(i);
                        
                        %gps index at the test scan
                        gps_GT_tst = index_gps(j); 
                        
                        % initialize H matrix
                        H = zeros(3,N);
                        
                        %find test scan position
                        elm_x = N -(3*j-1); elm_y = N -(3*j-2); elm_th = N -(3*j-3);
                        testX = [x(elm_x); x(elm_y); x(elm_th)];
                        
                        %Predict displacement
                        testXinv = invert(testX); %do the inversion transformation
                        res = compound(testXinv.x,curX); %do the compounding
                        motion.q = res.x;

                        %Build H matrix
                        H(1:3,1:3) = res.Jb;
                        H(1:3,elm_x:elm_th) = res.Ja * testXinv.oJ;
                        
                        %Predicted covariance
                        motion.Pq = H*P*H';
                        
                        %Prepare the scans
                        Sref = scan_data(j).scan;
                        Scur = scan_data(end).scan;
                        
%                         %Predict displacement (consecutive)
%                         res1 = odom_localframe_v1(scan_data(j),scan_data(end));
%                         odom.q = res1.pos;
%                         odom.Pq = res1.cov;
                        
                        %remove the NaNs from the vectors
                        indexDelete = isnan(Sref.cart(1,:));
                        Sref.cart(:,indexDelete) = [];
                        Sref.P(:,:,indexDelete) = [];

                        indexDelete = isnan(Scur.cart(1,:));
                        Scur.cart(:,indexDelete) = [];
                        Scur.P(:,:,indexDelete) = [];
                       
                        %pIC Scan Matching
                        if CONFIG.sm_only == 1 && nu_scans ~= 23 && nu_scans ~= 24
                            [o,q_corrected,num_it,assoc] = pIC(Sref, Scur, motion);
                            %                         [o,q_corrected,num_it,assoc] = pIC(Sref, Scur, odom);
                            q_corrected(3) = normAngle(q_corrected(3));
                        
                        
                        %check if there are enough associations
                        pIC_test = min(size(Sref.cart,2),size(Scur.cart,2)) * 0.8;
%                         if (size(assoc.n,2) < pIC_test) || (o < 1)
%                             q_corrected = motion.q;
%                         end
                        if  (o < 1)
                            pause;
                        end
                            
                            % replot scan points on-line
                            if (CONFIG.plot_scans == 4) && (CONFIG.plot_slam == 1);
                                scans2 = Composition(testX(:,1),Sref.cart);
                                set(PLOT.handle_scan2,'XData',scans2(1,:),'YData',scans2(2,:));
                                drawnow
                            end
                        
%                             R = R_sm; sm_unc = 'fixed';
%                             R = PARAM.sm.unc_LS;sm_unc = 'LS';
%                             R = motion.Pq; sm_unc = 'odometry';
%                             R = CONFIG.sm_K*(Jb*PARAM.sm.unc_LS*Jb'); 
                            R = PARAM.sm.unc_Haralick; sm_unc = 'Haralick';
                        
                        %and update only then
                        if (size(assoc.n,2) > pIC_test) && (o == 1) && (CONFIG.state_dist > 0)
                            z = q_corrected;
                            z(3) = normAngle(z(3));
                            
                            %Do EKF update
                            v = z - motion.q;
                            v(3) = normAngle(v(3));
                            
                            S = motion.Pq + R;
                            
%                             K = (P*H')*inv(S);
                            K = (P*H')/(S);
                            x = x + K * v;
%                             P =(eye(length(x))-K*H)*P;
                            %Joseph Form
                            P=(eye(length(x))-K*H)*P*(eye(length(x))-K*H)' + K*R*K'; 
                            x(3) = normAngle(x(3));
                            
                            %store grid positions
                            grid = [grid; N-1 N-elm_x];
%                             Pq_gps_store(:,nu_scans) = [beams.GT_gpsN(beams.ref_point);
%                                                         beams.GT_gpsE(beams.ref_point);
%                                                         beams.GT_mti(beams.ref_point)];
                            Pq_Har_store(:,nu_scans) = sqrt(diag(PARAM.sm.unc_Haralick(1:3,1:3)));
                            Pq_LS_store(:,nu_scans) = sqrt(diag(PARAM.sm.unc_LS(1:3,1:3)));
                            
%                             % replot scan points on-line
%                             if (CONFIG.plot_scans == 4) && (CONFIG.plot_slam == 1);
%                                 scans2 = Composition(testX(:,1),Sref.cart);
%                                 set(PLOT.handle_scan2,'XData',scans2(1,:),'YData',scans2(2,:));
%                                 drawnow
%                             end
                            % plot grid of constrains
                            if (CONFIG.plot_grid == 1) && (~isempty(grid)) && (CONFIG.plot_slam == 1)
                                grid_tmp = size(x,1)-grid;
                                for i=1:size(grid_tmp,1)
                                    grid_fnl = [grid_fnl [x(grid_tmp(i,1));x(grid_tmp(i,1)+1)] [x(grid_tmp(i,2));x(grid_tmp(i,2)+1)] [NaN;NaN]];
                                end
                                set(PLOT.grid,'XData',grid_fnl(1,:),'YData',grid_fnl(2,:));
                                grid_fnl = [];
                                drawnow
                            end
                            % make images for video
                            if (CONFIG.makeimages == 1) && (CONFIG.plot_slam == 1)
                                h = findobj(gca,'-regexp','DisplayName','[^'']');
                                h(7)=[];
                                legend(h,'Location','SouthEast');
                                drawnow
                                saveas(gcf, ['path', num2str(frame),'.png']);
                                frame = frame + 1;
                            end
                        end
                        
                        % compute scan matching trajectory, but not SLAM
                        if (size(assoc.n,2) > pIC_test) && (o == 1) && ...
                                (CONFIG.sm_only == 1) && (CONFIG.state_dist == 0)
                            sm = compound(x(4:6),q_corrected);
                            x(1:3) = sm.x;
                        end
                        end
                    end
                end

                %If is the first scan just initialize state vector.
                if start == 0 
                    %find the first part of the odometry
                    odom_k = odom_baseframe_v1(beams);
                    % initialize state vector
                    x = odom_k.pos;
                    P = odom_k.cov;
                    %Store gps index
                    index_gps(1) = 1;
                    start        = 1;
                end

                %% Storage
                PStore(:,nu_scans)    = sqrt(diag(P(1:3,1:3)));
                PStore_mti(nu_scans)    = PStore(3,nu_scans) + sqrt(P(3,3));
%                 XErrStore(:,nu_scans) = [x(1) - beams.GT_gpsN(beams.ref_point); 
%                                          x(2) - beams.GT_gpsE(beams.ref_point); 
%                                          normAngle(x(3) - beams.GT_mti(beams.ref_point))];
%                 GPSstore(:,nu_scans) = [beams.GT_gpsN(beams.ref_point); 
%                                         beams.GT_gpsE(beams.ref_point);
%                                         beams.GT_mti(beams.ref_point)];

                if start && CONFIG.debug
                    motion_store = [motion_store motion.q];
                    correction_store = [correction_store q_corrected];
                    odom_store = [odom_store res1.pos];
                    meta.er = o;
                    meta.num_it = num_it;
                    meta.assoc = assoc;
                    metadata = [metadata meta];
                    
                    %plot motion vectors
                    if CONFIG.debug == 2
                        test=compound(testX,q_corrected);
                        test1=compound(testX,motion.q);
                        test2=compound(testX,res1.pos);
                        a=[testX test.x];
                        b=[testX test1.x];
                        c=[testX test2.x];
                        set(PLOT.qpic,'XData',a(1,:),'YData',a(2,:));
                        set(PLOT.motionq,'XData',b(1,:),'YData',b(2,:));
                        set(PLOT.res,'XData',c(1,:),'YData',c(2,:));
                        plot(GPSstore(1,nu_scans),GPSstore(2,nu_scans),'co')
                        draw_ellipse_v2(test.x', PARAM.sm.unc_Haralick(1:2,1:2),'r',PLOT.qpic_unc);
                        draw_ellipse_v2(test1.x', motion.Pq(1:2,1:2),'g',PLOT.motionq_unc);
                        draw_ellipse_v2(test2.x', res1.cov(1:2,1:2),'k',PLOT.res_unc);
                    end
                end
%===============================================================================
                %Reset varables for starting new scan
                P_beam_storage = P_beam;
                nu_beam        = 0;
                beams.pos      = zeros(3,200); 
                beams.angle    = zeros(200,1); 
                beams.int      = zeros(200,PARAM.IS_size(2)); 
                beams.cov      = zeros(3,3,200);
%                 mti_store      = zeros(1,200);
%                 x_beam          = x_beam(1:8); 
%                 x_beam(1:3)     = 0;
%                 P_beam          = P_beam(1:8,1:8); 
%                 P_beam(1:3,1:3) = 0;
%                 start           = 1;
%                 odom_k_1        = odom_k;

                
            end

        case 3 %DGPS measurement available======================================
            DGPS=FILE.DGPS_tline(2:end); %store sensor raw data
            %convert degrees_minutes.fraction_minute to degrees.fraction_degree
            lat=floor(DGPS(:,1)/100)+((DGPS(:,1)/100)-floor(DGPS(:,1)/100))*100/60; 
            lon=floor(DGPS(:,2)/100)+((DGPS(:,2)/100)-floor(DGPS(:,2)/100))*100/60;
            [gps_e,gps_n] = deg2utm(lat,lon); %transform degrees to utm
            if nu_gps == 0 %if this is the first DGPS measurement store values
                %to make it coincident with the current vehicle position
                rest_gps_n=gps_n-x_beam(1); 
                rest_gps_e=gps_e-x_beam(2);
                %store for google overlay
                GPSraw = DGPS;
                GPSlat = lat;
                GPSlot = lon;
            end
            %store DGPS data
            nu_gps = nu_gps + 1;
            stored_gps_n(nu_gps) = gps_n-rest_gps_n; 
            stored_gps_e(nu_gps) = gps_e-rest_gps_e;
            stored_gps(1:2,:) = [stored_gps_n; stored_gps_e];
            
        case 4 %MTI measurement available=======================================
            At=(FILE.MTI_tline(1)-time); %time interval
            time=FILE.MTI_tline(1); %store time for next iteration
            MTI=FILE.MTI_tline(2:end); %store sensor raw data
            [ z_mti,H,R ] = MTI_measurement4D_sparus(MTI,[1 0]);
            
            %make local state prediction
            [x_beam,P_beam]=KalmanFilter4D_newstate8(x_beam,P_beam,At,z_mti,H,R);  
            [xbeam_odom,Pbeam_odom]=KalmanFilter4D_newstate8(xbeam_odom,Pbeam_odom,At,z_mti,H,R);    
            
    end %switch device
    
    %store trajectoty for pure odometry
    X_x=[X_x xbeam_odom(1)]; 
    X_y=[X_y xbeam_odom(2)];


    %% Check for end of file
    if feof(FILE.fid(device))==1; 
        fclose(FILE.fid(device)); %close file
        stop=1; %stop execution
    end
    
    %% --------- Plots at every step -------------------------------------------
    %Dispaly debug messages
    if CONFIG.display == 2
        clc; disp('State vector 1 size:');disp(size(x_beam,1));
        disp(['Scan # ' num2str(nu_scans)]);
        disp('State vector 2 size:'); disp(size(x,1));
    end
    
    if (CONFIG.plot_slam == 1)
        
        %Plot vehicle odometry
        if CONFIG.plot_odom
            set(PLOT.traj_odom,'XData',X_x,'YData',X_y) %plot the whole trajectory
            %plot the current vehicle position
            if CONFIG.plot_robot == 1;
                plot_robot_v2(xbeam_odom([1,2,4]),CONFIG.robot_size,CONFIG.plot_robot_type,PLOT.ictineu);
            end
        end
        
        %Plot odometry uncertainty
        if CONFIG.plot_ellipse_odom
            draw_ellipse_v2([xbeam_odom(1) xbeam_odom(2)], Pbeam_odom(1:2,1:2), 'b',PLOT.odom_unc)
        end
        
        if (device==2)
            %Represent sonar beams over the trajectory
            if CONFIG.plot_sonar
                Fmax = (floor(PARAM.segmentation.maxRange/PARAM.IS_range_res));
%                 Fmin = round(PARAM.segmentation.initRange/PARAM.IS_range_res);
                IS_filtered = IS;
                IS_filtered(Fmax+1:end) = 0;
%                 IS_filtered(3:Fmin) = 0;
                
                buffer_IS=[IS_filtered; buffer_IS]; %store beams
                buffer_pos=[xbeam_odom([1 2 4])  buffer_pos]; %store beam's position
                %number of beams to plot
                if CONFIG.plot_nu_beams == 1000
                    buffer_size = size(buffer_IS,2)-2; 
                else
                    buffer_size = CONFIG.plot_nu_beams;
                end
                if size(buffer_pos,2)>buffer_size   %size of the buffer (number of beams)
                    buffer_IS(buffer_size+1:end,:)=[];
                    buffer_pos(:,buffer_size+1:end)=[];
                end
                count=count+1;
                if count==buffer_size;  %Period of representation (number of beams)
                    plot_IS_v3(buffer_IS(:,3:end), buffer_IS(:,2), ...
                        PARAM.segmentation.maxRange, buffer_pos([1 2 3],:), CONFIG.plot_IS_mode, PLOT.scan_sector)
                    count=0;
                end
                drawnow
            end
        end
    end
    
    %% --------- Plots after full scan ----------------------------------------
    %Plot online vehicle position and uncertainty
    if (CONFIG.plot_slam == 1) && (IS(2)==0) && (device==2) % && (~isempty(assoc))
%     if (CONFIG.plot_slam == 1) && (nu_beam==0) && (device==2) % && (~isempty(assoc))
        
        %plot the DGPS trajectory
        if CONFIG.plot_gps
            set(PLOT.dgps_GT,'XData',stored_gps_n(1:gps_GT_k1),'YData',stored_gps_e(1:gps_GT_k1)); %plot the ground truth
            set(PLOT.dgps_RL,'XData',stored_gps_n(gps_GT_k1:nu_gps),'YData',stored_gps_e(gps_GT_k1:nu_gps)); %plot the total path
            gps_scn = index_gps(1:nu_scans); %find the scan positions
        end
        
        %plot the scan positions
        if CONFIG.display == 3,
%             text(z(1),z(2),num2str(i));
            set(PLOT.dgps_ScnPos,'XData',stored_gps_n(gps_scn),'YData',stored_gps_e(gps_scn));
%             text(stored_gps_n(gps_scn),stored_gps_e(gps_scn),num2str(nu_scans));
        end
        
        %plot the slam trajectory
        sxloc=reshape(x,3,size(x,1)/3);
        set(PLOT.traj_slam,'XData',sxloc(1,:),'YData',sxloc(2,:));
        
        %plot scan matching uncertainty
        if CONFIG.plot_ellipse_sm && (~isempty(assoc)); 
%             draw_ellipse_v2([x(1) x(2)], R_sm(1:2,1:2), 'g',PLOT.sm_unc_fix);
%             draw_ellipse_v2([x(1) x(2)], PARAM.sm.unc_LS(1:2,1:2)*CONFIG.sm_K, 'c',PLOT.sm_unc_LS);
            draw_ellipse_v2([x(1) x(2)], PARAM.sm.unc_LS(1:2,1:2), 'c',PLOT.sm_unc_LS);
            draw_ellipse_v2([x(1) x(2)], PARAM.sm.unc_Haralick(1:2,1:2),'r',PLOT.sm_unc_Haralick);
        end
        
        %plot slam uncertainty
        if CONFIG.plot_ellipse_slam; draw_ellipse_v2([x(1) x(2)], P(1:2,1:2), 'k',PLOT.slam_unc); end 

        %plot the current vehicle position
        if CONFIG.plot_robot == 2; plot_robot_v2(x(1:3),CONFIG.robot_size,CONFIG.plot_robot_type,PLOT.ictineu); end
        
        %plot grid of constrains
        if (CONFIG.plot_grid == 1) && (~isempty(grid))
            grid_tmp = size(x,1)-grid;
            for i=1:size(grid_tmp,1)
                grid_fnl = [grid_fnl [x(grid_tmp(i,1));x(grid_tmp(i,1)+1)] [x(grid_tmp(i,2));x(grid_tmp(i,2)+1)] [NaN;NaN]];
            end
            set(PLOT.grid,'XData',grid_fnl(1,:),'YData',grid_fnl(2,:));
            grid_fnl = [];
        end
        
%         %Plot vehicle odometry
%         if CONFIG.plot_odom 
%             set(PLOT.traj_odom,'XData',X_x,'YData',X_y) %plot the whole trajectory
%         end
%         %Plot odometry uncertainty
%         if CONFIG.plot_ellipse_odom
%             draw_ellipse_v2([xbeam_odom(1) xbeam_odom(2)], Pbeam_odom(1:2,1:2), 'b',PLOT.odom_unc)
%         end
        
        
        if (CONFIG.plot_scans  == 2) || (CONFIG.plot_scans  == 3) % && (~isempty(scan_k.data))
            if CONFIG.plot_scans == 3; scans = [scans Composition(x(:,1),scan_data(nu_scans).scan.cart)]; end
            if CONFIG.plot_scans == 2; scans = Composition(x(:,1),scan_data(nu_scans).scan.cart); end
            set(PLOT.handle_scan,'XData',scans(1,:),'YData',scans(2,:));
        end
        
        %         scans = [scans Composition(x(:,1),scan_k.data.cart)];
        %         set(handle_scan,'XData',scans(1,:),'YData',scans(2,:));
        
        if (CONFIG.plot_scans == 4)
            meg = size(x,1);
            scans = zeros(2,meg/3);
            for i=1:size(scan_data,2)
                scan_i = scan_data(i).scan;
                elm_x = meg-(3*i-1); elm_y = meg-(3*i-2); elm_th = meg-(3*i-3);
                z = [x(elm_x); x(elm_y); x(elm_th)];
                scans = [scans Composition(z,scan_i.cart)];
                if CONFIG.display == 3,
                    text(z(1),z(2),num2str(i));
                    text(GPSstore(i,1),GPSstore(i,2),num2str(i),'color','green');
                end
            end
            scans1 = Composition(x(:,1),scan_data(nu_scans).scan.cart);

            %     plot(scans(1,:),scans(2,:),'black.','MarkerSize',2,'DisplayName','scans');
            set(PLOT.handle_scan,'XData',scans(1,:),'YData',scans(2,:));
            set(PLOT.handle_scan1,'XData',scans1(1,:),'YData',scans1(2,:));
        end

% 
%     if CONFIG.plot_sonar %Represent sonar beams over the trajectory
%         buffer_IS=[IS; buffer_IS]; %store beams
%         buffer_pos=[x_beam([1 2 4]) + [x(1:2,1); 0] buffer_pos]; %store beam's position
%         buffer_size=1; %number of beams to plot
%         if size(buffer_pos,2)>buffer_size   %size of the buffer (number of beams)
%             buffer_IS(buffer_size+1:end,:)=[];
%             buffer_pos(:,buffer_size+1:end)=[];
%         end
%         
%         count=count+1;
%         if count==1;  %Period of representation (number of beams)
%             plot_IS_v2(buffer_IS(:,3:end), buffer_IS(:,2), 50, buffer_pos([1 2 3],:), 1, PLOT.scan_sector)
%             count=0;
%         end
%         drawnow
%     end

%         h = findobj(gca,'Type','line');
%         legend(h)
        drawnow;
%         saveas(gcf, ['path', num2str(frame),'.png']);
%         frame = frame + 1;
    end
    
end %while stop==0

%% Xapussa per fer scan matching amb l'ultim scan

%             if nu_beam == 200 %if a full scan complete do SLAM
                %counters for the full scans
                nu_scans = nu_scans+1; 
                NUM_SM = nu_scans;
                if CONFIG.display > 0, disp(['Scan # ' num2str(nu_scans)]); end
                if nu_scans == 22
                    lala=0;
                end
                time_emili = [time_emili time];
                %find the middle point to reference the scan
                beams.ref_point = floor(nu_beam/2);
%                 beams.ref_point = 1;

%===============================================================================                
                % build the scan
                [beams.seg] = Segmentation_v3(beams);
                [beams.scan] = ScanForming_slam_v6(beams);

%                 % test with old scanforming
%                 robotPosition.est = beams.pos;
%                 robotPosition.cov = beams.cov;
%                 q.est = beams.pos(:,1);
%                 q.cov = beams.cov(:,:,1);
%                 [q,scan] = ScanForming_old(beams.seg, robotPosition, q); 
%                 beams.scan = scan;
              
                %store scans data
%                 scan_data(nu_scans) = beams;
                scan_data = [scan_data beams];
                
                %Store the gps indexes 
                index_gps(nu_scans+1) = nu_gps;   %at the next scan
                gps_GT_k1 = index_gps(nu_scans);   %at the current scan
                
                %if this is not the first scan do SLAM
                if start == 1                     
                    %find the full odometry
                    odom_k = odom_baseframe_v1(scan_data(nu_scans-1),scan_data(nu_scans));
 
                    %augment the state vector
                    [x,P] = Cloning(x,P,tam_x);
                     
                    %predict the next global position
                    [x,P] = Prediction_ScanMatching_v1(x,P,odom_k); 
                    
                    %find candidates for loop closing
                    [loop_inx] = findloops_v4(x);

                    %initialize current variables
                    N    = size(x,1);
                    curX = x(1:3); %current position

                    %do Scan matching and SLAM
                    for i = 1:size(loop_inx,2)
                        %test scan number
                        j = loop_inx(i);
                        
                        %gps index at the test scan
                        gps_GT_tst = index_gps(j); 
                        
                        % initialize H matrix
                        H = zeros(3,N);
                        
                        %find test scan position
                        elm_x = N -(3*j-1); elm_y = N -(3*j-2); elm_th = N -(3*j-3);
                        testX = [x(elm_x); x(elm_y); x(elm_th)];
                        
                        %Predict displacement
                        testXinv = invert(testX); %do the inversion transformation
                        res = compound(testXinv.x,curX); %do the compounding
                        motion.q = res.x;

                        %Build H matrix
                        H(1:3,1:3) = res.Jb;
                        H(1:3,elm_x:elm_th) = res.Ja * testXinv.oJ;
                        
                        %Predicted covariance
                        motion.Pq = H*P*H';
                        
                        %Prepare the scans
                        Sref = scan_data(j).scan;
                        Scur = scan_data(end).scan;
                        
%                         %Predict displacement (consecutive)
%                         res1 = odom_localframe_v1(scan_data(j),scan_data(end));
%                         odom.q = res1.pos;
%                         odom.Pq = res1.cov;
                        
                        %remove the NaNs from the vectors
                        indexDelete = isnan(Sref.cart(1,:));
                        Sref.cart(:,indexDelete) = [];
                        Sref.P(:,:,indexDelete) = [];

                        indexDelete = isnan(Scur.cart(1,:));
                        Scur.cart(:,indexDelete) = [];
                        Scur.P(:,:,indexDelete) = [];
                       
                        %pIC Scan Matching
                        if CONFIG.sm_only == 1 && nu_scans ~= 23 && nu_scans ~= 24
                            [o,q_corrected,num_it,assoc] = pIC(Sref, Scur, motion);
                            %                         [o,q_corrected,num_it,assoc] = pIC(Sref, Scur, odom);
                            q_corrected(3) = normAngle(q_corrected(3));
                        
                        
                        %check if there are enough associations
                        pIC_test = min(size(Sref.cart,2),size(Scur.cart,2)) * 0.8;
%                         if (size(assoc.n,2) < pIC_test) || (o < 1)
%                             q_corrected = motion.q;
%                         end
                        if  (o < 1)
                            pause;
                        end
                            
                            % replot scan points on-line
                            if (CONFIG.plot_scans == 4) && (CONFIG.plot_slam == 1);
                                scans2 = Composition(testX(:,1),Sref.cart);
                                set(PLOT.handle_scan2,'XData',scans2(1,:),'YData',scans2(2,:));
                                drawnow
                            end
                        
%                             R = R_sm; sm_unc = 'fixed';
%                             R = PARAM.sm.unc_LS;sm_unc = 'LS';
%                             R = motion.Pq; sm_unc = 'odometry';
%                             R = CONFIG.sm_K*(Jb*PARAM.sm.unc_LS*Jb'); 
                            R = PARAM.sm.unc_Haralick; sm_unc = 'Haralick';
                        
                        %and update only then
                        if (size(assoc.n,2) > pIC_test) && (o == 1) && (CONFIG.state_dist > 0)
                            z = q_corrected;
                            z(3) = normAngle(z(3));
                            
                            %Do EKF update
                            v = z - motion.q;
                            v(3) = normAngle(v(3));
                            
                            S = motion.Pq + R;
                            
%                             K = (P*H')*inv(S);
                            K = (P*H')/(S);
                            x = x + K * v;
%                             P =(eye(length(x))-K*H)*P;
                            %Joseph Form
                            P=(eye(length(x))-K*H)*P*(eye(length(x))-K*H)' + K*R*K'; 
                            x(3) = normAngle(x(3));
                            
                            %store grid positions
                            grid = [grid; N-1 N-elm_x];
%                             Pq_gps_store(:,nu_scans) = [beams.GT_gpsN(beams.ref_point);
%                                                         beams.GT_gpsE(beams.ref_point);
%                                                         beams.GT_mti(beams.ref_point)];
                            Pq_Har_store(:,nu_scans) = sqrt(diag(PARAM.sm.unc_Haralick(1:3,1:3)));
                            Pq_LS_store(:,nu_scans) = sqrt(diag(PARAM.sm.unc_LS(1:3,1:3)));
                            
%                             % replot scan points on-line
%                             if (CONFIG.plot_scans == 4) && (CONFIG.plot_slam == 1);
%                                 scans2 = Composition(testX(:,1),Sref.cart);
%                                 set(PLOT.handle_scan2,'XData',scans2(1,:),'YData',scans2(2,:));
%                                 drawnow
%                             end
                            % plot grid of constrains
                            if (CONFIG.plot_grid == 1) && (~isempty(grid)) && (CONFIG.plot_slam == 1)
                                grid_tmp = size(x,1)-grid;
                                for i=1:size(grid_tmp,1)
                                    grid_fnl = [grid_fnl [x(grid_tmp(i,1));x(grid_tmp(i,1)+1)] [x(grid_tmp(i,2));x(grid_tmp(i,2)+1)] [NaN;NaN]];
                                end
                                set(PLOT.grid,'XData',grid_fnl(1,:),'YData',grid_fnl(2,:));
                                grid_fnl = [];
                                drawnow
                            end
                            % make images for video
                            if (CONFIG.makeimages == 1) && (CONFIG.plot_slam == 1)
                                h = findobj(gca,'-regexp','DisplayName','[^'']');
                                h(7)=[];
                                legend(h,'Location','SouthEast');
                                drawnow
                                saveas(gcf, ['path', num2str(frame),'.png']);
                                frame = frame + 1;
                            end
                        end
                        
                        % compute scan matching trajectory, but not SLAM
                        if (size(assoc.n,2) > pIC_test) && (o == 1) && ...
                                (CONFIG.sm_only == 1) && (CONFIG.state_dist == 0)
                            sm = compound(x(4:6),q_corrected);
                            x(1:3) = sm.x;
                        end
                        end
                    end
                end

                %If is the first scan just initialize state vector.
                if start == 0 
                    %find the first part of the odometry
                    odom_k = odom_baseframe_v1(beams);
                    % initialize state vector
                    x = odom_k.pos;
                    P = odom_k.cov;
                    %Store gps index
                    index_gps(1) = 1;
                    start        = 1;
                end

                %% Storage
                PStore(:,nu_scans)    = sqrt(diag(P(1:3,1:3)));
                PStore_mti(nu_scans)    = PStore(3,nu_scans) + sqrt(P(3,3));
%                 XErrStore(:,nu_scans) = [x(1) - beams.GT_gpsN(beams.ref_point); 
%                                          x(2) - beams.GT_gpsE(beams.ref_point); 
%                                          normAngle(x(3) - beams.GT_mti(beams.ref_point))];
%                 GPSstore(:,nu_scans) = [beams.GT_gpsN(beams.ref_point); 
%                                         beams.GT_gpsE(beams.ref_point);
%                                         beams.GT_mti(beams.ref_point)];

                if start && CONFIG.debug
                    motion_store = [motion_store motion.q];
                    correction_store = [correction_store q_corrected];
                    odom_store = [odom_store res1.pos];
                    meta.er = o;
                    meta.num_it = num_it;
                    meta.assoc = assoc;
                    metadata = [metadata meta];
                    
                    %plot motion vectors
                    if CONFIG.debug == 2
                        test=compound(testX,q_corrected);
                        test1=compound(testX,motion.q);
                        test2=compound(testX,res1.pos);
                        a=[testX test.x];
                        b=[testX test1.x];
                        c=[testX test2.x];
                        set(PLOT.qpic,'XData',a(1,:),'YData',a(2,:));
                        set(PLOT.motionq,'XData',b(1,:),'YData',b(2,:));
                        set(PLOT.res,'XData',c(1,:),'YData',c(2,:));
                        plot(GPSstore(1,nu_scans),GPSstore(2,nu_scans),'co')
                        draw_ellipse_v2(test.x', PARAM.sm.unc_Haralick(1:2,1:2),'r',PLOT.qpic_unc);
                        draw_ellipse_v2(test1.x', motion.Pq(1:2,1:2),'g',PLOT.motionq_unc);
                        draw_ellipse_v2(test2.x', res1.cov(1:2,1:2),'k',PLOT.res_unc);
                    end
                end
%===============================================================================
                %Reset varables for starting new scan
                P_beam_storage = P_beam;
                nu_beam        = 0;
                beams.pos      = zeros(3,200); 
                beams.angle    = zeros(200,1); 
                beams.int      = zeros(200,PARAM.IS_size(2)); 
                beams.cov      = zeros(3,3,200);
%                 mti_store      = zeros(1,200);
%                 x_beam          = x_beam(1:8); 
%                 x_beam(1:3)     = 0;
%                 P_beam          = P_beam(1:8,1:8); 
%                 P_beam(1:3,1:3) = 0;
%                 start           = 1;
%                 odom_k_1        = odom_k;

                







%% PLOTS ext %%
    %%%%%%%%%%%%%%%

    %plot slam off line
if CONFIG.plot_slam == 2 %Plot vehicle position and uncertainty
    sxloc=reshape(x,3,size(x,1)/3);
    set(PLOT.traj_slam,'XData',sxloc(1,:),'YData',sxloc(2,:));
    set(PLOT.dgps_GT,'XData',stored_gps_n,'YData',stored_gps_e);
%     set(PLOT.dgps_store,'XData',GPSstore(:,1),'YData',GPSstore(:,2));
    
end

%plot scans
if (CONFIG.plot_scans == 1) || ((CONFIG.plot_scans) && (CONFIG.plot_slam == 2))
    meg = size(x,1);
    % scans = zeros(2,meg/3);
    for i=1:size(scan_data,2)
        scan_i = scan_data(i).scan;
        elm_x = meg-(3*i-1); elm_y = meg-(3*i-2); elm_th = meg-(3*i-3);
        z = [x(elm_x); x(elm_y); x(elm_th)];
        %         R = P(elm_x:elm_th,elm_x:elm_th);
        scans = [scans Composition(z,scan_i.cart)];
        %         scans(i) = Composition(z,scan_i.cart); %not working, need cells
        if CONFIG.display == 3,
            text(z(1),z(2),num2str(i));
            text(GPSstore(i,1),GPSstore(i,2),num2str(i),'color','green');
        end
    end
    plot(scans(1,:),scans(2,:),'black.','MarkerSize',2,'DisplayName','scans');

    %plot slam uncertainty
    if CONFIG.plot_ellipse_slam; 
        draw_ellipse_v2([x(1) x(2)], P(1:2,1:2), 'k',PLOT.slam_unc); 
    end 
    
    %plot the grid
    if (CONFIG.plot_grid == 1) && (~isempty(grid))
        grid_tmp = size(x,1)-grid;
        for i=1:size(grid_tmp,1)
            grid_fnl = [grid_fnl [x(grid_tmp(i,1)); x(grid_tmp(i,1)+1)]...
                       [x(grid_tmp(i,2)); x(grid_tmp(i,2)+1)]...
                       [NaN;NaN]];
        end
        set(PLOT.grid,'XData',grid_fnl(1,:),'YData',grid_fnl(2,:));
%         grid_fnl = [];
    end
end


if PARAM.sm.estep_method == 0, methode = 'N.N.'; end
if PARAM.sm.estep_method == 1, methode = 'V.P.'; end
if CONFIG.plot_slam == 1, ploting = 'online'; end
if CONFIG.plot_slam == 2, ploting = 'offline'; end
if isempty(sm_unc), sm_unc = 'none'; end

title(['\it\sigma \rm^2_s_m: ' sm_unc ...
       ', SLAM range: ' num2str(CONFIG.max_range) ...
       ' m, State dist = ' num2str(CONFIG.state_dist)...
       ', Elapsed time: ' num2str(toc/60)...
       ' min (' ploting '),  SM methode: ' methode ...
       ',  file: ' mfilename ',  plotted: ' datestr(now)]);
drawnow;


%Plot the errors
% traj_corrected = fliplr(sxloc);
% traj_corrected(:,1) = [];
% check_error(traj_corrected);
% 
% Pgps = [1 1]';
% figure;
% title('SLAM error and uncertainty within 2-\sigma bound');
% subplot(3,1,1);
% plot(XErrStore(1,:));
% hold on;
% plot(2*PStore(1,:),'r');
% plot(-2*PStore(1,:),'r');
% plot(2*(PStore(1,:)+Pgps(1)),'g');
% plot(-2*(PStore(1,:)+Pgps(1)),'g');
% ylabel('x (m)');
% 
% subplot(3,1,2);
% plot(XErrStore(2,:));
% hold on;
% plot(2*PStore(2,:),'r');
% plot(-2*PStore(2,:),'r')
% plot(2*(PStore(1,:)+Pgps(2)),'g');
% plot(-2*(PStore(1,:)+Pgps(2)),'g');
% ylabel('y (m)');
% xlabel('scans');
% 
% subplot(3,1,3);
% plot(XErrStore(3,:)*180/pi);
% hold on;
% plot(2*PStore(3,:)*180/pi,'r');
% plot(-2*PStore(3,:)*180/pi,'r')
% plot(2*(PStore_mti)*180/pi,'g');
% plot(-2*(PStore_mti)*180/pi,'g')
% ylabel('Theta (deg)'); 


if CONFIG.debug
    X=reshape(x,3,[]);
    X=fliplr(X);
    
    Xdis=zeros(3,217);
    for i= 1:217
        Xinv=invert(X(:,i));
        dis=compound(Xinv.x,X(:,i+1));
        Xdis(:,i)=dis.x;
    end
    
    b=diff(GPSstore');
    
    figure
    plot(1:217,motion_store(1,:),'b','DisplayName','motion.q')
    hold all
    plot(1:217,correction_store(1,:),'g','DisplayName','qmin')
    plot(1:217,odom_store(1,:),'r','DisplayName','displacement')
    plot(1:217,Xdis(1,1:217),'k','DisplayName','final X')
    plot(1:217,b(:,1),'c','DisplayName','Ground Truth')
    ylabel('x (m)');
    xlabel('scans');
    hold off
    
    figure
    plot(1:217,motion_store(2,:),'b','DisplayName','motion.q')
    hold all
    plot(1:217,correction_store(2,:),'g','DisplayName','qmin')
    plot(1:217,odom_store(2,:),'r','DisplayName','displacement')
    plot(1:217,Xdis(2,1:217),'k','DisplayName','final X')
    plot(1:217,b(:,2),'c','DisplayName','Ground Truth')
    ylabel('y (m)');
    xlabel('scans');
    hold off
    
    figure
    plot(1:217,motion_store(3,:),'b','DisplayName','motion.q')
    hold all
    plot(1:217,correction_store(3,:),'g','DisplayName','qmin')
    plot(1:217,odom_store(3,:),'r','DisplayName','displacement')
    plot(1:217,Xdis(3,1:217),'k','DisplayName','final X')
    plot(1:217,normAngle(b(:,3)),'c','DisplayName','Ground Truth')
    ylabel('Theta (rad)');
    xlabel('scans');
    hold off
end
                    
%% END MAIN=====================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fid = fopen('deadreckoining.txt','w');
fprintf(fid, '%.3f %.10f %.10f %.10f\n', deadreckoning');
fclose(fid);

tmp = reshape(x,[3 38]);
% afegir deadreckoning primer scan 
scanmatching_emili = [time_emili' tmp(:,end:-1:1)'];
scanmatching_emili = [deadreckoning(1,:); scanmatching_emili];
plot(scanmatching_emili(:,2),scanmatching_emili(:,3),'.-c');
fid_sm = fopen('sm.txt','w');
fprintf(fid_sm, '%.3f %.10f %.10f %.10f\n', scanmatching_emili');
fclose(fid_sm);

time_interp = deadreckoning(deadreckoning(:,1)<=scanmatching_emili(end,1),1);
%->provar spline o cubic -> linial molt dolent
interp = interp_trajectory(time_interp',scanmatching_emili(:,1)',scanmatching_emili(:,2:4)');
scanmatching_emili_interp=[time_interp interp'];
plot(scanmatching_emili_interp(:,2),scanmatching_emili_interp(:,3),'.-b');
fid_sm = fopen('sm_interp_rot_spline.txt','w');
fprintf(fid_sm, '%.3f %.10f %.10f %.10f\n', scanmatching_emili_interp');
fclose(fid_sm);


disp(['Elapsed time: ' num2str(toc/60) ' (min)'])
fclose('all');