function t = interp_trajectory(stime,vtime,vtraj)

%sensor_trajectory: compute trajectory of the sensor
%stime: sensor time
%vtime: vehiche time
%vtraje: vehicle trajectory [3xN] [x y yaw]

global PARAM;

x_interp = interp1(vtime,vtraj(1,:),stime,'cubic');
y_interp = interp1(vtime,vtraj(2,:),stime,'cubic');
ang_interp=cumsum([vtraj(3,1) normalize(diff(vtraj(3,:)))]);
ang_interp = interp1(vtime,ang_interp,stime,'spline');

t = [x_interp; y_interp; ang_interp];
end